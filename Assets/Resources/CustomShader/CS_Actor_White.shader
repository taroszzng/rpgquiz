// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "CS_Actor/White" {
	Properties {
		_AlphaTex ("Mask", 2D) = "" {}
		_Color ("AnimColor", Color) = (1,1,1,1)
		_TintAlpha ("TintAlpha", float) = 1
	}
	// 2 texture stage GPUs
	SubShader {
		Tags { 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		Blend SrcAlpha OneMinusSrcAlpha

		Fog { Mode Off }		
		Cull Off
		Lighting Off
		ZWrite Off

		Pass 
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct appdata_t
			{
				float4 vertex   : POSITION;				
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;				
				half2 texcoord  : TEXCOORD0;
			};

			fixed4 _Color;
			fixed _TintAlpha;
			
			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;				
				return OUT;
			}

			sampler2D _AlphaTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				half4 color = half4(1, 1, 1, tex2D(_AlphaTex, IN.texcoord).a * _Color.a * _TintAlpha);
				clip (color.a - 0.01);
				return color;
			}
		ENDCG
		}				
	}	
}





