// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "CS_Specular/Specular" 
{
		Properties 
		{
			_SpecularColor("Specular Color", Color) = (1,1,1,1)
			_Shininess("Specular", Range(0,100)) = 0
			_SpeculaTex("Specula Texture", 2D) = "white" {}

			_Color("Color", Color) = (1,1,1,1)
			_MainTex("Albedo (RGB)", 2D) = "white" {}
			_BumpMap("Normal", 2D) = "bump" {}
			_BumpPower("Normal Power", Range(0,1)) = 0

			_RimColor("Rim Color", Color) = (1,1,1,1)
			_rimOpacity("Rim Opacity", Range(0,1)) = 0
			_RimPower("Rim Power", Range(-50, 50)) = 0
		}

		SubShader
		{
			Tags { "RenderType" = "Opaque" }

			CGPROGRAM

	#pragma surface surf my

			sampler2D _MainTex;
			sampler2D _BumpMap;
			sampler2D _SpeculaTex;
		
			float4 _Color;
			float _RimPower;
			float4 _RimColor;
			float _rimOpacity;
			float _BumpPower;

			float _Shininess;
			float4 _SpecularColor;

			struct Input
			{
				float2 uv_MainTex;
				float2 uv_BumpMap;
				float2 uv_SpeculaTex;
				float3 viewDir;
			};

			float4 Lightingmy(SurfaceOutput s, float3 lightDir, float atten, float3 viewDir)
			{
				//---diffuse---
				float NdotL;
				float3 diffuseCol;

				NdotL = dot(normalize(s.Normal), normalize(lightDir)) *0.5 + 0.5;

				diffuseCol = NdotL * s.Albedo * _LightColor0.rgb * atten;

				//---sepcualer---
				float3 h = normalize(viewDir + lightDir);
				float nh = saturate(dot(s.Normal, h));
				float spec = pow(nh, s.Specular);
				float3 specularCol = spec * _SpecularColor.rgb * s.Gloss;

				//---final---
				float4 finalCol;

				finalCol.rgb = specularCol;
				finalCol.a = s.Alpha;
				
				return finalCol;
			}

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				
				o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap)) * _BumpPower;
				
				float rim = pow(1 - dot(o.Normal, IN.viewDir), _RimPower);

				o.Gloss = tex2D(_SpeculaTex, IN.uv_SpeculaTex);
				o.Albedo = c.rgb * _Color;
				o.Emission = rim * _RimColor.rgb * _rimOpacity;
				o.Specular = _Shininess;
				o.Alpha = c.a;
			}

			ENDCG
	}

	FallBack "Diffuse"
}



