#ifndef BLEND_OPS
#define BLEND_OPS

#ifdef BMScale10
return Scale(color, 1.1f);
#endif
#ifdef BMScale11
return Scale( color, 1.1f);
#endif
#ifdef BMScale12
return Scale(color, 1.2f);
#endif
#ifdef BMScale13
return Scale(color, 1.3f);
#endif
#ifdef BMScale14
return Scale(color, 1.4f);
#endif
#ifdef BMScale15
return Scale(color, 1.5f);
#endif
#ifdef BMScale16
return Scale(color, 1.6f);
#endif
#ifdef BMScale17
return Scale(color, 1.7f);
#endif
#ifdef BMScale18
return Scale(color, 1.8f);
#endif
#ifdef BMScale19
return Scale(color, 1.9f);
#endif
#ifdef BMScale20
return Scale(color, 2.0f);
#endif
 
#endif
