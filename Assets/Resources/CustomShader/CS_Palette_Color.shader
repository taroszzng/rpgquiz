// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "CS_Palette/Color"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("AddColor", Color) = (1,0,0,0.5)
		_PaletteTex ("Palette", 2D) = "white" {}
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		ZTest LEqual
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			AlphaTest Greater 0.1

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				half2 texcoord  : TEXCOORD0;
			};		
			

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				return OUT;
			}

			fixed4 _Color;
			sampler2D _MainTex;
			sampler2D _PaletteTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord);
				fixed a256 = c.a * 255;
				fixed upper =  floor(a256 / 16)/16 + 0.04;
				fixed lower =  fmod(a256, 16)/16 + 0.04;

				fixed4 rc = tex2D(_PaletteTex, fixed2(upper,lower));
				rc.r += _Color.r*_Color.a;				
				rc.g += _Color.g*_Color.a;
				rc.b += _Color.b*_Color.a;
				return rc;
			}
		ENDCG
		}
	}
}
