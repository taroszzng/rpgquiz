#ifndef BLEND_MODES
#define BLEND_MODES

fixed4 Scale(fixed4 b, float f)
{ 
	fixed4 r = b * f;

	return r;
}

#endif
