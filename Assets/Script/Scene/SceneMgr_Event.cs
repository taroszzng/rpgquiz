﻿using UnityEngine;
using System.Collections.Generic;

public partial class SceneMgr : MonoBehaviour
{
    public class EventBuff
    {
        public nsENUM.eEVENTBUFF m_eBuff;
        public int m_nEventValue;
        public int m_nEventValue2;
        public int m_nPre;//퍼센트
    }

    public EventBuff m_EventBuff = new EventBuff();

    public void BattleEvent()
    {
        if (!m_bBattleStart)
            return;

        int nRan = Random.Range(0, (int)nsENUM.eVALUE.eVALUE_3);

        if ((int)nsENUM.eVALUE.eVALUE_1 == nRan)
            BattleEventAtk();
    }

    void BattleEventAtk() //공격 관련 이벤트
    {
        int nRan = Random.Range(0, (int)nsENUM.eVALUE.eVALUE_2);

        if ((int)nsENUM.eVALUE.eVALUE_1 == nRan)//이벤트 발생
        {
            m_bBattleNotic = true;

            string strNotic = SharedObject.g_TableMgr.m_Lan.Get(1375000).m_strDec;

            nRan = Random.Range(0, (int)nsENUM.eVALUE.eVALUE_8);

            switch ((nsENUM.eVALUE)nRan)
            {
                case nsENUM.eVALUE.eVALUE_0:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_3;
                    break;
                case nsENUM.eVALUE.eVALUE_1:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_5;
                    break;
                case nsENUM.eVALUE.eVALUE_2:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_7;
                    break;
                case nsENUM.eVALUE.eVALUE_3:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_10;
                    break;
                case nsENUM.eVALUE.eVALUE_4:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_20;
                    break;
                case nsENUM.eVALUE.eVALUE_5:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_30;
                    break;
                case nsENUM.eVALUE.eVALUE_6:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_50;
                    break;
                case nsENUM.eVALUE.eVALUE_7:
                    nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_100;
                    break;
            }

            strNotic = string.Format(strNotic, nRan);

            SharedObject.g_AMode.SetEffectNotic(strNotic);

            m_EventBuff.m_eBuff = nsENUM.eEVENTBUFF.eEVENTBUFF_ATK_PLUS;
            m_EventBuff.m_nEventValue = nRan;

            SharedObject.g_CollectorMgr.EventBuff();
        }
    }
}