﻿using UnityEngine;
using System.Collections.Generic;

public partial class SceneMgr : MonoBehaviour
{
    public class Combo
    {
        public int m_nLoseCount = 0;//연속 패배관련 체크하기 위해
        public int m_nWinCount = 0;

        public nsENUM.eCOMBOTYPE m_eComboType = nsENUM.eCOMBOTYPE.eCOMBOTYPE_END;   
    }

    public Combo m_Combo = new Combo();

    bool m_bBattleStart; //베틀 스타트
    bool m_bBattleNotic; //베틀 공지

    bool m_bInvenColor;//인벤 칼라
    bool m_bBattleColor;//베틀 칼라

    int m_nCurStage;
    bool m_bMonsterClear;
    bool m_bBattlePlay;//베틀중 전투 진행상태
    int m_nMonsterPerID;

    bool m_bCri = false;

    nsENUM.eTEAM m_eWin = nsENUM.eTEAM.BLACK_TEAM;

    public bool BattleStart
    {
        set { m_bBattleStart = value; }
        get { return m_bBattleStart; }
    }

    public bool BattleNotic
    {
        set { m_bBattleNotic = value; }
        get { return m_bBattleNotic; }
    }

    public bool InvenColor
    {
        set { m_bInvenColor = value; }
        get { return m_bInvenColor; }
    }

    public bool BattleColor
    {
        set { m_bBattleColor = value; }
        get { return m_bBattleColor; }
    }

    public int CurStage
    {
        set { m_nCurStage = value; }
        get { return m_nCurStage; }
    }

    public bool MonsterClear
    {
        set { m_bMonsterClear = value; }
        get { return m_bMonsterClear; }
    }

    public bool BattlePlay
    {
        set { m_bBattlePlay = value; }
        get { return m_bBattlePlay; }
    }

    public int MonsterPerID
    {
        set { m_nMonsterPerID = value; }
        get { return m_nMonsterPerID; }
    }

    public bool Cri
    {
        set { m_bCri = value; }
        get { return m_bCri; }
    }

    public nsENUM.eTEAM Win
    {
        set { m_eWin = value; }
        get { return m_eWin; }
    }

    void ResetBattle()
    {
        m_eWin = nsENUM.eTEAM.BLACK_TEAM;
    }

    void ReSetCombo()
    {
        switch(m_Combo.m_eComboType)
        {
            case nsENUM.eCOMBOTYPE.eCOMBOTYPE_WIN://이기면 진카운트 초기화
                {
                    m_Combo.m_nLoseCount = 0;
                }
                break;
            case nsENUM.eCOMBOTYPE.eCOMBOTYPE_LOSE://지면 이긴 카운트 초기화
                {
                    m_Combo.m_nWinCount = 0;
                }
                break;
            default:
                {
                    m_Combo.m_nWinCount = 0;
                    m_Combo.m_nLoseCount = 0;
                }
                break;
        }
    }

    public int GetComboCount()//연승시 연승 카운트, 연패후 승리시 패배카운트
    {
        int comboid = 0;

        if (1 < m_Combo.m_nWinCount)//1보다 커야 계속 승
            comboid = m_Combo.m_nWinCount - 1;
        else if (1 < m_Combo.m_nLoseCount)
            comboid = ((int)nsENUM.eVALUE.eVALUE_10 + m_Combo.m_nLoseCount) - 1;//연패하다 승리하면 연패 카운트를 줘야 함

        return comboid;
    }

    public void SetCombo(nsENUM.eCOMBOTYPE _e)
    {
        switch(_e)
        {
            case nsENUM.eCOMBOTYPE.eCOMBOTYPE_WIN://승리시엔 로즈카운트로 데미지 줘야함
                {
                    if((int)nsENUM.eVALUE.eVALUE_11 > m_Combo.m_nWinCount)
                        m_Combo.m_nWinCount += 1;
                }
                break;
            case nsENUM.eCOMBOTYPE.eCOMBOTYPE_LOSE://연승하다 지면 바로 0만들어야 함.안그럼 get카운트에서 연승카운트 계산
                {
                    if ((int)nsENUM.eVALUE.eVALUE_11 > m_Combo.m_nLoseCount)
                        m_Combo.m_nLoseCount += 1;

                    m_Combo.m_nWinCount = 0;
                }
                break;
            default:
                {
                    m_Combo.m_nWinCount = 0;
                    m_Combo.m_nLoseCount = 0;
                }
                break;
        }

        m_Combo.m_eComboType = _e;
    }
}