﻿using UnityEngine;
using System.Collections;

public partial class SceneMgr : MonoBehaviour
{
    nsENUM.eUI m_eUI = nsENUM.eUI.eUI_NONE;//UI 스탭중

    nsENUM.eSCENE m_eScene = nsENUM.eSCENE.eSCENE_NONE;

    int m_nStageSelect = 0;
    int m_nMonsterSelect = 0;
    int m_nClearMonsterID = 0;

    public nsENUM.eUI UI
    {
        set { m_eUI = value; }
        get { return m_eUI; }
    }

    public nsENUM.eSCENE  Scene
    {
        set { m_eScene = value; }
        get { return m_eScene; }
    }

    public int StageSelect
    {
        set { m_nStageSelect = value; }
        get { return m_nStageSelect; }
    }

    public int MonsterSelect
    {
        set { m_nMonsterSelect = value; }
        get { return m_nMonsterSelect; }
    }

    public int ClearMonsterID
    {
        set { m_nClearMonsterID = value; }
        get { return m_nClearMonsterID; }
    }

    public void ChangeScene(nsENUM.eSCENE _eScene, bool _bLoadingScene = true)
    {
        Debug.Log("ChangeScene = " + _eScene + "m_SceneState.m_Scene = " + m_eScene);

        if (_eScene == m_eScene)
            return;

        m_eScene = _eScene;

        SharedObject.g_CollectorMgr.Mode(m_eScene);
    }

    public bool ChangeBack()
    {
        bool bback = false;

        switch(m_eUI)
        {
            case nsENUM.eUI.eUI_STAGE://스테이지
                {
                    if(null != SharedObject.g_Main)
                        SharedObject.g_Main.SetStart();

                    m_eUI = nsENUM.eUI.eUI_TITLE;

                    bback = true;
                }
                break;
            case nsENUM.eUI.eUI_MONSTERSELECT://몬스터 선택 -> 스테이지
                {
                    if(null != SharedObject.g_Mode)
                        SharedObject.g_Mode.SetMonsterSelect(false);

                    m_eUI = nsENUM.eUI.eUI_STAGE;

                    bback = true;
                }
                break;
            case nsENUM.eUI.eUI_AMODEEVENT://이벤트 -> 몬스터 선택
                {
                    if (null != SharedObject.g_Mode)
                    {
                        SharedObject.g_Mode.SetMonsterSelect(true);
                        SharedObject.g_Mode.GOAMode.SetActive(false);
                    }

                    m_eUI = nsENUM.eUI.eUI_MONSTERSELECT;

                    bback = true;
                }
                break;
        }

        return bback;
    }
}