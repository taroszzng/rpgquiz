﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class SceneProperty : MonoBehaviour
{
#if UNITY_EDITOR
    public nsENUM.eLANGUAGE OptionLan = nsENUM.eLANGUAGE.eLANGUAGE_NONE;//옵션 랭귀지 테스트 
#endif

    void Awake()
    {
        if (SharedObject.g_SceneProperty != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        SharedObject.g_SceneProperty = this;
        DontDestroyOnLoad(gameObject);

        if (SharedObject.g_EffectPanelMgr == null)
        {
            GameObject objEffectPanel = Resources.Load("Prefabs/EffectPanelMgr") as GameObject;

            if (null == objEffectPanel)
                return;

            objEffectPanel = GameObject.Instantiate(objEffectPanel) as GameObject;

            SharedObject.g_UICamera = objEffectPanel.transform.Find("Camera").GetComponent<Camera>();
        }

        InitCheat();
    }

    // Use this for initialization
    void Start()
    {
    }
}
