﻿#define RELEASE

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public partial class SceneProperty : MonoBehaviour
{
    enum eCHEATTYPE
    {
        eCHEATTYPE_RESET,
        eCHEATTYPE_PLAYER,
        eCHEATTYPE_MONSTER,
        eCHEATTYPE_ITEM,
        eCHEATTYPE_END,
    }

    public GameObject GOCheat;

    public Text TEXTTableName;
    public Text TEXTTableID;
    public Text TEXTValue;

    eCHEATTYPE m_eCheat;

    void InitCheat()
    {
#if RELEASE
        GOCheat.SetActive(false);
#else
        GOCheat.SetActive(true);
#endif
    }

    public void OnBtnCheatEnable()
    {
        transform.Find("Canvas_Cheat/Cheat").gameObject.SetActive(true);
    }

    public void OnBtnCheatDisable()
    {
        transform.Find("Canvas_Cheat/Cheat").gameObject.SetActive(false);
    }

    public void CheatType(Toggle toggle)
    {
        if (toggle.isOn == false)
            return;

        switch (toggle.name)
        {
            case "Reset":
                {
                    TEXTTableName.text = "Data Reset";
                    m_eCheat = eCHEATTYPE.eCHEATTYPE_RESET;
                }
                break;
            case "PlayerID":
                {
                    TEXTTableName.text = "PlayerID";
                    m_eCheat = eCHEATTYPE.eCHEATTYPE_PLAYER;
                }
                break;
            case "MonsterID":
                {
                    TEXTTableName.text = "MonsterID";
                    m_eCheat = eCHEATTYPE.eCHEATTYPE_MONSTER;
                }
                break;
            case "ItemID":
                {
                    TEXTTableName.text = "ItemID";
                    m_eCheat = eCHEATTYPE.eCHEATTYPE_ITEM;
                }
                break;
        }
    }

    public void OnBtnCheatDo()//실행
    {
        if (0 == TEXTTableID.text.Length)
            return;

        string strValue = "0";

        if (0 < TEXTValue.text.Length)
            strValue = TEXTValue.text;

        SharedObject.g_PlayerPrefsData.AllLoadData();

        int nTableID = System.Convert.ToInt32(TEXTTableID.text);
        long lValue = System.Convert.ToInt64(strValue);

        switch (m_eCheat)
        {
            case eCHEATTYPE.eCHEATTYPE_RESET:
                {
                    SharedObject.g_PlayerPrefsData.ResetData();
                }
                break;
            case eCHEATTYPE.eCHEATTYPE_PLAYER:
                {
                    if (SharedObject.g_TableMgr.m_Player.IsData(nTableID))
                        SharedObject.g_PlayerPrefsData.PlayerID = nTableID;
                }
                break;
            case eCHEATTYPE.eCHEATTYPE_MONSTER:
                {
                    if (SharedObject.g_TableMgr.m_Monster.IsData(nTableID))
                    {
                        Table_Monster.Info info = SharedObject.g_TableMgr.m_Monster.Get(nTableID);

                        SharedObject.g_PlayerPrefsData.MonsterID = nTableID;
                        SharedObject.g_PlayerPrefsData.ClearStage = info.m_nClearStage;
                    }
                }
                break;
            case eCHEATTYPE.eCHEATTYPE_ITEM:
                {
                }
                break;
        }

        string strpath = SharedObject.g_PlayerPrefsData.GetDataPath();

        SharedObject.g_PlayerPrefsData.SaveFile(strpath);//전체 저장

        transform.Find("Canvas_Cheat/Cheat").gameObject.SetActive(false);
    }
}