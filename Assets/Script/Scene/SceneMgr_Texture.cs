﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public partial class SceneMgr : MonoBehaviour
{
    [SerializeField]
    List<Texture2D> TexMonsterIcon = new List<Texture2D>();

    [SerializeField]
    List<Texture2D> TexItemIcon = new List<Texture2D>();

    public Sprite CreateMonsterIcon(int _nMonsterID)//240-320
    {
        int nID = _nMonsterID - 1;

        int nIndex = nID / 64;

        int nX = nID % 8;
        int nY = (nID - (nIndex * 64)) / 8;

        //Debug.Log("MonsterID = " + _nMonsterID + " nIndex = " + nIndex + " nX =" + nX + " nY = " + nY);

        return Sprite.Create(TexMonsterIcon[nIndex], 
            new Rect(nX * 128f, 896 - (nY * 128f), 128f, 128f), 
            new Vector2(0.5f, 0.5f));
    }

    public Sprite CreateItemIcon(int _ItemID)//
    {
        int nID = _ItemID - 1;

        int nIndex = nID / 64;

        int nX = nID % 8;
        int nY = (nID - (nIndex * 64)) / 8;

        return Sprite.Create(TexItemIcon[nIndex], 
            new Rect(nX * 128f, 896 - (nY * 128f), 128f, 128f), 
            new Vector2(0.5f, 0.5f));
    }
}