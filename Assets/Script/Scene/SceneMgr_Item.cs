﻿using UnityEngine;
using System.Collections.Generic;

public partial class SceneMgr : MonoBehaviour
{
    int m_nBuyID;

    Dictionary<int, bool> m_Player = new Dictionary<int, bool>();
    Dictionary<int, bool> m_Monster = new Dictionary<int, bool>();

    nsENUM.eITEMAPPLY m_eAllItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eHpItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eAtkItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eLuckItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eTimeItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eDeleteItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eHintItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
    nsENUM.eITEMAPPLY m_eBuffItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;

    public nsENUM.eITEMAPPLY BuffItem
    {
        get { return m_eBuffItem; }
    }

    public int BuyID
    {
        set { m_nBuyID = value; }
        get { return m_nBuyID; }
    }

    public void ResetItemUse()
    {
        m_Player.Clear();
        m_Monster.Clear();

        m_eAllItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
        m_eHpItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
        m_eAtkItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
        m_eLuckItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;

        m_eTimeItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
        m_eDeleteItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
        m_eHintItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;
        m_eBuffItem = nsENUM.eITEMAPPLY.eITEMAPPLY_END;

        m_bBattleNotic = false;

        m_EventBuff.m_eBuff = nsENUM.eEVENTBUFF.eEVENTBUFF_END;
        m_EventBuff.m_nEventValue = 0;
        m_EventBuff.m_nEventValue2 = 0;
        m_EventBuff.m_nPre = 0;

        m_bCri = false;
    }

    public bool ItemCheck(nsENUM.eTEAM _e, int _nItemID)//이미 쓴거면 false
    {
        bool buse = false;

        switch (_e)
        {
            case nsENUM.eTEAM.BLUE_TEAM:
                {
                    if (!m_Player.ContainsKey(_nItemID))
                        buse = true;
                }
                break;
            case nsENUM.eTEAM.RED_TEAM:
                {
                    if (!m_Monster.ContainsKey(_nItemID))
                        buse = true;
                }
                break;
        }

        return buse;
    }

    public int ItemApply(nsENUM.eTEAM _e, int _nItemID)//아이템별 사용 같은 아이템은 사용 못하게
    {
        int nMsg = 1370015;//아이템 중복 사용 불가

        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if(null == info)
            return 0;

        switch((nsENUM.eITEMAPPLY)info.m_nApply)
        {
            case nsENUM.eITEMAPPLY.eITEMAPPLY_BUFF:
                {
                    if (nsENUM.eITEMAPPLY.eITEMAPPLY_BUFF != m_eBuffItem)
                    {
                        m_eBuffItem = nsENUM.eITEMAPPLY.eITEMAPPLY_BUFF;

                        int nRan = Random.Range(0, (int)nsENUM.eVALUE.eVALUE_20);

                        if (nRan <= (int)nsENUM.eVALUE.eVALUE_4)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_5;
                        else if (nRan > (int)nsENUM.eVALUE.eVALUE_4 && nRan <= (int)nsENUM.eVALUE.eVALUE_8)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_7;
                        else if (nRan > (int)nsENUM.eVALUE.eVALUE_8 && nRan <= (int)nsENUM.eVALUE.eVALUE_12)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_10;
                        else if (nRan > (int)nsENUM.eVALUE.eVALUE_12 && nRan <= (int)nsENUM.eVALUE.eVALUE_15)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_20;
                        else if (nRan > (int)nsENUM.eVALUE.eVALUE_15 && nRan <= (int)nsENUM.eVALUE.eVALUE_17)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_30;
                        else if (nRan == (int)nsENUM.eVALUE.eVALUE_18)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_50;
                        else if (nRan == (int)nsENUM.eVALUE.eVALUE_19)
                            nRan = (int)nsENUM.eBATTLEEVENT.eBATTLEEVENT_100;

                        float fpre = nRan / 100f;

                        SharedObject.g_CollectorMgr.ItemBuff(nsENUM.eEVENTBUFF.eEVENTBUFF_HP_MINUS, fpre);//아이템 버프로 hp를 감소

                        nMsg = 1;
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_TIME:
                {
                    if (nsENUM.eITEMAPPLY.eITEMAPPLY_HINT != m_eTimeItem)
                    {
                        m_eTimeItem = nsENUM.eITEMAPPLY.eITEMAPPLY_TIME;

                        int ntime = Random.Range(5, (int)nsENUM.eVALUE.eVALUE_11);

                        SharedObject.g_AModeSet.LastTime = ntime;

                        SharedObject.g_AModeSet.MaxTime = ntime;

                        nMsg = 1;
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_DELETE://제거
                {
                    if (nsENUM.eITEMAPPLY.eITEMAPPLY_DELETE != m_eDeleteItem)
                    {
                        Table_Quiz.Info quiz = SharedObject.g_TableMgr.m_Quiz.Get(SharedObject.g_AModeSet.QuizID);

                        if (null != quiz)
                        {
                            m_eDeleteItem = nsENUM.eITEMAPPLY.eITEMAPPLY_DELETE;

                            SharedObject.g_AModeSet.SetBtnExample(quiz.m_nAnswer-1);

                            nMsg = 1;
                        }
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_HINT://힌트
                {
                    if (nsENUM.eITEMAPPLY.eITEMAPPLY_HINT != m_eHintItem)
                    {
                        Table_Quiz.Info quiz = SharedObject.g_TableMgr.m_Quiz.Get(SharedObject.g_AModeSet.QuizID);

                        if (null != quiz)
                        {
                            m_eHintItem = nsENUM.eITEMAPPLY.eITEMAPPLY_HINT;

                            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 0, quiz.m_strHint);

                            nMsg = 1;
                        }
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ALL:
                {
                    if (m_eAllItem != nsENUM.eITEMAPPLY.eITEMAPPLY_ALL)
                    {
                        SharedObject.g_CollectorMgr.ItemStatUse(_e, (nsENUM.eITEMTYPE)info.m_nType, _nItemID);

                        m_eAllItem = nsENUM.eITEMAPPLY.eITEMAPPLY_ALL;

                        nMsg = 1;
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_HP:
                {
                    if (m_eHpItem != nsENUM.eITEMAPPLY.eITEMAPPLY_HP)
                    {
                        SharedObject.g_CollectorMgr.ItemStatUse(_e, (nsENUM.eITEMTYPE)info.m_nType, _nItemID);

                        m_eHpItem = nsENUM.eITEMAPPLY.eITEMAPPLY_HP;

                        nMsg = 1;
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ATK:
                {
                    if (m_eAtkItem != nsENUM.eITEMAPPLY.eITEMAPPLY_ATK)
                    {
                        SharedObject.g_CollectorMgr.ItemStatUse(_e, (nsENUM.eITEMTYPE)info.m_nType, _nItemID);

                        m_eAtkItem = nsENUM.eITEMAPPLY.eITEMAPPLY_ATK;

                        nMsg = 1;
                    }
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK:
                {
                    if (m_eLuckItem != nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK)
                    {
                        SharedObject.g_CollectorMgr.ItemStatUse(_e, (nsENUM.eITEMTYPE)info.m_nType, _nItemID);

                        m_eLuckItem = nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK;

                        nMsg = 1;
                    }
                }
                break;
            default://스탯 관련 아이템 사용
                break;
        }

        return nMsg;
    }

    public bool CheckItemUse(nsENUM.eTEAM _e, int _nItemID)//아이템 사용 검사
    {
        bool buse = false;

        switch (_e)
        {
            case nsENUM.eTEAM.BLUE_TEAM:
                {
                    if (!m_Player.ContainsKey(_nItemID))
                        buse = true;
                }
                break;
            case nsENUM.eTEAM.RED_TEAM:
                {
                    if (!m_Monster.ContainsKey(_nItemID))
                        buse = true;
                }
                break;
        }

        return buse;
    }

    public bool ItemUse(nsENUM.eTEAM _e, int _nItemID)//아이템 실제 사용
    {
        bool buse = false;

        switch (_e)
        {
            case nsENUM.eTEAM.BLUE_TEAM:
                {
                    if (!m_Player.ContainsKey(_nItemID))
                    {
                        m_Player.Add(_nItemID, true);

                        buse = true;
                    }
                }
                break;
            case nsENUM.eTEAM.RED_TEAM:
                {
                    if (!m_Monster.ContainsKey(_nItemID))
                    {
                        m_Monster.Add(_nItemID, true);

                        buse = true;
                    }
                }
                break;
        }

        return buse;
    }
}