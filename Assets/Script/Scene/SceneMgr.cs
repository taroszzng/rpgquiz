﻿#define RELEASE

using UnityEngine;

public partial class SceneMgr : MonoBehaviour
{
    bool m_bPause = false;
    bool m_bGameExit;
    bool m_bApplicationPause;

    public bool GameExit
    {
        set { m_bGameExit = value; }
        get { return m_bGameExit; }
    }

    public bool Pause
    {
        set { m_bPause = value; }
        get { return m_bPause; }
    }

    public bool ApplicationPause
    {
        set { m_bApplicationPause = value; }
        get { return m_bApplicationPause; }
    }

    public void OnApplicationPause(bool paused)
    {
        Debug.Log(" paused = " + paused + " ApplicationPause = " + m_bApplicationPause);

#if UNITY_EDITOR
    #if DEVELOPMENT
        return;
    #endif
#endif
        if (paused)
        {
            if (!m_bApplicationPause)//구글 접속 시도시도 paused = true
            {
                m_bPause = true;

                SharedObject.g_Main.SetPauseExit(true);//일시정지

                Application.runInBackground = false;
            }

            m_bApplicationPause = false;

            string strpath = SharedObject.g_PlayerPrefsData.GetDataPath();

            SharedObject.g_PlayerPrefsData.SaveFile(strpath);//전체 저장
        }
    }

    private void OnApplicationQuit()
    {
        Debug.Log(" OnApplicationQuit ");

        string strpath = SharedObject.g_PlayerPrefsData.GetDataPath();

        SharedObject.g_PlayerPrefsData.SaveFile(strpath);//전체 저장
    }

    void Awake()
    {
        SharedObject.g_SceneMgr = this;

        SharedObject.GetTableMgr();

        Screen.SetResolution(720, 1280, true);
    }

    void Start()
    {
        SharedObject.g_PlayerPrefsData.AllLoadData();//선택된 모드나 저장된 모드로 처리 최초 레벨 1로 설정 

        m_nCurStage = SharedObject.g_PlayerPrefsData.ClearStage;

        Debug.Log("MonsterID = " + SharedObject.g_PlayerPrefsData.MonsterID);

        SharedObject.g_Tutorial.InitTutorial();

        InitSceneMgr();
    }

    public void ReSet()
    {
        SharedObject.g_AMode.ReSet();//주사위 다시 리셋

        ResetBattle();
        ResetItemUse();//아이템 리셋
        ReSetCombo(); //콤보 리셋
        BattleEvent();

        SharedObject.g_CollectorMgr.ResetStatItem();//캐릭터 리셋
    }

    void InitSceneMgr()
    {
    }

    void Update()// Update is called once per frame
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnEsc();
    }

    void OnEsc()
    {
        if (m_bPause)
        {
            m_bPause = false;

            SharedObject.g_Main.SetPauseExit(false);//일시정지

            return;
        }

        if (m_bGameExit)
        {
            SetEsc(!m_bGameExit);

            SharedObject.g_Main.SetGameExit(m_bGameExit);//게임종료팝업

            return;
        }

        if (null != SharedObject.g_3Depth)
        {
            SharedObject.g_3Depth.gameObject.SetActive(false);
            SharedObject.g_Main.CheckMode(false);

            return;
        }

        if (null != SharedObject.g_2Depth)
        {
            SetEsc(false);

            SharedObject.g_2Depth.gameObject.SetActive(false);

            return;
        }

        if (null != SharedObject.g_1Depth)
        {
            SharedObject.g_1Depth.gameObject.SetActive(false);

            return;
        }

        if (SharedObject.CheckStack())
            return;

        if (ChangeBack())
            return;

        SetEsc(!m_bGameExit);

        SharedObject.g_Main.SetGameExit(m_bGameExit);//게임종료팝업
    }

    void SetEsc(bool _bActive)
    {
        m_bPause = _bActive;
        m_bGameExit = _bActive;
    }

    public void Shutdown()
    {
        Debug.Log("Application.Quit");

        Application.Quit();
    }
}
