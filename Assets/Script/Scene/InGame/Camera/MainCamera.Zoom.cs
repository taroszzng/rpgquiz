﻿using UnityEngine;
using System.Collections;

public partial class MainCamera : MonoBehaviour
{    
    public bool m_IsCameraAni = false;
    public float m_ZoomDelta = 0f;

    string m_CameraAniName;
    float m_StartCameraAniTime;
    float m_PlayCameraAniTime;

    public Rtti.eRTTI m_ZoomPrefabOwner;

    public class cZoomInfo
    {
        public float m_StartDelay;
        public bool m_IsZoom;          // m_Dest 값이 확대인지 축소 인지
        public float m_Dest;        
        public float m_FadeIn_Velocity;
        public float m_Duration;
        public float m_FadeOut_Velocity;

        public float m_FadeIn_Time;
        public float m_FadeOut_Time;
        public float m_FadeIn_MoveSpeed;
        public float m_FadeOut_MoveSpeed;
        public Vector3 m_DeltaDir;
    }

    cZoomInfo m_ZoomInfo = new cZoomInfo();
    bool m_IsEndStage = false;

    protected void ResetZoom()
    {
        m_ZoomDelta = 0f;
        m_IsEndStage = false;        
        m_IsFallowMy = true;

        m_ZoomPrefabOwner = Rtti.eRTTI.NONE;
    }

    IEnumerator ZoomCoroutine()
    {
        float dt, delta;

        if (m_ZoomInfo.m_StartDelay > 0)
        {
            yield return new WaitForSeconds(m_ZoomInfo.m_StartDelay);
        }

        Transform trCon = transform.parent.parent;
        bool bMoveCamera = false;

        if (m_ZoomInfo.m_DeltaDir != Vector3.zero)
        {
            bMoveCamera = true;
            m_IsFallowMy = true;
        }

        while (true)
        {
            dt = Time.deltaTime;
            delta = dt * m_ZoomInfo.m_FadeIn_Velocity;

            if (bMoveCamera == true)
            {
                if ((m_ZoomInfo.m_FadeIn_Time -= dt) > 0f)
                {
                    trCon.position += m_ZoomInfo.m_DeltaDir * dt * m_ZoomInfo.m_FadeIn_MoveSpeed;
                }
                else
                {
                    trCon.position += m_ZoomInfo.m_DeltaDir * (m_ZoomInfo.m_FadeIn_Time + dt) * m_ZoomInfo.m_FadeIn_MoveSpeed;
                    bMoveCamera = false;
                }
            }

            if( m_ZoomInfo.m_IsZoom )
            {
                if ((m_ZoomDelta += delta) < m_ZoomInfo.m_Dest)
                {
                    m_ZoomDelta = m_ZoomInfo.m_Dest;
                    break;
                }
            }
            else
            {
                if ((m_ZoomDelta += delta) > m_ZoomInfo.m_Dest)
                {
                    m_ZoomDelta = m_ZoomInfo.m_Dest;
                    break;
                }
            }
            
            yield return null;
        }

        if (m_ZoomInfo.m_Duration > 0)
        {
            yield return new WaitForSeconds(m_ZoomInfo.m_Duration);
        }

        if (m_ZoomInfo.m_DeltaDir != Vector3.zero)
        {
            bMoveCamera = true;
        }

        while (true)
        {
            dt = Time.deltaTime;
            delta = dt * m_ZoomInfo.m_FadeOut_Velocity;

            if (bMoveCamera == true)
            {
                if ((m_ZoomInfo.m_FadeOut_Time -= dt) > 0f)
                {
                    trCon.position -= m_ZoomInfo.m_DeltaDir * dt * m_ZoomInfo.m_FadeOut_MoveSpeed;
                }
                else
                {
                    trCon.position -= m_ZoomInfo.m_DeltaDir * (m_ZoomInfo.m_FadeOut_Time + dt) * m_ZoomInfo.m_FadeOut_MoveSpeed;
                    bMoveCamera = false;
                }
            }

            if (m_ZoomInfo.m_IsZoom)
            {
                if ((m_ZoomDelta += delta) > 0)
                {
                    break;
                }
            }
            else
            {
                if ((m_ZoomDelta += delta) < 0)
                {
                    break;
                }
            }

            yield return null;
        }

        ResetZoom();        
    }

    IEnumerator ZoomEndStageCoroutine()
    {
        CameraFilterPack_Blur_Focus filter = SharedObject.g_MainCamera.gameObject.AddComponent<CameraFilterPack_Blur_Focus>();

        CameraFilterPack_Blur_Focus.ChangeEyes = 20.0f;

        float dt, delta;

        if (m_ZoomInfo.m_StartDelay > 0)
        {
            yield return new WaitForSeconds(m_ZoomInfo.m_StartDelay);
        }

        Transform trCon = transform.parent.parent;
        bool bMoveCamera = false;

        if (m_ZoomInfo.m_DeltaDir != Vector3.zero)
        {
            bMoveCamera = true;
            m_IsFallowMy = false;
        }

        while (true)
        {
            dt = Time.deltaTime;
            delta = dt * m_ZoomInfo.m_FadeIn_Velocity;

            if (CameraFilterPack_Blur_Focus.ChangeEyes > 3.0f)
            {
                CameraFilterPack_Blur_Focus.ChangeEyes -= (17f * dt) / m_ZoomInfo.m_FadeIn_Time;

                if (CameraFilterPack_Blur_Focus.ChangeEyes < 3.0f)
                    CameraFilterPack_Blur_Focus.ChangeEyes = 3.0f;
            }

            if (bMoveCamera)
            {
                if ((m_ZoomInfo.m_FadeIn_Time -= dt) > 0f)
                {
                    trCon.position += m_ZoomInfo.m_DeltaDir * dt * m_ZoomInfo.m_FadeIn_MoveSpeed;
                }
                else
                {
                    trCon.position += m_ZoomInfo.m_DeltaDir * (m_ZoomInfo.m_FadeIn_Time + dt) * m_ZoomInfo.m_FadeIn_MoveSpeed;
                    bMoveCamera = false;
                }
            }

            if (m_ZoomInfo.m_IsZoom)
            {
                if ((m_ZoomDelta + delta) < m_ZoomInfo.m_Dest)
                {
                    m_ZoomDelta = m_ZoomInfo.m_Dest;
                    break;
                }
                m_ZoomDelta += delta;
            }
            else
            {
                if ((m_ZoomDelta + delta) > m_ZoomInfo.m_Dest)
                {
                    m_ZoomDelta = m_ZoomInfo.m_Dest;
                    break;
                }
                m_ZoomDelta += delta;
            }

            yield return null;
        }

        yield return new WaitForSeconds(m_ZoomInfo.m_Duration);

        if (m_ZoomInfo.m_DeltaDir != Vector3.zero)
        {
            bMoveCamera = true;
        }

        while (true)
        {
            dt = Time.deltaTime;
            delta = dt * m_ZoomInfo.m_FadeOut_Velocity;

            if (CameraFilterPack_Blur_Focus.ChangeEyes < 3.0f)
            {
                CameraFilterPack_Blur_Focus.ChangeEyes += (17f * dt) / m_ZoomInfo.m_FadeOut_Time;
            }

            if (bMoveCamera == true)
            {
                if ((m_ZoomInfo.m_FadeOut_Time -= dt) > 0f)
                {
                    trCon.position -= m_ZoomInfo.m_DeltaDir * dt * m_ZoomInfo.m_FadeOut_MoveSpeed;
                }
                else
                {
                    trCon.position -= m_ZoomInfo.m_DeltaDir * (m_ZoomInfo.m_FadeOut_Time + dt) * m_ZoomInfo.m_FadeOut_MoveSpeed;
                    bMoveCamera = false;
                }
            }

            if (m_ZoomInfo.m_IsZoom)
            {
                if ((m_ZoomDelta + delta) > 0)
                {
                    m_ZoomDelta = 0;
                    break;
                }
                m_ZoomDelta += delta;
            }
            else
            {
                if ((m_ZoomDelta + delta) < 0)
                {
                    m_ZoomDelta = 0;
                    break;
                }
                m_ZoomDelta += delta;
            }

            yield return null;
        }

        Destroy(filter);
    }

    public void Zoom(int CameraID)
    {
        //if (m_IsEndStage == true)
        //    return;

        //Table_CameraZoom.Info info = SharedObject.g_TableMgr.m_CameraZoom.Get(CameraID);

        //Zoom( info.m_fStartDelay, info.m_fZoom, info.m_fTime_Start, info.m_fDuration, info.m_fTime_End, Vector3.zero );
    }

    public void Zoom(float StartDelay, float ZoomDest, float BlendInTime, float Duration, float BlendOutTime, Vector3 vDeltaPos)
    {
        m_ZoomInfo.m_FadeIn_Time = (BlendInTime * Time.timeScale);
        m_ZoomInfo.m_FadeOut_Time = (BlendOutTime * Time.timeScale);

        m_ZoomInfo.m_StartDelay = StartDelay;
        m_ZoomInfo.m_Dest = ZoomDest;
        m_ZoomInfo.m_IsZoom = (m_ZoomInfo.m_Dest < 0);
        m_ZoomInfo.m_Duration = Duration * Time.timeScale;
        m_ZoomInfo.m_FadeIn_Velocity = (m_ZoomInfo.m_Dest) / m_ZoomInfo.m_FadeIn_Time;
        m_ZoomInfo.m_FadeOut_Velocity = (-m_ZoomInfo.m_Dest) / m_ZoomInfo.m_FadeOut_Time;

        if (vDeltaPos != Vector3.zero)
        {
            vDeltaPos.z = 0;
            float lenth = vDeltaPos.magnitude;
            m_ZoomInfo.m_FadeIn_MoveSpeed = lenth / m_ZoomInfo.m_FadeIn_Time;
            m_ZoomInfo.m_FadeOut_MoveSpeed = lenth / m_ZoomInfo.m_FadeOut_Time;
            vDeltaPos.Normalize();
        }
        m_ZoomInfo.m_DeltaDir = vDeltaPos;

        StopCoroutine("ZoomCoroutine");
        ResetZoom();
        StartCoroutine("ZoomCoroutine");
    }

    public void ZoomEndStage(float StartDelay, float ZoomDest, float BlendInTime, float Duration, float BlendOutTime, Vector3 vDeltaPos)
    {
        m_IsEndStage = true;

        m_ZoomInfo.m_FadeIn_Time = (BlendInTime * Time.timeScale);
        m_ZoomInfo.m_FadeOut_Time = (BlendOutTime * Time.timeScale);

        m_ZoomInfo.m_StartDelay = StartDelay;
        m_ZoomInfo.m_Dest = ZoomDest;
        m_ZoomInfo.m_IsZoom = (m_ZoomInfo.m_Dest < 0);
        m_ZoomInfo.m_Duration = Duration * Time.timeScale;
        m_ZoomInfo.m_FadeIn_Velocity = (m_ZoomInfo.m_Dest) / m_ZoomInfo.m_FadeIn_Time;
        m_ZoomInfo.m_FadeOut_Velocity = (-m_ZoomInfo.m_Dest) / m_ZoomInfo.m_FadeOut_Time;

        if (vDeltaPos != Vector3.zero)
        {
            vDeltaPos.z = 0;
            float lenth = vDeltaPos.magnitude;
            m_ZoomInfo.m_FadeIn_MoveSpeed = lenth / m_ZoomInfo.m_FadeIn_Time;
            m_ZoomInfo.m_FadeOut_MoveSpeed = lenth / m_ZoomInfo.m_FadeOut_Time;
            vDeltaPos.Normalize();

            StopCoroutine("ShakeCoroutine");
        }

        m_ZoomInfo.m_DeltaDir = vDeltaPos;

        ResetZoom();

        StartCoroutine("ZoomEndStageCoroutine");
    }

    public void ZoomStartEvent(float StartDelay, float ZoomDest, float BlendInTime, float Duration, float BlendOutTime, Vector3 vDeltaPos)
    {
        m_IsEndStage = true;

        m_ZoomInfo.m_FadeIn_Time = (BlendInTime * Time.timeScale);
        m_ZoomInfo.m_FadeOut_Time = (BlendOutTime * Time.timeScale);

        m_ZoomInfo.m_StartDelay = StartDelay;
        m_ZoomInfo.m_Dest = ZoomDest;
        m_ZoomInfo.m_IsZoom = (m_ZoomInfo.m_Dest < 0);
        m_ZoomInfo.m_Duration = Duration * Time.timeScale;
        m_ZoomInfo.m_FadeIn_Velocity = (m_ZoomInfo.m_Dest) / m_ZoomInfo.m_FadeIn_Time;
        m_ZoomInfo.m_FadeOut_Velocity = (-m_ZoomInfo.m_Dest) / m_ZoomInfo.m_FadeOut_Time;

        m_ZoomInfo.m_DeltaDir = vDeltaPos;

        ResetZoom();

        StartCoroutine("ZoomStartEventCoroutine");
    }

    IEnumerator ZoomStartEventCoroutine()
    {
        float dt, delta;

        if (m_ZoomInfo.m_StartDelay > 0)
        {
            yield return new WaitForSeconds(m_ZoomInfo.m_StartDelay);
        }

        Transform trCon = transform.parent.parent;

        bool bMoveCamera = false;

        if (m_ZoomInfo.m_DeltaDir != Vector3.zero)
        {
            bMoveCamera = true;
            m_IsFallowMy = false;
        }

        while (true)
        {
            dt = Time.deltaTime;
            delta = dt * m_ZoomInfo.m_FadeIn_Velocity;

            if (bMoveCamera)
            {
                if ((m_ZoomInfo.m_FadeIn_Time -= dt) > 0f)
                {
                    trCon.position += m_ZoomInfo.m_DeltaDir * dt * m_ZoomInfo.m_FadeIn_MoveSpeed;
                }
                else
                {
                    trCon.position += m_ZoomInfo.m_DeltaDir * (m_ZoomInfo.m_FadeIn_Time + dt) * m_ZoomInfo.m_FadeIn_MoveSpeed;
                    bMoveCamera = false;
                }
            }

            if (m_ZoomInfo.m_IsZoom)
            {
                if ((m_ZoomDelta + delta) < m_ZoomInfo.m_Dest)
                {
                    m_ZoomDelta = m_ZoomInfo.m_Dest;
                    break;
                }
                m_ZoomDelta += delta;
            }
            else
            {
                if ((m_ZoomDelta + delta) > m_ZoomInfo.m_Dest)
                {
                    m_ZoomDelta = m_ZoomInfo.m_Dest;
                    break;
                }
                m_ZoomDelta += delta;
            }

            yield return null;
        }

        yield return new WaitForSeconds(m_ZoomInfo.m_Duration);
    }

    public void ZoomEndEvent()
    {
        StartCoroutine("ZoomEndEventCoroutine");
    }

    IEnumerator ZoomEndEventCoroutine()
    {
        float dt, delta;

        Transform trCon = transform.parent.parent;

        bool bMoveCamera = false;

        if (m_ZoomInfo.m_DeltaDir != Vector3.zero)
        {
            bMoveCamera = true;
        }

        while (true)
        {
            dt = Time.deltaTime;
            delta = dt * m_ZoomInfo.m_FadeOut_Velocity;

            if (bMoveCamera == true)
            {
                if ((m_ZoomInfo.m_FadeOut_Time -= dt) > 0f)
                {
                    trCon.position -= m_ZoomInfo.m_DeltaDir * dt * m_ZoomInfo.m_FadeOut_MoveSpeed;
                }
                else
                {
                    trCon.position -= m_ZoomInfo.m_DeltaDir * (m_ZoomInfo.m_FadeOut_Time + dt) * m_ZoomInfo.m_FadeOut_MoveSpeed;
                    bMoveCamera = false;
                }
            }

            if (m_ZoomInfo.m_IsZoom)
            {
                if ((m_ZoomDelta + delta) > 0)
                {
                    m_ZoomDelta = 0;
                    break;
                }
                m_ZoomDelta += delta;
            }
            else
            {
                if ((m_ZoomDelta + delta) < 0)
                {
                    m_ZoomDelta = 0;
                    break;
                }
                m_ZoomDelta += delta;
            }

            yield return null;
        }
    }

    //public void ZoomPrefab( string name, Rtti.eRTTI Owner )
    //{
    // 기존게 몬스터 인데 플레이어거 들어오면 플레이어거 무시
    //switch( m_ZoomPrefabOwner )
    //{
    //    case Rtti.eRTTI.MONSTER:
    //        {
    //            switch( Owner )
    //            {
    //                case Rtti.eRTTI.MYPLAYER:
    //                    return;
    //            }
    //        }
    //        break;
    //}

    //AnimationClip clip = m_AniPrefab.GetClip(name);

    //if( clip == null )
    //{
    //    clip = AssetBundleManager.Load<AnimationClip>("Controller/Camera/" + name, "anim", eAB_Kind.Anim);
    //    if( clip == null )
    //    {
    //        Debug.LogError("Controller/Camera/" + name + " : 카메라 파일이 없습니다.");
    //        return;
    //    }
    //    m_AniPrefab.AddClip(clip, name);
    //}

    //assert_cs.set(clip);

    //m_CameraAniName = name;
    //m_AniPrefab.Rewind(m_CameraAniName);
    //m_AniPrefab.Play(m_CameraAniName);

    //CancelInvoke("_InvokeCameraAni");
    //Invoke("_InvokeCameraAni", GF.AniLen(clip));
    //m_ZoomPrefabOwner = Owner;
    //m_StartCameraAniTime = Time.realtimeSinceStartup;
    //m_IsCameraAni = true;
    //}

    //void InvokeCameraAni()
    //{
    //    m_AniPrefab.Stop();
    //    m_IsCameraAni = false;

    //    ResetZoom();
    //}
}
