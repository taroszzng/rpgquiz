﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public partial class MainCamera : MonoBehaviour
{
    [System.Serializable]
    public enum eFX_MAP
    {
        NONE,
        MAP_DESERT,
        MAP_WATER,

        SKILL,
    }

    public enum eSUB_CAMERA
    {
        FX_01   = 0,
        FX_02,
        _COUNT,
    }
    Camera[] m_SubCameraList = new Camera[(int)eSUB_CAMERA._COUNT];

    Camera Get_FxCamera( eSUB_CAMERA camera )
    {
        if( m_SubCameraList[(int)camera] == null )
        {
            //GameObject obj = AssetBundleManager.Load("Prefabs/_Scene_Ingame/Fx_Camera", "prefab") as GameObject;
            //obj = GameObject.Instantiate(obj) as GameObject;
            //obj.transform.SetParent(transform, false);
            //m_SubCameraList[(int)camera] = obj.GetComponent<Camera>();
            //switch( camera )
            //{
            //    case eSUB_CAMERA.FX_01:
            //        m_SubCameraList[(int)camera].cullingMask = (int)nsENUM.ePHY_LAYER_MASK.Camera_Fx_01;
            //        break;
            //    case eSUB_CAMERA.FX_02:
            //        m_SubCameraList[(int)camera].cullingMask = (int)nsENUM.ePHY_LAYER_MASK.Camera_Fx_02;
            //        break;
            //    default:
            //        assert_cs.set("default : " + camera);
            //        break;
            //} 
        }

        m_SubCameraList[(int)camera].gameObject.SetActive(true);
        m_SubCameraList[(int)camera].orthographicSize = m_ZoomOrg;
        Transform trMesh = m_SubCameraList[(int)camera].transform.Find("Mesh");
        trMesh.localScale = new Vector3(m_CameraFOV_X * 2f, m_CameraFOV_Y * 2f, 1f);

        m_Camera.enabled = false;
        m_Camera.enabled = true;
        return m_SubCameraList[(int)camera];
    }

    //public void Fx_Map(eFX_MAP type)
    //{
    //    switch( type )
    //    {
    //        case eFX_MAP.MAP_DESERT:
    //            {
    //                SharedObject.g_StageMng.LayerChange(nsENUM.ePHY_LAYER.Camera_Fx_02, StageMng.eLAYER_APPLY.SKY);

    //                Camera fxc = _Get_FxCamera(eSUB_CAMERA.FX_02);
    //                fxc.gameObject.AddComponent<CameraFilterPack_Distortion_Wave_Horizontal>();
    //                fxc.gameObject.AddComponent<CameraFilterPack_Blur_Steam>();
    //                CameraFilterPack_Distortion_Wave_Horizontal.ChangeWaveIntensity = 23.9f;
    //                CameraFilterPack_Blur_Steam.ChangeRadius = 0.015f;
    //                CameraFilterPack_Blur_Steam.ChangeQuality = 0.65f;
    //            }
    //            break;
    //        case eFX_MAP.MAP_WATER:
    //            {
    //                SharedObject.g_StageMng.LayerChange(nsENUM.ePHY_LAYER.Camera_Fx_02, StageMng.eLAYER_APPLY.SKY);

    //                Camera fxc = _Get_FxCamera(eSUB_CAMERA.FX_02);
    //                fxc.gameObject.AddComponent<CameraFilterPack_Distortion_Flag>();
    //                fxc.gameObject.AddComponent<CameraFilterPack_AAA_WaterDrop>();
    //                CameraFilterPack_Distortion_Flag.ChangeDistortion = 0.25f;
    //                CameraFilterPack_AAA_WaterDrop.ChangeDistortion = 18.5f;
    //                CameraFilterPack_AAA_WaterDrop.ChangeSizeX = 0.78f;
    //                CameraFilterPack_AAA_WaterDrop.ChangeSizeY = 0.32f;
    //                CameraFilterPack_AAA_WaterDrop.ChangeSpeed = 0.58f;
    //            }
    //            break;
    //        case eFX_MAP.SKILL:
    //            {
    //                SharedObject.g_StageMng.SaveLayer();
    //                SharedObject.g_StageMng.LayerChange(nsENUM.ePHY_LAYER.Camera_Fx_01, StageMng.eLAYER_APPLY.ALL);
    //            }
    //            break;
    //    }
    //}

    //public void Fx_Map_Release(eFX_MAP type)
    //{
    //    switch (type)
    //    {
    //        case eFX_MAP.SKILL:
    //            {
    //                SharedObject.g_StageMng.LoadLayer();
    //            }
    //            break;
    //    }
    //}
}
