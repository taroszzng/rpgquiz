﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class MainCamera : MonoBehaviour 
{
    public enum eCAMERA_LOCK
    {
        UNLOCK,
        L_LOCK,
        R_LOCK,
    }

    public bool m_IsFallowMy = true;

    float m_OldViewPort;
    public float m_CameraFOV_X;
    public float m_CameraFOV_Y;
    protected eCAMERA_LOCK m_CameraLock = eCAMERA_LOCK.UNLOCK;
    protected Vector3 m_vOrgPos;
    protected Vector3 m_vOrgPosZone;
    float m_ZonePosY;
    protected Vector3 m_OldPlayerPos;
    public Camera m_Camera;
    public Animation m_AniPrefab;

    [System.NonSerialized]
    public float m_ZoomOrg =7.2f;

    [System.NonSerialized]
    float m_ZoomCur = 7.2f;

    float m_PlayerCameraOffsetY;
    bool m_ApplyOffset = false;
    
    public enum eMOVE_TYPE
    {
        FOLLOW,
        FREEZE_X,
        FREEZE_XY,
    }
    public eMOVE_TYPE m_MoveType = eMOVE_TYPE.FOLLOW;

    public enum eHEIGHT_TYPE
    {
        APPLY_FROM_2D,
        APPLY_FROM_3D,
    }
    public eHEIGHT_TYPE m_HeightType = eHEIGHT_TYPE.APPLY_FROM_2D;

    public float m_ChangeYRate = 0.6f;

    public bool IsFreeMove() { return m_MoveType == eMOVE_TYPE.FOLLOW; }

    void Awake()
    {
        m_ZoomDelta = 0f;
        SharedObject.g_MainCamera = this;
        m_Camera = gameObject.GetComponent<Camera>();

        //m_Camera.cullingMask = (int)nsENUM.ePHY_LAYER_MASK.e;

        m_ZoomCur = CheckWidth();
        m_Camera.orthographicSize = m_ZoomCur;
        m_OldViewPort = m_Camera.orthographicSize;
        m_vOrgPos = transform.position;
        m_vOrgPosZone = m_vOrgPos;

        Vector3 v1 = m_Camera.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 v2 = m_Camera.ViewportToWorldPoint(new Vector3(1, 1, 0));
        m_CameraFOV_X = (v2.x - v1.x) * 0.5f;
        m_CameraFOV_Y = (v2.y - v1.y) * 0.5f;

        m_AniPrefab = transform.parent.gameObject.AddComponent<Animation>();
        m_AniPrefab.playAutomatically = false;

        InitShake();
    }

    float CheckWidth()
    {
        /* 320*480 = 2:3 = 0.66
        * 640*960 = 2:3
        * 
        * 768*1024 = 3:4 = 0.75
        * 1536*2048 = 3:4
        * 2048*2732 = 3:4
        * 
        * 480*800 = 3:5  = 0.6
        * 768*1280 = 3:5
        * 
        * 640*1136 = 9:16 = 0.5625
        * 720*1280 = 9:16
        * 750*1334 = 9:16
        * 1440*2560 = 9:16
        * 1080*1920 = 9:16
        * 
        * 1440*2880 = 9:18 = 0.5
        * 
        * 1440*2960 = 9:18.5 = 0.4864
        * 
        * 1125*2436 = 9:19.5 = 0.4618
        * 
        * 800*1280 = 10:16  = 0.625
        * 1200*1920 = 10:16
        * 1600*2560 = 10:16*/

        float fvalue = 7f;

        float frate = (float)Screen.height / (float)Screen.width;

        if (0.75 <= frate ) //3:4
        {
            fvalue = 12f;
        }
        else if (0.66 <= frate && 0.74 > frate) //2:3
        {
            fvalue = 11.2f;
        }
        else if (0.62 <= frate && 0.66 > frate) //10:16
        {
            fvalue = 11.2f;
        }
        else if (0.6 <= frate && 0.62 > frate) //3:5
        {
            fvalue = 11f;
        }
        else if(0.56 <= frate && 0.6 > frate)//9:16
        {
            fvalue = 10.7f;
        }
        else if (0.5 <= frate && 0.56 > frate)//9:18
        {
            fvalue = 10f;
        }
        else if (0.48 <= frate && 0.5 > frate) //9:18.5
        {
            fvalue = 9.8f;
        }
        else if ( 0.48 > frate) //9:19.5
        {
            fvalue = 9.3f;
        }

        Debug.Log("MainCamera width = " + Screen.width + " height = " + Screen.height);
        Debug.Log("MainCamera fvalue = " + fvalue + " frate = " + frate );

        return fvalue;
    }

    public void Reset( float size, Vector3 vPlayerPos )
    {
        ResetShakeTr();
        ResetZoom();

        m_OldPlayerPos = vPlayerPos;

        m_CameraLock = eCAMERA_LOCK.UNLOCK;

        m_vOrgPosZone = m_vOrgPos + SharedObject.g_CurZone.transform.position;
        Vector3 vCamera = m_vOrgPosZone;

        switch( m_MoveType )
        {
            case eMOVE_TYPE.FOLLOW:
                vCamera.x = vPlayerPos.x;
                break;
            case eMOVE_TYPE.FREEZE_X:                
            case eMOVE_TYPE.FREEZE_XY:
                break;
        }
        
        transform.position = vCamera;
        m_PlayerCameraOffsetY = m_OldPlayerPos.y - vCamera.y;
        m_ZonePosY = SharedObject.g_CurZone.transform.position.y;

        if( m_ZoomCur != size )
        {
            m_ZoomCur = size;
            ChangeViewPort(m_ZoomCur, vPlayerPos);
        }        
    }

    void CameraLimit(bool bOrgPosY = false)
    {        
        Vector3 camerapos = m_vOrgPosZone;
        switch (m_MoveType)
        {
            case eMOVE_TYPE.FOLLOW:
                camerapos.x = m_OldPlayerPos.x;
                camerapos.y = transform.parent.position.y + m_vOrgPosZone.y + ((m_OldPlayerPos.y - m_vOrgPosZone.y) - m_PlayerCameraOffsetY) * m_ChangeYRate;
                break;
            case eMOVE_TYPE.FREEZE_X:
                camerapos.y = transform.parent.position.y + m_vOrgPosZone.y + ((m_OldPlayerPos.y - m_vOrgPosZone.y) - m_PlayerCameraOffsetY) * m_ChangeYRate;
                break;
            case eMOVE_TYPE.FREEZE_XY:
                break;
        }

        //float dy = SharedObject.g_CurZone.Bottom + m_CameraFOV_Y + m_ZonePosY;
        //if (camerapos.y < dy)
        //{
        //    camerapos.y = dy;
        //}

        //dy = SharedObject.g_CurZone.Top - m_CameraFOV_Y + m_ZonePosY;
        //if (camerapos.y > dy)
        //{
        //    camerapos.y = dy;
        //}

        if (camerapos.x - m_CameraFOV_X < SharedObject.g_CurZone.Left)
        {
            camerapos.x = SharedObject.g_CurZone.Left + m_CameraFOV_X;
        }
        else if (camerapos.x + m_CameraFOV_X > SharedObject.g_CurZone.Right)
        {
            camerapos.x = SharedObject.g_CurZone.Right - m_CameraFOV_X;
        }

        if (bOrgPosY)//[kks][Camera]:y값은 고정 APPLY_FROM_2D시 y값이 변해 쉐이크가 안먹을때 있으므로
            camerapos.y = m_vOrgPosZone.y;

        //SharedObject.g_CurStage.UpdateBG(camerapos - transform.position);
        //transform.position = camerapos;
    }

    public void ChangeViewPort( float size, Vector3 CenterPos )
    {
        m_OldPlayerPos = CenterPos;
        m_Camera.orthographicSize = size;        
        m_OldViewPort = size;

        float oldFOV = m_CameraFOV_X;
        Vector3 v1 = m_Camera.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 v2 = m_Camera.ViewportToWorldPoint(new Vector3(1, 1, 0));
        m_CameraFOV_X = (v2.x - v1.x) * 0.5f;
        m_CameraFOV_Y = (v2.y - v1.y) * 0.5f;

        for (int i = 0; i < (int)eSUB_CAMERA._COUNT; ++i)
        {
            if (m_SubCameraList[i] == null)
                continue;

            m_SubCameraList[i].orthographicSize = size;
            m_SubCameraList[i].transform.Find("Mesh").localScale = new Vector3(m_CameraFOV_X * 2f, m_CameraFOV_Y * 2f, 1f);
        }

        float dx = m_CameraFOV_X - oldFOV;

        Vector3 camerapos = m_vOrgPosZone;
        switch (m_MoveType)
        {
            case eMOVE_TYPE.FOLLOW:
                camerapos.x = CenterPos.x;
                camerapos.y = transform.parent.position.y + m_vOrgPosZone.y + ((CenterPos.y - m_vOrgPosZone.y) - m_PlayerCameraOffsetY) * m_ChangeYRate;
                break;
            case eMOVE_TYPE.FREEZE_X:
                camerapos.y = transform.parent.position.y + m_vOrgPosZone.y + ((CenterPos.y - m_vOrgPosZone.y) - m_PlayerCameraOffsetY) * m_ChangeYRate;
                break;
            case eMOVE_TYPE.FREEZE_XY:
                break;
        }

        //float dy = SharedObject.g_CurZone.m_Bottom + m_CameraFOV_Y + m_ZonePosY;
        //if (camerapos.y < dy)
        //{
        //    camerapos.y = dy;
        //}
        //dy = SharedObject.g_CurZone.m_Top - m_CameraFOV_Y + m_ZonePosY;
        //if (camerapos.y > dy)
        //{
        //    camerapos.y = dy;
        //}

        switch (m_CameraLock)
        {
            case eCAMERA_LOCK.UNLOCK:
                {
                    if (camerapos.x - m_CameraFOV_X < SharedObject.g_CurZone.Left)
                    {
                        camerapos.x = SharedObject.g_CurZone.Left + m_CameraFOV_X;
                        m_CameraLock = eCAMERA_LOCK.L_LOCK;
                    }
                    else if (camerapos.x + m_CameraFOV_X > SharedObject.g_CurZone.Right)
                    {
                        camerapos.x = SharedObject.g_CurZone.Right - m_CameraFOV_X;
                        m_CameraLock = eCAMERA_LOCK.R_LOCK;
                    }
                }
                break;
            case eCAMERA_LOCK.L_LOCK:
                {
                    camerapos.x = SharedObject.g_CurZone.Left + m_CameraFOV_X;

                    if (CenterPos.x > camerapos.x)
                    {
                        camerapos.x = CenterPos.x;
                        m_CameraLock = eCAMERA_LOCK.UNLOCK;
                    }
                }
                break;
            case eCAMERA_LOCK.R_LOCK:
                {
                    camerapos.x = SharedObject.g_CurZone.Right - m_CameraFOV_X;

                    if (CenterPos.x < camerapos.x)
                    {
                        camerapos.x = CenterPos.x;
                        m_CameraLock = eCAMERA_LOCK.UNLOCK;
                    }
                }
                break;
            default:
                {
                    assert_cs.msg("Default : " + m_CameraLock);
                }
                break;
        }

        //SharedObject.g_CurStage.UpdateBG(camerapos - transform.position);
        //transform.position = camerapos;
    }
    
    void LateUpdate()
    {
        if (SharedObject.g_SceneMgr.Pause)
            return;

        float Zoom = m_ZoomCur + m_ZoomDelta;

        if (Zoom != m_OldViewPort)
        {
            ChangeViewPort(Zoom, m_vPlayerPos);
        }

        if (m_bCameraShake == true)
        {
            CameraLimit(true);
            m_ApplyOffset = true;
        }

        if ((m_IsCameraAni == true))
        {
            CameraLimit();
            m_ApplyOffset = true;
        }
        // 리미트때 변화된 값을 돌려주자
        else if ( m_ApplyOffset == true )
        {            
            m_OldPlayerPos = Vector3.zero;
            
            //SharedObject.g_MyPlayer.ResetCamera();

            m_ApplyOffset = false;
        }
    }

    Vector3 m_vPlayerPos;

    public void UpdateCamera_By_My( Vector3 vPlayerPos )
    {
        m_vPlayerPos = vPlayerPos;

        if (m_IsFallowMy == false)
            return;

        if (vPlayerPos == m_OldPlayerPos)
            return;

        Vector3 camerapos = m_vOrgPosZone;

        bool x_move = false;
        switch (m_MoveType)
        {
            case eMOVE_TYPE.FOLLOW:
                x_move = true;
                camerapos.x = vPlayerPos.x;
                camerapos.y = transform.parent.position.y + m_vOrgPosZone.y + ((vPlayerPos.y - m_vOrgPosZone.y) - m_PlayerCameraOffsetY) * m_ChangeYRate;        
                break;
            case eMOVE_TYPE.FREEZE_X:
                camerapos.y = transform.parent.position.y + m_vOrgPosZone.y + ((vPlayerPos.y - m_vOrgPosZone.y) - m_PlayerCameraOffsetY) * m_ChangeYRate;
                break;
            case eMOVE_TYPE.FREEZE_XY:
                return;
        }        

        //float dy = SharedObject.g_CurZone.m_Bottom + m_CameraFOV_Y + m_ZonePosY;
        //if (camerapos.y < dy)
        //{
        //    camerapos.y = dy;
        //}
        //dy = SharedObject.g_CurZone.m_Top - m_CameraFOV_Y + m_ZonePosY;
        //if (camerapos.y > dy)
        //{
        //    camerapos.y = dy;
        //}      

        if (x_move == true)
        {
            switch (m_CameraLock)
            {
                case eCAMERA_LOCK.UNLOCK:
                    {
                        camerapos.x = vPlayerPos.x;

                        if (camerapos.x - m_CameraFOV_X < SharedObject.g_CurZone.Left)
                        {
                            camerapos.x = SharedObject.g_CurZone.Left + m_CameraFOV_X;
                            m_CameraLock = eCAMERA_LOCK.L_LOCK;
                        }
                        else if (camerapos.x + m_CameraFOV_X > SharedObject.g_CurZone.Right)
                        {
                            camerapos.x = SharedObject.g_CurZone.Right - m_CameraFOV_X;
                            m_CameraLock = eCAMERA_LOCK.R_LOCK;
                        }
                    }
                    break;
                case eCAMERA_LOCK.L_LOCK:
                    {
                        if (vPlayerPos.x >= camerapos.x)
                        {
                            camerapos.x = vPlayerPos.x;
                            if (camerapos.x - m_CameraFOV_X < SharedObject.g_CurZone.Left)
                            {
                                camerapos.x = SharedObject.g_CurZone.Left + m_CameraFOV_X;
                            }
                            else
                            {
                                m_CameraLock = eCAMERA_LOCK.UNLOCK;
                            }
                        }
                    }
                    break;
                case eCAMERA_LOCK.R_LOCK:
                    {
                        if (vPlayerPos.x <= camerapos.x)
                        {
                            camerapos.x = vPlayerPos.x;
                            if (camerapos.x + m_CameraFOV_X > SharedObject.g_CurZone.Right)
                            {
                                camerapos.x = SharedObject.g_CurZone.Right - m_CameraFOV_X;
                                m_CameraLock = eCAMERA_LOCK.R_LOCK;
                            }
                            else
                            {
                                m_CameraLock = eCAMERA_LOCK.UNLOCK;
                            }
                        }
                    }
                    break;
                default:
                    {
                        assert_cs.msg("Default : " + m_CameraLock);
                    }
                    break;
            }
        }        

        //assert_cs.set(camerapos.x >= SharedObject.g_CurZone.Left - m_CameraFOV_X);
        //assert_cs.set(camerapos.x <= SharedObject.g_CurZone.Right + m_CameraFOV_X);
        //SharedObject.g_CurStage.UpdateBG(camerapos - transform.position);

        transform.position = camerapos;
        m_OldPlayerPos = vPlayerPos;        
    }
}
 