﻿using UnityEngine;
using System.Collections;

public partial class MainCamera : MonoBehaviour
{
    bool m_bCameraShake = false;
    Transform m_ShakeTr;

    public class cShakeInfo
    {
        public float m_StartDelay;

        public bool m_UseTotalTime;
        public float m_TotalTime;
        
        public Vector3 m_Dest;
        public Vector3 m_Shake;
        public Vector3 m_Dir;

        public float m_RemainDist;
        public float m_RemainCountDist;    // 원점

        public bool m_UseCount;
        public int m_Count;

        public float m_Veclocity;

        public bool m_UseDamping;
        public float m_Damping;
        public float m_DampingTime;
    }
    cShakeInfo m_ShakeInfo = new cShakeInfo();
    

    protected void InitShake()
    {
        m_ShakeTr = transform.parent;
        m_bCameraShake = false;
        
    }

    protected void ResetShakeTr()
    {
        m_ShakeTr.localPosition = Vector3.zero;
        m_bCameraShake = false;

        CameraLimit();
    }

    IEnumerator ShakeCoroutine()
    {
        m_bCameraShake = true;

        float dt, dist;

        if(m_ShakeInfo.m_StartDelay > 0 )
        {
            yield return new WaitForSeconds(m_ShakeInfo.m_StartDelay);
        }

        while( true )
        {
            dt = Time.fixedDeltaTime;
            dist = dt*m_ShakeInfo.m_Veclocity;           
            
            if( ( m_ShakeInfo.m_RemainDist -= dist ) > 0 )
            {
                m_ShakeTr.localPosition += m_ShakeInfo.m_Dir * dist;
                
                float rc = transform.position.x - m_CameraFOV_X - SharedObject.g_CurZone.Left;

                if( rc < 0 )
                {
                    m_ShakeTr.localPosition += new Vector3( -rc, 0, 0 );
                }

                rc = SharedObject.g_CurZone.Right - (transform.position.x + m_CameraFOV_X);

                if (rc < 0)
                {
                    m_ShakeTr.localPosition += new Vector3(rc, 0, 0);
                }

                CameraLimit(true);

                if( m_ShakeInfo.m_UseCount )
                {
                    if( ( m_ShakeInfo.m_RemainCountDist -= dist ) < 0 )
                    {
                        m_ShakeInfo.m_RemainCountDist = float.MaxValue;
                        if (--m_ShakeInfo.m_Count <= 0)
                            break;
                    }
                }                
            }
            else
            {
                if (m_ShakeInfo.m_UseDamping)
                {
                    float distDamping = Mathf.Max(m_ShakeInfo.m_Damping * m_ShakeInfo.m_DampingTime, m_ShakeInfo.m_Damping * dt);

                    if (m_ShakeInfo.m_Shake.magnitude > distDamping)
                    {
                        m_ShakeInfo.m_Shake -= m_ShakeInfo.m_Dir * distDamping;
                    }
                    else
                    {
                        // 원점에서 끝내기 위해서
                        m_ShakeInfo.m_UseCount = true;
                        m_ShakeInfo.m_Count = 1;
                    }
                }

                m_ShakeTr.localPosition = m_ShakeInfo.m_Dest - m_ShakeInfo.m_Dir*(-m_ShakeInfo.m_RemainDist);

                float rc = transform.position.x - m_CameraFOV_X - SharedObject.g_CurZone.Left;
                if (rc < 0)
                {
                    m_ShakeTr.localPosition += new Vector3(-rc, 0, 0);
                }

                rc = SharedObject.g_CurZone.Right - (transform.position.x + m_CameraFOV_X);
                if (rc < 0)
                {
                    m_ShakeTr.localPosition += new Vector3(rc, 0, 0);
                }

                CameraLimit(true);

                m_ShakeInfo.m_Shake = -m_ShakeInfo.m_Shake;
                m_ShakeInfo.m_Dest = m_ShakeInfo.m_Shake;
                m_ShakeInfo.m_Dir = -m_ShakeInfo.m_Dir;

                float len = m_ShakeInfo.m_Shake.magnitude;

                m_ShakeInfo.m_RemainCountDist = len + m_ShakeInfo.m_RemainDist;
                m_ShakeInfo.m_RemainDist += len*2f;

                m_ShakeInfo.m_DampingTime = m_ShakeInfo.m_RemainDist / m_ShakeInfo.m_Veclocity;

                if( m_ShakeInfo.m_RemainDist < dist )
                {
                    break;
                }
            }                        

            if( m_ShakeInfo.m_UseTotalTime && ((m_ShakeInfo.m_TotalTime -= dt) < 0) )
                break;

            yield return new WaitForFixedUpdate();
        }

        ResetShakeTr();

        yield break;
    }

    public void Shake(int CameraID, Rtti.eRTTI rtti, bool bOwnerFront )
    {
        if (m_IsFallowMy == false)
            return;

        Table_Camera.Info info = SharedObject.g_TableMgr.m_Camera.Get(CameraID);

        if (info.m_bLocal)
        {
            if (rtti != Rtti.eRTTI.MYPLAYER)
                return;
        }

        m_ShakeInfo.m_StartDelay = info.m_fStartDelay;

        m_ShakeInfo.m_TotalTime = info.m_fTime;
        m_ShakeInfo.m_UseTotalTime = (m_ShakeInfo.m_TotalTime > 0f);

        m_ShakeInfo.m_Shake = new Vector3(info.m_fShake_X, info.m_fShake_Y, 0);

        if (bOwnerFront == false)
            m_ShakeInfo.m_Shake.x = -m_ShakeInfo.m_Shake.x;

        m_ShakeInfo.m_Dest = m_ShakeInfo.m_Shake;

        m_ShakeInfo.m_Dir = m_ShakeInfo.m_Shake;
        m_ShakeInfo.m_Dir.Normalize();

        m_ShakeInfo.m_RemainDist = m_ShakeInfo.m_Shake.magnitude;
        m_ShakeInfo.m_RemainCountDist = float.MaxValue;

        m_ShakeInfo.m_Veclocity = info.m_fSpeed;

        m_ShakeInfo.m_Damping = info.m_fDamping;
        m_ShakeInfo.m_UseDamping = (m_ShakeInfo.m_Damping > 0f);

        if (m_ShakeInfo.m_UseDamping)
        {
            m_ShakeInfo.m_DampingTime = m_ShakeInfo.m_RemainDist / m_ShakeInfo.m_Veclocity;
        }

        m_ShakeInfo.m_Count = info.m_nShakeCount;
        m_ShakeInfo.m_UseCount = (m_ShakeInfo.m_Count > 0);

        StopCoroutine("ShakeCoroutine");
        ResetShakeTr();
        StartCoroutine("ShakeCoroutine");
    }
}
