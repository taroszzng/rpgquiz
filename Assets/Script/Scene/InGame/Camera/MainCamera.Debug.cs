﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class MainCamera : MonoBehaviour
{


#if UNITY_EDITOR
    DrawLine m_DrawLine = new DrawLine();
    public enum eDRAW_COLOR
    {
        RED,
        GREEN,
        GRAY,

        _COUNT
    }
    List<BoxCollider>[] m_LineArray = new List<BoxCollider>[(int)eDRAW_COLOR._COUNT];
    List<BoxCollider>[] m_GizmoArray = new List<BoxCollider>[(int)eDRAW_COLOR._COUNT];
    public void SetDraw(BoxCollider box, eDRAW_COLOR color)
    {
        if (m_LineArray[(int)color] == null)
            m_LineArray[(int)color] = new List<BoxCollider>();
        if (m_GizmoArray[(int)color] == null)
            m_GizmoArray[(int)color] = new List<BoxCollider>();

        //if (SharedObject.g_SceneProperty.m_EditorOpt.m_ViewCollider_InGame == true)
        //    m_LineArray[(int)color].Add(box);

        //if (SharedObject.g_SceneProperty.m_EditorOpt.m_ViewCollider_Scene == true)
        //    m_GizmoArray[(int)color].Add(box);
    }

    void OnPostRender()
    {
        for (int i = 0; i < (int)eDRAW_COLOR._COUNT; ++i)
        {
            Color color = Color.white;
            switch (i)
            {
                case (int)eDRAW_COLOR.GREEN: color = Color.green; break;
                case (int)eDRAW_COLOR.GRAY: color = Color.gray; break;
                case (int)eDRAW_COLOR.RED: color = Color.red; break;
            }

            if (m_LineArray[i] == null)
                continue;

            foreach (BoxCollider box in m_LineArray[i])
            {
                if (box == null)
                    continue;
                m_DrawLine.Draw(box, color);
            }

            m_LineArray[i].Clear();
        }
    }

    void OnDrawGizmos()
    {
        for (int i = 0; i < (int)eDRAW_COLOR._COUNT; ++i)
        {
            Gizmos.color = Color.white;
            switch (i)
            {
                case (int)eDRAW_COLOR.GREEN: Gizmos.color = Color.green; break;
                case (int)eDRAW_COLOR.GRAY: Gizmos.color = Color.gray; break;
                case (int)eDRAW_COLOR.RED: Gizmos.color = Color.red; break;
            }

            if (m_GizmoArray[i] == null)
                continue;

            Vector3 v;
            foreach (BoxCollider box in m_GizmoArray[i])
            {
                if (box == null)
                    continue;

                v = box.transform.rotation * box.center + box.transform.position;
                Vector3 size = box.size;
                size.x *= box.gameObject.transform.localScale.x;
                size.y *= box.gameObject.transform.localScale.y;
                size.z *= box.gameObject.transform.localScale.z;

                //Actor.To_SR_Coordinate(ref v);

                Gizmos.DrawWireCube(v, size);
            }

            m_GizmoArray[i].Clear();
        }
    }

#endif
}
