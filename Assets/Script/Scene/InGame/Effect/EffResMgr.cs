using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EffResMgr : MonoBehaviour 
{
    public enum eEFFECT_TYPE
    {
        NORMAL,
        AE,
        PlayMaker,
        BULLET,
        BULLET_NOT_USE_DURATION,
        HIT,
        DIE,
    }

    [System.Serializable]
    protected class cAssetInfo
    {
        public cAssetInfo( eEFFECT_TYPE type, string prefabName) 
        {
            m_Prefab = Resources.Load(prefabName) as GameObject;
            assert_cs.set(m_Prefab, "����Ʈ �������� �������� �ʽ��ϴ�. : [" + prefabName + "]");

            if (null == m_Prefab)
                return;

            m_GameObj = GameObject.Instantiate(m_Prefab) as GameObject;
            m_2DPos = m_GameObj.transform.Find("2D_Pos");

            m_EffectType = type;

            //switch( eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_SCALE )
            //{
            //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_2DPOS_W:
            //        m_2DPos.localScale = parent.GetEffectScale( true );
            //        break;
            //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_2DPOS_H:
            //        m_2DPos.localScale = parent.GetEffectScale( false );
            //        break;
            //}
        }

        public GameObject m_Prefab;
        public GameObject m_GameObj;
        public Transform m_2DPos;
        //public nsTIME.STOP m_StopControl;
        public bool m_ApplyStopControl;
        public float m_Duration;

        public ParticleSystem m_PSRoot;
        public eEFFECT_TYPE m_EffectType;
        public ParticleEdit m_Edit = null;        
    }

    [System.Serializable]
    protected class cPrefabInfo
    {
        public string m_PrefabName;
        public float m_SaveDuration;
        public float m_DeleteTime;
        public bool m_UseCheckState = false;
    }
    [System.NonSerialized]
    protected Dictionary<int, cPrefabInfo> m_PrefabList = new Dictionary<int, cPrefabInfo>();
    [SerializeField]
    protected List<cAssetInfo> m_LiveAssetInfo = new List<cAssetInfo>();

    int m_CurEffect_Hit = 0;
    int m_CurEffect_Die = 0;

    void Awake()
    {
    }

	// Update is called once per frame
	void Update () 
    {
        float dt = Time.deltaTime;

        cAssetInfo A_Info = null;
        int nCnt = m_LiveAssetInfo.Count;
        for( int i=nCnt-1; i>-1; --i )
        {
            A_Info = m_LiveAssetInfo[ i ];
            if (A_Info.m_GameObj.activeSelf == false)
            {
                switch (A_Info.m_EffectType)
                {
                    case eEFFECT_TYPE.HIT:
                        --m_CurEffect_Hit;
                        break;
                    case eEFFECT_TYPE.DIE:
                        --m_CurEffect_Die;
                        break;
                }
                GameObject.Destroy(A_Info.m_GameObj);                
                m_LiveAssetInfo.RemoveAt(i);                
                continue;
            }

            //A_Info.m_2DPos.transform.localPosition = Actor.To_SR_Coordinate_ToVec3(A_Info.m_GameObj.transform.position.z);

            //if( A_Info.m_StopControl != null )
            //{
            //    if( A_Info.m_StopControl.m_Use == true )
            //    {
            //        if( A_Info.m_ApplyStopControl == false )
            //        {
            //            A_Info.m_ApplyStopControl = true;
            //            A_Info.m_Duration += A_Info.m_StopControl.m_Duration;
            //            StartCoroutine( _StopEffectCoroutine( A_Info ) );
            //        }                    
            //    }
            //    else
            //    {
            //        A_Info.m_ApplyStopControl = false; 
            //    }
            //}

            if ((A_Info.m_Duration -= dt) < 0)
            {
                switch (A_Info.m_EffectType)
                {
                    case eEFFECT_TYPE.HIT:
                        --m_CurEffect_Hit;
                        break;
                    case eEFFECT_TYPE.DIE:
                        --m_CurEffect_Die;
                        break;
                }

                GameObject.Destroy(A_Info.m_GameObj);
                m_LiveAssetInfo.RemoveAt(i);
                continue;
            }
        }
	}

    IEnumerator _StopEffectCoroutine( cAssetInfo A_Info )
    {
        A_Info.m_PSRoot.Pause( true );

        yield return new WaitForSeconds( 0.5f );//A_Info.m_StopControl.m_Duration );

        if (A_Info.m_GameObj == null)
            yield break;

        if (A_Info.m_GameObj.activeSelf == false)
			yield break;

        A_Info.m_PSRoot.Play(true);
        A_Info.m_ApplyStopControl = false;                        
    }

    public GameObject LoadEffect(Character parent, string path)
    {
        cPrefabInfo prefab = _GetPrefabInfo(path, parent);

        // �����ε忡�� �ε常 �ϴ� �س��� ����
        //if (eo == MngCollector.eLOAD_EFFECT_OPTION.PRELOAD ||
        //    parent == null)
        //{
        //    return null;
        //}        

        cAssetInfo asset = _GetAsset( eEFFECT_TYPE.NORMAL, prefab, parent, true);

        // ���� üũ�ؼ� ���� ���� ����
        if (prefab.m_UseCheckState == true)
        {
            if (asset.m_Edit.Check_ReqState(parent.m_State) == false)
            {
                asset.m_GameObj.SetActive( false );
                return null;
            }
        }

        Transform tr = asset.m_GameObj.transform;

        if ((asset.m_Edit == null) || (asset.m_Edit.m_bFollowParent))
        {
            tr.SetParent(parent.GetEffectTr(), false);

            //if ((eo & MngCollector.eLOAD_EFFECT_OPTION.POS_CENTER) != 0)
            //{
            //    Vector3 pos = tr.position;
            //    pos.y += parent.GetHeightExtend();
            //    tr.position = pos;
            //}

            //if ((eo & MngCollector.eLOAD_EFFECT_OPTION.POS_TOP) != 0)
            //{
            //    Vector3 pos = tr.position;
            //    pos.y += parent.GetHeight();
            //    tr.position = pos;
            //}

            //switch (eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_ROT)
            //{
            //    case MngCollector.eLOAD_EFFECT_OPTION.ROT_PARENT:
            //        break;

            //    case MngCollector.eLOAD_EFFECT_OPTION.ROT_RIGHT:
            //        if (parent.m_IsFront == false)
            //            tr.localRotation = Quaternion.Euler(0, 180, 0);
            //        break;
            //    case MngCollector.eLOAD_EFFECT_OPTION.ROT_LEFT:
            //        if( parent.m_IsFront == true)
            //            tr.localRotation = Quaternion.Euler(0, 180, 0);
            //        break;
            //    default:
            //        assert_cs.msg("default : " + (eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_ROT));
            //        break;
            //}
        }
        else
        {
            Vector3 pos = tr.localPosition + parent.transform.position;

            //if ((eo & MngCollector.eLOAD_EFFECT_OPTION.POS_CENTER) != 0)
            //{
            //    pos.y += parent.GetHeightExtend();
            //}

            //if ((eo & MngCollector.eLOAD_EFFECT_OPTION.POS_TOP) != 0)
            //{
            //    pos.y += parent.GetHeight();
            //}

            tr.position = pos;

            //switch( eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_ROT )
            //{
            //    case MngCollector.eLOAD_EFFECT_OPTION.ROT_PARENT:
            //        tr.rotation = parent.m_IsFront ? tr.localRotation : tr.localRotation * Quaternion.Euler(0, 180, 0);
            //        break;
            //    case MngCollector.eLOAD_EFFECT_OPTION.ROT_RIGHT:
            //        tr.rotation = tr.localRotation;
            //        break;
            //    case MngCollector.eLOAD_EFFECT_OPTION.ROT_LEFT:
            //        tr.rotation = tr.localRotation * Quaternion.Euler(0, 180, 0);
            //        break;
            //    default:
            //        assert_cs.msg("default : " + (eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_ROT));
            //        break;
            //}            

            tr.SetParent(SharedObject.g_CollectorMgr.m_RootEffect, true);
        }

        //if ((eo & MngCollector.eLOAD_EFFECT_OPTION.DONT_APPLY_STOP) == 0)
        //{
        //    asset.m_StopControl = parent.m_StopControl;
        //}      
  
        asset.m_ApplyStopControl = false;
        //asset.m_2DPos.transform.localPosition = Actor.To_SR_Coordinate_ToVec3(asset.m_GameObj.transform.position.z);

        return asset.m_GameObj;
    }

    bool _Check_Cnt( eEFFECT_TYPE type )
    {
        //switch (type)
        //{
        //    case eEFFECT_TYPE.HIT:
        //        if (SharedObject.g_SceneProperty.GetGraphicOpt().m_Effect_Hit <= m_CurEffect_Hit)
        //            return false;
        //        ++m_CurEffect_Hit;
        //        break;
        //    case eEFFECT_TYPE.DIE:
        //        if (SharedObject.g_SceneProperty.GetGraphicOpt().m_Effect_Die <= m_CurEffect_Die)
        //            return false;
        //        ++m_CurEffect_Die;
        //        break;
        //}
        return true;
    }

	public GameObject LoadEffect( eEFFECT_TYPE type, Vector3 pos, bool IsFront, string path, float LifeTime = 0f )
	{
        if (_Check_Cnt(type) == false)
            return null;

		cPrefabInfo prefab = _GetPrefabInfo (path, null);
        assert_cs.set(prefab != null);

        cAssetInfo asset = _GetAsset(type, prefab, null, true);

        if( LifeTime != 0f )
        {
            if( type != eEFFECT_TYPE.BULLET_NOT_USE_DURATION )
                asset.m_Duration = LifeTime;
            else
                asset.m_Duration = 9999f;
        }

        Transform tr = asset.m_GameObj.transform;
        pos += tr.localPosition;
        tr.position = pos;

        tr.rotation = IsFront ? tr.localRotation : tr.localRotation * Quaternion.Euler(0, 180, 0);

        tr.SetParent(SharedObject.g_CollectorMgr.m_RootEffect, true);

        //asset.m_2DPos.transform.localPosition = Actor.To_SR_Coordinate_ToVec3(asset.m_GameObj.transform.position.z);

        return asset.m_GameObj;
	}

    void _EffectScale( Character parent, ParticleSystem ps )
    {
        //switch( eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_SCALE )
        //{
        //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_COSTOM:
        //        ps.startSize = ps.startSize * parent.GetCustomScale();
        //        break;
        //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_PS_W:
        //        ps.startSize = ps.startSize * parent.GetWidth();
        //        break;
        //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_PS_H:
        //        ps.startSize = ps.startSize * parent.GetHeight();
        //        break;
        //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_2DPOS_W:
        //    case MngCollector.eLOAD_EFFECT_OPTION.SCALE_2DPOS_H:
        //    case MngCollector.eLOAD_EFFECT_OPTION.NONE:
        //        break;
        //    default:
        //        assert_cs.msg("default : " + (eo & MngCollector.eLOAD_EFFECT_OPTION.MASK_SCALE));
        //        break;
        //}
    }
	
    protected cAssetInfo _GetAsset( eEFFECT_TYPE type, cPrefabInfo prefab, Character parent, bool DoInstance)
    {
        cAssetInfo asset = null;
        asset = new cAssetInfo(type, prefab.m_PrefabName);
        GameObject obj = asset.m_GameObj;
        asset.m_Duration = prefab.m_SaveDuration;            
        assert_cs.set(asset.m_2DPos);
        Transform tr = asset.m_2DPos.GetChild(0);
        assert_cs.set(tr);

        asset.m_PSRoot = tr.GetComponent<ParticleSystem>();

        if (asset.m_PSRoot == null)
        {
            int nChildCnt = tr.childCount;
            for (int i = 0; i < nChildCnt; ++i )
            {
                asset.m_PSRoot = tr.GetChild(i).GetComponent<ParticleSystem>();
                if (asset.m_PSRoot != null)
                    break;
            }                
        }
        //assert_cs.set(asset.m_PSRoot != null);

        if (null != asset.m_PSRoot)
        {
            asset.m_Edit = asset.m_PSRoot.GetComponent<ParticleEdit>();
            _EffectScale(parent, asset.m_PSRoot);
        }

        ParticleSystem[] ParticleArray = tr.GetComponentsInChildren<ParticleSystem>();
        ParticleSystem ps;

        int nCnt = ParticleArray.Length;
        for (int i = 0; i < nCnt; ++i )
        {
            ps = ParticleArray[i];

            if (asset.m_PSRoot == ps)
                continue;

            _EffectScale(parent, ps);
        }

        if (DoInstance == true)
        {
            asset.m_Duration = prefab.m_SaveDuration;
            m_LiveAssetInfo.Add(asset);
        }
        return asset;
    }

    protected cPrefabInfo _GetPrefabInfo(string path, Character parent )
    {
        string pathL = path.ToLower();
        int HashCode = pathL.GetHashCode();

        if (m_PrefabList.ContainsKey(HashCode) == true)
            return m_PrefabList[HashCode];

        return _LoadEffect_FromPrefab(HashCode, pathL, parent);
    }     

    protected cPrefabInfo _LoadEffect_FromPrefab(int HashCode, string path, Character parent )
	{
        cPrefabInfo NewPrefabInfo = new cPrefabInfo();

        NewPrefabInfo.m_PrefabName = path;

		// Prefab Load
        cAssetInfo asset = _GetAsset( eEFFECT_TYPE.NORMAL, NewPrefabInfo, parent, false);

        if (null != asset.m_PSRoot)
        {
            if (asset.m_PSRoot.loop == false)
                NewPrefabInfo.m_SaveDuration = asset.m_PSRoot.duration;
            else
                NewPrefabInfo.m_SaveDuration = float.MaxValue;
        }
        else
            NewPrefabInfo.m_SaveDuration = float.MaxValue;

        NewPrefabInfo.m_UseCheckState = asset.m_Edit != null ? asset.m_Edit.m_bReqState_Use : false;

        m_PrefabList.Add(HashCode, NewPrefabInfo);
        GameObject.Destroy(asset.m_GameObj);

        return NewPrefabInfo;
	}
}
