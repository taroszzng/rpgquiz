using UnityEngine;
using System.Collections;

public class ParticleEdit : MonoBehaviour 
{
    [System.Serializable]
    public class cTexSheetAni
    {
        public int m_nColl;
        public int m_nRow;
        public int m_nCycles = 1;
    }

    public bool                 m_bReqState_Use = false;
    public ActorState     m_ReqState;
    public bool                 m_bNotDetach = false;
    public bool                 m_bFollowParent = true;

    public bool Check_ReqState(ActorState charState)
    {
        if (m_bReqState_Use == false)
            return true;

        if (m_ReqState.m_eAttack != ActorState.eATTACK.NONE)
        {
            if (m_ReqState.m_eAttack != charState.m_eAttack)
                return false;
        }

        if (m_ReqState.m_eBody != ActorState.eBODY.NONE)
        {
            if (m_ReqState.m_eBody != charState.m_eBody)
                return false;
        }

        if (m_ReqState.m_eDamage != ActorState.eDAMAGE.NONE)
        {
            if (m_ReqState.m_eDamage != charState.m_eDamage)
                return false;
        }

        if (m_ReqState.m_eJump != ActorState.eJUMP.NONE)
        {
            if (m_ReqState.m_eJump != charState.m_eJump)
                return false;
        }

        if (m_ReqState.m_eMove != ActorState.eMOVE.NONE)
        {
            if (m_ReqState.m_eMove != charState.m_eMove)
                return false;
        }

        return true;
    }
}
