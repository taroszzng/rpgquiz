﻿using UnityEngine;
using System.Collections;

public abstract class Rtti : MonoBehaviour 
{
    public enum eRTTI       //오브젝트 종류
    {
        NONE,               //무

        MONSTER,            //몹
        MYPLAYER,           //자신
        MAPOBJCT,

        BULLET,                //총알
        BULLETLINE,

        PET,                //[kks]:팻

        WORLD,
    }

    public int m_nUniqID;           //고유id
    public int m_nModelID;          //모델id

    [System.NonSerialized]
    public eRTTI m_eRTTI = eRTTI.NONE;

    [System.NonSerialized]
    public nsENUM.eTEAM m_eTeam;

    protected abstract void InitRTTI();

#if UNITY_EDITOR
    protected bool m_bPrefabData = false;
#endif

    public virtual void Initialize(string _strPrefab)
    {
#if UNITY_EDITOR
        m_bPrefabData = true;
#endif

        InitRTTI();
    }

    public virtual void Init(nsENUM.eTEAM _eTeam, int _nUniqID, int _nModelID)
    {
        m_eTeam = _eTeam;

        m_nUniqID = _nUniqID;
        m_nModelID = _nModelID;
    }

#if UNITY_EDITOR
    void Start()
    {
        if (!m_bPrefabData)
        {
            gameObject.SetActive(false);
            return;
        }
    }
#endif

    protected virtual void OnEnable() {}
}
