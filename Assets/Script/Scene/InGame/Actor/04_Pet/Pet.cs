﻿using UnityEngine;
using System.Collections;

public partial class Pet : AniCharacter
{
    protected override void InitRTTI() { m_eRTTI = eRTTI.PET; }

    protected override bool FindAniType()
    {
        return true;
    }
}