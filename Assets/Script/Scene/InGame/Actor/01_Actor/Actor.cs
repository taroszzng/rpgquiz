using UnityEngine;
using System.Collections;

//	화면상 케릭터들의 기본 클래스
public abstract class Actor : Rtti 
{
    [System.Serializable]
    public enum eLIVE_STATE
    {
        LIVE,
        DIE,
        DIE_READY,      //준비
        DIE_CORPSE,     //시체
    }

    [HideInInspector]
    protected eLIVE_STATE m_eLiveState;

    //	좌표계 변환
    protected Transform m_ColliderPos;

    //protected abstract Transform Find2DPos();
    //public Transform Get2DPos() { return m_2DPos; }

    protected abstract Transform FindColliderPos();

    public bool IsDie()
    {
        return m_eLiveState != eLIVE_STATE.LIVE;
    }

    public override void Initialize(string _strPrefab)
    {
        base.Initialize(_strPrefab);

        //m_2DPos = Find2DPos();
        m_ColliderPos = FindColliderPos();

        //if (m_CalAttack != null)
        //    m_CalAttack.Init((SpineActor)this);
    }

    public virtual void Init(nsENUM.eTEAM _eTeam, int _nUniqID, int _nModelID, Vector3 _vPos)
    {
        base.Init(_eTeam, _nUniqID, _nModelID);

        SetLiveState(eLIVE_STATE.LIVE);

        transform.position = _vPos;

        //if (m_CalAttack != null)
        //    m_CalAttack.Reset();
    }

    public void SetLiveState(eLIVE_STATE _eState)
    {
        m_eLiveState = _eState;

        if (m_eLiveState == eLIVE_STATE.DIE_READY)
        {
            //m_fLiveStateTime = SharedObject.g_CollectorMng.GetPlayTime();
        }
    }
}
