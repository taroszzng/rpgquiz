﻿using UnityEngine;
using System.Collections;

public abstract partial class Character : Actor
{
    //	상태 
    public ActorState m_State = new ActorState();

    protected void ResetState()
    {
        m_State.Reset(this);
        m_State.m_eBody = ActorState.eBODY.GROUND;
    }

    protected virtual void StateMove(bool _bRun)
    {
        if (_bRun)
        {
            if (m_State.m_eMove == ActorState.eMOVE.STOP)
            {
                SetState_Move(ActorState.eMOVE.MOVE);
                SetAnimation_Default();
            }
        }
        else
        {
            if (m_State.m_eMove == ActorState.eMOVE.MOVE)
            {
                if (m_State.m_eBody == ActorState.eBODY.GROUND)
                    m_Rigidbody.velocity = Vector3.zero;

                SetState_Move(ActorState.eMOVE.STOP);
                SetAnimation_Default();

                //NetSend_Stop();
            }
        }
    }

    public virtual void SetState_Body(ActorState.eBODY _eVal)
    {
        m_State.m_eBody = _eVal;

        switch (_eVal)
        {
            case ActorState.eBODY.GROUND:
            case ActorState.eBODY.AIR:
                break;
            default:
                assert_cs.msg("default : " + _eVal);
                break;
        }
    }

    public void SetState_Attack(ActorState.eATTACK _eVal)
    {
        m_State.m_eAttack = _eVal;

        switch (_eVal)
        {
            case ActorState.eATTACK.NONE:
                    break;
            case ActorState.eATTACK.ING:
            case ActorState.eATTACK.READY:
            case ActorState.eATTACK.CHANGE_READY:
                break;
            default:
                assert_cs.msg("default : " + _eVal);
                break;
        }
    }

    public virtual void SetState_Damage(ActorState.eDAMAGE _eVal, ActorState.eDAMAGE_CAUSE _eCause)
    {
        SetState_Move(ActorState.eMOVE.STOP);

        m_State.m_eDamage = _eVal;

        switch (_eVal)
        {
            case ActorState.eDAMAGE.NONE:
            case ActorState.eDAMAGE.DAMAGE:
            case ActorState.eDAMAGE.DOWN_ING:
            case ActorState.eDAMAGE.DOWN_END:
            case ActorState.eDAMAGE.KNOCK:
                break;
            default:
                assert_cs.msg("default : " + _eVal);
                break;
        }
    }

    public void SetState_Move(ActorState.eMOVE _eVal)
    {
        m_State.m_eMove = _eVal;

        switch (_eVal)
        {
            case ActorState.eMOVE.STOP:
            case ActorState.eMOVE.MOVE:
                break;
            default:
                assert_cs.msg("default : " + _eVal);
                break;
        }
    }

    public void SetState_Jump(ActorState.eJUMP _eVal)
    {
        m_State.m_eJump = _eVal;

        switch (_eVal)
        {
            case ActorState.eJUMP.NONE:
            case ActorState.eJUMP.JUMP_1:
            case ActorState.eJUMP.JUMP_2:
                break;

            default:
                assert_cs.msg("default : " + _eVal);
                break;
        }
    }

    public void AddCondition(ActorState.eCONDITION _eCon, float val = 0f)
    {
        m_State.m_eCondition |= _eCon;

        switch (_eCon)
        {
            case ActorState.eCONDITION.DONT_ATK:
                {
                    //ResetSkill();
                }
                break;
            case ActorState.eCONDITION.STUN:
                {
                    //ResetSkill();
                }
                break;
            case ActorState.eCONDITION.FREEZE:
                {
                    //SpineActor sa = (SpineActor)this;
                    //sa.StopMotion(val, false, Vector3.zero);
                }
                break;
            case ActorState.eCONDITION.CONFUSE:
                break;
            default:
                assert_cs.msg("default : " + _eCon);
                break;
        }
    }

    public void ReleaseCondition(ActorState.eCONDITION _eCon)
    {
        m_State.m_eCondition &= ~_eCon;
    }

    public virtual void ReSetJump()//[kks][Bug]:워닝시 애니중인 것 리셋
    {
        m_State.m_eJump = ActorState.eJUMP.NONE;
    }

    public void SetDir(ActorState.eDIR _eDir)
    {
        if (m_State.m_eDir == _eDir)
            return;

        m_State.m_eDir = _eDir;

        SetRotation(_eDir);
    }

    public bool SetRotation(ActorState.eDIR _eDir)
    {
        m_State.m_eMove = ActorState.eMOVE.MOVE;

        switch (_eDir)
        {
            case ActorState.eDIR.eDIR_FRONT:
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                break;
            case ActorState.eDIR.eDIR_LEFT:
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                break;
            case ActorState.eDIR.eDIR_RIGHT:
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                break;
            case ActorState.eDIR.eDIR_BACK:
                break;
        }

        return true;
    }
}
