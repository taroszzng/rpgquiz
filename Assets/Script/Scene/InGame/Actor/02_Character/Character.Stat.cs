﻿using UnityEngine;
using System.Collections;

public abstract partial class Character : Actor
{
    [System.Serializable]
    public class cSTAT
    {
        //public float m_fMoveSpeedRate = 1.0f;

        public float m_fMoveSpeedX = 2f;  //이속x

        public float m_fMaxHp;
        public float m_fCurHp;
        public float m_fAtk;
        public float m_fLuck;

        public float m_fMaxHpPlus;
        public float m_fCurHpPlus;
        public float m_fAtkPlus;
        public float m_fLuckPlus;

        public float m_fEventBuff;
    }

    public cSTAT m_Stat = new cSTAT();

    public virtual void InitStat()
    {
        m_Stat.m_fMaxHpPlus = 0f;
        m_Stat.m_fCurHpPlus = 0f;
        m_Stat.m_fAtkPlus = 0f;
        m_Stat.m_fLuckPlus = 0f;

        m_Stat.m_fEventBuff = 0f;
    }

    protected virtual void InitItem() {}

    protected virtual float SetMode() { return 0f; }

    public float GetAtk()
    {
        return m_Stat.m_fAtk;
    }

    public float GetHpFill()
    {
        return m_Stat.m_fCurHp / m_Stat.m_fMaxHp;
    }

    public float GetHp()
    {
        return m_Stat.m_fCurHp;
    }

    public float GetMaxHp()
    {
        return m_Stat.m_fMaxHp;
    }

    public string GetStrHp()
    {
        int nmaxhp = (int)m_Stat.m_fMaxHp;
        int nhp = (int)m_Stat.m_fCurHp;

        return nhp.ToString() + " / " + nmaxhp.ToString();
    }

    public void ItemStatUse(int _nItemID)//아이템 적용
    {
        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if (null == info)
            return;

        switch ((nsENUM.eITEMAPPLY)info.m_nApply)
        {
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ALL:
                {
                    m_Stat.m_fMaxHpPlus += m_Stat.m_fMaxHp * info.m_fAbility;
                    m_Stat.m_fCurHpPlus += m_Stat.m_fCurHp * info.m_fAbility;

                    m_Stat.m_fAtkPlus += m_Stat.m_fAtk * info.m_fAbility;
                    m_Stat.m_fLuckPlus += m_Stat.m_fLuck * info.m_fAbility;

                    m_Stat.m_fMaxHp += m_Stat.m_fMaxHpPlus;
                    m_Stat.m_fCurHp += m_Stat.m_fCurHpPlus;
                    m_Stat.m_fAtk += m_Stat.m_fAtkPlus;
                    m_Stat.m_fLuck += m_Stat.m_fLuckPlus;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_HP:
                {
                    m_Stat.m_fMaxHpPlus += m_Stat.m_fMaxHp * info.m_fAbility;
                    m_Stat.m_fCurHpPlus += m_Stat.m_fCurHp * info.m_fAbility;

                    m_Stat.m_fMaxHp += m_Stat.m_fMaxHpPlus;
                    m_Stat.m_fCurHp += m_Stat.m_fCurHpPlus;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ATK:
                {
                    m_Stat.m_fAtkPlus += m_Stat.m_fAtk * info.m_fAbility;

                    m_Stat.m_fAtk += m_Stat.m_fAtkPlus;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK:
                {
                    m_Stat.m_fLuckPlus += m_Stat.m_fLuck * info.m_fAbility;

                    m_Stat.m_fLuck += m_Stat.m_fLuckPlus;
                }
                break;
        }
    }

    public void UnquipItem(int _nItemID)//탈착
    {
        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if (null == info)
            return;

        Table_Level.Info level = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

        if (null == level)
            return;

        switch ((nsENUM.eITEMAPPLY)info.m_nApply)
        {
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ALL:
                {
                    m_Stat.m_fMaxHp -= level.m_fHP * info.m_fAbility;
                    m_Stat.m_fCurHp -= level.m_fHP * info.m_fAbility;
                    m_Stat.m_fAtk -= level.m_fAtk * info.m_fAbility;
                    m_Stat.m_fLuck -= level.m_nLuck * info.m_fAbility;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_HP:
                {
                    m_Stat.m_fMaxHp -= level.m_fHP * info.m_fAbility;
                    m_Stat.m_fCurHp -= level.m_fHP * info.m_fAbility;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ATK:
                {
                    m_Stat.m_fAtk -= level.m_fAtk * info.m_fAbility;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK:
                {
                    m_Stat.m_fLuck -= level.m_nLuck * info.m_fAbility;
                }
                break;
        }

        Debug.Log("UnEquip fAbility " + info.m_fAbility + " MaxHp = " + m_Stat.m_fMaxHp + 
            " CurHp = " + m_Stat.m_fCurHp + " Atk = " + m_Stat.m_fAtk + " Luck = " + m_Stat.m_fLuck);
    }

    public void EquipItem(int _nItemID)//장착
    {
        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if (null == info)
            return;

        Table_Level.Info level = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

        if (null == level)
            return;

        switch ((nsENUM.eITEMAPPLY)info.m_nApply)
        {
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ALL:
                {
                    m_Stat.m_fMaxHp += level.m_fHP * info.m_fAbility;
                    m_Stat.m_fCurHp += level.m_fHP * info.m_fAbility;
                    m_Stat.m_fAtk += level.m_fAtk * info.m_fAbility;
                    m_Stat.m_fLuck += level.m_nLuck * info.m_fAbility;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_HP:
                {
                    m_Stat.m_fMaxHp += level.m_fHP * info.m_fAbility;
                    m_Stat.m_fCurHp += level.m_fHP * info.m_fAbility;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_ATK:
                {
                    m_Stat.m_fAtk += level.m_fAtk * info.m_fAbility;
                }
                break;
            case nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK:
                {
                    m_Stat.m_fLuck += level.m_nLuck * info.m_fAbility;
                }
                break;
        }

        Debug.Log("Equip fAbility " + info.m_fAbility + " MaxHp = " + m_Stat.m_fMaxHp +
            " CurHp = " + m_Stat.m_fCurHp + " Atk = " + m_Stat.m_fAtk + " Luck = " + m_Stat.m_fLuck);
    }

    public void ResetStatItem()
    {
        m_Stat.m_fMaxHp -= m_Stat.m_fMaxHpPlus;
        m_Stat.m_fCurHp -= m_Stat.m_fCurHpPlus;
        m_Stat.m_fAtk -= m_Stat.m_fAtkPlus;
        m_Stat.m_fLuck -= m_Stat.m_fLuckPlus;

        m_Stat.m_fMaxHpPlus = 0f;
        m_Stat.m_fCurHpPlus = 0f;
        m_Stat.m_fAtkPlus = 0f;
        m_Stat.m_fLuckPlus = 0f;

        m_Stat.m_fEventBuff = 0f;
    }

    public virtual void EventBuff(nsENUM.eEVENTBUFF _e, float _f)//자신의 것만
    {
        switch(_e)
        {
            case nsENUM.eEVENTBUFF.eEVENTBUFF_ATK_PLUS:
                {
                    m_Stat.m_fEventBuff += m_Stat.m_fAtk * _f;
                }
                break;
            case nsENUM.eEVENTBUFF.eEVENTBUFF_ATK_COMBO:
                {
                    m_Stat.m_fEventBuff += m_Stat.m_fAtk * _f;
                }
                break;
        }
    }
}
