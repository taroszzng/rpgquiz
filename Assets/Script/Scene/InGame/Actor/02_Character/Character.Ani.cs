﻿using UnityEngine;
using System.Collections;

public abstract partial class Character : Actor
{
    public virtual bool SetAnimation(eANIMODE _eMode, eANI _eAni, bool _bReset = false, bool _bIgnoreLock = false)
    {
        if ((false == _bIgnoreLock) && (false == m_State.IsEnableAni()))
            return false;

        return true;
    }

    public virtual void SetAnimation_Default() {}

    public virtual void AddAnimation_Default() {}

    public virtual void ReSetAni()//[kks][Bug]:워닝시 애니중인 것 리셋
    {
        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE, true);

        m_State.Reset(this);
    }
}
