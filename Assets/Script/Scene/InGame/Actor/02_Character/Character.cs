﻿using UnityEngine;
using System.Collections;

public abstract partial class Character : Actor
{
    [HideInInspector]
    public Rigidbody m_Rigidbody;
    protected RigidbodyConstraints m_OrgRigidBodyConstraints;

    public BoxCollider m_HitCollider;

    public bool m_bIgnoreForce = false;

    protected abstract BoxCollider FindHitCollider();

    public Prop_Effect m_PropEffect = null;

    public Transform GetEffectTr()
    {
        return transform;
    }

    public override void Initialize(string _strPrefab)
    {
        base.Initialize(_strPrefab);

        m_Rigidbody = gameObject.GetComponent<Rigidbody>();

        if (null != m_Rigidbody)
            m_OrgRigidBodyConstraints = m_Rigidbody.constraints;

        m_HitCollider = FindHitCollider();

        m_PropEffect = Prop_EffectMgr.Pop(this);

        if (null != m_ColliderPos)
        {
            Vector3 scale = m_ColliderPos.localScale;
            //scale.z *= SharedObject.g_CurZone.m_fZoneMoveZRate;
            m_ColliderPos.localScale = scale;
        }

        //if (_IsShadow())
        //{
        //    GameObject gameobj = AssetBundleManager.Load("Prefabs/_Scene_Ingame/Shadow", "prefab", eAB_Kind.Etc, true) as GameObject;
        //    assert_cs.set(gameobj, "mGameObject == null : " + "Prefabs/_Scene_Ingame/Shadow");

        //    // New Position, Rotation
        //    gameobj = GameObject.Instantiate(gameobj) as GameObject;
        //    gameobj.SetActive(true);
        //    m_ShadowTr = gameobj.transform;
        //    m_ShadowTr.SetParent(transform, true);
        //    m_ShadowTr.FindChild("Image").localScale = new Vector3(m_Size.x, m_Size.z, 1);
        //}

        //_Initialize_Buff();
    }

    public override void Init(nsENUM.eTEAM _eTeam, int _nUniqID, int _nModelID, Vector3 _vPos)
    {
        base.Init(_eTeam, _nUniqID, _nModelID, _vPos);

        //ResetState();

        //AE_ResetForce();
        //ReleaseCollider();
        //InitSkill();
        //ResetBuff();
        InitStat();

        m_bIgnoreForce = m_Rigidbody == null;

        SetState_Move(ActorState.eMOVE.STOP);
        SetState_Body(ActorState.eBODY.GROUND);
    }

    protected virtual void FixedUpdate()
    {
        UpdateAutoMove();
    }

    protected virtual void Update()
    {
        UpdateKey();
    }

    protected virtual void UpdateKey(){}

    public void SetScale(float _fScale)
    {
        Vector3 vScale = transform.localScale;

        vScale.x = _fScale;
        vScale.y = _fScale;
        vScale.z = _fScale;

        transform.localScale = vScale;
    }

    public void SetPos(float _fPosX, float _fPosY, float _fPosZ)
    {
        Vector3 vec3 = transform.position;

        vec3.x += _fPosX;
        vec3.y += _fPosY;
        vec3.z += _fPosZ;

        transform.position = vec3;
    }

    public virtual void Fight() {}

    public virtual void Damage()
    {
        if (0 >= m_Stat.m_fCurHp)
            SetLiveState(eLIVE_STATE.DIE);
    }

    protected float Damage(float _fAtk, float _fEventBuff)
    {
        float fdamage = _fAtk;

        float fmode = SetMode();//모드 추가 공격

        if (nsENUM.eTEAM.RED_TEAM == m_eTeam)
        {
            SharedObject.g_AModeSet.EFHit(SharedObject.g_SceneMgr.Cri, fdamage + fmode, _fEventBuff);//데미지 표시

            if (nsENUM.eTEAM.BLUE_TEAM == SharedObject.g_SceneMgr.Win)//플레이어 이길때만 적용
                SharedObject.g_AMode.SetEffectCombo();//콤보 이미지
        }

        fdamage += (_fEventBuff + fmode);

        if (0 > fdamage)//빼야 하니 -가 나오면 0으로 
            fdamage = 0;

        return fdamage; 
    }

    public virtual void Die()
    {
        Debug.Log(" ===== Die ===== ");
    }
}