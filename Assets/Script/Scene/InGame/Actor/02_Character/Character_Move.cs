﻿using UnityEngine;
using System.Collections;

public abstract partial class Character : Actor
{
    protected class cAutoMoveInfo//	이동
    {
        public enum eSTATE
        {
            NONE,
            USE,
        }

        public eSTATE m_eState;

        public Vector3 m_vDir;
        public float m_fRemainDist;
        public bool m_bUseAni;
        public float m_fDistPerSec;
    }

    protected cAutoMoveInfo m_AutoMoveInfo = new cAutoMoveInfo();

    public bool IsEndAutoMove() { return m_AutoMoveInfo.m_eState == cAutoMoveInfo.eSTATE.NONE; }

    public virtual void AutoMove(Vector3 _vPos, bool _bUseAnimation)
    {
        m_AutoMoveInfo.m_vDir = _vPos - transform.position;
        m_AutoMoveInfo.m_fRemainDist = m_AutoMoveInfo.m_vDir.magnitude;

        if (m_AutoMoveInfo.m_fRemainDist == 0)
        {
            m_AutoMoveInfo.m_eState = cAutoMoveInfo.eSTATE.USE;

            EndAutoMove();

            return;
        }

        m_AutoMoveInfo.m_vDir.Normalize();

        float dx = Mathf.Abs(m_AutoMoveInfo.m_vDir.x);
        float dz = Mathf.Abs(m_AutoMoveInfo.m_vDir.z);

        if (dx != 0)
        {
            float Angle = Mathf.Atan(dz / dx);
            assert_cs.set(Angle >= 0f, "Angle = " + Angle * 180f / Mathf.PI);
            assert_cs.set(Angle <= Mathf.PI * 0.5f, "Angle = " + Angle * 180f / Mathf.PI);

            m_AutoMoveInfo.m_fDistPerSec = m_Stat.m_fMoveSpeedX / Mathf.Cos(Angle);
        }

        m_AutoMoveInfo.m_bUseAni = _bUseAnimation;
        m_AutoMoveInfo.m_eState = cAutoMoveInfo.eSTATE.USE;

        if (ActorState.eATTACK.CHANGE_READY == m_State.m_eAttack)
            SetState_Attack(ActorState.eATTACK.NONE);
    }

    protected bool UpdateAutoMove()
    {
        if (false == m_State.IsEnableAutoMove())
            return true;

        //float fdist = m_AutoMoveInfo.m_fDistPerSec * Time.fixedDeltaTime * m_Stat.m_fMoveSpeedX;

        //if (m_AutoMoveInfo.m_fRemainDist > fdist)
        //{
        //    m_AutoMoveInfo.m_fRemainDist -= fdist;

        //    SetDeltaPos(m_AutoMoveInfo.m_vDir * fdist);

        //    if (m_AutoMoveInfo.m_bUseAni)
        //        StateMove(true);

        //    return true;
        //}

        //Vector3 vDeltaRemain = m_AutoMoveInfo.m_vDir * m_AutoMoveInfo.m_fRemainDist;

        //SetDeltaPos(vDeltaRemain);

        //EndAutoMove();

        return false;
    }

    public void SetDeltaPos(Vector3 _vPos)
    {
        _vPos.z *= 1.0f;

        transform.position += _vPos;
    }

    public virtual void EndAutoMove()
    {
        m_AutoMoveInfo.m_eState = cAutoMoveInfo.eSTATE.NONE;

        StateMove(false);

        EndAutoMoveState();
    }

    protected virtual void EndAutoMoveState()//이동이 완료됐을때 상태
    {
        switch (m_State.m_eDir)
        {
            case ActorState.eDIR.eDIR_FRONT:
                SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);
                break;
        }
    }
}