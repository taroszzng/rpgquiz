﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract partial class AniCharacter : Character
{
    public Animator m_Animator = null;

    protected abstract bool FindAniType();

    public override void Initialize(string _strPrefab)
    {
        base.Initialize(_strPrefab);

        bool IsActive = gameObject.activeSelf;

        if (IsActive == false)
            gameObject.SetActive(true);

        if (IsActive != gameObject.activeSelf)
            gameObject.SetActive(IsActive);

        m_Animator = transform.GetComponent<Animator>();
    }

    public override void Init(nsENUM.eTEAM _eTeam, int _nUniqID, int _nModelID, Vector3 _vPos)
    {
        base.Init(_eTeam, _nUniqID, _nModelID, _vPos);

        if (!FindAniType())
            return;

        m_eApplyAni = eANI.eANI_NONE;
        m_eLastAni = eANI.eANI_NONE;
        m_eNextAni = eANI.eANI_NONE;

        m_eCurAniMode = eANIMODE.eANIMODE_ETC;

        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);
    }

    protected override BoxCollider FindHitCollider()
    {
        //string strBox = strBasePath() + m_strSpineName + "/HitBox/Hit";

        //Transform tr = transform.FindChild(strBox);

        //if (tr != null)
        //    return tr.GetComponent<BoxCollider>();

        return null;
    }

    protected override Transform FindColliderPos()
    {
        return transform.Find("Collider");
    }

    protected override void Update()
    {
        base.Update();

        //ApplyAni();
    }
}
