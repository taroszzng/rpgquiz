﻿using UnityEngine;
using System.Collections;

public abstract partial class AniCharacter : Character
{
    eANIMODE m_eCurAniMode;

    eANI m_eLastAni = eANI.eANI_NONE;

    eANI m_eNextAni;

    eANI m_eAni_Run;
    eANI m_eAni_Idle;

    protected eANI m_eApplyAni;

    public virtual float AtkTime() { return 1f; }

    protected virtual float GetAniScale_Move() { return 1.0f; }
    protected virtual float GetAniScale_Skill() { return 1.0f; }

    public override bool SetAnimation(eANIMODE _eMode, eANI _eAni, bool _bReset = false, bool _bIgnoreLock = false)
    {
        if (base.SetAnimation(_eMode, _eAni, _bReset, _bIgnoreLock) == false)
            return false;

        eANI apply = _eAni;

        if ((false == _bReset) && (m_eApplyAni == _eAni))
            return false;

        if (false == gameObject.activeSelf)
        {
            return true;
        }

        m_eApplyAni = apply;

        m_eNextAni = eANI.eANI_NONE;
        m_eCurAniMode = _eMode;

        SetAnimationComplete(m_eApplyAni);

        return true;
    }

    protected void SetAniScaleTime()
    {
        float scaleTime = 1f;

        switch (m_eCurAniMode)
        {
            case eANIMODE.eANIMODE_MOVE:
                scaleTime = GetAniScale_Move();
                break;
            case eANIMODE.eANIMODE_SKILL:
                scaleTime = GetAniScale_Skill();
                break;
            case eANIMODE.eANIMODE_ETC:
                scaleTime = 1f;
                break;
            default:
                assert_cs.msg("default : " + m_eCurAniMode);
                break;
        }

        //m_Animator.speed = scaleTime;
    }

    public override void SetAnimation_Default()
    {
        if (false == m_State.IsEnableDefaultAni())
            return;

        switch (m_State.m_eBody)
        {
            case ActorState.eBODY.GROUND:
                {
                    switch (m_State.m_eMove)
                    {
                        case ActorState.eMOVE.STOP:
                            SetAnimation(eANIMODE.eANIMODE_ETC, m_eAni_Idle);
                            break;
                        case ActorState.eMOVE.MOVE:
                            SetAnimation(eANIMODE.eANIMODE_MOVE, m_eAni_Run);
                            break;
                        default:
                            assert_cs.msg("default : " + m_State.m_eMove);
                            break;
                    }
                }
                break;
            case ActorState.eBODY.AIR:
                {
                    //if (m_Rigidbody.velocity.y >= 0)
                    //    SetAnimation(eANI_TYPE.ETC, eANI.Jump_Start_Ing);
                    //else
                    //    SetAnimation(eANI_TYPE.ETC, eANI.Jump_Fall_Ing);
                }
                break;
            default:
                assert_cs.msg("default : " + m_State.m_eBody);
                break;
        }
    }

    public override void AddAnimation_Default()
    {
        if (false == m_State.IsEnableDefaultAni())
            return;

        switch (m_State.m_eBody)
        {
            case ActorState.eBODY.GROUND:
                {
                    switch (m_State.m_eMove)
                    {
                        case ActorState.eMOVE.STOP:
                            AddAnimation(m_eAni_Idle);
                            break;
                        case ActorState.eMOVE.MOVE:
                            AddAnimation(m_eAni_Run);
                            break;
                        default:
                            assert_cs.msg("default : " + m_State.m_eMove);
                            break;
                    }
                }
                break;
            case ActorState.eBODY.AIR:
                {
                    //if(m_Rigidbody.velocity.y >= 0)
                    //    AddAnimation(eANI.Jump_Start_Ing);
                    //else
                    //    AddAnimation(eANI.Jump_Fall_Ing);
                }
                break;
            default:
                assert_cs.msg("default : " + m_State.m_eBody);
                break;
        }
    }

    public void AddAnimation(eANI _eAni)
    {
        m_eNextAni = _eAni;
    }

    public float GetAni_Len(eANI _eAni)
    {
        //Prop_AniInfo.cClipInfo info = m_PropAniInfo.GetClip(ani);
        //if (info.m_Loop == true)
        //    return 0f;

        return 1;//info.m_Duration;
    }

    public float GetEndAni_Time(eANI _eAni)
    {
        //Prop_AniInfo.cClipInfo info = m_PropAniInfo.GetClip(ani);
        //if (info.m_EndEvent == 0f)
        //{
        //    return info.m_Duration;
        //}

        return 1;//info.m_EndEvent;
    }

    protected virtual bool SetAnimationComplete(eANI _eAni)//애니메이션 실행
    {
        if (eANI.eANI_NONE == _eAni)
            return false;

        return true;
    }

    protected virtual void AnimationPlay(eANI _eAni, AniValue _ani) {}
}
