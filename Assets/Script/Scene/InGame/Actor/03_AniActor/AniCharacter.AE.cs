﻿using UnityEngine;
using System.Collections;

public abstract partial class AniCharacter : Character
{
    public void AE_Animation(eANI _eAni)
    {
        m_eApplyAni = eANI.eANI_NONE;
        m_eLastAni = eANI.eANI_NONE;

        if (m_eNextAni == eANI.eANI_NONE)
            SetAnimation_Default();
        else
            SetAnimation(eANIMODE.eANIMODE_ETC, m_eNextAni, false, true);
    }
}
