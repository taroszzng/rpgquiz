﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Prop_EffectMgr
{
    static List<Prop_Effect> m_Pool = null;
    static Transform m_Root = null;

    static public void Init()
    {
        assert_cs.set(m_Pool == null);
        m_Pool = new List<Prop_Effect>();

        GameObject Root = new GameObject("PropEffectMgr");
        m_Root = Root.transform;
    }

    static public void Delete()
    {
        assert_cs.set(m_Pool != null);
        m_Pool.Clear();
        m_Pool = null;
        m_Root = null;
    }

    static public Prop_Effect Pop( Character p )
    {
        int cnt = m_Pool.Count;
        for( int i=0; i<cnt; ++i )
        {
            if (m_Pool[i].transform.childCount > 0)
                continue;

            Prop_Effect res = m_Pool[i];
            m_Pool.RemoveAt(i);
            res.Init(p);
            res.transform.SetParent(p.transform, false);
            return res;
        }

        GameObject obj = new GameObject("Prop_Effect");
        Prop_Effect newRes = obj.AddComponent<Prop_Effect>();
        newRes.Init(p);
        newRes.transform.SetParent(p.transform, false);
        return newRes;
    }

    static public void Push( Prop_Effect prop )
    {
#if UNITY_EDITOR
        int cnt = prop.m_EffectList.Count;
        for (int i = 0; i < cnt; ++i )
        {
            assert_cs.set(prop.m_EffectList[i].m_Object.activeSelf == false, "PropEffect 는 제거 될때 차일드 이펙트는 다 비활성 상태여야 한다");
        }
#endif
        prop.transform.SetParent(m_Root, false);
        m_Pool.Add(prop);
    }
}
