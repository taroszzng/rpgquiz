﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ActorState          //상태클래스
{
    [System.Serializable]
    public enum eSTATE_TYPE
    {
        BODY,
        ATTACK,
        DAMAGE,
        MOVE,
        JUMP,
    }

    [System.Serializable]
    public enum eCHANGE//변신
    {
        CHANGE_0 = 0,
        CHANGE_1 = 1,
        CHANGE_2 = 2,
        CHANGE_3 = 3,
        CHANGE_4 = 4,
        MAX
    }

    [System.Serializable]
    public enum eBODY //위치
    {
        NONE = 0,
        GROUND, //땅
        AIR,    //공중
    }

    [System.Serializable]
    public enum eATTACK
    {
        NONE = 100,
        ING,                // 스킬 사용 중
        READY,              // EnableInput 받고 ChangeAni까지 대기
        CHANGE_READY,       // EnableChange 받고 키 입력까지 대기        
    }

    [System.Serializable]
    public enum eDAMAGE
    {
        NONE = 200,
        DAMAGE,
        Empty,              // 현재 안쓰임 : 지우면 값 밀려서 안됨
        DOWN_ING,
        DOWN_END,
        KNOCK,
    }

    [System.Serializable]
    public enum eDAMAGE_CAUSE
    {
        TIME = 220,
        ATTACKED,
        DO_SKILL,
        FSM,
    }

    [System.Serializable]
    public enum eMOVE
    {
        NONE = 300,
        STOP,
        MOVE,
    }

    [System.Serializable]
    public enum eJUMP
    {
        NONE = 400,
        JUMP_1,
        JUMP_2,
    }

    [System.Serializable]
    public enum ePLAY
    {
        START = 500,
        PLAY,
        NEXT_ZONE,
    }

    [System.Serializable]
    public enum eCONDITION
    {
        NONE = 0,
        STUN = 0x00000001,   //스턴
        DONT_ATK = 0x00000002,   //공격불가
        FREEZE = 0x00000004,   //일시정지

        CONFUSE = 0x00000010,   //혼란
    }

    public enum eOE_BIT
    {
        OE_Normal = 1 << 0,
        OE_Down = 1 << 1,
        OE_KnockBack = 1 << 2,
        OE_Upper = 1 << 3,

        OE_ALL = OE_Normal | OE_Down | OE_KnockBack | OE_Upper,
    }

    public enum eLOCK
    {
        ANI = 1 << 1,
        MOVE = 1 << 2,
        AUTOMOVE = 1 << 3,
        ROTATION = 1 << 4,
        ATTACK = 1 << 5,
        DASH = 1 << 6,
    }

    [System.Serializable]
    public enum eDIR//방향
    {
        eDIR_FRONT,
        eDIR_LEFT,
        eDIR_RIGHT,
        eDIR_BACK,
        eDIR_END
    }

    public eBODY m_eBody = eBODY.NONE;
    public eATTACK m_eAttack = eATTACK.NONE;
    public eDAMAGE m_eDamage = eDAMAGE.NONE;
    public eMOVE m_eMove = eMOVE.NONE;
    public eJUMP m_eJump = eJUMP.NONE;
    public ePLAY m_ePlay = ePLAY.START;
    public eCONDITION m_eCondition = eCONDITION.NONE;
    public eCHANGE m_eChange = eCHANGE.CHANGE_0;
    public eDIR m_eDir = eDIR.eDIR_FRONT;//앞

    public bool m_bChangeAnimation = false; //[kks][Actor]:모션 체인지 관련
    public bool m_bAppear = true; // 등장중인지 - 등장중에는 맞지 않는다.

    public int m_nOE_Bit = (int)eOE_BIT.OE_ALL;

    Character m_Parent;
    private int m_LockLevel;
    private uint m_Lock = 0;

    public void Reset(Character _parent)
    {
        m_Parent = _parent;
        m_eBody = ActorState.eBODY.GROUND;
        m_eAttack = eATTACK.NONE;
        m_eDamage = eDAMAGE.NONE;
        m_eMove = eMOVE.STOP;
        m_eJump = eJUMP.NONE;
        m_eCondition = eCONDITION.NONE;
        m_eDir = eDIR.eDIR_FRONT;
        //m_eChange = eCHANGE.CHANGE_0;

        if (m_Parent.m_eRTTI == Rtti.eRTTI.MONSTER)
            m_bAppear = true;
        else
            m_bAppear = false;

        _ResetLock();
    }

    protected void _ResetLock()
    {
        m_Lock = 0;
        m_LockLevel = 0;
    }

    public void LOCK_SET(eLOCK _eType)
    {
        m_Lock |= (uint)_eType;
    }

    public void LOCK_SETALL(int _nLevel)
    {
        m_Lock = 0xffffffff;
        m_LockLevel = _nLevel;
    }

    public void LOCK_RELEASE(eLOCK _eType)
    {
        m_Lock &= ~(uint)_eType;
    }

    public bool LOCK_RELEASEALL(int _nLevel)
    {
        if (m_LockLevel > _nLevel)
            return false;

        _ResetLock();

        return true;
    }

    public bool LOCK_IS(eLOCK _eType)
    {
        return (m_Lock & (uint)_eType) != 0;
    }

    public bool IsEnableRotation()
    {
        //if (PlayEvent.Inst().IsPlay())
        //    return false;

        if (m_eDamage != eDAMAGE.NONE)
            return false;

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }

        //if (false == m_Parent.IsEnableSkillMove())
        //    return false;

        if (LOCK_IS(eLOCK.ROTATION))
            return false;

        return true;
    }

    public bool IsMoveBtnDown()//[kks][Bug]:이상상태시 이동 못하게
    {
        if (m_eDamage != eDAMAGE.NONE)
            return false;

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }
        return true;
    }

    public bool IsEnableAutoMove()
    {
        if (m_eDamage != eDAMAGE.NONE)
            return false;

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }

        switch (m_eAttack)
        {
            case eATTACK.ING:
            case eATTACK.READY:
                {
                    //if (false == m_Parent.IsEnableSkillMove())
                    //    return false;
                }
                break;
            case eATTACK.CHANGE_READY:
            case eATTACK.NONE:
                break;
            default:
                assert_cs.msg("default :" + m_eAttack);
                break;
        }

        if (LOCK_IS(eLOCK.AUTOMOVE))
            return false;

        if (eMOVE.STOP == m_eMove)
            return false;

        return true;
    }

    public bool IsEnableMove()
    {
        //if (PlayEvent.Inst().IsPlay())
        //    return false;

        if (m_eDamage != eDAMAGE.NONE)
            return false;

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }

        //if (((SpriteActor)m_Parent).IsInvoke(SpriteActor.eINVOKE.STOP_MOTION) == true)
        //    return false;

        switch (m_eAttack)
        {
            case eATTACK.ING:
            case eATTACK.READY:
                {
                    //if (false == m_Parent.IsEnableSkillMove())
                    //    return false;
                }
                break;
            case eATTACK.CHANGE_READY:
            case eATTACK.NONE:
                break;
            default:
                assert_cs.msg("default :" + m_eAttack);
                break;
        }

        if (LOCK_IS(eLOCK.MOVE))
        {
            if (LOCK_IS(eLOCK.AUTOMOVE))
                return false;
        }

        return true;
    }

    public bool IsEnableAni()
    {
        if (m_eDamage != eDAMAGE.NONE)
            return false;

        if (LOCK_IS(eLOCK.ANI))
            return false;

        return true;
    }

    public bool IsEnableAttack()
    {
        //if (PlayEvent.Inst().IsPlay())
        //    return false;

        switch (m_eDamage)
        {
            case eDAMAGE.NONE:
            case eDAMAGE.DOWN_ING:
            case eDAMAGE.DOWN_END:
                break;
            default:
                return false;
        }

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.DONT_ATK) != 0)
                return false;
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }

        if (LOCK_IS(eLOCK.ATTACK))
            return false;

        return true;
    }

    public bool IsEnableJump()
    {
        //if (PlayEvent.Inst().IsPlay())
        //    return false;

        switch (m_eDamage)
        {
            case eDAMAGE.NONE:
                break;
            default:
                return false;
        }

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }

        //if (((SpriteActor)m_Parent).IsInvoke(SpriteActor.eINVOKE.STOP_MOTION) == true)
        //    return false;

        if (LOCK_IS(eLOCK.MOVE))
            return false;

        return true;
    }

    public bool IsEnableReserveBehavior()
    {
        //if (PlayEvent.Inst().IsPlay())
        //    return false;

        switch (m_eDamage)
        {
            case eDAMAGE.NONE:
                break;
            default:
                return false;
        }

        if (m_eCondition != eCONDITION.NONE)
        {
            if ((m_eCondition & eCONDITION.STUN) != 0)
                return false;
            if ((m_eCondition & eCONDITION.FREEZE) != 0)
                return false;
        }

        //if (((SpriteActor)m_Parent).IsInvoke(SpriteActor.eINVOKE.STOP_MOTION) == true)
        //    return false;

        if (LOCK_IS(eLOCK.MOVE))
            return false;

        return true;
    }

    public bool IsEnableDefaultAni()
    {
        //if(m_Parent.IsDie())
        //    return false;

        if (m_eDamage != eDAMAGE.NONE)
            return false;

        if (m_eCondition != eCONDITION.NONE)
        {
        }

        switch (m_eAttack)
        {
            case eATTACK.NONE:
                break;
            case eATTACK.ING:
            case eATTACK.READY:
            case eATTACK.CHANGE_READY:
                return false;
        }

        return true;
    }

    public bool IsEnableDamage()
    {
        // 등장중이 아니라면 다 맞자
        return m_bAppear == false;
    }

    public bool IsChange()
    {
        return m_eChange != eCHANGE.CHANGE_0;
    }
}
