﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Prop_Effect : MonoBehaviour
{
    public enum eCREATE_TYPE
    {
        SKILL,
        BUFF,
        ETC,
    }

    public class cINFO
    {
        public GameObject       m_Object;
        public eCREATE_TYPE     m_CreateType;
        public ParticleEdit     m_Edit;
    }    

    public List<cINFO> m_EffectList = new List<cINFO>();
    protected Character m_Parent;
    protected GameObject m_AniEffect = null;

    public void Init( Character Parent )
    {
        assert_cs.set(m_EffectList.Count == 0);
        m_Parent = Parent;
        m_AniEffect = null;
    }

    public void DelEffect( string name )
    {
        int cnt = m_EffectList.Count;
        if (cnt == 0)
            return;

        GameObject obj = null;
        for( int i=0; i<cnt; ++i )
        {
            obj = m_EffectList[i].m_Object;

            if (obj.activeSelf == false)
                continue;

            if (obj.name.Contains(name) == true)
            {
                obj.SetActive(false);
            }
        }
    }

    public void Delete()
    {
        ResetEffect();
        Prop_EffectMgr.Push(this);

        _SetAniEffect(null);
    }
    
    public void ResetEffect()
    {
        cINFO info;

        _SetAniEffect(null);

        int cnt = m_EffectList.Count;
        for (int i = 0; i < cnt; ++i )
        {
            info = m_EffectList[i];
            if (info.m_Object == null)
            {
                continue;
            }

            info.m_Object.SetActive(false);
        }
        m_EffectList.Clear();
    }
    
    void _SetAniEffect(GameObject obj)
    {
        if( m_AniEffect != null )
        {
            m_AniEffect.SetActive(false);
            m_AniEffect = null;
        }

        if (obj == null)
            return;

        m_AniEffect = obj;
    }

    public void SetAniEffect( string str )
    {
        if( string.IsNullOrEmpty( str ) == true )
        {
            _SetAniEffect(null);
            return;
        }

        _SetAniEffect(SharedObject.g_CollectorMgr.LoadEffect(m_Parent, string.Format("AniEffect/{0}_{1:00}", str)));
    }

    public GameObject LoadEffect( string path,  eCREATE_TYPE ct )
    {
        GameObject obj = SharedObject.g_CollectorMgr.LoadEffect(m_Parent, path);        

        cINFO info = new cINFO();
        info.m_Object = obj;
        info.m_CreateType = ct;
        info.m_Edit = obj.transform.GetComponentInChildren<ParticleEdit>();
        m_EffectList.Add(info);

        return obj;
    }

    public void UpdateEffect()
    {
        GameObject obj;
        for( int i=m_EffectList.Count-1; i>=0; --i )
        {
            obj = m_EffectList[i].m_Object;
            if( (obj==null)||(obj.activeSelf == false) )
            {
                m_EffectList.RemoveAt(i);
            }
        }
    }

    public void CancelSkill()
    {
        cINFO info;
        int nCnt = m_EffectList.Count;
        for (int i = 0; i < nCnt; ++i)
        {
            info = m_EffectList[i];

            if (info.m_Object == null)
            {
                continue;
            }

            if (info.m_Edit != null)
            {

            }

            switch( info.m_CreateType )
            {
                case eCREATE_TYPE.SKILL:
                    info.m_Object.SetActive(false);
                    break;

                case eCREATE_TYPE.BUFF:
                case eCREATE_TYPE.ETC:
                    break;

                default:
                    assert_cs.msg("default : " + info.m_CreateType);
                    break;
            }
        }
    }

    public void DetachEffect()
    {
        cINFO info;
        int nCnt = m_EffectList.Count;
        for (int i=0; i < nCnt; ++i)
        {
            info = m_EffectList[i];

            if( info.m_Object == null )
            {
                continue;
            }

            if( info.m_Edit != null )
            {
                if (info.m_Edit.m_bNotDetach)
                    continue;
            }

            info.m_Object.transform.SetParent(SharedObject.g_CollectorMgr.m_RootEffect, true);
        }
    }
}
