﻿using UnityEngine;
using System.Collections;

public class BarControl : MonoBehaviour 
{
    public enum eCHANGE //방향
    {
        NONE,
        LEFT,
        RIGHT,
    }

    eCHANGE m_eChange;

    Transform m_trBar;
    Transform m_trDelta;

    float m_fParentScaleX;
    float m_fExtentSizeX;    

    Vector3 m_vPosHP;
    Vector3 m_vPosDelta;

    float m_fWaitTime;
    float m_fBarRate;
    float m_fDeltaRate;

    void Awake()
    {
        m_trBar = transform.Find("Bar");
        m_vPosHP = m_trBar.localPosition;
        m_trDelta = transform.Find("Delta");
        m_vPosDelta = m_trDelta.localPosition;

        SpriteRenderer sr = m_trBar.GetComponent<SpriteRenderer>();        
        m_fExtentSizeX = sr.bounds.extents.x;

        Reset();
    }

    void Start()
    {
        m_fParentScaleX = transform.parent.localScale.x;
    }

    void Update()
    {
        if (m_eChange == eCHANGE.NONE)
            return;

        if ((m_fWaitTime -= Time.deltaTime) > 0f)
            return;

        switch (m_eChange)
        {
            case eCHANGE.LEFT:
                {
                    m_fDeltaRate -= Time.deltaTime;

                    if (m_fDeltaRate < m_fBarRate)
                    {
                        m_eChange = eCHANGE.NONE;
                        m_fDeltaRate = m_fBarRate;
                        m_trDelta.gameObject.SetActive(false);
                    }
                    else
                    {
                        Resize_Delta();
                    }
                }
                break;
            case eCHANGE.RIGHT:
                {
                    m_fBarRate += Time.deltaTime;

                    if (m_fBarRate > m_fDeltaRate)
                    {
                        m_eChange = eCHANGE.NONE;
                        m_fBarRate = m_fDeltaRate;
                        m_trDelta.gameObject.SetActive(false);
                    }
                    else
                    {
                        Resize_Bar();
                    }
                }
                break;
        }
    }

    public void Reset()
    {
        m_fBarRate = 1f;
        m_fDeltaRate = 1f;
        m_eChange = eCHANGE.NONE;

        m_trDelta.gameObject.SetActive(false);

        m_trBar.localScale = new Vector3(1, 1, 1);
        m_vPosHP.x = 0;
        m_vPosDelta.x = 0;
        m_trBar.localPosition = m_vPosHP;
    }

    void Resize_Bar()
    {
        m_trBar.localScale = new Vector3(m_fBarRate, 1, 1);
        m_vPosHP.x = -(m_fExtentSizeX * (1f - m_fBarRate));
        m_trBar.localPosition = m_vPosHP;

        Resize_Delta();
    }

    void Resize_Delta()
    {
        m_trDelta.localScale = new Vector3(m_fDeltaRate, 1, 1);
        m_vPosDelta.x = -(m_fExtentSizeX * (1f - m_fDeltaRate));
        m_trDelta.localPosition = m_vPosDelta;
    }

    public void SetRate(float _fRate)
	{
        switch (m_eChange)
        {
            case eCHANGE.NONE:
                {
                    if (_fRate == m_fBarRate)
                        return;

                    m_fWaitTime = 0.5f;
                    m_trDelta.gameObject.SetActive(true);

                    if (_fRate < m_fBarRate)
                    {
                        m_eChange = eCHANGE.LEFT;
                        m_fBarRate = _fRate;

                        Resize_Bar();                       
                    }
                    else
                    {
                        m_eChange = eCHANGE.RIGHT;
                        m_fDeltaRate = _fRate;

                        Resize_Delta();
                    }
                }
                break;
            case eCHANGE.LEFT:
                {
                    if (_fRate == m_fBarRate)
                        return;

                    if (_fRate > m_fBarRate)
                    {
                        m_eChange = eCHANGE.RIGHT;
                        m_fDeltaRate = _fRate;

                        Resize_Delta();
                    }
                    else
                    {
                        m_fBarRate = _fRate;
                        Resize_Bar();
                    }
                }
                break;
            case eCHANGE.RIGHT:
                {
                    if (_fRate == m_fDeltaRate)
                        return;

                    if (_fRate < m_fBarRate)
                    {
                        m_eChange = eCHANGE.LEFT;
                        m_fDeltaRate = m_fBarRate;
                        m_fBarRate = _fRate;

                        Resize_Bar();
                    }
                    else
                    {
                        m_fDeltaRate = _fRate;
                        Resize_Delta();
                    }
                }
                break;
        }
	}    
}
