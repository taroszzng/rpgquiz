﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class MyPlayer : AniCharacter
{
    Table_Player.Info m_PlayerInfo;

    protected override void InitRTTI() { m_eRTTI = eRTTI.MYPLAYER; }

    public override void Initialize(string _strPrefab)
    {
        base.Initialize(_strPrefab);
    }

    public override void Init(nsENUM.eTEAM _eTeam, int _nUniqID, int _nModelID, Vector3 _vPos)
    {
        m_PlayerInfo = SharedObject.g_TableMgr.m_Player.Get(_nModelID);

        base.Init(_eTeam, _nUniqID, _nModelID, _vPos);

        if (null != m_HitCollider)
            m_HitCollider.gameObject.layer = (int)nsENUM.ePHY_LAYER.RED;

        SetScale(m_PlayerInfo.m_fScale);
    }

    public override void InitStat()
    {
        base.InitStat();

        SetLiveState(eLIVE_STATE.LIVE);

        InitItem();
    }

    protected override void InitItem()
    {
        Table_Level.Info info = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

        if (null == info)
            return;

        bool bhp = false;

        m_Stat.m_fMaxHp = info.m_fHP;

        if (0 < SharedObject.g_PlayerPrefsData.PlayerHP)
        {
            m_Stat.m_fCurHp = SharedObject.g_PlayerPrefsData.PlayerHP;

            bhp = true;
        }
        else
            m_Stat.m_fCurHp = info.m_fHP;

        m_Stat.m_fAtk = info.m_fAtk;
        m_Stat.m_fLuck = (float)info.m_nLuck;

        int nCount = SharedObject.g_PlayerPrefsData.m_listAbility.Count;

        for (int i = 0; i < nCount; ++i)
        {
            Item item = SharedObject.g_PlayerPrefsData.GetItem(SharedObject.g_PlayerPrefsData.m_listAbility[i]);

            if (null == item)
                continue;

            switch (item.m_eItemApply)
            {
                case nsENUM.eITEMAPPLY.eITEMAPPLY_ALL:
                    {
                        m_Stat.m_fMaxHp += m_Stat.m_fMaxHp * item.m_fItemAbility;

                        if (!bhp)
                            m_Stat.m_fCurHp += m_Stat.m_fCurHp * item.m_fItemAbility;

                        m_Stat.m_fAtk += m_Stat.m_fAtk * item.m_fItemAbility;
                        m_Stat.m_fLuck += m_Stat.m_fLuck * item.m_fItemAbility;
                    }
                    break;
                case nsENUM.eITEMAPPLY.eITEMAPPLY_HP:
                    {
                        m_Stat.m_fMaxHp += m_Stat.m_fMaxHp * item.m_fItemAbility;

                        if (!bhp)
                            m_Stat.m_fCurHp += m_Stat.m_fCurHp * item.m_fItemAbility;
                    }
                    break;
                case nsENUM.eITEMAPPLY.eITEMAPPLY_ATK:
                    {
                        m_Stat.m_fAtk += m_Stat.m_fAtk * item.m_fItemAbility;
                    }
                    break;
                case nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK:
                    {
                        m_Stat.m_fLuck += m_Stat.m_fLuck * item.m_fItemAbility;
                    }
                    break;
            }
        }
    }

    protected override bool FindAniType()
    {
        if (null == m_PlayerInfo)
            return false;

        switch ((nsENUM.eANI_TYPE)m_PlayerInfo.m_nAniType)
        {
            case nsENUM.eANI_TYPE.eANI_TYPE_ANIMATOR:
                {
                    m_Animator = transform.GetComponent<Animator>();
                }
                break;
            default:
                return false;
        }

        return true;
    }

    protected override float SetMode()
    {
        float fvalue = 0;

        float fMaxhp = SharedObject.g_CollectorMgr.Monster.GetMaxHp();
        float fHp = SharedObject.g_CollectorMgr.Monster.GetHp();
        float fAtk = 0f; 

        if ((fMaxhp * 0.2f) >= fHp)//100% 추가 공격
        {
            fvalue = 0.3f;

            fAtk = SharedObject.g_CollectorMgr.Monster.GetAtk();
        }
        else if ((fMaxhp * 0.5f) >= fHp)//50% 추가
        {
            fvalue = 0.2f;

            fAtk = SharedObject.g_CollectorMgr.Monster.GetAtk() * fvalue;
        }

        return fAtk;
    }

    public override void Damage()
    {
        float fatk = SharedObject.g_CollectorMgr.Monster.m_Stat.m_fAtk;
        float fbuff = SharedObject.g_CollectorMgr.Monster.m_Stat.m_fEventBuff;

        float fDamage = Damage(fatk, fbuff);

        m_Stat.m_fCurHp -= fDamage;

        SharedObject.g_PlayerPrefsData.PlayerHP = m_Stat.m_fCurHp;

        SharedObject.g_CollectorMgr.SetHp(true, GetMaxHp(), GetHp());//hp바

        base.Damage();

        if (eLIVE_STATE.DIE == m_eLiveState)
            Die();
    }

    public override void Die()
    {
        base.Die();

        SharedObject.g_CollectorMgr.SetDie(true);
    }

    public override void EventBuff(nsENUM.eEVENTBUFF _e, float _f)//현재는 공격만 적용
    {
        base.EventBuff(_e, _f);

        switch (_e)
        {
            case nsENUM.eEVENTBUFF.eEVENTBUFF_HP_MINUS://hp 감소
                {
                    if (null != SharedObject.g_CollectorMgr.Monster)
                    {
                        float fhp = SharedObject.g_CollectorMgr.Monster.GetHp();

                        m_Stat.m_fEventBuff += fhp * _f;
                    }
                }
                break;
        }
    }

    protected override void UpdateKey()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.R))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DAM01);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_ATK01);
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DIE);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            SetRotation(ActorState.eDIR.eDIR_LEFT);

            //Vector3 vec = SharedObject.g_CurZone.TRCenterPos.position;

            //vec.x = vec.x - 3f;

            //AutoMove(vec, true);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            SetRotation(ActorState.eDIR.eDIR_RIGHT);

            //AutoMove(SharedObject.g_CurZone.TRCharacterPos.position, true);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            SetRotation(ActorState.eDIR.eDIR_FRONT);
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            //DistortionFlag();
        }
#endif
    }

    //protected override bool SetAnimationComplete(eANI _eAni)//애니메이션 실행
    //{
    //    if (!base.SetAnimationComplete(_eAni))
    //        return false;

    //    AniValue ani = AnimationTable(_eAni);

    //    AnimationPlay(_eAni, ani);

    //    return true;
    //}

    //AniValue AnimationTable(eANI _eAni)
    //{
    //    AniValue ani;

    //    switch (_eAni)
    //    {
    //        case eANI.eANI_RUN:
    //            ani = m_PlayerInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_RUN];
    //            break;
    //        case eANI.eANI_ATK01:
    //            ani = m_PlayerInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_ATK];
    //            break;
    //        case eANI.eANI_DAM01:
    //            ani = m_PlayerInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_DAM];
    //            break;
    //        case eANI.eANI_DIE:
    //            ani = m_PlayerInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_DIE];
    //            break;
    //        default:
    //            ani = m_PlayerInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_IDLE];
    //            break;
    //    }

    //    return ani;
    //}

    //protected override void AnimationPlay(eANI _eAni, AniValue _ani)
    //{
    //    if (_eAni != eANI.eANI_DAM01)//런은 목표위치에 도착시 호출됌, idle은 시간 없음
    //    {
    //        StartCoroutine(AniTime(_ani.m_fAniTime));

    //        m_Animator.Play(_ani.m_strAni);
    //    }
    //    else if (_eAni == eANI.eANI_DAM01)
    //    {
    //        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);

    //        StartCoroutine(DamAniTime(_ani));
    //    }
    //    else
    //        m_Animator.Play(_ani.m_strAni);
    //}

    //IEnumerator AniTime(float _fTime)//애니메이션 시간
    //{
    //    float ftime = _fTime;

    //    if (0 >= ftime)
    //        ftime = 1.0f;

    //    yield return new WaitForSeconds(ftime);

    //    switch (m_eApplyAni)
    //    {
    //        case eANI.eANI_IDLE:
    //            {
    //                if (nsENUM.eTEAM.WHITE_TEAM == SharedObject.g_DiceMgr.WinTeam)//무승부
    //                {
    //                    SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
    //                    SetDir(ActorState.eDIR.eDIR_LEFT);
    //                    SetState_Move(ActorState.eMOVE.MOVE);

    //                    //AutoMove(SharedObject.g_CurZone.TRCharacterPos.position, true);
    //                }
    //            }
    //            break;
    //        case eANI.eANI_ATK01:
    //            {
    //                SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
    //                SetDir(ActorState.eDIR.eDIR_LEFT);
    //                SetState_Move(ActorState.eMOVE.MOVE);

    //                //AutoMove(SharedObject.g_CurZone.TRCharacterPos.position, true);
    //            }
    //            break;
    //        case eANI.eANI_RUN:
    //            EndAutoMove();
    //            break;
    //        case eANI.eANI_DIE:
    //            break;
    //    }
    //}

    //public override float AtkTime()
    //{
    //    AniValue ani = m_PlayerInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_ATK];

    //    return ani.m_fAniTime <= 0 ? 1f : ani.m_fAniTime;
    //}

    //IEnumerator DamAniTime(AniValue _ani)
    //{
    //    float ftime = SharedObject.g_CollectorMgr.Monster.AtkTime() - 0.5f;

    //    yield return new WaitForSeconds(ftime);

    //    m_Animator.Play(_ani.m_strAni);

    //    Damage();

    //    yield return new WaitForSeconds(_ani.m_fAniTime);

    //    if (IsDie())//죽었을때
    //    {
    //        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DIE);
    //    }
    //    else
    //    {
    //        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
    //        SetDir(ActorState.eDIR.eDIR_LEFT);
    //        SetState_Move(ActorState.eMOVE.MOVE);

    //        //AutoMove(SharedObject.g_CurZone.TRCharacterPos.position, true);
    //    }
    //}

    //protected override void EndAutoMoveState()//이동이 완료됐을때 상태
    //{
    //    base.EndAutoMoveState();

    //    switch (m_State.m_eDir)
    //    {
    //        case ActorState.eDIR.eDIR_LEFT://목표 지점 도착
    //            {
    //                SetDir(ActorState.eDIR.eDIR_FRONT);

    //                EndAutoMove();

    //                SharedObject.g_DiceMgr.ReSet();
    //                SharedObject.g_AMode.ReSetPlayer();//주사위 다시 리셋
    //            }
    //            break;
    //        case ActorState.eDIR.eDIR_RIGHT://캐릭터 공격
    //            {
    //                if (m_eTeam == SharedObject.g_DiceMgr.WinTeam) //공격 여부
    //                    SetAnimation(eANIMODE.eANIMODE_SKILL, eANI.eANI_ATK01);
    //                else if (nsENUM.eTEAM.WHITE_TEAM == SharedObject.g_DiceMgr.WinTeam)
    //                    SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);
    //                else
    //                    SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DAM01);
    //            }
    //            break;
    //    }
    //}

    //public override void Fight()
    //{
    //    //SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
    //    //SetDir(ActorState.eDIR.eDIR_RIGHT);
    //    //SetState_Move(ActorState.eMOVE.MOVE);

    //    //Vector3 vec = SharedObject.g_CurZone.TRCenterPos.position;

    //    //vec.x = vec.x - 3f;

    //    //AutoMove(vec, true);
    //}


}