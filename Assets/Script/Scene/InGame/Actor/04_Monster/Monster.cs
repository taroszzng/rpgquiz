﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class Monster : AniCharacter
{
    Table_Monster.Info m_MonsterInfo;

    protected override void InitRTTI() { m_eRTTI = eRTTI.MONSTER; }

    public override void Initialize(string _strPrefab)
    {
        base.Initialize(_strPrefab);
    }

    public override void Init(nsENUM.eTEAM _eTeam, int _nUniqID, int _nModelID, Vector3 _vPos)
    {
        m_MonsterInfo = SharedObject.g_TableMgr.m_Monster.Get(_nModelID);

        base.Init(_eTeam, _nUniqID, _nModelID, _vPos);

        if (null != m_HitCollider)
            m_HitCollider.gameObject.layer = (int)nsENUM.ePHY_LAYER.RED;

        SetScale( m_MonsterInfo.m_fScale );

        SetPos(m_MonsterInfo.m_fPosX, m_MonsterInfo.m_fPosY, m_MonsterInfo.m_fPosZ );
    }

    public override void InitStat()
    {
        base.InitStat();

        if (null == m_MonsterInfo)
            return;

        InitItem();

        SharedObject.g_SceneMgr.MonsterPerID = m_MonsterInfo.m_nPercentage;
    }

    protected override void InitItem()
    {
        m_Stat.m_fMaxHp = m_MonsterInfo.m_fHP;

        if (0 < SharedObject.g_PlayerPrefsData.MonsterHP)
            m_Stat.m_fCurHp = SharedObject.g_PlayerPrefsData.MonsterHP;
        else
            m_Stat.m_fCurHp = m_MonsterInfo.m_fHP;

        m_Stat.m_fAtk = m_MonsterInfo.m_fAtk;
        m_Stat.m_fLuck = (float)m_MonsterInfo.m_nLuck;
    }

    protected override bool FindAniType()
    {
        if (null == m_MonsterInfo)
            return false;

        switch((nsENUM.eANI_TYPE)m_MonsterInfo.m_nAniType)
        {
            case nsENUM.eANI_TYPE.eANI_TYPE_ANIMATOR:
                {
                    m_Animator = transform.GetComponent<Animator>();
                }
                break;
            default:
                return false;
        }

        return true;
    }

    protected override bool SetAnimationComplete(eANI _eAni)//애니메이션 실행
    {
        if (!base.SetAnimationComplete(_eAni))
            return false;

        AniValue ani = AnimationTable(_eAni);

        AnimationPlay(_eAni, ani);

        //Debug.Log("_eAni = " + _eAni);

        return true;
    }

    AniValue AnimationTable(eANI _eAni)
    {
        AniValue ani;

        switch (_eAni)
        {
            case eANI.eANI_RUN:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_RUN];
                break;
            case eANI.eANI_ATK01:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_ATK01];
                break;
            case eANI.eANI_ATK02:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_ATK02];
                break;
            case eANI.eANI_ATK03:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_ATK03];
                break;
            case eANI.eANI_DAM01:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_DAM];
                break;
            case eANI.eANI_DIE:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_DIE];
                break;
            default:
                ani = m_MonsterInfo.m_ArrayAniValue[(int)eANIVALUE.eANIVALUE_IDLE];
                break;
        }

        return ani;
    }

    protected override void AnimationPlay(eANI _eAni, AniValue _ani)
    {
        if (_eAni == eANI.eANI_DAM01)
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);

            StartCoroutine(DamAniTime(_ani));
        }
        else if(_eAni == eANI.eANI_ATK01 ||
            _eAni == eANI.eANI_ATK02 ||
            _eAni == eANI.eANI_ATK03)//런은 목표위치에 도착시 호출됌, idle은 시간 없음
        {
            StartCoroutine(AtkTime(_ani.m_fAniTime));

            m_Animator.Play(_ani.m_strAni);
        }
        else
        {
            StartCoroutine(AniTime(_ani.m_fAniTime));

            m_Animator.Play(_ani.m_strAni);
        }
    }

    IEnumerator AtkTime(float _fTime)
    {
        float ftime = _fTime;

        if (0 >= ftime)
            ftime = 1.0f;

        float fani = ftime / 2;

        yield return new WaitForSeconds(fani);

        SharedObject.g_MainCamera.Shake(100001, m_eRTTI, true);

        SharedObject.g_AMode.GOEFUserDamage.SetActive(true);//유저 맞는 화면

        yield return new WaitForSeconds(fani);

        SharedObject.g_CollectorMgr.Player.Damage();

        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
        SetDir(ActorState.eDIR.eDIR_RIGHT);
        SetState_Move(ActorState.eMOVE.MOVE);
    }

    IEnumerator AniTime(float _fTime)//애니메이션 시간
    {
        float ftime = _fTime;

        if (0 >= ftime)
            ftime = 1.0f;

        yield return new WaitForSeconds(ftime);

        switch (m_eApplyAni)
        {
            case eANI.eANI_IDLE:
                {
                    if (nsENUM.eTEAM.WHITE_TEAM == SharedObject.g_SceneMgr.Win)//무승부
                    {
                        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
                        SetDir(ActorState.eDIR.eDIR_RIGHT);
                        SetState_Move(ActorState.eMOVE.MOVE);

                        //AutoMove(SharedObject.g_CurZone.TRCenterPos.position, true);

                        SharedObject.g_SceneMgr.Win = nsENUM.eTEAM.BLACK_TEAM;
                    }
                }
                break;
            case eANI.eANI_RUN:
                EndAutoMove();
                break;
            case eANI.eANI_DIE:
                Die();
                break;
        }
    }

    IEnumerator DamAniTime(AniValue _ani)
    {
        yield return new WaitForSeconds(1f);

        m_Animator.Play(_ani.m_strAni);

        float fani = _ani.m_fAniTime;

        SharedObject.g_MainCamera.Shake(100001, m_eRTTI, true);

        Damage();//몬스터 히트 텍스트

        yield return new WaitForSeconds(fani);

        SharedObject.g_CollectorMgr.DamageHp(false);//hp바
        
        if (IsDie())//죽었을때
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DIE);

            StartCoroutine(ClearCoroutine());
        }
        else
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
            SetDir(ActorState.eDIR.eDIR_RIGHT);
            SetState_Move(ActorState.eMOVE.MOVE);

            //AutoMove(SharedObject.g_CurZone.TRMonsterPos.position, true);
        }
    }

    protected override float SetMode()
    {
        float fvalue = 0;

        float fMaxhp = SharedObject.g_CollectorMgr.Player.GetMaxHp();
        float fHp = SharedObject.g_CollectorMgr.Player.GetHp();
        float fAtk = 0f;

        if ((fMaxhp * 0.2f) >= fHp)//100% 추가 공격
        {
            fvalue = 0.3f;

            fAtk = SharedObject.g_CollectorMgr.Player.GetAtk();
        }
        else if ((fMaxhp * 0.5f) >= fHp)//50% 추가
        {
            fvalue = 0.2f;

            fAtk = SharedObject.g_CollectorMgr.Player.GetAtk() * fvalue;
        }

        if (0 < fvalue)
            SharedObject.g_AMode.SetEffectMode(fvalue);

        return fAtk;
    }

    public override void Damage()
    {
        float fatk = SharedObject.g_CollectorMgr.Player.m_Stat.m_fAtk;
        float fbuff = SharedObject.g_CollectorMgr.Player.m_Stat.m_fEventBuff;

        float fDamage = Damage(fatk, fbuff);

        m_Stat.m_fCurHp -= fDamage;

        SharedObject.g_PlayerPrefsData.MonsterHP = m_Stat.m_fCurHp;

        base.Damage();
    }

    public override void Die()
    {
        base.Die();

        SharedObject.g_CollectorMgr.SetDie(false);
    }

    IEnumerator ClearCoroutine()
    {
        float ScaleTime = 0.2f;
        float SlowTime = 3f;
        float SlowTimeConvertSlow = ScaleTime * SlowTime;

        SharedObject.g_CollectorMgr.TimeScale(ScaleTime, SlowTime);
        SharedObject.g_MainCamera.ZoomEndStage(0f, -1.5f, 1.5f, SlowTime - 1.5f, 0.5f, Vector3.zero);

        yield return new WaitForSeconds(SlowTimeConvertSlow + 2.0f);  
    }

    protected override void EndAutoMoveState()//이동이 완료됐을때 상태
    {
        base.EndAutoMoveState();

        switch (m_State.m_eDir)
        {
            case ActorState.eDIR.eDIR_LEFT://목표 지점 도착후 공격
                {
                    if (SharedObject.g_SceneMgr.Win == m_eTeam) //공격 여부
                    {
                        int ran = Random.Range(0, (int)nsENUM.eVALUE.eVALUE_3);

                        eANI eani = ran + eANI.eANI_ATK01;

                        SetAnimation(eANIMODE.eANIMODE_SKILL, eani);
                    }
                    else
                        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DAM01);
                }
                break;
            case ActorState.eDIR.eDIR_RIGHT://몬스터 목표 지점 도착
                {
                    SetDir(ActorState.eDIR.eDIR_FRONT);

                    EndAutoMove();

                    transform.position = SharedObject.g_CurZone.TRCenterPos.position;

                    SetPos(m_MonsterInfo.m_fPosX, m_MonsterInfo.m_fPosY, m_MonsterInfo.m_fPosZ);

                    SharedObject.g_SceneMgr.ReSet();
                }
                break;
        }
    }

    public override void EventBuff(nsENUM.eEVENTBUFF _e, float _f)//현재는 공격만 적용
    {
        base.EventBuff(_e, _f);

        switch (_e)
        {
            case nsENUM.eEVENTBUFF.eEVENTBUFF_HP_MINUS://hp 감소
                {
                    if (null != SharedObject.g_CollectorMgr.Player)
                    {
                        float fhp = SharedObject.g_CollectorMgr.Player.GetHp();

                        m_Stat.m_fEventBuff += fhp * _f;
                    }
                }
                break;
        }
    }

    public override void Fight()
    {
        SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
        SetDir(ActorState.eDIR.eDIR_LEFT);
        SetState_Move(ActorState.eMOVE.MOVE);

        //Vector3 vec = SharedObject.g_CurZone.TRCenterPos.position;

        //vec.x = vec.x + 3f;

        //AutoMove(vec, true);
    }

    protected override void UpdateKey()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.R))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DAM01);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_ATK01);
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_DIE);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
            SetDir(ActorState.eDIR.eDIR_LEFT);
            SetState_Move(ActorState.eMOVE.MOVE);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_RUN);
            SetDir(ActorState.eDIR.eDIR_RIGHT);
            SetState_Move(ActorState.eMOVE.MOVE);

            //AutoMove(SharedObject.g_CurZone.TRMonsterPos.position, true);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            SetAnimation(eANIMODE.eANIMODE_ETC, eANI.eANI_IDLE);
            SetDir(ActorState.eDIR.eDIR_FRONT);
            SetState_Move(ActorState.eMOVE.STOP);
        }
#endif
    }
}