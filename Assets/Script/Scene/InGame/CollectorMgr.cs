﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public partial class CollectorMgr : MonoBehaviour
{
    protected EffResMgr	m_EffResMgr = null;    

    [System.NonSerialized]
    public Transform m_RootEffect;

    MyPlayer m_Player;
    Monster m_Monster;

    public MyPlayer Player
    {
        get { return m_Player; }
    }

    public Monster Monster
    {
        get { return m_Monster; }
    }

    void Awake()
    {
        SharedObject.g_CollectorMgr = this;

        m_EffResMgr = GetComponent<EffResMgr>();

        GameObject obj = GameObject.Find("EffectViewer");

        if (obj != null)
            m_RootEffect = obj.transform;

        Prop_EffectMgr.Init();
    }

    void OnDestroy()
    {
        Prop_EffectMgr.Delete();
    }

    public void Mode(nsENUM.eSCENE _eScene)//신에서 호출
    {
        SharedObject.g_CurZone.Mode(_eScene);

        PlayerSpawn(SharedObject.g_PlayerPrefsData.PlayerID);

        if (SharedObject.g_PlayerPrefsData.Ending)
        {
            if (null != m_Monster)
            {
                DestroyObject(m_Monster.gameObject);
                StartCoroutine(EndingMonsterSpawn());
            }
            else
                MonsterSpawn(SharedObject.g_SceneMgr.MonsterSelect);
        }
        else
            MonsterSpawn(SharedObject.g_PlayerPrefsData.MonsterID);
    }

    void PlayerSpawn(int _nCostumeID)
    {
        if (null != m_Player)
            return;

        Table_Player.Info info = SharedObject.g_TableMgr.m_Player.Get(_nCostumeID);

        string strPrefab = "";
        string strPath = "Prefabs/InGame/Player/" + info.m_strPrefab;//캐릭터 의상 전체로 로드

        GameObject obj = Resources.Load(strPath) as GameObject;

        if (null == obj)
            return;

        obj = GameObject.Instantiate(obj, Vector3.zero, Quaternion.Euler(0f, info.m_fRot, 0f)) as GameObject;

        m_Player = obj.AddComponent<MyPlayer>();

        if (null == m_Player)
        {
            Destroy(obj);
            return;
        }

        m_Player.Initialize(strPrefab);

        Vector3 vec = CheckPos(SharedObject.g_CurZone.TRCenterPos.position, true);

        m_Player.Init(nsENUM.eTEAM.BLUE_TEAM, 0, _nCostumeID, vec);
    }

    void MonsterSpawn(int _nMonsterID)
    {
        if (null != m_Monster)
            return;

        Table_Monster.Info info = SharedObject.g_TableMgr.m_Monster.Get(_nMonsterID);

        if (null == info)
            return;

        string strPath = "Prefabs/InGame/Monster/Prefab/" + info.m_strPrefab;

        GameObject obj = Resources.Load(strPath) as GameObject;

        if (null == obj)
            return;

        obj = GameObject.Instantiate(obj, Vector3.zero, Quaternion.Euler(0f, info.m_fRot, 0f)) as GameObject;

        m_Monster = obj.AddComponent<Monster>();

        if (null == m_Monster)
        {
            Destroy(obj);
            return;
        }

        m_Monster.Initialize(info.m_strPrefab);

        Vector3 vec = CheckPos(SharedObject.g_CurZone.TRCenterPos.position);

        m_Monster.Init(nsENUM.eTEAM.RED_TEAM, 1, info.m_nID, vec);
    }

    IEnumerator EndingMonsterSpawn()
    {
        yield return new WaitForSeconds(0.5f);

        MonsterSpawn(SharedObject.g_SceneMgr.MonsterSelect);
    }

    public void Fight()
    {
        if(null != SharedObject.g_SceneMgr.m_EventBuff)
        {
            nsENUM.eEVENTBUFF e = SharedObject.g_SceneMgr.m_EventBuff.m_eBuff;

            if (e != nsENUM.eEVENTBUFF.eEVENTBUFF_END)
                EventBuff();
        }

        ComboBuff();//콤보버프

        m_Monster.Fight();
    }

    public void SetHp()//hp설정
    {
        if (null != m_Player)
            SetHp(true, m_Player.GetMaxHp(), m_Player.GetHp());

        if (null != m_Monster)
            SetHp(false, m_Monster.GetMaxHp(), m_Monster.GetHp());
    }

    public void SetHp(bool _bPlayer, float _fMaxHp, float _fHp)//ui에 적용
    {
        SharedObject.g_AModeSet.SetHp(_bPlayer, _fMaxHp, _fHp); 
    }

    public void DamageHp(bool _bPlayer)
    {
        if (_bPlayer)
        {
            if (null != m_Player)
                SharedObject.g_AModeSet.SetHp(_bPlayer, m_Player.GetMaxHp(), m_Player.GetHp());
        }
        else
        {
            if(null != m_Monster)
                SharedObject.g_AModeSet.SetHp(_bPlayer, m_Monster.GetMaxHp(), m_Monster.GetHp());
        }
    }

    public void SetDie(bool _bPlayer)
    {
        SharedObject.g_SoundScript.PlayBGM(nsENUM.eSOUNDBGM.eSOUNDBGM_TITLE);

        SharedObject.g_SceneMgr.BattleStart = false;

        //SharedObject.g_DiceMgr.ReSet();

        SharedObject.g_AModeSet.InitHp();//0으로 초기화

        SharedObject.g_SceneMgr.MonsterClear = !_bPlayer;

        Table_Monster.Info info = SharedObject.g_TableMgr.m_Monster.Get(SharedObject.g_PlayerPrefsData.MonsterID);

        if (null == info)
            return;

        if (_bPlayer)//플레이어 죽었을때
        {
            Table_Level.Info level = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

            if (null != level)
            {
                int nexp = SharedObject.g_PlayerPrefsData.Exp;

                nexp += level.m_nExp;

                if (100 <= nexp)//레벨업
                {
                    if (SharedObject.g_PlayerPrefsData.Level < (info.m_nLevel + level.m_nMaxLevel))
                        SharedObject.g_PlayerPrefsData.Level += 1;     
                }

                SharedObject.g_PlayerPrefsData.Exp = nexp;
            }

            SharedObject.g_SceneMgr.ResetItemUse();//클리어든 죽었던 아이템 초기화

            SharedObject.g_PlayerPrefsData.PlayerHP = 0;

            m_Player.InitStat();

            SharedObject.g_PlayerPrefsData.MonsterHP = 0;

            m_Monster.InitStat();
        }
        else//몬스터 죽었을때
        {
            if (null != m_Player)
                DestroyObject(m_Player.gameObject);//삭제

            if (null != m_Monster)
                DestroyObject(m_Monster.gameObject);

            SharedObject.g_PlayerPrefsData.PlayerHP = 0;

            if (null != info)
            {
                if (info.m_nNextID == SharedObject.g_PlayerPrefsData.MonsterID)//마지막 보스
                    SharedObject.g_PlayerPrefsData.Ending = true;

                SharedObject.g_SceneMgr.ClearMonsterID = SharedObject.g_PlayerPrefsData.MonsterID;

                SharedObject.g_PlayerPrefsData.MonsterID = info.m_nNextID;

                info = SharedObject.g_TableMgr.m_Monster.Get(info.m_nNextID);

                if (null != info)
                    SharedObject.g_PlayerPrefsData.ClearStage = info.m_nClearStage;

                if (SharedObject.g_bGoogleLogin)
                    GPGSIds.GoogleClearMonster(info.m_nClearStage);
            }

            Table_Level.Info level = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

            if (null != level)
            {
                SharedObject.g_PlayerPrefsData.Level = level.m_nNextLevel;
                SharedObject.g_PlayerPrefsData.Exp = 0;
            }
        }

        if (null != SharedObject.g_Mode.GO2Depth)
            SharedObject.g_Mode.GO2Depth.SetActive(true);//.SetPopUp();//결과
    }

    public void ItemStatUse(nsENUM.eTEAM _e, nsENUM.eITEMTYPE _eItem, int _nItemID)//캐릭터 스탯 적용 소모성
    {
        if (null == m_Player || null == m_Monster)
            return;

        ResetStatItem();

        switch(_e)
        {
            case nsENUM.eTEAM.BLUE_TEAM:
                {
                    if (nsENUM.eITEMTYPE.eITEMTYPE_CONSUME == _eItem)
                        m_Player.ItemStatUse(_nItemID);
                    else if (nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL == _eItem)
                        m_Player.EquipItem(_nItemID);
                }
                break;
            case nsENUM.eTEAM.RED_TEAM:
                {
                    if (nsENUM.eITEMTYPE.eITEMTYPE_CONSUME == _eItem)
                        m_Monster.ItemStatUse(_nItemID);
                    else if (nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL == _eItem)
                        m_Monster.EquipItem(_nItemID);
                }
                break; 
        }

        if(SharedObject.g_SceneMgr.BattleStart)
            SetHp();//Hp갱신
    }

    public void UnquipItem(nsENUM.eTEAM _e, int _nItemID)//탈착
    {
        if (null == m_Player || null == m_Monster)
            return;

        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if (null == info)
            return;

        if (nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL != (nsENUM.eITEMTYPE)info.m_nType)
            return;

        ResetStatItem();

        switch (_e)
        {
            case nsENUM.eTEAM.BLUE_TEAM:
                {
                    m_Player.UnquipItem(_nItemID);
                }
                break;
            case nsENUM.eTEAM.RED_TEAM:
                {
                    m_Monster.UnquipItem(_nItemID);
                }
                break;
        }

        //if (SharedObject.g_SceneMgr.BattleStart)
        //    SetHp();//Hp갱신
    }

    public void ItemBuff(nsENUM.eEVENTBUFF _e, float _f)//캐릭터만 사용
    {
        if (null == m_Player)
            return;

        m_Player.EventBuff(_e, _f);
    }

    public void EventBuff()//버프 발동
    {
        if (null == m_Player || null == m_Monster)
            return;

        bool buff = false;
        nsENUM.eEVENTBUFF ebuff = SharedObject.g_SceneMgr.m_EventBuff.m_eBuff;
        float fvalue = 0;

        switch (ebuff)
        {
            case nsENUM.eEVENTBUFF.eEVENTBUFF_ATK_PLUS:
                {
                    fvalue = (float)SharedObject.g_SceneMgr.m_EventBuff.m_nEventValue / 100f;

                    buff = true;
                }
                break;
        }

        if (!buff)
            return;

        if (nsENUM.eTEAM.BLUE_TEAM == SharedObject.g_SceneMgr.Win)//플레이어
        {
            m_Player.EventBuff(ebuff, fvalue);
        }
        else if (nsENUM.eTEAM.RED_TEAM == SharedObject.g_SceneMgr.Win)//몬스터
        {
            m_Monster.EventBuff(ebuff, fvalue);
        }
    }

    void ComboBuff()//콤보 관련 데미지
    {
        int nid = SharedObject.g_SceneMgr.GetComboCount();//콤보 2부터 시작

        if (0 >= nid)
            return;

        Table_Combo.Info info = SharedObject.g_TableMgr.m_Combo.Get(nid);

        if (null == info)
            return;

        if (null != m_Player)
            m_Player.EventBuff(nsENUM.eEVENTBUFF.eEVENTBUFF_ATK_COMBO, info.m_fValue);
    }

    public void ResetStatItem()//아이템 초기화
    {
        if (null == m_Player || null == m_Monster)
            return;

        m_Player.ResetStatItem();
        m_Monster.ResetStatItem();
    }

    public void SetPause(bool _b)
    {
        if (null == m_Monster)
            return;

        if (!_b)
        {
            m_Monster.m_Animator.StopPlayback();
        }
        else
        {
            m_Monster.m_Animator.StartPlayback();
        }

        Debug.Log("CollectorMgr = " + _b);
    }

    public void TimeScale(float fScale, float fDelay)
    {
        // 완전 정지 상태에선 시간이 흐르지 않기 떄문에 정상 작동이 되지 않는다.
        assert_cs.set(fScale > 0f);
        Time.timeScale = fScale;

        StartCoroutine(TimeScaleCoroutine(fDelay * fScale));
    }
    
    IEnumerator TimeScaleCoroutine( float fDelay )
    {
        yield return new WaitForSeconds(fDelay);

        Time.timeScale = 1f;
    }

    public GameObject LoadEffect(Character Parent, string path)
    {
        GameObject eff = m_EffResMgr.LoadEffect(Parent, path);
       
        if (eff == null)
            return null;

        return eff;
    }

    public GameObject LoadEffect_Pos(EffResMgr.eEFFECT_TYPE type, Character Parent, Vector3 pos3D, bool IsFront, string path,
        float LifeTime = 0f, float fScale = 1.0f)
    {
        GameObject eff = m_EffResMgr.LoadEffect(type, pos3D, IsFront, path, LifeTime);

        if (eff == null)
            return null;
        else//[kks][Effect]:크기조절 = 스몰, 미디움, 라지로 나눔 보류 = (파티클 ChildCount를 다 돌면서 스케일을 조정해야 한다.)
        {
            //if (1.0f != fScale)//[kks][Effect]:크기조절
            //{
            //    int nCount = eff.transform.childCount;

            //    for (int i = 0; i < nCount; ++i)
            //        eff.transform.GetChild(i).localScale = new Vector3(fScale, fScale, fScale);
            //}
        }

        return eff;
    }

    public GameObject LoadEffect_Pos(EffResMgr.eEFFECT_TYPE type, Vector3 pos3D, bool IsFront, string path, float LifeTime = 0f)
    {
        if (path == null)
            return null;

        return m_EffResMgr.LoadEffect(type, pos3D, IsFront, path, LifeTime);
    }

    Vector3 CheckPos(Vector3 _vec3, bool _bfalse = false)//해상도별 위치 수정,(true = 캐릭터, false = 몬스터)
    {
        Vector3 vec = _vec3;

        float fPos = CheckWidth(vec.x);

        vec.x = fPos;

        return vec;
    }

    float CheckWidth(float _fx)
    {
        return _fx;

        /* 320*480 = 2:3 = 0.66
        * 640*960 = 2:3
        * 
        * 768*1024 = 3:4 = 0.75
        * 1536*2048 = 3:4
        * 2048*2732 = 3:4
        * 
        * 480*800 = 3:5  = 0.6
        * 768*1280 = 3:5
        * 
        * 640*1136 = 9:16 = 0.5625
        * 720*1280 = 9:16
        * 750*1334 = 9:16
        * 1440*2560 = 9:16
        * 1080*1920 = 9:16
        * 
        * 1440*2880 = 9:18 = 0.5
        * 
        * 1440*2960 = 9:18.5 = 0.4864
        * 
        * 1125*2436 = 9:19.5 = 0.4618
        * 
        * 800*1280 = 10:16  = 0.625
        * 1200*1920 = 10:16
        * 1600*2560 = 10:16*/

        bool bRate = true;

        float fx = 0;
        float fwidth = 0f;

        if (0 < _fx) //
            fx = SharedObject.g_CurZone.Right - _fx;
        else
            fx = SharedObject.g_CurZone.Left - _fx;

        float frate = (float)Screen.height / (float)Screen.width;

        if (0.75 <= frate) //3:4
        {
            frate = (0.75f / 0.56f) - 0.5f;
        }
        else if (0.66 <= frate && 0.74 > frate) //2:3
        {
            frate = (0.66f / 0.56f) - 0.8f;
        }
        else if (0.62 <= frate && 0.66 > frate) //10:16
        {
            frate = (0.62f / 0.56f) - 0.5f;
        }
        else if (0.6 <= frate && 0.62 > frate) //3:5
        {
            frate = (0.6f / 0.56f) - 0.48f;
        }
        else if (0.56 <= frate && 0.6 > frate)
        {
            frate = 0;
        }
        else if (0.5 <= frate && 0.56 > frate)//9:18
        {
            frate = 1f - (0.5f / 0.56f);
        }
        else if (0.48 <= frate && 0.5 > frate) //9:18.5
        {
            frate = 1f - (0.48f / 0.56f);
        }
        else if (0.48 > frate) //9:19.5
        {
            frate = 1f - (0.46f / 0.56f);
        }

        if (bRate)
            fwidth = _fx - (fx * frate);
        else
            fwidth = _fx;

        Debug.Log("CheckWidth Screen = " + (float)Screen.height / (float)Screen.width + " width = " + Screen.width + " height = " + Screen.height);
        Debug.Log("CheckWidth fwidth = " + fwidth + " _fx = " + _fx + " fx = " + fx + " frate = " + frate);
        Debug.Log("CheckWidth left = " + SharedObject.g_CurZone.Left + " right = " + SharedObject.g_CurZone.Right);

        return fwidth;
    }
}
