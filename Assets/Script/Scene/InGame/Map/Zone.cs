﻿using UnityEngine;
using System.Collections.Generic;

public class Zone : MonoBehaviour
{
    public Transform TRCenterPos;
    public Transform TREffect;
    public Transform TRBG;

    GameObject m_GOBG = null;

    List<GameObject> m_list = new List<GameObject>();

    float m_fLeft;
    float m_fRight;

    public float Left
    {
        get { return m_fLeft; }
    }

    public float Right
    {
        get { return m_fRight; }
    }

    void Awake()
    {
        SharedObject.g_CurZone = this;

        ZoneInit();
    }

    void ZoneInit()
    {
        m_fLeft = transform.Find("Position/Left").localPosition.x;
        m_fRight = transform.Find("Position/Right").localPosition.x;
    }

    void SetEffect(int _nStage)
    {
        int nCount = m_list.Count;

        if (0 < nCount)
        {
            for (int i = 0; i < nCount; ++i)
                DestroyObject(m_list[i]);
        }

        Table_Stage.Info info = SharedObject.g_TableMgr.m_Stage.Get(_nStage);

        if (null == info)
            return;

        string strPath = "Prefabs/FX_Effect/" + info.m_strEffect;//캐릭터 의상 전체로 로드

        GameObject obj = Resources.Load(strPath) as GameObject;

        if (null == obj)
            return;

        obj = GameObject.Instantiate(obj, Vector3.zero, Quaternion.Euler(0f, 0f, 0f)) as GameObject;

        obj.transform.SetParent(TREffect);

        obj.transform.position = TREffect.position;

        Vector3 vec = obj.transform.position;

        vec.y = info.m_fY;

        obj.transform.position = vec;

        m_list.Add(obj);
    }

    void SetBG(int _nStage)
    {
        if (null != m_GOBG)
            DestroyObject(m_GOBG);

        Table_Stage.Info info = SharedObject.g_TableMgr.m_Stage.Get(_nStage);

        if (null == info)
            return;

        string strPath = "Prefabs/Stage/" + info.m_strBG;//캐릭터 의상 전체로 로드

        m_GOBG = Resources.Load(strPath) as GameObject;

        if (null == m_GOBG)
            return;

        m_GOBG = GameObject.Instantiate(m_GOBG, Vector3.zero, Quaternion.Euler(0f, 0f, 0f)) as GameObject;

        m_GOBG.transform.SetParent(TRBG);
        m_GOBG.transform.position = TRBG.position;
    }

    public void Mode(nsENUM.eSCENE _eScene)
    {
        int nCurStage = (SharedObject.g_SceneMgr.CurStage - 1) / (int)nsENUM.eVALUE.eVALUE_6;

        nCurStage += 1;

        SetBG(nCurStage);
        SetEffect(nCurStage);
    }
}