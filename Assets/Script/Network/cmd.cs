using System;
using System.Collections.Generic;

namespace Network
{
	class CPacketCmd
	{
        private const ushort m_CMD_CONNECT = 27068;
        public ushort CMD_CONNECT
        {
            get { return m_CMD_CONNECT; }
        }

		private const ushort m_CMD_IN_DISCONNECT = 32434;
		public ushort CMD_IN_DISCONNECT
		{
			get { return m_CMD_IN_DISCONNECT; }
		}

        private const ushort m_CMD_CL_LOGIN = 30809;
        public ushort CMD_CL_LOGIN
        {
            get { return m_CMD_CL_LOGIN; }
        }

        private const ushort m_CMD_LOGOUT = 31180;
        public ushort CMD_LOGOUT
        {
            get { return m_CMD_LOGOUT; }
        }

        private const ushort m_CMD_CL_VERSION = 35000;
        public ushort CMD_CL_VERSION
        {
            get { return m_CMD_CL_VERSION; }
        }

		private const ushort m_CMD_IN_ID = 14805;
		public ushort CMD_IN_ID
		{
			get { return m_CMD_IN_ID; }
		}

		private const ushort m_CMD_IN_CLOSE = 4286;
		public ushort CMD_IN_CLOSE
		{
			get { return m_CMD_IN_CLOSE; }
		}

		private const ushort m_CMD_IN_BROADCASTING = 9241;
		public ushort CMD_IN_BROADCASTING
		{
			get { return m_CMD_IN_BROADCASTING; }
		}

		private const ushort m_CMD_MSG = 6073;
		public ushort CMD_MSG
		{
			get { return m_CMD_MSG; }
		}

		private const ushort m_CMD_SEND_ALL = 21332;
		public ushort CMD_SEND_ALL
		{
			get { return m_CMD_SEND_ALL; }
		}

		private const ushort m_CMD_CL_LOGIN_ACK = 12871;
		public ushort CMD_CL_LOGIN_ACK
		{
			get { return m_CMD_CL_LOGIN_ACK; }
		}

		private const ushort m_CMD_CL_MATCHING = 15307;
		public ushort CMD_CL_MATCHING
		{
			get { return m_CMD_CL_MATCHING; }
		}

		private const ushort m_CMD_MATCHING_ACK = 8139;
		public ushort CMD_MATCHING_ACK
		{
			get { return m_CMD_MATCHING_ACK; }
		}

		private const ushort m_CMD_CL_ENTER_ZONE = 31321;
		public ushort CMD_CL_ENTER_ZONE
		{
			get { return m_CMD_CL_ENTER_ZONE; }
		}

		private const ushort m_CMD_ENTER_ZONE_ACK = 473;
		public ushort CMD_ENTER_ZONE_ACK
		{
			get { return m_CMD_ENTER_ZONE_ACK; }
		}

		private const ushort m_CMD_PC_LOGIN_INFO = 16328;
		public ushort CMD_PC_LOGIN_INFO
		{
			get { return m_CMD_PC_LOGIN_INFO; }
		}

		private const ushort m_CMD_PC_SITE_IN = 27855;
		public ushort CMD_PC_SITE_IN
		{
			get { return m_CMD_PC_SITE_IN; }
		}

		private const ushort m_CMD_CL_PC_MOVE = 2889;
		public ushort CMD_CL_PC_MOVE
		{
			get { return m_CMD_CL_PC_MOVE; }
		}

		private const ushort m_CMD_PC_MOVE_ACK = 9225;
		public ushort CMD_PC_MOVE_ACK
		{
			get { return m_CMD_PC_MOVE_ACK; }
		}

		private const ushort m_CMD_CL_PC_ATTACK_READY = 27646;
		public ushort CMD_CL_PC_ATTACK_READY
		{
			get { return m_CMD_CL_PC_ATTACK_READY; }
		}

		private const ushort m_CMD_PC_ATTACK_READY_ACK = 2366;
		public ushort CMD_PC_ATTACK_READY_ACK
		{
			get { return m_CMD_PC_ATTACK_READY_ACK; }
		}

		private const ushort m_CMD_CL_PC_ATTACK = 29610;
		public ushort CMD_CL_PC_ATTACK
		{
			get { return m_CMD_CL_PC_ATTACK; }
		}

		private const ushort m_CMD_PC_ATTACK_ACK = 20842;
		public ushort CMD_PC_ATTACK_ACK
		{
			get { return m_CMD_PC_ATTACK_ACK; }
		}

		private const ushort m_CMD_NPC_SITE_IN = 17629;
		public ushort CMD_NPC_SITE_IN
		{
			get { return m_CMD_NPC_SITE_IN; }
		}

		private const ushort m_CMD_NPC_ATTACK_READY = 31902;
		public ushort CMD_NPC_ATTACK_READY
		{
			get { return m_CMD_NPC_ATTACK_READY; }
		}

		private const ushort m_CMD_NPC_ATTACK = 3146;
		public ushort CMD_NPC_ATTACK
		{
			get { return m_CMD_NPC_ATTACK; }
		}

		private const ushort m_CMD_NPC_KNOCKBACK = 8281;
		public ushort CMD_NPC_KNOCKBACK
		{
			get { return m_CMD_NPC_KNOCKBACK; }
		}

		private const ushort m_CMD_NPC_MOVE = 31721;
		public ushort CMD_NPC_MOVE
		{
			get { return m_CMD_NPC_MOVE; }
		}

		private const ushort m_CMD_NPC_BATTLE_STATE = 11470;
		public ushort CMD_NPC_BATTLE_STATE
		{
			get { return m_CMD_NPC_BATTLE_STATE; }
		}

		private const ushort m_CMD_DIE = 28708;
		public ushort CMD_DIE
		{
			get { return m_CMD_DIE; }
		}

		private const ushort m_CMD_CL_PC_CHANGE = 27512;
		public ushort CMD_CL_PC_CHANGE
		{
			get { return m_CMD_CL_PC_CHANGE; }
		}

		private const ushort m_CMD_PC_CHANGE_ACK = 56;
		public ushort CMD_PC_CHANGE_ACK
		{
			get { return m_CMD_PC_CHANGE_ACK; }
		}

		private const ushort m_CMD_CL_CHAT = 5312;
		public ushort CMD_CL_CHAT
		{
			get { return m_CMD_CL_CHAT; }
		}

		private const ushort m_CMD_CHAT_ACK = 12864;
		public ushort CMD_CHAT_ACK
		{
			get { return m_CMD_CHAT_ACK; }
		}

		private const ushort m_CMD_CL_MUTANT_CARD = 4754;
		public ushort CMD_CL_MUTANT_CARD
		{
			get { return m_CMD_CL_MUTANT_CARD; }
		}

		private const ushort m_CMD_DB_LOGIN = 28080;
		public ushort CMD_DB_LOGIN
		{
			get { return m_CMD_DB_LOGIN; }
		}

	}
}

