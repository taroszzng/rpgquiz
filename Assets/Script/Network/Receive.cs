using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network
{
    interface IReceive
    {
        void DoRun(byte[] msg, int len);
    }

    class CReceive : MonoBehaviour
    {
        private Dictionary<ushort, IReceive> m_RecvList;

        public void Init()
        {
            CPacketCmd cmd = new CPacketCmd();

            m_RecvList = new Dictionary<ushort, IReceive>()
            {
                { cmd.CMD_CL_LOGIN_ACK, gameObject.AddComponent<CCMD_CL_LOGIN_ACK>() },
                { cmd.CMD_CL_VERSION, gameObject.AddComponent<CCMD_CL_VERSION_ACK>() },
                { cmd.CMD_LOGOUT, gameObject.AddComponent<CCMD_LOGOUT>() },
               
                //{ cmd.CMD_MATCHING_ACK, gameObject.AddComponent<CCMD_MATCHING_ACK>() },
                //{ cmd.CMD_ENTER_ZONE_ACK, gameObject.AddComponent<CCMD_ENTER_ZONE_ACK>() },
                //{ cmd.CMD_PC_LOGIN_INFO, gameObject.AddComponent<CCMD_PC_LOGIN_INFO>() },
                //{ cmd.CMD_PC_SITE_IN, gameObject.AddComponent<CCMD_PC_SITE_IN>() },
                //{ cmd.CMD_PC_MOVE_ACK, gameObject.AddComponent<CCMD_PC_MOVE_ACK>() },
                //{ cmd.CMD_PC_ATTACK_READY_ACK, gameObject.AddComponent<CCMD_PC_ATTACK_READY_ACK>() },
                //{ cmd.CMD_PC_ATTACK_ACK, gameObject.AddComponent<CCMD_PC_ATTACK_ACK>() },
                //{ cmd.CMD_NPC_SITE_IN, gameObject.AddComponent<CCMD_NPC_SITE_IN>() },
                //{ cmd.CMD_NPC_MOVE, gameObject.AddComponent<CCMD_NPC_MOVE>() },
                //{ cmd.CMD_NPC_KNOCKBACK, gameObject.AddComponent<CCMD_NPC_KNOCKBACK>() },
                //{ cmd.CMD_NPC_ATTACK_READY, gameObject.AddComponent<CCMD_NPC_ATTACK_READY>() },
                //{ cmd.CMD_NPC_ATTACK, gameObject.AddComponent<CCMD_NPC_ATTACK>() },
                //{ cmd.CMD_NPC_BATTLE_STATE, gameObject.AddComponent<CCMD_NPC_BATTLE_STATE>() },
                //{ cmd.CMD_DIE, gameObject.AddComponent<CCMD_DIE>() },
                //{ cmd.CMD_PC_CHANGE_ACK, gameObject.AddComponent<CCMD_PC_CHANGE_ACK>() },
                //{ cmd.CMD_CHAT_ACK, gameObject.AddComponent<CCMD_CHAT_ACK>() },
            };
        }

        public void Parse(byte[] msg)
        {
            int len = (int)BitConverter.ToUInt16(msg, 0);
            ushort cmd = BitConverter.ToUInt16(msg, 2);

            if (m_RecvList.ContainsKey(cmd))
            {
                m_RecvList[cmd].DoRun(msg, len);
            }
            else
            {
                Debug.Log("cmd error " + cmd);
            }
        }
    }

    class CCMD_CL_LOGIN_ACK : MonoBehaviour, IReceive
    {
        public void DoRun(byte[] msg, int len)
        {
            //PacketCMD_CL_LOGIN_ACK packet = new PacketCMD_CL_LOGIN_ACK();

            //packet.ReadSet(msg, len);
            //packet.Read();

            //if (0 == packet.get_flag())
            //{
            //    if (null == SharedObject.g_Main)
            //        return;

            //    SharedObject.g_Main.GOLogin.SetActive(false);

            //    SharedObject.g_Main.CheckPrologue();
            //}
        }
    }

    class CCMD_LOGOUT : MonoBehaviour, IReceive
    {
        public void DoRun(byte[] msg, int len)
        {
            PacketCMD_LOGOUT packet = new PacketCMD_LOGOUT();

            packet.ReadSet(msg, len);
            packet.Read();
            //int unique_id = (int)packet.get_unique_id();
            //Pc unit = ObjectManager.Instance.Get(unique_id) as Pc;
            //Destroy(unit.gameObject);
            //ObjectManager.Instance.Remove(unique_id);
        }
    }

    class CCMD_CL_VERSION_ACK : MonoBehaviour, IReceive
    {
        public void DoRun(byte[] msg, int len)
        {
            //PacketCMD_CL_VERSION_ACK packet = new PacketCMD_CL_VERSION_ACK();

            //packet.ReadSet(msg, len);
            //packet.Read();

            //bool bPass = true;

            //if (0 < packet.GetVersion())
            //{
            //    if (SharedObject.g_nVersion < packet.GetVersion())//구글 apk 업데이트 구현
            //        SharedObject.g_Main.OnClientUpdate();
            //}

            //if (bPass)
            //{
            //    SharedObject.g_Main.GOLogin.SetActive(false);

            //    SharedObject.g_Main.CheckPrologue();

            //    Debug.Log("서버 패킷 CCMD_CL_VERSION_ACK");
            //}
        }
    }

    //class CCMD_MATCHING_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        Application.LoadLevel("test");
    //    }
    //}

    //class CCMD_ENTER_ZONE_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_ENTER_ZONE_ACK packet = new PacketCMD_ENTER_ZONE_ACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        int unique_id = (int)packet.get_unique_id();
    //        int table_id = (int)packet.get_table_id();
    //        Vector3 pos = new Vector3();
    //        pos.Set(packet.get_x(), packet.get_y(), packet.get_z());
    //        PlayData.Instance.MyPc = ObjectManager.Instance.CreatePc(unique_id, table_id, pos);
    //        PlayData.Instance.MyPc.AddCharacterController();
    //        PlayData.Instance.MyPc.ChangeFlag = 0;
    //        PlayData.Instance.MainHud = Instantiate(Resources.Load("prefab/hud/MainHud")) as GameObject;
    //        GameObject.Find("main").GetComponent<main>().SetLoading(false);
    //    }
    //}

    //class CCMD_PC_LOGIN_INFO : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_PC_LOGIN_INFO packet = new PacketCMD_PC_LOGIN_INFO();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        PlayData.Instance.MyPc.MaxHp = packet.get_max_hp();
    //        PlayData.Instance.MyPc.Hp = packet.get_hp();
    //        PlayData.Instance.MyPc.Speed = packet.get_speed();
    //        PlayData.Instance.MyPc.Login();
    //        PlayData.Instance.CameraControl.GetComponent<CameraControl>().Set();
    //    }
    //}

    //class CCMD_PC_SITE_IN : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_PC_SITE_IN packet = new PacketCMD_PC_SITE_IN();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Vector3 pos = new Vector3();
    //        int n = 0;
    //        while (true)
    //        {
    //            ushort unique_id = packet.get_unique_id(n);
    //            if (0xffff == unique_id)
    //            {
    //                break;
    //            }
    //            ushort table_id = packet.get_table_id(n);
    //            pos.Set(packet.get_x(n), packet.get_y(n), packet.get_z(n));
    //            Pc unit = ObjectManager.Instance.CreatePc((int)unique_id, (int)table_id, pos);
    //            unit.MaxHp = packet.get_max_hp(n);
    //            unit.Hp = packet.get_hp(n);
    //            unit.Speed = packet.get_speed(n);
    //            unit.ChangeFlag = ReadData.Instance.GameData.PcData.Get(table_id).chanagemode;
    //            n++;
    //        }
    //    }
    //}

    //class CCMD_PC_MOVE_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_PC_MOVE_ACK packet = new PacketCMD_PC_MOVE_ACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Pc other_pc = ObjectManager.Instance.Get((int)packet.get_unique_id()) as Pc;
    //        if (null == other_pc)
    //        {
    //            return;
    //        }
    //        other_pc.m_end_pos.Set(packet.get_x(), packet.get_y(), packet.get_z());
    //        other_pc.Move();
    //    }
    //}

    //class CCMD_PC_ATTACK_READY_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_PC_ATTACK_READY_ACK packet = new PacketCMD_PC_ATTACK_READY_ACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Pc attacker = ObjectManager.Instance.Get((int)packet.get_attacker_id()) as Pc;
    //        if (null == attacker)
    //        {
    //            return;
    //        }
    //        attacker.AttackType = (int)packet.get_attack_type();
    //        attacker.Attack((int)packet.get_skill_id());
    //    }
    //}

    //class CCMD_PC_ATTACK_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_PC_ATTACK_ACK packet = new PacketCMD_PC_ATTACK_ACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Actor attacker = ObjectManager.Instance.Get((int)packet.get_attacker_id()) as Actor;
    //        Actor defender = ObjectManager.Instance.Get((int)packet.get_defender_id()) as Actor;
    //        if (defender)
    //        {
    //            defender.Damage(attacker, packet.get_damage(), (int)packet.get_skill_id());
    //        }
    //    }
    //}

    //class CCMD_NPC_SITE_IN : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_NPC_SITE_IN packet = new PacketCMD_NPC_SITE_IN();
    //        packet.ReadSet(msg, len);
    //        packet.Read();

    //        Vector3 pos = new Vector3();
    //        int n = 0;
    //        while (true)
    //        {
    //            ushort unique_id = packet.get_unique_id(n);
    //            if (0xffff == unique_id)
    //            {
    //                break;
    //            }
    //            int table_id = (int)packet.get_table_id(n);
    //            pos.Set(packet.get_x(n), packet.get_y(n), packet.get_z(n));
    //            Npc unit = ObjectManager.Instance.CreateNpc((int)unique_id, table_id, pos);
    //            //Debug.Log(packet.get_x(n) + "/" + packet.get_y(n) + "/" + packet.get_z(n));
    //            unit.MaxHp = packet.get_max_hp(n);
    //            unit.Hp = packet.get_hp(n);
    //            if (1 == packet.get_flag(n))
    //            {
    //                GameObject effect = ObjectManager.Instance.CreateGameObject("SpawnEffect");
    //                effect.transform.position = unit.transform.position;
    //                ParticleManager.Instance.Add(effect);
    //            }
    //            n++;
    //        }
    //    }
    //}

    //class CCMD_NPC_MOVE : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_NPC_MOVE packet = new PacketCMD_NPC_MOVE();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Npc unit = ObjectManager.Instance.Get((int)packet.get_unique_id()) as Npc;
    //        if (null == unit)
    //        {
    //            return;
    //        }
    //        unit.Speed = packet.get_speed();
    //        unit.m_end_pos.x = packet.get_x();
    //        unit.m_end_pos.y = packet.get_y();
    //        unit.m_end_pos.z = packet.get_z();
    //        unit.Move();
    //    }
    //}

    //class CCMD_NPC_KNOCKBACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_NPC_KNOCKBACK packet = new PacketCMD_NPC_KNOCKBACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Npc unit = ObjectManager.Instance.Get((int)packet.get_unique_id()) as Npc;
    //        if (null == unit)
    //        {
    //            return;
    //        }
    //        unit.m_end_pos.x = packet.get_x();
    //        unit.m_end_pos.y = packet.get_y();
    //        unit.m_end_pos.z = packet.get_z();
    //        unit.Knockback();
    //    }
    //}

    //class CCMD_NPC_ATTACK_READY : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_NPC_ATTACK_READY packet = new PacketCMD_NPC_ATTACK_READY();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Actor defender = ObjectManager.Instance.Get((int)packet.get_defender_id()) as Actor;
    //        Actor attacker = ObjectManager.Instance.Get((int)packet.get_attacker_id()) as Actor;
    //        //if (defender)
    //        //{
    //        //    defender.Damage(attacker, packet.get_damage(), (int)packet.get_skill_id());
    //        //}
    //        if (attacker)
    //        {
    //            attacker.AttackTarget = defender;
    //            attacker.Attack((int)packet.get_skill_id());
    //        }
    //    }
    //}

    //class CCMD_NPC_ATTACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_NPC_ATTACK packet = new PacketCMD_NPC_ATTACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Actor defender = ObjectManager.Instance.Get((int)packet.get_defender_id()) as Actor;
    //        Actor attacker = ObjectManager.Instance.Get((int)packet.get_attacker_id()) as Actor;
    //        if (defender)
    //        {
    //            defender.Damage(attacker, packet.get_damage(), (int)packet.get_skill_id());
    //        }
    //        //if (attacker)
    //        //{
    //        //    attacker.Attack((int)packet.get_skill_id());
    //        //}
    //    }
    //}

    //class CCMD_NPC_BATTLE_STATE : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_NPC_BATTLE_STATE packet = new PacketCMD_NPC_BATTLE_STATE();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Actor unit = ObjectManager.Instance.Get((int)packet.get_unique_id()) as Actor;
    //        if (unit)
    //        {
    //            unit.BattleState = (int)packet.get_state();
    //        }
    //    }
    //}

    //class CCMD_DIE : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_DIE packet = new PacketCMD_DIE();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Actor unit = ObjectManager.Instance.Get((int)packet.get_unique_id()) as Actor;
    //        if (unit)
    //        {
    //            unit.Die();
    //        }
    //    }
    //}

    //class CCMD_PC_CHANGE_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_PC_CHANGE_ACK packet = new PacketCMD_PC_CHANGE_ACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        Pc unit = ObjectManager.Instance.Get((int)packet.get_unique_id()) as Pc;
    //        if (unit)
    //        {
    //            unit.ChangeMode(packet.get_table_id(), packet.get_speed());
    //        }
    //    }
    //}

    //class CCMD_CHAT_ACK : MonoBehaviour, IReceive
    //{
    //    public void DoRun(byte[] msg, int len)
    //    {
    //        PacketCMD_CHAT_ACK packet = new PacketCMD_CHAT_ACK();
    //        packet.ReadSet(msg, len);
    //        packet.Read();
    //        PlayData.Instance.ChatProcess.Add(packet.get_send_nick(), packet.get_chat_msg());
    //    }
    //}
}
