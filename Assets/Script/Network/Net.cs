﻿using UnityEngine;
using System.Collections;
using Network;

public class Net : Singleton<Net>
{
    private CClientSocket m_client_socket = new CClientSocket();
    private CReceive m_recv;

    public void Init()
    {
        m_recv = gameObject.AddComponent<CReceive>();

        Debug.Log("Net = " + m_recv);

        m_client_socket.Init();

        Debug.Log(" ClientSocket = " + m_client_socket);

        m_recv.Init();
    }

    public bool Connect(string ip, int port)
    {
        Debug.Log(" ip = " + ip + " port = " +port);

        return m_client_socket.Connect(ip, port);
    }

    public void Close()
    {
        Debug.Log(" Net Close " );

        m_client_socket.Close();
    }

    public void Send(byte[] msg, int len)
    {
        m_client_socket.Send(msg, len);
    }

    public bool Update()
    {
        if (false == m_client_socket.Recv())
        {
            return false;
        }

        foreach (byte[] packet in m_client_socket.MsgList)
        {
            m_recv.Parse(packet);
        }

        m_client_socket.MessageClear();

        return true;
    }
}
