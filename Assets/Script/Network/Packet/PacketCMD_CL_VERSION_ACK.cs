using System;
using System.Collections.Generic;
using System.Text;

namespace Network
{
    class PacketCMD_CL_VERSION_ACK : CPacketBase
    {
        private ushort m_Version; // attacker unique id

        public ushort GetVersion()
        {
            return m_Version;
        }

        public bool Read()
        {
            ReadElement(out m_Version);

            if (FaultProperty)
            {
                return false;
            }

            return true;
        }
    };
};
