using System;
using System.Collections.Generic;
using System.Text;

namespace Network
{
    class PacketCMD_LOGOUT : CPacketBase
    {
        private ushort unique_id; // unique id

        public ushort get_unique_id()
        {
            return unique_id;
        }

        public bool Read()
        {
            ReadElement(out unique_id);

            if (FaultProperty)
            {
                return false;
            }

            return true;
        }
    };
};
