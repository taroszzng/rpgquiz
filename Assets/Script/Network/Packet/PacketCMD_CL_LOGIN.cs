using System;
using System.Collections.Generic;
using System.Text;

namespace Network
{
    class PacketCMD_CL_LOGIN : CPacketBase
    {
        private string m_strEmail;
        //private string id;
        //private string pw;

        public void SetEmail(string _strEmail)
        {
            int _len = Encoding.Default.GetByteCount(_strEmail);

            if (_len >= 50 || _len <= 0)
            {
                FaultProperty = true;
                return;
            }

            m_strEmail = _strEmail;
        }

        //public void set_id(string _id)
        //{
        //    int _len = Encoding.Default.GetByteCount(_id);

        //    if(_len >= 30 || _len <= 0)
        //    {
        //        FaultProperty = true;
        //        return;
        //    }
        //    id = _id;
        //}

        //public void set_pw(string _pw)
        //{
        //    int _len = Encoding.Default.GetByteCount(_pw);

        //    if(_len >= 30 || _len <= 0)
        //    {
        //        FaultProperty = true;
        //        return;
        //    }
        //    pw = _pw;
        //}

        public bool Write()
        {
            WriteElement(m_strEmail);
            //WriteElement(id);
            //WriteElement(pw);

            if (FaultProperty)
                return false;

            WriteHeader();

            return true;
        }
    };
};
