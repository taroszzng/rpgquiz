﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Network
{
    class CClientSocket
    {
        struct RECV_PARSER
        {
            public int head;
            public int tail;

            public void Clear()
            {
                head = 0;
                tail = 0;
            }
            public int GetInterval()
            {
                return tail - head;
            }
        }
        private RECV_PARSER m_RecvParser;

        private const int RECV_SIZE = 1024 * 8;
        private const int RECV_CRITICAL_SIZE = RECV_SIZE / 8;
        private const int HEADER_SIZE = 6;
        private Socket m_Socket;
        private byte[] m_RecvBuf = new byte [RECV_SIZE];
        private List<byte[]> m_MsgList = new List<byte[]>();

        public Socket Socket
        {
            get { return m_Socket; }
        }

        public List<byte[]> MsgList
        {
            get { return m_MsgList; }
        }

        public void Init()
        {
            m_RecvParser.Clear();
            m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public bool Connect(string ip, int port)
        {
            IPEndPoint ip_endpt = new IPEndPoint(IPAddress.Parse(ip), port);

            try
            {
                m_Socket.Connect(ip_endpt);
                m_Socket.Blocking = false;
                m_Socket.NoDelay = false;
                //m_Socket.ReceiveBufferSize = 8192;
                //m_Socket.SendBufferSize = 8192;

                byte[] msg = new byte[10];
                Send(msg, 10);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool Recv()
        {
            SocketError err;

            int count = m_Socket.Receive(m_RecvBuf, m_RecvParser.tail, RECV_SIZE - m_RecvParser.tail, SocketFlags.None, out err);

            if (count > 0)
            {
                m_RecvParser.tail += count;
            }
            else
            {
                return false;
            }

            while(true)
            {
                ushort len = BitConverter.ToUInt16(m_RecvBuf, m_RecvParser.head);
                int interval = m_RecvParser.GetInterval();

                if (interval < 0)
                {
                    return false;
                }
                if (interval < HEADER_SIZE || len > interval)
                {
                    break;
                }

                byte[] temp_buf = new byte[512];
                Array.Copy(m_RecvBuf, m_RecvParser.head, temp_buf, 0, len);
                m_MsgList.Add(temp_buf);

                m_RecvParser.head += len;

                if (m_RecvParser.tail == m_RecvParser.head)
                {
                    m_RecvParser.Clear();
                    return true;
                }
                else if (m_RecvParser.head >= RECV_CRITICAL_SIZE)
                {
                    interval = m_RecvParser.GetInterval();
                    if (interval < m_RecvParser.head)
                    {
                        if (interval > 0)
                        {
                            Array.Copy(m_RecvBuf, m_RecvParser.head, m_RecvBuf, 0, interval);
                            m_RecvParser.head = 0;
                            m_RecvParser.tail = interval;
                        }
                        else
                        {
                            m_RecvParser.Clear();
                        }
                    }
                }
            }
            return true;
        }

        public void Send(byte[] msg, int len)
        {
            m_Socket.Send(msg, len, 0);
        }

        public void Close()
        {
            m_Socket.Close();
        }

        public void MessageClear()
        {
            m_MsgList.Clear();
        }
    }
}
