﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Network
{
    class CPacketBase
    {
        private const int MAX_PACKET_SIZE = 1024 * 8;
        private byte[] m_Buf = new byte[MAX_PACKET_SIZE];
        private int m_Size;
        private int m_Pos;

        enum PACKET_TYPE : int
        {
            STATIC = 0,
            VARIABLE,
            COMPRESS
        }

        enum PACKET_PASS : int
        {
            NOT_PASS = 0,
            PASS
        }

        enum PACKET_ENCRYPT : int
        {
            DECRYPT = 0,	// 암호화 안됨
            ENCRYPT			// 암호화 됨
        }

        public ushort CmdProperty
        {
            get; set;
        }

        public bool FaultProperty
        {
            get; set;
        }

        public byte[] BufProperty
        {
            get { return m_Buf; }
        }

        public int PosProperty
        {
            get { return m_Pos; }
        }

        public int SizeProperty
        {
            get { return m_Size; }
            set { m_Size = value;  }
        }

        public int GetRestSize()
        {
            if (SizeProperty < PosProperty) return 0;
            return SizeProperty - PosProperty;
        }

        public void ReadSet(byte[] pBuf, int len)
        {
            m_Buf = pBuf;
            m_Size = len;   // header size를 제외한 메세지 크기
            m_Pos = 6;      // header size는 제외하고 읽는다.
            FaultProperty = false;
        }

        public void WriteSet()
        {
            m_Size = 6;
            m_Pos = 6; // header size 이후부터 데이터 입력
            FaultProperty = false;
        }

        public void Copy(CPacketBase arg)
        {
            m_Size = arg.SizeProperty;
            m_Pos = arg.PosProperty;
            m_Buf = arg.BufProperty;
            FaultProperty = arg.FaultProperty;
        }

        // read
        public void ReadElement(out sbyte arg)
        {
            if (m_Pos < 0 || m_Pos >= m_Size)
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            arg = (sbyte)m_Buf[m_Pos++];
        }

        public void ReadElement(out byte arg)
        {
            if (m_Pos < 0 || m_Pos >= m_Size)
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            arg = m_Buf[m_Pos++];
        }

        public void ReadElement(out short arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(short)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            short val = BitConverter.ToInt16(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(short);
        }

        public void ReadElement(out ushort arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(ushort)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            ushort val = BitConverter.ToUInt16(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(ushort);
        }

        public void ReadElement(out int arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(int)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            int val = BitConverter.ToInt32(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(int);
        }

        public void ReadElement(out uint arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(uint)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            uint val = BitConverter.ToUInt32(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(uint);
        }

        public void ReadElement(out float arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(float)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }

            float val = (float)BitConverter.ToSingle(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(float);
        }

        public void ReadElement(out long arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(long)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            long val = BitConverter.ToInt64(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(long);
        }

        public void ReadElement(out ulong arg)
        {
            if (m_Pos < 0 || m_Size < (m_Pos + sizeof(ulong)))
            {
                FaultProperty = true;
                arg = 0;
                return;
            }
            ulong val = BitConverter.ToUInt64(m_Buf, m_Pos);
            arg = val;
            m_Pos += sizeof(ulong);
        }

        public void ReadElement(out string buf)
        {
            if (m_Pos < 0)
            {
                FaultProperty = true;
                buf = "";
                return;
            }

            short len;
            ReadElement(out len);

            if (0 == len)
            {
                buf = "";
                return;
            }

            if (len < 0 || (len + m_Pos) > m_Size)
            {
                FaultProperty = true;
                buf = "";
                return;
            }

            byte[] temp = new byte[len];
            Array.Copy(m_Buf, m_Pos, temp, 0, len);
            buf = Encoding.Default.GetString(temp);
            m_Pos += len;
        }

        // write
        public void WriteElement(sbyte arg)
        {
            if (m_Pos >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            m_Buf[m_Pos++] = (byte)arg;
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(byte arg)
        {
            if (m_Pos >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            m_Buf[m_Pos++] = arg;
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(short arg)
        {
            if ((m_Pos + sizeof(short)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(short));

            m_Pos += sizeof(short);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(ushort arg)
        {
            if ((m_Pos + sizeof(ushort)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(ushort));

            m_Pos += sizeof(ushort);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(int arg)
        {
            if ((m_Pos + sizeof(int)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(int));

            m_Pos += sizeof(int);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(uint arg)
        {
            if ((m_Pos + sizeof(uint)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(uint));

            m_Pos += sizeof(uint);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(float arg)
        {
            if ((m_Pos + sizeof(float)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(float));

            m_Pos += sizeof(float);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(long arg)
        {
            if ((m_Pos + sizeof(long)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(long));

            m_Pos += sizeof(long);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(ulong arg)
        {
            if ((m_Pos + sizeof(ulong)) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            byte[] val = BitConverter.GetBytes(arg);
            Array.Copy(val, 0, m_Buf, m_Pos, sizeof(ulong));

            m_Pos += sizeof(ulong);
            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteElement(string buf)
        {
            int _len = Encoding.Default.GetBytes(buf).Length;
            if (_len < 0)
            {
                return;
            }

            WriteElement((ushort)_len);
            if (0 == _len)
            {
                return;
            }
            if (FaultProperty)
            {
                return;
            }
            if ((m_Pos + _len) >= MAX_PACKET_SIZE)
            {
                FaultProperty = true;
                return;
            }
            Array.Copy(Encoding.Default.GetBytes(buf), 0, m_Buf, m_Pos, _len);
            m_Pos += _len;

            if (m_Size < m_Pos)
            {
                m_Size = m_Pos;
            }
        }

        public void WriteHeader()
        {
            byte[] val = BitConverter.GetBytes((ushort)m_Size);
            Array.Copy(val, 0, m_Buf, 0, sizeof(ushort));
            val = BitConverter.GetBytes(CmdProperty);
            Array.Copy(val, 0, m_Buf, 2, sizeof(ushort));
        }
    }
}
