﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum eTUTORIAL
{
    eTUTORIAL_NONE,
    eTUTORIAL_INVEN,
    eTUTORIAL_BATTLE,
    eTUTORIAL_END
}

public partial class Tutorial : MonoBehaviour
{
    Transform m_Canvas;
    bool m_Trigger = false;

    eTUTORIAL m_eCurTutorial = eTUTORIAL.eTUTORIAL_NONE;
    eTUTORIAL m_eInitTutorial = eTUTORIAL.eTUTORIAL_NONE;

    public bool IsTutorial() { return m_eCurTutorial != eTUTORIAL.eTUTORIAL_NONE; }
    public eTUTORIAL GetTutorial() { return m_eCurTutorial; }

    void Awake()
    {
        if (SharedObject.g_Tutorial == null)
        {
            SharedObject.g_Tutorial = this;

            m_Canvas = transform;
        }
    }

    public void InitTutorial()
    {
        m_eInitTutorial = SharedObject.g_PlayerPrefsData.Tutorial;
    }

    public void PlayTutorial(eTUTORIAL _e)
    {
        if (m_eCurTutorial == _e)
            return;

        if (SharedObject.g_PlayerPrefsData.Tutorial != _e)
            return;

        switch (_e)
        {
            case eTUTORIAL.eTUTORIAL_INVEN:
                StartCoroutine("Inven");//가방
                break;
            case eTUTORIAL.eTUTORIAL_BATTLE:
                StartCoroutine("Battle");//전투
                break;
            default:
                assert_cs.msg("default : " + _e);
                break;
        }
    }

    public void StopTutorial(eTUTORIAL _e)
    {
        switch (_e)
        {
            case eTUTORIAL.eTUTORIAL_INVEN:
                StopCoroutine("Inven");
                break;
            case eTUTORIAL.eTUTORIAL_BATTLE:
                StopCoroutine("Battle");
                break;
            default:
                assert_cs.msg("default : " + _e);
                break;
        }

        m_eCurTutorial = eTUTORIAL.eTUTORIAL_NONE;
    }

    public void OnPopupBtn()
    {
        if (SharedObject.g_PlayerPrefsData.Tutorial != m_eCurTutorial)
            return;

        m_Trigger = true;
    }

    void TutorialStart(eTUTORIAL _eCur, bool _bLockRayCast = true)
    {
        m_eCurTutorial = _eCur;
    }

    void TutorialEnd()
    {
        m_eCurTutorial = eTUTORIAL.eTUTORIAL_NONE;
    }

    IEnumerator Popup(string _str)
    {
        string strPath = "Prefabs/Tutorial/" + _str;

        m_Trigger = false;

        GameObject obj = Resources.Load(strPath) as GameObject;

        if (null == obj)
            yield break;

        obj = GameObject.Instantiate(obj) as GameObject;
        obj.transform.SetParent(m_Canvas, false);

        while (m_Trigger == false)
        {
            yield return new WaitForFixedUpdate();
        }

        GameObject.DestroyImmediate(obj);
    }

    IEnumerator Inven()//
    {
        TutorialStart(eTUTORIAL.eTUTORIAL_INVEN, false);

        yield return StartCoroutine(Popup("Inven_1"));

        yield return new WaitForFixedUpdate();

        yield return StartCoroutine(Popup("Inven_2"));

        yield return new WaitForFixedUpdate();

        yield return StartCoroutine(Popup("Inven_3"));

        yield return new WaitForFixedUpdate();

        SharedObject.g_PlayerPrefsData.Tutorial = eTUTORIAL.eTUTORIAL_BATTLE;

        TutorialEnd();

        PlayTutorial(eTUTORIAL.eTUTORIAL_BATTLE);
    }

    IEnumerator Battle()//
    {
        TutorialStart(eTUTORIAL.eTUTORIAL_BATTLE, false);

        if(eTUTORIAL.eTUTORIAL_BATTLE != m_eInitTutorial)
            yield return StartCoroutine(Popup("Battle_1"));

        yield return StartCoroutine(Popup("Battle_2"));

        yield return new WaitForFixedUpdate();

        yield return StartCoroutine(Popup("Battle_3"));

        yield return new WaitForFixedUpdate();

        yield return StartCoroutine(Popup("Battle_4"));


        SharedObject.g_PlayerPrefsData.Tutorial = eTUTORIAL.eTUTORIAL_END;

        PlayTutorial(eTUTORIAL.eTUTORIAL_BATTLE);

        TutorialEnd();
    }
}
