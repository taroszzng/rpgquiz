﻿using UnityEngine;
using UnityEngine.UI;

public class UI_Damage : MonoBehaviour
{
    public Text TEXTDmg;

    public void SetDamage(int _nDmg, int _nPlus)
    {
        if (null == TEXTDmg)
            return;

        string dmg = _nDmg.ToString();
        string plus = "";

        if(0 < _nPlus)
            plus = " + " + _nPlus.ToString();

        string str = dmg + plus;

        TEXTDmg.text = str;
    }

    public void EventClose()
    {
        transform.gameObject.SetActive(false);
    }
}
