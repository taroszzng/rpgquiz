﻿using UnityEngine;
using UnityEngine.UI;

public class UI_StageList : MonoBehaviour
{
    public GameObject GOLock;
    public GameObject GOClear;
    public GameObject GOOpen;

    public Button BTNSelect;

    public Text TEXTStage;
    public Text TEXTNum;

    Table_Stage.Info m_Info;

    public void SetStage(Table_Stage.Info _Info)
    {
        m_Info = _Info;

        CheckStage();
    }

    public void CheckStage()
    {
        string strstage = SharedObject.g_TableMgr.m_Lan.Get(m_Info.m_nName).m_strDec;

        TEXTStage.text = strstage;
        TEXTNum.text = m_Info.m_nID.ToString();

        bool bclear = false;
        bool block = true;
        bool bopen = false;

        if (m_Info.m_nComp < SharedObject.g_PlayerPrefsData.ClearStage)
        {
            bclear = true;
            block = false;
        }
        else
        {
            if (m_Info.m_nComp >= SharedObject.g_PlayerPrefsData.ClearStage &&
                (m_Info.m_nComp - (int)nsENUM.eVALUE.eVALUE_6) < SharedObject.g_PlayerPrefsData.ClearStage)
            {
                block = false;
                bopen = true;
            }
        }

        if (nsENUM.eSTAGECOMP.eSTAGECOMP_NONE == (nsENUM.eSTAGECOMP)m_Info.m_nCompType)
            bclear = false;

        if (block || bclear)
        {
            if (SharedObject.g_PlayerPrefsData.Ending)
                BTNSelect.interactable = true;
            else
                BTNSelect.interactable = false;
        }
        else
            BTNSelect.interactable = true;

        GOClear.SetActive(bclear);
        GOLock.SetActive(block);
        GOOpen.SetActive(bopen);
    }

    public void OnBtnSelect()
    {
        if (SharedObject.g_PlayerPrefsData.Ending)
            SharedObject.g_SceneMgr.StageSelect = m_Info.m_nID;

        SharedObject.g_Mode.SetMonsterSelect(true);
    }
}
