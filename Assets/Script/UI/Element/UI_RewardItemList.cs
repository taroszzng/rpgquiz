﻿using UnityEngine;
using UnityEngine.UI;

public class UI_RewardItemList : MonoBehaviour
{
    public Image IMGIcon;
    public Text TEXTValue;

    int m_nItemID;

    public void InitRewardItem(int _nItemID, int _nIcon, int _nValue)
    {
        m_nItemID = _nItemID;

        if (null != IMGIcon)
            IMGIcon.sprite = SharedObject.g_SceneMgr.CreateItemIcon(_nIcon);

        if (null != TEXTValue)
            TEXTValue.text = _nValue.ToString();
    }

    public void OnBtnSelect()
    {
        SharedObject.g_Main.ItemToolTip(m_nItemID);
    }
}
