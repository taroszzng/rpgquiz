﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_MonsterSelectList : MonoBehaviour
{
    public GameObject GOLock;
    public GameObject GOClear;
    public GameObject GOFaceLock;

    public Image IMGIcon;

    public Text TEXTLv;
    public Text TEXTName;
    public Text TEXTStat;

    public Button BTNSelect;

    int m_nMonID;

    public void SetMonsterSelect(int _nMonID)
    {
        m_nMonID = _nMonID;

        CheckMonsterSelect();
    }

    public void CheckMonsterSelect()//만들어진후 검사
    {
        Table_Monster.Info info = (Table_Monster.Info)SharedObject.g_TableMgr.m_Monster.Get(m_nMonID);

        if (null == info)
            return;

        if (null != IMGIcon)
            IMGIcon.sprite = SharedObject.g_SceneMgr.CreateMonsterIcon(info.m_nIcon);

        string str = SharedObject.g_TableMgr.m_Lan.Get(1300409).m_strDec;

        str = string.Format(str, info.m_nLevel);

        if(null != TEXTLv)
            TEXTLv.text = str;

        str = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;

        if (null != TEXTName)
            TEXTName.text = str;

        if(null != TEXTStat)
            TEXTStat.text = info.m_fAtk.ToString();

        bool bclear = false;
        bool block = true;
        bool btn = false;
        bool bface = false;

        if (info.m_nLevel < SharedObject.g_PlayerPrefsData.ClearStage)
        {
            bclear = true;
            block = false;
            btn = false;
        }
        else
        {
            if (info.m_nLevel > SharedObject.g_PlayerPrefsData.ClearStage)
            {
                btn = false;
                bface = true;
            }
            else
            {
                block = false;
                btn = true;
            }
        }

        if (null != BTNSelect)
        {
            if (SharedObject.g_PlayerPrefsData.Ending)
                BTNSelect.interactable = true;
            else
                BTNSelect.interactable = btn;
        }

        if (null != GOClear)
            GOClear.SetActive(bclear);

        if (null != GOLock)
            GOLock.SetActive(block);

        if (null != GOFaceLock)
            GOFaceLock.SetActive(bface);
    }

    public void OnBtnSelect()
    {
        if (SharedObject.g_PlayerPrefsData.Ending)
        {
            SharedObject.g_SceneMgr.MonsterSelect = m_nMonID;

            if (nsENUM.eSCENE.eSCENE_MODE == SharedObject.g_SceneMgr.Scene)
                SharedObject.g_SceneMgr.ChangeScene(nsENUM.eSCENE.eSCENE_MODE);
            else
                SharedObject.g_CollectorMgr.Mode(nsENUM.eSCENE.eSCENE_MODE);

            StartCoroutine(ModeSelect());
        }
        else
        {
            if (nsENUM.eSCENE.eSCENE_MODE == SharedObject.g_SceneMgr.Scene)
                SharedObject.g_SceneMgr.ChangeScene(nsENUM.eSCENE.eSCENE_MODE);
            else
                SharedObject.g_CollectorMgr.Mode(nsENUM.eSCENE.eSCENE_MODE);

            SharedObject.g_Mode.AModeSelect();//선택
        }
    }

    IEnumerator ModeSelect()
    {
        yield return new WaitForSeconds(1f);

        SharedObject.g_Mode.AModeSelect();//선택
    }
}
