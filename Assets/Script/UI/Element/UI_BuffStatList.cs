﻿using UnityEngine;
using UnityEngine.UI;

public class UI_BuffStatList : MonoBehaviour
{
    public Image IMGIcon;
    public Text TEXTName;
    public Text TEXTPlus;
    public Text TEXTApply;

    int m_nItemID;

    public int ItemID
    {
        get { return m_nItemID; }
    }

    public void SetBuffStat(Item _item)
    {
        m_nItemID = _item.m_nItemID;

        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(m_nItemID);

        if (null == info)
            return;

        if (null != IMGIcon)
            IMGIcon.sprite = SharedObject.g_SceneMgr.CreateItemIcon(info.m_nIcon);

        string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;

        float fability = info.m_fAbility * 100;

        string strplus = "+" + fability.ToString() + "%";

        if(null != TEXTName)
            TEXTName.text = strname;

        if (null != TEXTPlus)
            TEXTPlus.text = strplus;

        int nlan = 1300399 + info.m_nApply;

        string strapply = SharedObject.g_TableMgr.m_Lan.Get(nlan).m_strDec;

        if (null != TEXTApply)
            TEXTApply.text = strapply;
    }
}
