﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public partial class UI_PauseExit : MonoBehaviour
{
    public GameObject GOCancel;

    public Text TEXTLan;
    public Text TEXTOK;

    public void SetPause(bool _bActive)
    {
        SharedObject.g_SceneMgr.Pause = _bActive;

        SharedObject.g_CollectorMgr.SetPause(_bActive);

        int nLan = 1380003;//게임종료
        int nOk = 1370001;

        if (_bActive)
            nLan = 1390002;

        Table_Lan.Info info = SharedObject.g_TableMgr.m_Lan.Get(nLan);

        if (null != info)
            TEXTLan.text = info.m_strDec;

        info = SharedObject.g_TableMgr.m_Lan.Get(nOk);

        TEXTOK.text = info.m_strDec;

        GOCancel.SetActive(!SharedObject.g_SceneMgr.Pause);
    }

    void Pause()
    {
        SharedObject.g_SceneMgr.Pause = false;
        SharedObject.g_SceneMgr.GameExit = false;

        transform.gameObject.SetActive(false);

        SharedObject.g_CollectorMgr.SetPause(false);
    }

    public void OnBtnGameExitOk()
    {
        if (!SharedObject.g_SceneMgr.Pause)
            SharedObject.g_SceneMgr.Shutdown();
        else
        {
            if (SharedObject.g_SceneMgr.GameExit)//게임종료 전단계
                SetPause(false);
            else
                Pause();
        }
    }

    public void OnBtnGameExitCancel()
    {
        Pause(); 
    }
}