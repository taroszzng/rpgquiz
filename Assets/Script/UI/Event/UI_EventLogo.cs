﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Network;

public class UI_EventLogo : MonoBehaviour
{
    public void EventLogo()
    {
        Debug.Log("Logo Start");

        StartCoroutine(Logo());
    }

    IEnumerator Logo()
    {
        yield return new WaitForSeconds(0.1f);

        SharedObject.g_Main.SetStart(true);
    }

    public void EventSoundEffect(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}