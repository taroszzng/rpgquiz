﻿using UnityEngine;
using UnityEngine.UI;

public class UI_EffectNotic : MonoBehaviour
{
    public Text TEXTNotic;

    public void SetEffectNotic(string _str)
    {
        TEXTNotic.text = _str;
    }

    public void EffectEvent()
    {
        Debug.Log("EffectEvent");
    }

    public void EffectEventClose()
    {
        transform.gameObject.SetActive(false);
    }

    public void EffectEventSound(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}