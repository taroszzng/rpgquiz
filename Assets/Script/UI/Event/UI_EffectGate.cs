﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Network;

public class UI_EffectGate : MonoBehaviour
{
    public void EventEffectGate()
    {
        Debug.Log("EventEffectGate");
    }

    public void EventEffectClose()
    {
        transform.gameObject.SetActive(false);
    }

    public void EventSoundEffect(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}