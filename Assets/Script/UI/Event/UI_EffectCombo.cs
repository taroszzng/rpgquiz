﻿using UnityEngine;
using UnityEngine.UI;

public class UI_EffectCombo : MonoBehaviour
{
    public Text TEXTCombo;

    public void SetEffectCombo(string _str)
    {
        if(null != TEXTCombo)
            TEXTCombo.text = _str;
    }

    public void EffectCombo()
    {
        Debug.Log("EffectCombo");
    }

    public void EffectComboClose()
    {
        transform.gameObject.SetActive(false);
    }

    public void EffectComboSound(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}