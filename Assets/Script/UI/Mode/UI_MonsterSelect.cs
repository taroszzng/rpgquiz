﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MonsterSelect : MonoBehaviour
{
    public GameObject GOPrefab;

    public Transform TRArea;

    List<UI_MonsterSelectList> m_list = new List<UI_MonsterSelectList>();

    private void Awake()
    {
        SharedObject.g_MonsterSelect = this;
    }

    private void OnEnable()
    {
        if(0 == m_list.Count)
            CrateMonsterSelect();
        else
            ChangeMonsterSelect();

        SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_MONSTERSELECT;
    }

    private void OnDisable()
    {
        SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_NONE;
    }

    void CrateMonsterSelect()
    {
        int nCurStage = (SharedObject.g_SceneMgr.CurStage - 1) / (int)nsENUM.eVALUE.eVALUE_6;

        nCurStage *= (int)nsENUM.eVALUE.eVALUE_6;

        if (SharedObject.g_PlayerPrefsData.Ending)
            nCurStage = (SharedObject.g_SceneMgr.StageSelect - 1) * (int)nsENUM.eVALUE.eVALUE_6;

        for (int i = 1; i <= (int)nsENUM.eVALUE.eVALUE_6; ++i)
        {
            int nMonsterNum = 10000 + (i + nCurStage);

            if (!SharedObject.g_TableMgr.m_Monster.IsData(nMonsterNum))
                break;

            GameObject obj = GameObject.Instantiate(GOPrefab) as GameObject;

            if (null == obj)
                continue;

            obj.transform.SetParent(TRArea);

            obj.transform.localScale = new Vector3(1, 1, 1);

            UI_MonsterSelectList ui = obj.GetComponent<UI_MonsterSelectList>();

            if (null != ui)
                ui.SetMonsterSelect(nMonsterNum);

            m_list.Add(ui);
        }
    }

    void DeleteMonsterSelect()
    {
        int nCount = m_list.Count;

        for(int i=0; i<nCount; ++i)
            DestroyObject(m_list[i].gameObject);

        m_list.Clear();
    }

    void MonsterSelect()
    {
        int nCurStage = (SharedObject.g_SceneMgr.StageSelect - 1) * (int)nsENUM.eVALUE.eVALUE_6;

        for (int i = 1; i <= (int)nsENUM.eVALUE.eVALUE_6; ++i)
        {
            int nMonsterNum = 10000 + (i + nCurStage);

            if (!SharedObject.g_TableMgr.m_Monster.IsData(nMonsterNum))
                break;

            GameObject obj = GameObject.Instantiate(GOPrefab) as GameObject;

            if (null == obj)
                continue;

            obj.transform.SetParent(TRArea);

            obj.transform.localScale = new Vector3(1, 1, 1);

            UI_MonsterSelectList ui = obj.GetComponent<UI_MonsterSelectList>();

            if (null != ui)
                ui.SetMonsterSelect(nMonsterNum);

            m_list.Add(ui);
        }
    }

    void ChangeMonsterSelect()
    {
        DeleteMonsterSelect();

        if (SharedObject.g_PlayerPrefsData.Ending)
            MonsterSelect(); //엔딩 후엔 아무 몬스터나 잡게
        else
            CrateMonsterSelect();

        Debug.Log(" ChangeMonsterSelect ");
    }

    public void OnBtnBack()
    {
        SharedObject.g_Mode.SetMonsterSelect(false);
    }
}
