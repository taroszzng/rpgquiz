﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UI_Mode : MonoBehaviour
{
    public GameObject GOStage;
    public GameObject GOMonsterSelect;
    public GameObject GOAMode;

    public GameObject GO2Depth;
    public GameObject GOPayment;

    private void Awake()
    {
        SharedObject.g_Mode = this;
    }

    public void AModeSelect()
    {
        GOStage.SetActive(false);
        GOMonsterSelect.SetActive(false);

        GOAMode.SetActive(true);
    }

    public void SetStage()
    {
        GOStage.SetActive(true);
        GOAMode.SetActive(false);
    }

    public void SetMonsterSelect(bool _bActive)
    {
        GOStage.SetActive(!_bActive);
        GOMonsterSelect.SetActive(_bActive);
    }
}
