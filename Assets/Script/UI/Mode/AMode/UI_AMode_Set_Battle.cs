﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public GameObject GOBattleTip;
        public GameObject GOQuiz;

        public Transform TREFHitPos;

        public Image IMGPlayerHp;
        public Image IMGMonHp;

        public Text TEXTPlayerHp;
        public Text TEXTMonHp;

        public Text TEXTMonName;

        public Text TEXTTime;//시간

        public List<GameObject> GOListItemEff;
        public List<Button> BTNListItem;//아이템 on/off
        public List<Image> IMGListItem;//아이템 이미지
        public List<Text> TEXTListItem;//아이템 텍스트

        public Slider SDTime;//타임 슬라이드

        bool m_bBattleCreate = false;

        float m_fPlayerHp = 0f;
        float m_fMonsterHp = 0f;

        float m_fLastTime = 0;
        float m_fMaxTime = 0;

        float m_fCurTime = 0f;

        public void InitHp()
        {
            m_fPlayerHp = 0f;
            m_fMonsterHp = 0f;

            IMGPlayerHp.fillAmount = 0f;
            TEXTPlayerHp.text = "0";

            IMGMonHp.fillAmount = 0f;
            TEXTMonHp.text = "0";

            InitLuckTime();

            m_fLastTime = (float)nsENUM.eDEFINE.eDEFINE_QUIZTIME + m_fLuckTime;
            m_fMaxTime = m_fLastTime;

            SetHp(true, 0, 1);
            SetHp(false, 0, 1);

            GOO.SetActive(false);
            GOX.SetActive(false);

            for (int i = 0; i < GOListClear.Count; ++i)
                GOListClear[i].SetActive(false);
        }

        void SetBattle()
        {
            hScroller = listScroller[(int)m_eSetTab];
            hScroller.Delegate = this;

            if (eTUTORIAL.eTUTORIAL_BATTLE == SharedObject.g_PlayerPrefsData.Tutorial)
            {
                SharedObject.g_Tutorial.OnPopupBtn();

                SharedObject.g_Tutorial.PlayTutorial(eTUTORIAL.eTUTORIAL_BATTLE);
            }

            InitLuckTime();

            if (!m_bBattleCreate)
            {
                m_bBattleCreate = true;

                CreateBattle();

                CreateQuizArray();
                SetQuizText();

                if (null != SharedObject.g_CollectorMgr)
                {
                    float fmax = SharedObject.g_CollectorMgr.Player.GetMaxHp();

                    SetHp(true, fmax, fmax);

                    fmax = SharedObject.g_CollectorMgr.Monster.GetMaxHp();

                    SetHp(false, fmax, fmax);
                }
            }
            else
                SetBattleEquip();

            SetMonsterNameText();

            SetBattleStart(SharedObject.g_SceneMgr.BattleStart);

            if (!m_bQuizStart)
            {
                StopCoroutine(SetQuizTime());
                StartCoroutine(SetQuizTime());

                m_bQuizStart = true;
            }
        }

        void InitLuckTime()
        {
            int nluck = SharedObject.g_PlayerPrefsData.GetLuck(nsENUM.eTEAM.BLUE_TEAM);

            float fability = SharedObject.g_PlayerPrefsData.GetAbility(nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK);

            fability = ((float)nluck * fability) + 0.5f;

            m_fLuckTime = (int)fability;
        }

        void CreateBattle()
        {
            int nCount = BTNListItem.Count;

            for (int i = 0; i < nCount; ++i)
            {
                bool bactive = false;

                if (i < SharedObject.g_PlayerPrefsData.m_listBattle.Count)
                {
                    Item item = SharedObject.g_PlayerPrefsData.GetItem(SharedObject.g_PlayerPrefsData.m_listBattle[i]);

                    if (null != item)
                    {
                        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(item.m_nItemID);

                        if (null != info)
                        {
                            bactive = true;

                            IMGListItem[i].sprite = SharedObject.g_SceneMgr.CreateItemIcon(info.m_nIcon);
                            TEXTListItem[i].text = item.m_nItemValue.ToString();
                        }
                    }
                }

                BTNListItem[i].interactable = true;
                BTNListItem[i].gameObject.SetActive(bactive);
            }
        }

        void SetQuiz(bool _bActive)
        {
            GOQuiz.SetActive(_bActive);
            GOBattleTip.SetActive(!_bActive);
        }

        public void SetBattleEquip()//전투 아이템 설정
        {
            CreateBattle();
        }

        public void SetMonsterNameText()
        {
            int nID = SharedObject.g_PlayerPrefsData.MonsterID;

            if (SharedObject.g_PlayerPrefsData.Ending)
                nID = SharedObject.g_SceneMgr.MonsterSelect;

            Table_Monster.Info info = SharedObject.g_TableMgr.m_Monster.Get(nID);

            if (null == info)
                return;

            string str = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;

            TEXTMonName.text = str;
        }

        public void SetHp(bool _bPlayer, float _fMaxHp, float _fHp)
        {
            if (_bPlayer)
            {
                StartCoroutine(UpdatePlayerHp(_fMaxHp, _fHp));

                m_fPlayerHp = (m_fPlayerHp <= 0) ? _fMaxHp : _fHp;//초기값
            }
            else
            {
                StartCoroutine(UpdateMonsterHp(_fMaxHp, _fHp));

                m_fMonsterHp = (m_fMonsterHp <= 0) ? _fMaxHp : _fHp;//초기값
            }
        }

        IEnumerator UpdatePlayerHp(float _fMaxHp, float _fHp)
        {
            float fhp = m_fPlayerHp;
            float fvalue = fhp/ (float)nsENUM.eVALUE.eVALUE_60;

            float fvalueplus = _fMaxHp / (float)nsENUM.eVALUE.eVALUE_60;

            bool bup = false;

            while (true)
            {
                yield return new WaitForFixedUpdate();

                if (fhp > _fHp)
                {
                    fhp -= fvalue;

                    if (_fHp > fhp)
                        fhp = _fHp;
                }
                else
                {
                    fhp += fvalueplus;

                    if (_fHp < fhp)
                        fhp = _fHp;

                    bup = true;
                }

                IMGPlayerHp.fillAmount = fhp / _fMaxHp;

                TEXTPlayerHp.text = ((int)fhp).ToString();

                if(bup)
                {
                    if (fhp >= _fHp)
                        break;
                }
                else
                {
                    if (fhp <= _fHp)
                        break;
                }
            }

            yield break;
        }

        IEnumerator UpdateMonsterHp(float _fMaxHp, float _fHp)
        {
            float fhp = m_fMonsterHp;
            float fvalue = fhp / (float)nsENUM.eVALUE.eVALUE_60;

            float fvalueplus = _fMaxHp / (float)nsENUM.eVALUE.eVALUE_60;

            bool bup = false;

            while (true)
            {
                yield return new WaitForFixedUpdate();

                if (fhp > _fHp)
                {
                    fhp -= fvalue;

                    if (_fHp > fhp)
                        fhp = _fHp;
                }
                else
                {
                    fhp += fvalueplus;

                    if (_fHp < fhp)
                        fhp = _fHp;

                    bup = true;
                }

                IMGMonHp.fillAmount = fhp / _fMaxHp;

                TEXTMonHp.text = ((int)fhp).ToString();

                if (bup)
                {
                    if (fhp >= _fHp)
                        break;
                }
                else
                {
                    if (fhp <= _fHp)
                        break;
                }
            }

            yield break;
        }

        void SetBattleDelete(int _nIndex, int _nItemID, int _nItemValue)
        {
            Item item = SharedObject.g_PlayerPrefsData.GetItem(_nItemID);

            int nCount = item.m_nItemValue + _nItemValue;

            if (0 >= nCount)
                BTNListItem[_nIndex].interactable = false;

            GOListItemEff[_nIndex].SetActive(true);

            TEXTListItem[_nIndex].text = nCount.ToString();
        }

        public void OnBtnItemUse(int _nIndex)//아이템 사용
        {
            if (!SharedObject.g_SceneMgr.BattleStart)//베틀 스타트를 해야 진행
            {
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1375001);
                return;
            }

            if (eTUTORIAL.eTUTORIAL_BATTLE == SharedObject.g_PlayerPrefsData.Tutorial)
                SharedObject.g_Tutorial.OnPopupBtn();

            if (_nIndex >= SharedObject.g_PlayerPrefsData.m_listBattle.Count)//없음
                return;

            int nItemID = SharedObject.g_PlayerPrefsData.m_listBattle[_nIndex];

            if (SharedObject.g_SceneMgr.CheckItemUse(nsENUM.eTEAM.BLUE_TEAM, nItemID))//캐릭터 적용
            {
                int nmsg = SharedObject.g_SceneMgr.ItemApply(nsENUM.eTEAM.BLUE_TEAM, nItemID);

                if (1 == nmsg)//정상 작동
                {
                    SharedObject.g_SceneMgr.ItemUse(nsENUM.eTEAM.BLUE_TEAM, nItemID);

                    SetBattleDelete(_nIndex, nItemID, -1);//아이템 제거 전에 처리

                    SharedObject.g_PlayerPrefsData.ItemDelete(nItemID, -1);

                    SetInvenDelete(nItemID, -1);

                    SharedObject.g_AMode.SetEffectItem(nItemID);//메세지
                }
                else//정상적으로 사용 못할때
                {
                    SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, nmsg);
                }
            }
            else//아이템이 존재하지 않아 사용 할 수 업음
            {
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1370014);
            }
        }

        public void OnBtnExample(int _nIndex)//답 선택
        {
            if (SharedObject.g_AMode.UIAModePlay)
                return;

            SharedObject.g_AMode.UIAModePlay = true;

            if (eTUTORIAL.eTUTORIAL_BATTLE == SharedObject.g_PlayerPrefsData.Tutorial)
                SharedObject.g_Tutorial.OnPopupBtn();

            Table_Quiz.Info info = SharedObject.g_TableMgr.m_Quiz.Get(m_nQuizID);

            if (null == info)
                return;

            if((int)nsENUM.eVALUE.eVALUE_4 < info.m_nAnswer)
            {
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1375001);
                return;
            }

            nsENUM.eTEAM ewin = info.m_nAnswer == _nIndex ? nsENUM.eTEAM.BLUE_TEAM : nsENUM.eTEAM.RED_TEAM;

            SharedObject.g_SceneMgr.Win = ewin;//승리팀

            if (nsENUM.eTEAM.BLUE_TEAM == ewin)
            {
                SharedObject.g_PlayerPrefsData.Score += 1;

                if (SharedObject.g_bGoogleLogin)//점수
                    GPGSIds.GoogleLeaderboardSave(eGPGSIDS.eGPGSIDS_QUIZ, SharedObject.g_PlayerPrefsData.Score);

                SharedObject.g_SceneMgr.SetCombo(nsENUM.eCOMBOTYPE.eCOMBOTYPE_WIN);

                GOO.SetActive(true);
            }
            else
            {
                SharedObject.g_SceneMgr.SetCombo(nsENUM.eCOMBOTYPE.eCOMBOTYPE_LOSE);

                GOX.SetActive(true);
            }

            SharedObject.g_CollectorMgr.Fight();

            if(0 < _nIndex)
                GOListClear[_nIndex - 1].SetActive(true);
        }
    }
}
