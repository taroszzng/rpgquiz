﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public GameObject GOBookTip;

        bool m_bBook = false;

        int m_nClearStage = 0;

        List<ScrollerData> m_listBookData = new List<ScrollerData>();

        void SetBook()
        {
            hScroller = listScroller[(int)m_eSetTab];
            hScroller.Delegate = this;

            if (!m_bBook)
            {
                CreateBook();

                m_bBook = true;
            }
            else
            {
                SharedObject.g_AModeSet.SetClearBook();
            }

            if (0 < m_listBookData.Count)
                GOBookTip.SetActive(false);
            else
                GOBookTip.SetActive(true);
        }

        void CreateBook()//생성
        {
            var pair = SharedObject.g_TableMgr.m_Monster.m_Dictionary.GetEnumerator();

            while (pair.MoveNext())//
            {
                Table_Monster.Info info = pair.Current.Value;

                if (null == info || 0 == info.m_nID)
                    continue;

                string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;
                string strdec = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec;

                m_listBookData.Add(new ScrollerData()
                {
                    m_nID = info.m_nID,
                    m_eSet = m_eSetTab,
                    m_nValue = 1,
                    m_strName = strname,
                    m_strDec = strdec,
                    m_nIcon = info.m_nIcon,
                    m_bSelect = false,
                    m_bLock = info.m_nClearStage > SharedObject.g_PlayerPrefsData.ClearStage
                });//데이타 넣기
            }

            m_nClearStage = SharedObject.g_PlayerPrefsData.ClearStage;
        }

        public void SetClearBook()
        {
            if (m_nClearStage >= SharedObject.g_SceneMgr.CurStage)
                return;

            int nIndex = 1;

            int nCount = m_listBookData.Count;

            for(int i=0; i<nCount; ++i)
            {
                if (SharedObject.g_SceneMgr.CurStage > nIndex)
                {
                    m_listBookData[i].m_bLock = false;
                }
                else
                {
                    if(i > SharedObject.g_SceneMgr.CurStage)
                        break;
                }

                nIndex++;
            }

            if (nsENUM.eSETPOPUP.eSETPOPUP_BOOK == m_eSetTab)
            {
                float fpos = (float)(nIndex) / (float)m_listBookData.Count;

                hScroller.ReloadData(fpos);
            }

            m_nClearStage = SharedObject.g_SceneMgr.CurStage;
        }
    }
}
