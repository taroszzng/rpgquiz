﻿using UnityEngine;

public partial class UI_AMode : MonoBehaviour
{
    public EnhancedScrollerRpgDice.Set.UI_AMode_Set UIAModeSet;

    public GameObject GOEvent;
    public GameObject GOReward;

    public GameObject GOEFUserDamage;//유저 히트

    public Transform TREFHitPos; //몬스터 히트

    public UI_EffectNotic UIEffectNotic;
    public UI_EffectNotic UIEffectItem;//아이템 사용 노틱

    public UI_EffectCombo UIEffectCombo;

    bool m_bPlay = true;

    public bool UIAModePlay
    {
        set { m_bPlay = value; }
        get { return m_bPlay; }
    }

    void Awake()
    {
        SharedObject.g_AMode = this;
    }

    private void OnEnable()
    {
        if (nsENUM.eSCENE.eSCENE_MODE == SharedObject.g_SceneMgr.Scene)
            SharedObject.g_SceneMgr.ChangeScene(nsENUM.eSCENE.eSCENE_MODE);
        else
            SharedObject.g_CollectorMgr.Mode(nsENUM.eSCENE.eSCENE_MODE);
    }

    public void ReSet()
    {
        if(null != UIEffectNotic)
            UIEffectNotic.gameObject.SetActive(false);//이벤트 공지 끄기

        m_bPlay = false;

        UIAModeSet.SetQuizText();//퀴즈 다시
    }

    public void SetEffectNotic(string _str)//공지메세지
    {
        UIEffectNotic.gameObject.SetActive(true);

        UIEffectNotic.SetEffectNotic(_str);
    }

    public void SetEffectItem(int _nItemID)//아이템 사용 메세지
    {
        Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if (null == info)
            return;

        string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;
        string str = SharedObject.g_TableMgr.m_Lan.Get(1376002).m_strDec;

        str = string.Format(str, strname);

        UIEffectItem.gameObject.SetActive(true);

        UIEffectItem.SetEffectNotic(str);
    }

    public void SetEffectMode(float _fMode)//아이템 사용 메세지
    {
        Table_Lan.Info info = SharedObject.g_TableMgr.m_Lan.Get(1376005);

        if (null == info)
            return;

        _fMode *= 100;

        string str = info.m_strDec;

        str = string.Format(str, _fMode);

        UIEffectItem.gameObject.SetActive(true);

        UIEffectItem.SetEffectNotic(str);
    }

    public void SetEffectCombo()
    {
        int nid = SharedObject.g_SceneMgr.GetComboCount();//콤보 2부터 시작

        if (0 >= nid)
            return;

        Table_Combo.Info info = SharedObject.g_TableMgr.m_Combo.Get(nid);

        if (null == info)
            return;

        string str = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec;

        float fvalue = info.m_fValue * 100f;

        str = string.Format(str, fvalue);

        UIEffectCombo.gameObject.SetActive(true);

        UIEffectCombo.SetEffectCombo(str);
    }

    public void OnEvent()
    {
        UIAModeSet.gameObject.SetActive(false);

        GOEvent.gameObject.SetActive(true);

        SharedObject.g_CollectorMgr.Mode(nsENUM.eSCENE.eSCENE_MODE);
    }

    public void OnEventGameStart()
    {
        GOEvent.gameObject.SetActive(false);

        UIAModeSet.gameObject.SetActive(true);
    }
}
