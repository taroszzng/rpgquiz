﻿using UnityEngine;
using UnityEngine.UI;

public class UI_AMode_Event : MonoBehaviour
{
    public Text TEXTStage;
    public Text TEXTMonName;
    public Text TEXTMonDec;

    private void OnEnable()
    {
        SetAmodeEvent();

        SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_AMODEEVENT;
    }

    private void OnDisable()
    {
        SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_NONE;
    }

    public void SetAmodeEvent()//몬스터 클리어시 호출
    {
        SharedObject.g_MainCamera.ZoomStartEvent(0f, -1.5f, 1f, 1.5f, 0.5f, Vector3.zero);

        int nstage1 = SharedObject.g_PlayerPrefsData.ClearStage / (int)nsENUM.eVALUE.eVALUE_6;
        int nstage2 = SharedObject.g_PlayerPrefsData.ClearStage % (int)nsENUM.eVALUE.eVALUE_6;

        nstage1 += 1;

        if (0 == nstage2)
            nstage2 = (int)nsENUM.eVALUE.eVALUE_6;

        string strlen = SharedObject.g_TableMgr.m_Lan.Get(1370017).m_strDec;

        string strstage = string.Format(strlen, nstage1, nstage2);

        TEXTStage.text = strstage;

        int nID = SharedObject.g_PlayerPrefsData.MonsterID;

        if (SharedObject.g_PlayerPrefsData.Ending)
            nID = SharedObject.g_SceneMgr.MonsterSelect;

        Table_Monster.Info info = SharedObject.g_TableMgr.m_Monster.Get(nID);

        if (null == info)
            return;

        strlen = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;

        TEXTMonName.text = strlen;

        strlen = SharedObject.g_TableMgr.m_Lan.Get(info.m_nEvent).m_strDec;

        TEXTMonDec.text = strlen;
    }

    public void OnBtnStart()
    {
       SharedObject.g_MainCamera.ZoomEndEvent();

       SharedObject.g_AMode.OnEventGameStart();
    }

    public void OnBtnBack()
    {
        SharedObject.g_Mode.SetMonsterSelect(true);
        SharedObject.g_Mode.GOAMode.SetActive(false);
    }
}
