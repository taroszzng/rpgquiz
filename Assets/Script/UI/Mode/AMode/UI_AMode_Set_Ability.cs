﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public GameObject GOAbilityPrefab;

        public Transform TRAbility;

        public Text TEXTLevel;
        public Text TEXTTotal;
        public Text TEXTHp;
        public Text TEXTAtk;
        public Text TEXTLuck;

        public Text TEXTHpPlus;
        public Text TEXTAtkPlus;
        public Text TEXTLuckPlus;

        List<UI_BuffStatList> m_listBuffStat = new List<UI_BuffStatList>();

        void SetAbility()
        {
            if (SharedObject.g_PlayerPrefsData.m_bChangeAbility)
            {
                CreateAbility();

                SharedObject.g_PlayerPrefsData.m_bChangeAbility = false;
            }
            else
                SetAbilityItem();

            SetAbilityText();
        }

        void CreateAbility()
        {
            int nCount = SharedObject.g_PlayerPrefsData.m_listAbility.Count;

            Debug.Log("Count = " +nCount);

            for (int i = 0; i < nCount; ++i)
            {
                GameObject obj = GameObject.Instantiate(GOAbilityPrefab) as GameObject;

                if (null == obj)
                    continue;

                obj.transform.SetParent(TRAbility);

                obj.transform.localScale = new Vector3(1, 1, 1);

                UI_BuffStatList ui = obj.GetComponent<UI_BuffStatList>();

                if (null != ui)
                {
                    Item item = SharedObject.g_PlayerPrefsData.GetItem(SharedObject.g_PlayerPrefsData.m_listAbility[i]);

                    if (null != item)
                        ui.SetBuffStat(item);

                    m_listBuffStat.Add(ui);
                }
            }
        }

        public void DeleteAbilityAll()
        {
            int nCount = m_listBuffStat.Count;

            for (int i = 0; i < nCount; ++i)
                DestroyObject(m_listBuffStat[i].gameObject);

            m_listBuffStat.Clear();
        }

        bool CheckAbilityData(int _nItemID)
        {
            bool bdata = false;

            int nCount = m_listBuffStat.Count;

            for (int i = 0; i < nCount; ++i)
            {
                if (_nItemID == m_listBuffStat[i].ItemID)
                {
                    bdata = true;

                    break;
                }
            }

            return bdata;
        }

        public void SetAbilityItem()//무조건 장착
        {
            DeleteAbilityAll();

            CreateAbility();
        }

        public void SetAbilityText()
        {
            float fhp = 0;
            float fatk = 0;
            float fluck = 0;

            float fhpplus = 0;
            float fatkplus = 0;
            float fluckplus = 0;

            int nTotal = 0;

            if (null == SharedObject.g_CollectorMgr)
                return;

            Table_Level.Info info = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

            if (null == info)
                return;

            string strlen = SharedObject.g_TableMgr.m_Lan.Get(1300409).m_strDec;//level

            strlen = string.Format(strlen, SharedObject.g_PlayerPrefsData.Level);

            TEXTLevel.text = strlen;

            fhp = info.m_fHP;
            fatk = info.m_fAtk;
            fluck = info.m_nLuck;

            TEXTHp.text = ((int)fhp).ToString();
            TEXTAtk.text = ((int)fatk).ToString();
            TEXTLuck.text = ((int)fluck).ToString();

            int nCount = SharedObject.g_PlayerPrefsData.m_listAbility.Count;

            for (int i = 0; i < nCount; ++i)
            {
                Item item = SharedObject.g_PlayerPrefsData.GetItem(SharedObject.g_PlayerPrefsData.m_listAbility[i]);

                if (null == item)
                    continue;

                switch (item.m_eItemApply)
                {
                    case nsENUM.eITEMAPPLY.eITEMAPPLY_ALL:
                        {
                            fhpplus += item.m_fItemAbility;
                            fatkplus += item.m_fItemAbility;
                            fluckplus += item.m_fItemAbility;

                            fhp += fhp * item.m_fItemAbility;
                            fatk += fatk * item.m_fItemAbility;
                            fluck += fluck * item.m_fItemAbility;
                        }
                        break;
                    case nsENUM.eITEMAPPLY.eITEMAPPLY_HP:
                        {
                            fhpplus += item.m_fItemAbility;

                            fhp += fhp * item.m_fItemAbility;
                        }
                        break;
                    case nsENUM.eITEMAPPLY.eITEMAPPLY_ATK:
                        {
                            fatkplus += item.m_fItemAbility;

                            fatk += fatk * item.m_fItemAbility;
                        }
                        break;
                    case nsENUM.eITEMAPPLY.eITEMAPPLY_LUCK:
                        {
                            fluckplus += item.m_fItemAbility;

                            fluck += fluck * item.m_fItemAbility;
                        }
                        break;
                }
            }

            fhpplus *= 100;
            fatkplus *= 100;
            fluckplus *= 100;

            TEXTHpPlus.text = "+" + fhpplus.ToString() + "%";
            TEXTAtkPlus.text = "+" + fatkplus.ToString() + "%";
            TEXTLuckPlus.text = "+" + fluckplus.ToString() + "%";

            nTotal = (int)(fhp + fatk + fluck);

            TEXTTotal.text = nTotal.ToString();

            if (SharedObject.g_bGoogleLogin)
            {
                GPGSIds.GoogleLeaderboardSave(eGPGSIDS.eGPGSIDS_ALL, nTotal);
                GPGSIds.GoogleLeaderboardSave(eGPGSIDS.eGPGSIDS_HP, (int)fhp);
                GPGSIds.GoogleLeaderboardSave(eGPGSIDS.eGPGSIDS_ATK, (int)fatk);
                GPGSIds.GoogleLeaderboardSave(eGPGSIDS.eGPGSIDS_LUCK, (int)fluck);
            }
        }
    }
}
