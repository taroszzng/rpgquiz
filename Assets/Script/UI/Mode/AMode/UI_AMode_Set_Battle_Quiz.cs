﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        class QuizInfo
        {
            public Dictionary<int, int> m_Dictionary = new Dictionary<int, int>();
        }

        Dictionary<int, QuizInfo> m_DictionaryQuiz = new Dictionary<int, QuizInfo>();//퀴즈타입 갯수

        public GameObject GOO;
        public GameObject GOX;

        public List<Text> TEXTListExample;//문제 답
        public List<GameObject> GOListExample;//문제 힌트

        public List<GameObject> GOListClear;

        int m_nQuizID;

        bool m_bQuizStart;

        float m_fLuckTime = 0f;

        public int QuizID //퀴즈id
        {
            get { return m_nQuizID; }
        }

        public float LastTime //시간셋팅
        {
            set { m_fLastTime += value; }
        }

        public float MaxTime
        {
            set { m_fMaxTime += value; }
        }

        void CreateQuizArray()
        {
            m_DictionaryQuiz.Clear();

            for (int i = 0; i < (int)nsENUM.eQUIZTYPE.eQUIZTYPE_ETC; ++i)
            {
                QuizInfo info = new QuizInfo();

                if(null != info)
                    m_DictionaryQuiz.Add(i + 1, info);
            }
        }

        void RecurisiveQuiz()//퀴즈 중복 및 삭제
        {
            int ntype = 0;

            ntype = SharedObject.g_SceneMgr.CurStage / (int)nsENUM.eVALUE.eVALUE_6; //100단위
            int nmax = SharedObject.g_SceneMgr.CurStage % (int)nsENUM.eVALUE.eVALUE_6;//10단위

            nmax = (ntype * (int)nsENUM.eVALUE.eVALUE_20) + (nmax * (int)nsENUM.eVALUE.eVALUE_3);

            int nvalue = 0;

            while (true)
            {
                ntype = Random.Range(1, (int)nsENUM.eQUIZTYPE.eQUIZTYPE_END);//종목

                nvalue = Random.Range(1, nmax + 1);

                m_nQuizID = ntype * 100000 + nvalue;

                QuizInfo info = null;

                if(m_DictionaryQuiz.ContainsKey(ntype))
                    info = m_DictionaryQuiz[ntype];

                if(null != info)
                {
                    if(info.m_Dictionary.ContainsKey(m_nQuizID))//중복
                    {
                        if (nmax <= info.m_Dictionary.Count)
                            info.m_Dictionary.Clear();

                        Debug.Log("RecurisiveQuiz Clear = " + info.m_Dictionary.Count);

                        continue;
                    }
                    else
                    {
                        info.m_Dictionary.Add(m_nQuizID, m_nQuizID);

                        break;
                    }
                }
                else//퀴즈 새로 생성
                {
                    info = new QuizInfo();

                    if (null != info)
                    {
                        info.m_Dictionary.Add(m_nQuizID, m_nQuizID);

                        m_DictionaryQuiz.Add(ntype, info);
                    }

                    break;
                }
            }
        }

        public void SetQuizText()
        {
            GOO.SetActive(false);
            GOX.SetActive(false);

            RecurisiveQuiz();//갯수

            Table_Quiz.Info info = SharedObject.g_TableMgr.m_Quiz.Get(m_nQuizID);

            if (null == info)
                return;

            int ncount = info.m_ListExample.Count;

            for (int i = 0; i < ncount; ++i)
            {
                if (i < GOListExample.Count)
                    GOListExample[i].SetActive(false);

                if ("0" != info.m_ListExample[i])
                    TEXTListExample[i].text = info.m_ListExample[i];
                else
                    TEXTListExample[i].text = "TestID = " + info.m_nID.ToString();

                if (i < GOListClear.Count)
                    GOListClear[i].SetActive(false);
            }

            m_fLastTime = (float)nsENUM.eDEFINE.eDEFINE_QUIZTIME + m_fLuckTime;
            m_fMaxTime = m_fLastTime;

            SDTime.value = 1;

            TEXTTime.text = ((int)m_fLastTime).ToString();

            m_fCurTime = Time.time;
        }

        IEnumerator SetQuizTime()
        {
            while (true)
            {
                yield return new WaitForFixedUpdate();

                if(!SharedObject.g_SceneMgr.BattleStart)
                    continue;

                if (SharedObject.g_SceneMgr.Pause)
                    continue;

                if (Time.time >= 1f + m_fCurTime)
                {
                    m_fLastTime -= 1;

                    SDTime.value = m_fLastTime / m_fMaxTime;

                    if (m_fLastTime <= 0)
                    {
                        m_fLastTime = 0;

                        OnBtnExample(0);
                    }

                    TEXTTime.text = ((int)m_fLastTime).ToString();

                    m_fCurTime = Time.time;
                }
            }
        }

        int Recursive(int _nIndex)//정답을 제외한 랜덤으로 1개 삭제
        {
            int nvalue = _nIndex;

            while (true)
            {
                nvalue = Random.Range(0, (int)nsENUM.eVALUE.eVALUE_4);

                if (nvalue != _nIndex)
                    break;
            }

            Debug.Log("Recursive = " + nvalue + " _nIndex = " + _nIndex);

            return nvalue; //배열은 0부터이므로
        }

        public void SetBtnExample(int _nIndex)//삭제 아이템 사용
        {
            int index = Recursive(_nIndex);

            if (GOListExample.Count > index)
                GOListExample[index].SetActive(true);
        }
    }
}

