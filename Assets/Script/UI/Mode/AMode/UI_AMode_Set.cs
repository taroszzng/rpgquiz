﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public Button BTNStart;
        public Image IMGVs;

        public List<SetCellView> hCellViewPrefab;
        public List<EnhancedScroller> listScroller;
        public List<int> listSize;

        EnhancedScroller hScroller;

        nsENUM.eSETPOPUP m_eSetTab = nsENUM.eSETPOPUP.eSETPOPUP_BATTLE;

        public nsENUM.eSETPOPUP SetPopup
        {
            get { return m_eSetTab; }
        }

        protected void OnEnable()
        {
            if (null == SharedObject.g_AModeSet)
                SharedObject.g_AModeSet = this;

            Init();
            SetMain();

            SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_NONE;
        }

        protected void OnDisable()//OnEsc 호출 후 파괴될때 호출
        {
            SharedObject.g_AModeSet = null;

            m_bQuizStart = false;

            SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_NONE;
        }

        void Init()
        {
            SetBattleStart(false);

            SetQuiz(false);

            SharedObject.g_Tutorial.PlayTutorial(eTUTORIAL.eTUTORIAL_INVEN);
        }

        public void OnBack()
        {
            transform.gameObject.SetActive(false);
        }

        public void OnSelect(int _nIndex)
        {
            SelectTab(_nIndex);
        }

        void SelectTab(int _nIndex)
        {
            if (0 > _nIndex)
                _nIndex = 0;

            if (_nIndex == (int)m_eSetTab)
                return;

            m_eSetTab = (nsENUM.eSETPOPUP)_nIndex;

            Debug.Log("SetTab = " +m_eSetTab);

            SetMain();
        }

        void SetMain()
        {
            switch (m_eSetTab)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                    SetBattle();
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    SetInven();
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_ABILITY:
                    SetAbility();
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_BOOK:
                    SetBook();
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_SHOP:
                    SetShop();
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_ADS:
                    SetAds();
                    break;
                default:
                    break;
            }
        }

        public void InsertSet(nsENUM.eSETPOPUP _e, int _nItemID, int _nItemValue)
        {
            List<ScrollerData> list = null;

            switch (_e)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    {
                        if(m_bInvenCreate)
                            list = m_listInvenData;
                    }
                    break;
            }

            if (null == list)
                return;

            bool bInsert = false;

            int nCount = list.Count;

            for (int i = 0; i < nCount; ++i)
            {
                if (_nItemID == list[i].m_nID)
                {
                    list[i].m_nValue += _nItemValue;
                    bInsert = true;

                    break;
                }
            }

            if (bInsert)//이미 있으니 pass
                return;

            Table_Item.Info info = (Table_Item.Info)SharedObject.g_TableMgr.m_Item.Get(_nItemID);

            if (null == info)
                return;

            string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;
            string strdec = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec;

            list.Add(new ScrollerData()
            {
                m_nID = info.m_nID,
                m_nValue = _nItemValue,
                m_eSet = _e,
                m_strName = strname,
                m_strDec = strdec,
                m_nIcon = info.m_nIcon,
                m_bSelect = false,
            });//데이타 넣기
        }

        int DeleteSet(nsENUM.eSETPOPUP _e, int _nItemID, int _nItemValue)//제거
        {
            int nIndex = 0;

            List<ScrollerData> list = null;

            switch (_e)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    list = m_listInvenData;
                    break;
            }

            if (null == list)
                return nIndex;

            int nCount = list.Count;

            for (int i = 0; i < nCount; ++i)
            {
                if (_nItemID == list[i].m_nID)
                {
                    ScrollerData data = list[i];

                    data.m_nValue += _nItemValue;

                    if (0 >= data.m_nValue)//수량이 없으면 자동 해제
                    {
                        data.m_bSelect = false;

                        list.Remove(data);
                    }

                    break;
                }

                nIndex++;
            }

            return nIndex;
        }

        void SetBattleStart(bool _bActive)
        {
            BTNStart.gameObject.SetActive(!_bActive);
            IMGVs.gameObject.SetActive(_bActive);
        }

        public void OnBtnBattleStart()
        {
            SharedObject.g_SceneMgr.BattleStart = true;

            if (null != SharedObject.g_CollectorMgr)
                SharedObject.g_CollectorMgr.SetHp();

            SetBattleStart(true);
            SetQuiz(true);

            SharedObject.g_SoundScript.PlayBGM(nsENUM.eSOUNDBGM.eSOUNDBGM_BATTLE);

            SharedObject.g_Tutorial.OnPopupBtn();

            SharedObject.g_AMode.UIAModePlay = false;//퀴즈 시간 시작

            SetQuizText();
        }

        public void OnBtnExit()
        {
            SharedObject.g_Main.SetGameExit(true);
        }

        public void OnBtnBack()
        {
            SharedObject.g_AMode.OnEvent();
        }
    }
}
