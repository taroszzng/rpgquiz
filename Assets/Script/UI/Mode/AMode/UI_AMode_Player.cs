﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        int m_nEquipPlayer;//장착

        bool m_bCreatePlayer = false;

        List<ScrollerData> m_listPlayerData = new List<ScrollerData>();

        void SetCreatePlayer()
        {
            hScroller = listScroller[(int)m_eSetTab];
            hScroller.Delegate = this;

            if (!m_bCreatePlayer)
            {
                m_nEquipPlayer = SharedObject.g_PlayerPrefsData.PlayerID;

                CreatePlayer();

                m_bCreatePlayer = true;
            }
            //else
            //    SelectCharacterSkin(SharedObject.g_SceneMgr.SelectPlayerID);
        }

        void CreatePlayer()
        {
            if (0 != m_listPlayerData.Count)
                return;

            //int nCount = SharedObject.g_TableMgr.m_Skin.m_listCharacterSkin.Count;

            //bool bSelect = true;
            //int nSelect = 0;

            //for (int i = 0; i < nCount; i++)
            //{
            //    if (0 != i)
            //        bSelect = false;

            //    Table_Skin.Info info = (Table_Skin.Info)SharedObject.g_TableMgr.m_Skin.Get(SharedObject.g_TableMgr.m_Skin.m_listCharacterSkin[i]);

            //    m_listCharacterSkinData.Add(new ScrollerData()
            //    {
            //        m_nID = info.m_nID,
            //        m_eType = (nsENUM.eSKIN)info.m_nType,
            //        m_nIcon = info.m_nIcon,
            //        m_strName = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec,
            //        m_strDec = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec,
            //        //m_nLv = info.m_nLevel,
            //        //m_nMaxLv = nMax,
            //        m_bLock = SharedObject.g_TableMgr.m_Skin.m_listCharacterSkin[i] <= SharedObject.g_PlayerPrefsData.CharacterSkinID ? false : true,
            //        m_bSelect = bSelect
            //    });//데이타 넣기
            //}

            //float fpos = (float)(nSelect - 0.9f) / (float)m_listCharacterSkinData.Count;

            //hScroller.StartReRoadPos = fpos;

            PlayerInfo();
        }

        public void SelectPlayer(int _nID)
        {
            PlayerInfo();

            int nIndex = 0;
            int nCount = m_listPlayerData.Count;

            for (int i = 0; i < nCount; i++)
            {
                if (_nID == m_listPlayerData[i].m_nID)
                    break;

                nIndex++;
            }

            float fpos = (float)(nIndex + 1) / (float)m_listPlayerData.Count;

            hScroller.ReloadData(fpos);
        }

        void PlayerInfo()
        {
            //Table_Skin.Info info = SharedObject.g_SceneMgr.GetSelectCharacterSkinTable();

            //SetCharacterSkinText(info);

            //SetUISpine(info.m_strUISpine);

            //SetCharacterCondition();//조건설정

            //ListCharacterSkinSelect(info.m_nID);
        }

        //void SetPlayerText(Table_Skin.Info _info)
        //{
        //    Table_Lan.Info info = SharedObject.g_TableMgr.m_Lan.Get(_info.m_nName);

        //    TEXTCommonName.text = info.m_strDec;

        //    info = SharedObject.g_TableMgr.m_Lan.Get(_info.m_nDec);

        //    TEXTCommonDec.text = info.m_strDec;
        //}

        void SetPlayerCondition()//조건설정
        {
            //Table_Lan.Info lan = SharedObject.g_TableMgr.m_Lan.Get((int)nsENUM.eLAN.eLAN_SKIN_OPEN);

            //if (SharedObject.g_SceneMgr.SelectCharacterSkinID <= SharedObject.g_PlayerPrefsData.CharacterSkinID)//오픈상태
            //{
            //    TEXTCharacterSkinAction.text = lan.m_strDec; //스킨적용
            //    return;
            //}

            //lan = SharedObject.g_TableMgr.m_Lan.Get((int)nsENUM.eLAN.eLAN_SKIN_LOCKOPEN);

            //TEXTCharacterSkinAction.text = lan.m_strDec; //잠금해제

            //Table_Skin.Info info = SharedObject.g_SceneMgr.GetSelectCharacterSkinTable();

            //SetConditionList(info.m_listCondition);
        }

        ScrollerData GetPlayerData(int _nID)
        {
            ScrollerData data = null;

            int nCount = m_listPlayerData.Count;

            for (int i = 0; i < nCount; ++i)
            {
                if (m_listPlayerData[i].m_nID == _nID)
                {
                    data = m_listPlayerData[i];
                    break;
                }
            }

            return data;
        }

        void ListPlayerSelect(int _nID)
        {
            int nCount = m_listPlayerData.Count;

            for (int i = 0; i < nCount; ++i)
                m_listPlayerData[i].m_bSelect = false;

            ScrollerData data = GetPlayerData(_nID);

            if (null == data)
                return;

            data.m_bSelect = true;
        }

        void CheckCharacterSkin()
        {
            //if (SharedObject.g_SceneMgr.SelectPlayerID <= SharedObject.g_PlayerPrefsData.PlayerID)
            //    ActionPlayer();
            //else//락이면 오픈조건 체크
            //    CheckCPlayerLock();
        }

        void ActionPlayer()
        {
            ////오픈 중이면
            //if (m_nEquipPlayer == SharedObject.g_SceneMgr.SelectPlayerID)
            //{
            //    SharedObject.g_Amode.SetPopUp("", (int)nsENUM.eLAN.eLAN_COSTUME);//장착중입니다.
            //    return;
            //}

            //if (null == SharedObject.g_CollectorMgr)
            //    return;

            //m_nEquipPlayer = SharedObject.g_SceneMgr.SelectPlayerID;//의상번호

            //SharedObject.g_Amode.SetPopUp("", (int)nsENUM.eLAN.eLAN_COSTUME_CHANGE);

            //SharedObject.g_PlayerPrefsData.AllSaveData();

            //SharedObject.g_CollectorMgr.ChangeCostume(SharedObject.g_SceneMgr.SelectCharacterSkinID);
        }

        void CheckPlayerLock()//락 해제면 장착, 락이면 오픈조건 출력
        {
            //Table_Skin.Info info = SharedObject.g_TableMgr.m_Skin.Get(SharedObject.g_SceneMgr.SelectCharacterSkinID);

            //if (info == null)
            //    return;

            //if (CheckConditionList(info.m_listCondition))//오픈조건 체크 후 해제
            //{
            //    Upgrade(info.m_listCondition);//해제

            //    SharedObject.g_PlayerPrefsData.CharacterSkinID = SharedObject.g_SceneMgr.SelectCharacterSkinID;
            //    SharedObject.g_PlayerPrefsData.AllSaveData();

            //    ScrollerData data = GetCharacterSkinData(SharedObject.g_SceneMgr.SelectCharacterSkinID);

            //    if (null == data)
            //        return;

            //    data.m_bLock = false;

            //    SelectCharacterSkin(SharedObject.g_SceneMgr.SelectCharacterSkinID);

            //    SharedObject.g_Amode.SetPopUp("", (int)nsENUM.eLAN.eLAN_SKIN_1370017);
            //}
            //else
            //    SharedObject.g_Amode.SetPopUp("", (int)nsENUM.eLAN.eLAN_SKIN_1370016);
        }
    }
}
