﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public UI_Damage UINormal;
        public UI_Damage UICri;

        public void EFHit(bool _bCri, float _fDmg, float _fPlus)
        {
            if (_bCri)
            {
                UICri.SetDamage((int)_fDmg, (int)_fPlus);

                UICri.gameObject.SetActive(true);
            }
            else
            {
                UINormal.SetDamage((int)_fDmg, (int)_fPlus);

                UINormal.gameObject.SetActive(true);
            }

            //string strprefab = "DamageFont_Normal";

            //if (_bCri)
            //    strprefab = "DamageFont_Cri";

            //strprefab = "Prefabs/InGame/" + strprefab;
        }
    }
}
