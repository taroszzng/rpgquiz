﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public GameObject GOEternal;
        public GameObject GOConsume;

        List<ScrollerData> m_listShopConsumeData = new List<ScrollerData>();
        List<ScrollerData> m_listShopEternalData = new List<ScrollerData>();

        public int m_nConsume = 0;

        public void OnBtnTop(int _nConsume)//영구제 소모성
        {
            if (m_nConsume == _nConsume)
                return;

            m_nConsume = _nConsume;

            SetShop();
        }

        void SetShop()
        {
            bool beternal = false;
            bool bconsume = false;

            hScroller = listScroller[(int)m_eSetTab + m_nConsume];
            hScroller.Delegate = this;

            if (1 == m_nConsume)
            {
                if (0 == m_listShopConsumeData.Count)//생성
                    CreateShop();

                bconsume = true;
                beternal = false;
            }
            else
            {
                if (0 == m_listShopEternalData.Count)//생성
                    CreateShop();

                beternal = true;
                bconsume = false;
            }

            GOEternal.SetActive(beternal);
            GOConsume.SetActive(bconsume);
        }

        void CreateShop()//생성
        {
            var pair = SharedObject.g_TableMgr.m_Shop.m_Dictionary.GetEnumerator();

            while (pair.MoveNext())//
            {
                Table_Shop.Info info = pair.Current.Value;

                if (null == info)
                    continue;

                string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;
                string strdec = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec;
                string strPrice = SharedObject.g_TableMgr.m_Lan.Get(info.m_nPrice).m_strDec;

                if (1 == m_nConsume)//소모성
                {
                    if (0 == info.m_nType || 
                        nsENUM.eITEMTYPE.eITEMTYPE_CONSUME != (nsENUM.eITEMTYPE)info.m_nSort)//안팔거나 영구제는 패스
                        continue;

                    m_listShopConsumeData.Add(new ScrollerData()
                    {
                        m_nID = info.m_nID,
                        m_eSet = m_eSetTab,
                        m_strName = strname,
                        m_strDec = strdec,
                        m_strPrice = strPrice,
                        m_nValue = info.m_nItemValue,
                        m_nIcon = info.m_nIcon,
                        m_bSelect = false,
                    });//데이타 넣기
                }
                else
                {
                    if (0 == info.m_nType || 
                        nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL != (nsENUM.eITEMTYPE)info.m_nSort)//안팔거나 소모성은 패스
                        continue;

                    m_listShopEternalData.Add(new ScrollerData()
                    {
                        m_nID = info.m_nID,
                        m_eSet = m_eSetTab,
                        m_strName = strname,
                        m_strDec = strdec,
                        m_strPrice = strPrice,
                        m_nValue = info.m_nItemValue,
                        m_nIcon = info.m_nIcon,
                        m_bSelect = false,
                    });//데이타 넣기
                }
            }
        }
    }
}
