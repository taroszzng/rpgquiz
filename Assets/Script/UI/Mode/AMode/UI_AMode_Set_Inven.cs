﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public partial class UI_AMode_Set : MonoBehaviour, IEnhancedScrollerDelegate
    {
        bool m_bInvenCreate = false;

        List<ScrollerData> m_listInvenData = new List<ScrollerData>();

        void SetInven()
        {
            hScroller = listScroller[(int)m_eSetTab];
            hScroller.Delegate = this;

            if (!m_bInvenCreate)
            {
                CreateInven();

                m_bInvenCreate = true;

                if (eTUTORIAL.eTUTORIAL_INVEN == SharedObject.g_Tutorial.GetTutorial())
                    SharedObject.g_Tutorial.OnPopupBtn();
            }
            else
                SetInvenItem();
        }

        void CreateInven()//생성
        {
            Debug.Log("Count = " + m_listInvenData.Count);

            if (0 != m_listInvenData.Count)
                return;

            var pair = SharedObject.g_PlayerPrefsData.m_DictionaryItem.GetEnumerator();

            while (pair.MoveNext())//
            {
                Item item = pair.Current.Value;

                if (null == item)
                    continue;

                Debug.Log("CreateInven = " + item.m_nItemID);

                if (0 == item.m_nItemValue)
                    continue;

                Table_Item.Info info = (Table_Item.Info)SharedObject.g_TableMgr.m_Item.Get(item.m_nItemID);

                if (null == info)
                    continue;

                string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;
                string strdec = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec;

                m_listInvenData.Add(new ScrollerData()
                {
                    m_nID = info.m_nID,
                    m_nValue = item.m_nItemValue,
                    m_eSet = m_eSetTab,
                    m_strName = strname,
                    m_strDec = strdec,
                    m_nIcon = info.m_nIcon,
                    m_bSelect = item.m_bUse,
                });//데이타 넣기
            }
        }

        public void SetInvenItem()//무조건 장착
        {
            Debug.Log("SetInvenItem = " + m_listInvenData.Count);

            var pair = SharedObject.g_PlayerPrefsData.m_DictionaryItem.GetEnumerator();

            while (pair.MoveNext())//
            {
                Item item = pair.Current.Value;

                if (null == item)
                    continue;

                if (CheckInvenData(item.m_nItemID, item.m_nItemValue))//이미 존재
                    continue;

                Debug.Log("SetInvenItem = " + item.m_nItemID);

                if (0 == item.m_nItemValue)
                    continue;

                Table_Item.Info info = (Table_Item.Info)SharedObject.g_TableMgr.m_Item.Get(item.m_nItemID);

                if (null == info)
                    continue;

                string strname = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;
                string strdec = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec).m_strDec;

                m_listInvenData.Add(new ScrollerData()
                {
                    m_nID = info.m_nID,
                    m_nValue = item.m_nItemValue,
                    m_eSet = m_eSetTab,
                    m_strName = strname,
                    m_strDec = strdec,
                    m_nIcon = info.m_nIcon,
                    m_bSelect = item.m_bUse,
                });//데이타 넣기
            }
        }

        bool CheckInvenData(int _nItemID, int _nItemValue = -1)
        {
            bool bdata = false;

            int nCount = m_listInvenData.Count;

            for (int i = 0; i < nCount; ++i)
            {
                if (_nItemID == m_listInvenData[i].m_nID)
                {
                    if(-1 != _nItemValue)
                        m_listInvenData[i].m_nValue = _nItemValue;

                    bdata = true;

                    break;
                }
            }

            return bdata;
        }

        public void SetInvenEquip(int _nItemID, bool _bEquip)
        {
            if (!m_bInvenCreate)
                return;

            int nIndex = 0;

            int nCount = m_listInvenData.Count;

            for(int i=0; i < nCount; ++i)
            {
                if(_nItemID == m_listInvenData[i].m_nID)
                {
                    m_listInvenData[i].m_bSelect = _bEquip;

                    break;
                }

                nIndex++;
            }

            if (nsENUM.eSETPOPUP.eSETPOPUP_INVEN == m_eSetTab)
            {
                float fpos = (float)(nIndex - 0.9f) / (float)m_listInvenData.Count;

                hScroller.ReloadData(fpos);
            }
        }

        public void SetInvenDelete(int _nItemID, int _nItemValue)
        {
            if (!m_bInvenCreate)
                return;

            int nIndex = DeleteSet(nsENUM.eSETPOPUP.eSETPOPUP_INVEN, _nItemID, _nItemValue);

            if (nsENUM.eSETPOPUP.eSETPOPUP_INVEN == m_eSetTab)
            {
                float fpos = (float)(nIndex - 0.99f) / (float)m_listInvenData.Count;
                hScroller.ReloadData(fpos);
            }
        }
    }
}
