﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Stage : MonoBehaviour
{
    public GameObject GOPrefab;

    public Transform TRArea;

    List<UI_StageList> m_listStage = new List<UI_StageList>();

    private void Awake()
    {
        SharedObject.g_Stage = this;
    }

    private void OnEnable()
    {
        if(0 == m_listStage.Count)
            CrateStage();
        else
            ChangeStage();

        SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_STAGE;
    }

    private void OnDisable()
    {
        SharedObject.g_SceneMgr.UI = nsENUM.eUI.eUI_NONE;
    }

    void CrateStage()
    {
        var pair = SharedObject.g_TableMgr.m_Stage.m_Dictionary.GetEnumerator();

        while (pair.MoveNext())//
        {
            Table_Stage.Info info = pair.Current.Value;

            if (null == info ||
                0 == info.m_nID)
                continue;

            GameObject obj = GameObject.Instantiate(GOPrefab) as GameObject;

            if (null == obj)
                continue;

            obj.transform.SetParent(TRArea);

            obj.transform.localScale = new Vector3(1, 1, 1);

            UI_StageList ui = obj.GetComponent<UI_StageList>();

            if (null != ui)
                ui.SetStage(info);

            m_listStage.Add(ui);
        }
    }

    void ChangeStage()
    {
        int nCount = m_listStage.Count;

        for(int i=0; i<nCount; ++i)
            m_listStage[i].CheckStage();

        Debug.Log(" ChangeStage ");
    }

    public void OnBtnBack()
    {
        if(null != SharedObject.g_Main)
            SharedObject.g_Main.SetStart();
    }
}
