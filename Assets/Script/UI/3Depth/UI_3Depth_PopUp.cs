﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public partial class UI_3Depth_PopUp : UI_3Depth
{
    public GameObject GOOption;
    public GameObject GOGameExit;
    public GameObject GOItemTooltip;
    public GameObject GOMSG; //여러 메세지
    public GameObject GOCreative;
    public GameObject GOClientUpdate;

    void Awake()
    {
        AwakeOption();
        AwakeSound();
    }

    protected override void OnEnable()
    {
        base.OnEnable();//base.OnEnable 호출시 무조건 Esc로 해제 해야함.. 아님 재정의시 스택 사용 x 

        m_eUI = nsENUM.eUI_CLOSE.eUI_CLOSE_3DEPTH_POPUP;
    }

    protected override void OnDisable()//OnEsc 호출 후 파괴될때 호출
    {
        base.OnDisable();

        m_eUI = nsENUM.eUI_CLOSE.eUI_CLOSE_NONE;
    }

    public void SetPopUp(nsENUM.eUI_3DEPTH _e, int _nLan = 0, string _str = "")
    {
        bool bTooltip = false;
        bool bMsg = false;
        bool bOption = false;
        bool bCreative = false;
        bool bApkUpdate = false;

        switch (_e)
        {
            case nsENUM.eUI_3DEPTH.eUI3DEPTH_OPTION:
                {
                    bOption = true;
                }
                break;
            case nsENUM.eUI_3DEPTH.eUI3DEPTH_ITEMTOOLTIP:
                {
                    bTooltip = true;
                }
                break;
            case nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG:
                {
                    bMsg = true;

                    string str = _str;

                    if (0 != _nLan)
                        str = SharedObject.g_TableMgr.m_Lan.Get(_nLan).m_strDec;

                    TEXTMSG.text = str;
                }
                break;
            case nsENUM.eUI_3DEPTH.eUI3DEPTH_CREATIVE:
                {
                    bCreative = true;
                }
                break;
            case nsENUM.eUI_3DEPTH.eUI3DEPTH_CLIENTUPDATE:
                {
                    bApkUpdate = true;
                }
                break;
        }

        GOOption.SetActive(bOption);
        GOItemTooltip.SetActive(bTooltip);
        GOMSG.SetActive(bMsg);
        GOCreative.SetActive(bCreative);
        GOClientUpdate.SetActive(bApkUpdate);
    }

    public void PopUpClose()
    {
        transform.gameObject.SetActive(false);
    }
}