﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public partial class UI_3Depth_PopUp : UI_3Depth
{
    public Button BtnGoogleAchieve;
    public Button BtnGoogleLeaderboard;

    public Toggle TGKR;
    public Toggle TGEN;
    public Toggle TGJP;

    public Toggle TGBGMOK;
    public Toggle TGBGMCANCEL;
    public Toggle TGEffectOK;
    public Toggle TGEffectCANCEL;

    public Text TEXTVersion;

    int m_nBgm;
    int m_nEffect;

    void AwakeOption()
    {
        if (SharedObject.g_bGoogleLogin)
        {
            BtnGoogleAchieve.interactable = true;
            BtnGoogleLeaderboard.interactable = true;
        }
        else
        {
            BtnGoogleAchieve.interactable = false;
            BtnGoogleLeaderboard.interactable = false;
        }

        switch (SystemLanguage.Korean)
        {
            case SystemLanguage.Korean:
                {
                    TGEN.interactable = false;
                    TGJP.interactable = false;

                    TGKR.interactable = true;
                }
                break;
            case SystemLanguage.Japanese:
                {
                    TGKR.interactable = false;
                    TGEN.interactable = false;

                    TGJP.interactable = true;
                }
                break;
            default:
                {
                    TGKR.interactable = false;
                    TGJP.interactable = false;

                    TGEN.interactable = true;
                }
                break;
        }

        TEXTVersion.text = Application.version;
    }

    void AwakeSound()
    {
        TGBGMOK.isOn = SharedObject.g_PlayerPrefsData.OptionBgm == 1;
        TGBGMCANCEL.isOn = SharedObject.g_PlayerPrefsData.OptionBgm == 0;

        TGEffectOK.isOn = SharedObject.g_PlayerPrefsData.OptionEffect == 1;
        TGEffectCANCEL.isOn = SharedObject.g_PlayerPrefsData.OptionEffect == 0;

        m_nBgm = SharedObject.g_PlayerPrefsData.OptionBgm;
        m_nEffect = SharedObject.g_PlayerPrefsData.OptionEffect;
    }

    public void OnBtnBgm(int _nOn)
    {
        m_nBgm = _nOn;
    }

    public void OnBtnEffect(int _nOn)
    {
        m_nEffect = _nOn;
    }

    public void OnBtnOptionExit()
    {
        if (m_nBgm != SharedObject.g_PlayerPrefsData.OptionBgm ||
            m_nEffect != SharedObject.g_PlayerPrefsData.OptionEffect)
        {
            SharedObject.g_PlayerPrefsData.OptionBgm = m_nBgm;

            SharedObject.g_SoundScript.MuteBgm(m_nBgm == 1);

            SharedObject.g_PlayerPrefsData.OptionEffect = m_nEffect;
        }

        SharedObject.g_Main.CheckMode(false);

        transform.gameObject.SetActive(false);
    }

    public void OnBtnGoogleAchieve()
    {
        if (SharedObject.g_bGoogleLogin)
            GPGSIds.GoogleAchieveUI();
    }

    public void OnBtnGoogleLeaderboard()
    {
        if (SharedObject.g_bGoogleLogin)
            GPGSIds.GoogleLeaderboardUI();
    }

    public void OnFileSave(bool _bCash = false)//파일 특정 위치에서 저장
    {
        string strpath = SharedObject.g_PlayerPrefsData.GetOptionPath();
        string strfile = strpath + "/RPGDice.rpg";

        if (System.IO.File.Exists(strfile) == false)//없으면 폴더 생성 후 저장
            Directory.CreateDirectory(strpath);

        if (SharedObject.g_PlayerPrefsData.SaveFile(strpath))
        {
            if(!_bCash)
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390008);
        }
        else
        {
            if (!_bCash)
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390009);
        }
    }

    public void OnFileLoad()//파일 특정 위치에서 로드
    {
        string strpath = SharedObject.g_PlayerPrefsData.GetOptionPath();
        string strfile = strpath + "/RPGDice.rpg";

        Debug.Log("str = " + strfile);

        if (false == System.IO.File.Exists(strfile))//없으면 save를 하라고 해야함
        {
            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390010);
        }
        else
        {
            if (SharedObject.g_PlayerPrefsData.LoadFile(strpath))//저장
            {
                strpath = SharedObject.g_PlayerPrefsData.GetDataPath();

                SharedObject.g_PlayerPrefsData.SaveFile(strpath);

                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390011);
            }
            else
            {
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390012);
            }
        }
    }
}