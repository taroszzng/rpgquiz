﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public partial class UI_3Depth_PopUp : UI_3Depth
{
    public Image IMGIcon;

    public Text TEXTTitle;
    public Text TEXTDec;

    public void InitItemTooltip(int _nItemID)
    {
        Table_Item.Info info = (Table_Item.Info)SharedObject.g_TableMgr.m_Item.Get(_nItemID);

        if (null == info)
            return;

        if (null != IMGIcon)
            IMGIcon.sprite = SharedObject.g_SceneMgr.CreateItemIcon(info.m_nIcon);

        Table_Lan.Info lan = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName);

        TEXTTitle.text = lan.m_strDec;

        lan = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec);

        TEXTDec.text = lan.m_strDec;
    }

    public void InitBookTooltip(int _nMonID)
    {
        Table_Monster.Info info = (Table_Monster.Info)SharedObject.g_TableMgr.m_Monster.Get(_nMonID);

        if (null == info)
            return;

        if (null != IMGIcon)
            IMGIcon.sprite = SharedObject.g_SceneMgr.CreateMonsterIcon(info.m_nIcon);

        Table_Lan.Info lan = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName);

        TEXTTitle.text = lan.m_strDec;

        lan = SharedObject.g_TableMgr.m_Lan.Get(info.m_nDec);

        TEXTDec.text = lan.m_strDec;
    }

    public void OnItemTooltipExit()
    {
        transform.gameObject.SetActive(false);
    }
}