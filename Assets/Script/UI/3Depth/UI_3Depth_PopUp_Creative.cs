﻿using UnityEngine;
using System.Collections;

public partial class UI_3Depth_PopUp : UI_3Depth
{
    public void OnBtnCreativeExit()
    {
        SharedObject.g_Main.CheckMode(false);

        transform.gameObject.SetActive(false);
    }
}