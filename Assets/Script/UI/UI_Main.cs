﻿using UnityEngine;
using UnityEngine.EventSystems;

public partial class UI_Main : MonoBehaviour
{
    public GameObject GOLogo;
    public GameObject GOStart;
    public GameObject GOMode;

    public GameObject GOCaution;
    public GameObject GOFullScreen;
    public GameObject GOPrologue;
    public GameObject GOTitle;

    public UI_PauseExit UIPauseExit;

    public UI_3Depth_PopUp UI3DepthPopup;

    void Awake()
    {
        SharedObject.g_Main = this;

#if UNITY_ANDROID
        InitGoogle();
#else
#endif
    }

    public void SetGameExit(bool _bActive)
    {
        SharedObject.g_SceneMgr.GameExit = _bActive;

        UIPauseExit.SetPause(false);
        UIPauseExit.gameObject.SetActive(_bActive);
    }

    public void SetPauseExit(bool _bActive)
    {
        UIPauseExit.SetPause(_bActive);
        UIPauseExit.gameObject.SetActive(_bActive);
    }

    public void SetStart(bool _bActive)
    {
        GOLogo.SetActive(!_bActive);
        GOStart.SetActive(_bActive);

        if (!SharedObject.g_bGoogleLogin)
            Select();
    }

    public void OnBtnPause()
    {
        SetPauseExit(true);
    }

    public void OnBtnGameStart()
    {
        GOStart.SetActive(false);
        GOMode.SetActive(true);
    }

    public void SetStart()
    {
        GOStart.SetActive(true);
        GOMode.SetActive(false);
    }

    public void SetTitle()
    {
        GOPrologue.SetActive(false);
        GOTitle.SetActive(true);
    }

    public void OnBtnGameExit()
    {
        SharedObject.g_SceneMgr.Shutdown();
    }

    public void OnBtnCaution()//경고
    {
        GOCaution.SetActive(false);
        GOFullScreen.SetActive(true);
    }

    public void OnBtnFullScrren()
    {
        SharedObject.g_SceneMgr.ApplicationPause = true;

        GOFullScreen.SetActive(false);

        if (!SharedObject.g_PlayerPrefsData.Prologue)
            GOTitle.SetActive(true);
        else
            GOPrologue.SetActive(true);

        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayBGM(nsENUM.eSOUNDBGM.eSOUNDBGM_TITLE);

#if UNITY_ANDROID
        //Select();
#else
#endif
    }
}