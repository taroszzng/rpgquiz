﻿using UnityEngine;
using UnityEngine.EventSystems;

public partial class UI_Main : MonoBehaviour
{
    public void OnClientUpdate()//apk,업데이트
    {
        CheckMode();

        UI3DepthPopup.transform.gameObject.SetActive(true);

        UI3DepthPopup.SetPopUp(nsENUM.eUI_3DEPTH.eUI3DEPTH_CLIENTUPDATE);
    }

    public void OnBtnGameOption()//게임옵션
    {
        CheckMode();

        UI3DepthPopup.transform.gameObject.SetActive(true);

        UI3DepthPopup.SetPopUp(nsENUM.eUI_3DEPTH.eUI3DEPTH_OPTION);
    }

    public void OnPeople()//만든사람들
    {
        CheckMode();

        UI3DepthPopup.transform.gameObject.SetActive(true);

        UI3DepthPopup.SetPopUp(nsENUM.eUI_3DEPTH.eUI3DEPTH_CREATIVE);
    }

    public void ItemToolTip(int _nItemID)
    {
        UI3DepthPopup.transform.gameObject.SetActive(true);

        UI3DepthPopup.SetPopUp(nsENUM.eUI_3DEPTH.eUI3DEPTH_ITEMTOOLTIP);
        UI3DepthPopup.InitItemTooltip(_nItemID);//아이템
    }

    public void BookToolTip(int _nMonID)
    {
        UI3DepthPopup.transform.gameObject.SetActive(true);

        UI3DepthPopup.SetPopUp(nsENUM.eUI_3DEPTH.eUI3DEPTH_ITEMTOOLTIP);
        UI3DepthPopup.InitBookTooltip(_nMonID);//도감 
    }

    public void PopUp3Depth(nsENUM.eUI_3DEPTH _eDepth, int _nLan, string _str = "")
    {
        UI3DepthPopup.transform.gameObject.SetActive(true);
        UI3DepthPopup.SetPopUp(_eDepth, _nLan, _str);
    }

    public void CheckMode(bool _bActive = true)
    {
        //switch (SharedObject.g_SceneMgr.Scene)
        //{
        //    case nsENUM.eSCENE.eSCENE_MODE:
        //        break;
        //    default:
        //        GOMode.SetActive(_bActive);
        //        break;
        //}
    }
}