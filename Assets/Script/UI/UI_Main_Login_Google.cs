﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Network;
using UnityEngine.UI;

#if UNITY_ANDROID 
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine.SocialPlatforms;

using Com.Google.Android.Gms.Common;
using Google.Developers;
using Com.Google.Android.Gms.Common.Api;
using GooglePlayGames.Android;

public partial class UI_Main : MonoBehaviour
{
    //Com.Google.Android.Gms.Common.Api.GoogleApiClient mGoogleApiClient;

    public void InitGoogle()
    {
        Debug.Log("=================================== SharedObject.g_SelectGoogle = this======================================");
        Debug.Log("=================================== PlayGamesPlatform.Activate======================================");

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration
            .Builder()
            .Build();

        PlayGamesPlatform.InitializeInstance(config);

        PlayGamesPlatform.DebugLogEnabled = true;

        PlayGamesPlatform.Activate();
    }

    public void LogOutGoogle()
    {
        if (Social.localUser.authenticated)
            ((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut();
    }

    public void Select()
    {
        Debug.Log("Select GoogleConnect = " + SharedObject.g_SceneMgr.ApplicationPause);

        if (Social.localUser.authenticated)
            ((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut();

        Debug.Log("Select = " + Social.localUser.authenticated);

        string str = "guest";

        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    SharedObject.g_bGoogleLogin = true;

                    str = PlayGamesPlatform.Instance.GetUserId();

                    Debug.Log("Google ID = " + PlayGamesPlatform.Instance.GetUserId());
                    Debug.Log("Google Email = " + PlayGamesPlatform.Instance.GetUserEmail());
                }
                else
                {
                }

                //StartCoroutine(Login(str));

                Debug.Log("Google fail success = " + success);
                Debug.Log("Google fail str = " + str);

                return;
            });
        }
        else
        {
            str = PlayGamesPlatform.Instance.GetUserId();

            Debug.Log("Google ID = " + PlayGamesPlatform.Instance.GetUserId());
            Debug.Log("Google Email = " + PlayGamesPlatform.Instance.GetUserEmail());
        }

//        if (SharedObject.g_SceneMgr.ServerConnect)
//            return;

//#if UNITY_EDITOR
//        if (0 < TEXTLoginTest.text.Length)
//            str = TEXTLoginTest.text;
//#endif

//        StartCoroutine(Login(str));
    }
}
#endif
