﻿#define RELEASE
/*
이름:김광수
작업:스크롤
MailCellView = 보여지고 있는 스크롤 데이타들
[kks][Scroller]		스크롤
*/
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;
using System.Text;

namespace EnhancedScrollerRpgDice.Set
{
    public class SetCellView : nsEnhancedScroller.ScrollerBaseCellView
    {
        public GameObject GOEff;

        ScrollerData m_ScrollerData;

        public GameObject GOHold;
        public GameObject GOLock;
        public GameObject GOFaceLock;

        public Image IMGBG;
        public Image IMGIcon;

        public Text TEXTName;
        public Text TEXTDec;
        public Text TEXTBtnText;

        public Button BTNUse;

        void OnDestroy()
        {
            //if (m_ScrollerData != null)
            //    m_ScrollerData.selectedChanged -= SelectedChanged;
        }

        public override void SetData(int dataIndex, nsEnhancedScroller.ScrollerBaseData data, bool isVertical)//직접 엘리먼트를 작성
        {
            base.SetData(dataIndex, data, isVertical);

            m_ScrollerData = (ScrollerData)data;

            if (null == m_ScrollerData)
                return;

            SetBg();
            SetIcon();
            SetText();
            SetBtn();

            if (null != GOLock)
                GOLock.SetActive(m_ScrollerData.m_bLock);

            if (null != GOFaceLock)
                GOFaceLock.SetActive(m_ScrollerData.m_bLock);
        }

        void SetBg()
        {
            if (null == IMGBG)
                return;

            string str = "836551FF";

            switch(m_ScrollerData.m_eSet)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                    {
                        bool bcolor = SharedObject.g_SceneMgr.BattleColor;

                        if (bcolor)
                            str = "6D5546FF";

                        SharedObject.g_SceneMgr.BattleColor = !bcolor;
                    }
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    {
                        bool bcolor = SharedObject.g_SceneMgr.InvenColor;

                        if (bcolor)
                            str = "6D5546FF";

                        SharedObject.g_SceneMgr.InvenColor = !bcolor;
                    }
                    break;
                default:
                    {
                        if (0 != (m_ScrollerData.m_nID % 2))
                            str = "6D5546FF";
                    }
                    break;
            }

            IMGBG.color = ColorDefine.HexToColor(str);
        }

        void SetIcon()
        {
            if (null == IMGIcon)
                return;

            switch(m_ScrollerData.m_eSet)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_BOOK:
                        IMGIcon.sprite = SharedObject.g_SceneMgr.CreateMonsterIcon(m_ScrollerData.m_nIcon);
                    break;
                default:
                    IMGIcon.sprite = SharedObject.g_SceneMgr.CreateItemIcon(m_ScrollerData.m_nIcon);
                    break;
            } 
        }

        void SetText()
        {
            if (null != TEXTName)
            {
                switch(m_ScrollerData.m_eSet)
                {
                    //case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                    case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                        {
                            string strea = SharedObject.g_TableMgr.m_Lan.Get(1304006).m_strDec;

                            TEXTName.text = m_ScrollerData.m_strName + "\n" +
                                m_ScrollerData.m_nValue.ToString() + strea + "\n";
                        }
                        break;
                    case nsENUM.eSETPOPUP.eSETPOPUP_SHOP:
                        {
                            string str = SharedObject.g_TableMgr.m_Lan.Get(1304005).m_strDec;
                            string strea = SharedObject.g_TableMgr.m_Lan.Get(1304006).m_strDec;
                            string strprice = SharedObject.g_TableMgr.m_Lan.Get(1304004).m_strDec;

                            TEXTName.text = m_ScrollerData.m_strName + "\n" +
                                str + " " + m_ScrollerData.m_nValue.ToString() + strea + "\n" +
                                strprice + " " + m_ScrollerData.m_strPrice;
                        }
                        break;
                    case nsENUM.eSETPOPUP.eSETPOPUP_BOOK:
                        TEXTName.text = m_ScrollerData.m_strName;
                        break;
                }
            }

            if (null != TEXTDec)
                TEXTDec.text = m_ScrollerData.m_strDec;
        }

        void SetBtn()
        {
            if (nsENUM.eSETPOPUP.eSETPOPUP_INVEN != m_ScrollerData.m_eSet)
                return;

            Item item = SharedObject.g_PlayerPrefsData.GetItem(m_ScrollerData.m_nID);

            if (null == item)
                return;

            bool bHold = false;

            int nLan = 1370007; //사용

            if (nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL == item.m_eItemType)//영구제만 해제 불가 교체만 가능
            {
                if (m_ScrollerData.m_bSelect)
                    bHold = true;
            }
            else
            {
                if (m_ScrollerData.m_bSelect)
                    nLan = 1370012;   
            }

            if (null != TEXTBtnText)
            {
                string str = SharedObject.g_TableMgr.m_Lan.Get(nLan).m_strDec;

                TEXTBtnText.text = str;//해제와 사용 둘중하나.
            }

            if (null != GOHold)
                GOHold.SetActive(bHold);

            bool buse = true;

            if (0 >= m_ScrollerData.m_nValue)
                buse = false;

            if (null != BTNUse)//버튼 사용관련 잠김
                BTNUse.interactable = buse;
        }

        protected override void BtnSelect()//선택
        {
            base.BtnSelect();

            Debug.Log(" ===== SkinCellView BtnSelect ===== " + selected);
        }

        public void OnBtnUse()//사용
        {
            if (nsENUM.eSETPOPUP.eSETPOPUP_INVEN == m_ScrollerData.m_eSet)//인벤아이템
            {
                if (SharedObject.g_SceneMgr.BattlePlay)//전투중엔 교체 안되게
                {
                    SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1300037);
                    return;
                }
            }

            switch (m_ScrollerData.m_eSet)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN://배틀에 등록, 또는 능력치 적용
                    {
                        int nItemID = m_ScrollerData.m_nID;

                        if (m_ScrollerData.m_bSelect)//장착 탈착
                        {
                            Item item = SharedObject.g_PlayerPrefsData.GetItem(nItemID);

                            if (null == item)
                                return;

                            SharedObject.g_PlayerPrefsData.ItemUse(nItemID, false);//해제

                            m_ScrollerData.m_bSelect = false;

                            SharedObject.g_AModeSet.SetInvenEquip(nItemID, false);

                            SharedObject.g_CollectorMgr.ResetStatItem();
                        }
                        else//사용
                        {
                            if (eTUTORIAL.eTUTORIAL_INVEN == SharedObject.g_PlayerPrefsData.Tutorial)
                                SharedObject.g_Tutorial.OnPopupBtn();

                            Item item = SharedObject.g_PlayerPrefsData.GetItem(nItemID);

                            if (null == item)
                                return;

                            if (!SharedObject.g_PlayerPrefsData.CheckItem(item.m_nItemID))//장착 가능하면 false
                            {
                                nsENUM.eSETPOPUP popup = nsENUM.eSETPOPUP.eSKINPOPUP_END;

                                if (nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL == item.m_eItemType)//영구제 버프에 적용
                                    popup = nsENUM.eSETPOPUP.eSETPOPUP_ABILITY;
                                else
                                    popup = nsENUM.eSETPOPUP.eSETPOPUP_BATTLE;

                                //장착중인 같은 부위 아이템체크
                                int useitemid = SharedObject.g_PlayerPrefsData.CheckEquip(popup, item.m_nItemID);

                                if (-1 == useitemid)//장착 불가(4개가 다 찼을시에 같은부위가 없을시에 또는 장착중인 아이템?)
                                {
                                    int nLan = 1300037;
                                    //장착중인 갯수
                                    if ((int)nsENUM.eVALUE.eVALUE_4 <= SharedObject.g_PlayerPrefsData.CheckEquipCount(popup))
                                        nLan = 1300038;

                                    SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, nLan);

                                    return;
                                }

                                SharedObject.g_PlayerPrefsData.ItemUse(useitemid, false);//데이타 해제
                                SharedObject.g_PlayerPrefsData.ItemUse(item.m_nItemID, true);//데이타 장착

                                m_ScrollerData.m_bSelect = true;

                                //인벤쪽에 해제와 장착
                                SharedObject.g_AModeSet.SetInvenEquip(useitemid, false);
                                SharedObject.g_AModeSet.SetInvenEquip(item.m_nItemID, true);

                                if (useitemid != item.m_nItemID)//다르면 탈착
                                {
                                    SharedObject.g_CollectorMgr.UnquipItem(
                                        nsENUM.eTEAM.BLUE_TEAM, useitemid);//탈착
                                }

                                if (nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL == item.m_eItemType)//영구제만 바로 적용
                                {
                                    SharedObject.g_CollectorMgr.ItemStatUse(
                                        nsENUM.eTEAM.BLUE_TEAM, item.m_eItemType, item.m_nItemID);//장착
                                }
                            }
                        }
                    }
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_SHOP:
                    {
                        if(!SharedObject.g_bGoogleLogin)
                        {
#if !DEVELOPMENT
                            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1300040);

                            return;
#endif
                        }

                        Table_Shop.Info info = SharedObject.g_TableMgr.m_Shop.Get(m_ScrollerData.m_nID);

                        if (null != info)
                        {
                            Item item = SharedObject.g_PlayerPrefsData.GetItem(info.m_nItemID);//영구제는 있으면 실패로

                            if(null != item)
                            {
                                if(nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL == item.m_eItemType)
                                {   //영구제 아이템은 1번만 구입 가능
                                    SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1370013);

                                    return;
                                }
                            }

                            SharedObject.g_SceneMgr.BuyID = info.m_nID;

                            SharedObject.g_Mode.GOPayment.SetActive(true);//결제창

                            SharedObject.g_SceneMgr.ApplicationPause = true;
                        }
                    }
                    break;
            }
        }

        public void OnBtnInfo()//정보
        {
            switch (m_ScrollerData.m_eSet)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    {
                        SharedObject.g_Main.ItemToolTip(m_ScrollerData.m_nID);
                    }
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_SHOP:
                    {
                        Table_Shop.Info info = SharedObject.g_TableMgr.m_Shop.Get(m_ScrollerData.m_nID);

                        if (null == info)
                            return;

                        SharedObject.g_Main.ItemToolTip(info.m_nItemID);
                    }
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_BOOK:
                    {
                        SharedObject.g_Main.BookToolTip(m_ScrollerData.m_nID);
                    }
                    break;
            }
        }
    }
}