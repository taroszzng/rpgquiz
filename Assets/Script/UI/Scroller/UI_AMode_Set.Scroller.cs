﻿/*
이름:김광수
작업:[kks][Mail]:스크롤 셋팅 관련
*/
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerRpgDice.Set
{
    public class ScrollerData : nsEnhancedScroller.ScrollerBaseData//엘리먼트 데이타들
    {
        public int m_nID = 0;
        public nsENUM.eSETPOPUP m_eSet;
        public int m_nIcon = 0;
        public string m_strName = "";
        public string m_strDec = "";
        public string m_strPrice = "";
        public int m_nValue = 0;
        public bool m_bLock;
        public bool m_bSelect;
    }

    public partial class UI_AMode_Set: MonoBehaviour, IEnhancedScrollerDelegate
    {
        float m_ScrollerPos = 0f;

        void CellViewSelected(EnhancedScrollerCellView cellView)//선택시 활성화
        {
        }

        void Reload()
        {
            // tell the scrollers to reload
            //vScroller.ReloadData();
            hScroller.ReloadData();
        }

        #region Controller UI Handlers
        /// This handles the toggle for the masks
        /// <param name="val">Is the mask on?</param>
        public void MaskToggle_OnValueChanged(bool val)
        {
            // set the mask component of each scroller
            //vScroller.GetComponent<Mask>().enabled = val;
            hScroller.GetComponent<Mask>().enabled = val;
        }
        /// This handles the toggle fof the looping
        /// <param name="val">Is the looping on?</param>
        public void LoopToggle_OnValueChanged(bool val)
        {
            // set the loop property of each scroller
            //vScroller.Loop = val;
            hScroller.Loop = val;
        }
        #endregion

        #region EnhancedScroller Callbacks

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            int nCount = 0;

            switch (m_eSetTab)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    nCount = m_listInvenData.Count;
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_BOOK:
                    nCount = m_listBookData.Count;
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_SHOP:
                    {
                        if (1 == m_nConsume)
                            nCount = m_listShopConsumeData.Count;
                        else
                            nCount = m_listShopEternalData.Count;
                    }
                    break;
                default:
                    break;
            }

            return nCount;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return listSize[(int)m_eSetTab];
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            // first get a cell from the scroller. The scroller will recycle if it can.
            // We want a vertical cell prefab for the vertical scroller and a horizontal
            // prefab for the horizontal scroller.
            SetCellView cellView = scroller.GetCellView(hCellViewPrefab[(int)m_eSetTab]) as SetCellView;

            // set the name of the cell. This just makes it easier to see in our
            // hierarchy what the cell is
            //cellView.name = (scroller == vScroller ? "Vertical" : "Horizontal") + " " + m_MailData[dataIndex];

            // set the selected callback to the CellViewSelected function of this controller. 
            // this will be fired when the cell's button is clicked
            cellView.selected = CellViewSelected;

            nsEnhancedScroller.ScrollerBaseData data = null;

            switch (m_eSetTab)
            {
                case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_INVEN:
                    data = m_listInvenData[dataIndex];
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_BOOK:
                    data = m_listBookData[dataIndex];
                    break;
                case nsENUM.eSETPOPUP.eSETPOPUP_SHOP:
                    {
                        if (1 == m_nConsume)
                            data = m_listShopConsumeData[dataIndex];
                        else
                            data = m_listShopEternalData[dataIndex];
                    }
                    break;
                default:
                    break;
            }

            // set the data for the cell
            cellView.SetData(dataIndex, data, (scroller == hScroller));

            // return the cell view to the scroller
            return cellView;
        }
        #endregion
    }
}