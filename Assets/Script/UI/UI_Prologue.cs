﻿using UnityEngine;
using System.Collections.Generic;

public class UI_Prologue : MonoBehaviour
{
    public List<GameObject> GOList;

    public void OnBtnNext(int _nNext)
    {
        GOList[_nNext].SetActive(false);

        if (_nNext + 1 < GOList.Count)
            GOList[_nNext + 1].SetActive(true);
        else
            OnBtnSkip();
    }

    public void OnBtnSkip()
    {
        SharedObject.g_PlayerPrefsData.Prologue = false;

        if (null != SharedObject.g_Main)
            SharedObject.g_Main.SetTitle();
    }
}