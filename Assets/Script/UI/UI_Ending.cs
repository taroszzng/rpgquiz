﻿using UnityEngine;
using System.Collections.Generic;

public class UI_Ending : MonoBehaviour
{
    public List<GameObject> GOList;

    public void OnBtnNext(int _nNext)
    {
        GOList[_nNext].SetActive(false);

        if (_nNext + 1 < GOList.Count)
            GOList[_nNext + 1].SetActive(true);
        else
            OnBtnSkip();
    }

    public void OnBtnSkip()
    {
        if (null != SharedObject.g_Main)
        {
            SharedObject.g_Main.SetStart();
            SharedObject.g_Main.SetTitle();
            SharedObject.g_Mode.SetMonsterSelect(false);

            SharedObject.g_Mode.GO2Depth.SetActive(false);

            transform.gameObject.SetActive(false);
        }
    }
}