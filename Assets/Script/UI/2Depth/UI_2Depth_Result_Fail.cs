﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class UI_2Depth_Result : UI_2Depth
{
    public Text TEXTFail;

    void InitFail()
    {
        Table_Level.Info level = SharedObject.g_TableMgr.m_Level.Get(SharedObject.g_PlayerPrefsData.Level);

        if (null == level)
            return;

        string strlen = SharedObject.g_PlayerPrefsData.Exp.ToString();

        int nLan = 1380005;

        int nID = SharedObject.g_PlayerPrefsData.MonsterID;

        if (SharedObject.g_PlayerPrefsData.Ending)
            nID = SharedObject.g_SceneMgr.MonsterSelect;

        Table_Monster.Info mon = SharedObject.g_TableMgr.m_Monster.Get(nID);

        if (null == mon)
            return;

        if (SharedObject.g_PlayerPrefsData.Level < (mon.m_nLevel + level.m_nMaxLevel))
        {
            if (100 <= SharedObject.g_PlayerPrefsData.Exp)//레벨업
            {
                nLan = 1380010;

                SharedObject.g_PlayerPrefsData.Exp = 0;

                strlen = SharedObject.g_PlayerPrefsData.Level.ToString();
            }
        }
        else//더이상 업 불가
            nLan = 1380009;

        string str = SharedObject.g_TableMgr.m_Lan.Get(nLan).m_strDec;

        str = string.Format(str, strlen);

        TEXTFail.text = str;
    }

    void Fail()
    {
        SharedObject.g_AMode.OnEvent();

        GOFail.SetActive(false);
    }
}
