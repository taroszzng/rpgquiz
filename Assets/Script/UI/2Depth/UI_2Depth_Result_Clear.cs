﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class UI_2Depth_Result : UI_2Depth
{
    public GameObject GOClearPrefab;

    public Text TEXTClear;

    public Transform TRItem;

    List<GameObject> m_ListReward = new List<GameObject>();

    void CreateItem()
    {
        int nID = SharedObject.g_SceneMgr.ClearMonsterID;

        if (SharedObject.g_PlayerPrefsData.Ending)
            nID = SharedObject.g_SceneMgr.MonsterSelect;

        Table_Monster.Info info = SharedObject.g_TableMgr.m_Monster.Get(nID);

        if (null == info)
            return;

        Table_Reward.Info reward = SharedObject.g_TableMgr.m_Reward.Get(info.m_nReward);

        if (null == reward)
            return;

        int nCount = reward.m_listItem.Count;

        for(int i=0; i<nCount; ++i)
        {
            Table_Item.Info item = SharedObject.g_TableMgr.m_Item.Get(reward.m_listItem[i].m_nItemID);

            if (null == item)
                continue;

            GameObject obj = Instantiate(GOClearPrefab) as GameObject;

            if (null == obj)
                break;

            obj.transform.SetParent(TRItem);

            obj.transform.localScale = new Vector3(1, 1, 1);

            int nItemID = reward.m_listItem[i].m_nItemID;
            int nItemValue = reward.m_listItem[i].m_nItemValue;

            UI_RewardItemList ui = obj.GetComponent<UI_RewardItemList>();

            if (null != ui)
                ui.InitRewardItem(nItemID, item.m_nIcon, nItemValue); 

            m_ListReward.Add(obj);

            InsertItem(nItemID, nItemValue);//인벤에 아이템 저장
        }

        string str = SharedObject.g_TableMgr.m_Lan.Get(1380004).m_strDec;

        str = string.Format(str, SharedObject.g_PlayerPrefsData.Level);

        TEXTClear.text = str;
    }

    void InsertItem(int _nItemID, int _nItemValue)
    {
        SharedObject.g_PlayerPrefsData.ItemInsert(_nItemID, _nItemValue);//인벤 아이템 추가

        SharedObject.g_AModeSet.InsertSet(nsENUM.eSETPOPUP.eSETPOPUP_INVEN, _nItemID, _nItemValue);//인벤 ui에 저장
    }

    void DeleteItem()
    {
        int nCOunt = m_ListReward.Count;

        for(int i=0; i<nCOunt; ++i)
            DestroyObject(m_ListReward[i]);
    }

    void Clear()
    {
        DeleteItem();

        int nCurStage = (SharedObject.g_SceneMgr.CurStage - 1) / (int)nsENUM.eVALUE.eVALUE_6;
        int nClearStage = (SharedObject.g_PlayerPrefsData.ClearStage - 1) / (int)nsENUM.eVALUE.eVALUE_6;

        SharedObject.g_SceneMgr.CurStage = SharedObject.g_PlayerPrefsData.ClearStage;

        if (nCurStage < nClearStage)//스테이지로 갱신
        {
            SharedObject.g_AMode.OnEvent();
            SharedObject.g_Mode.SetStage();

            if (SharedObject.g_bGoogleLogin)
                GPGSIds.GoogleClearStage(SharedObject.g_PlayerPrefsData.ClearStage);//클리어시 구글로 전송
        }
        else
        {
            SharedObject.g_Mode.SetMonsterSelect(true);

            SharedObject.g_AMode.OnEvent();

            SharedObject.g_Mode.GOAMode.SetActive(false);
        }

        GOClear.SetActive(false);
    }
}
