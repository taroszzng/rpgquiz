﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class UI_2Depth_Result : UI_2Depth
{
    public GameObject GOClear;
    public GameObject GOFail;

    public GameObject GOEnding;

    protected override void OnEnable()
    {
        base.OnEnable();//base.OnEnable 호출시 무조건 Esc로 해제 해야함.. 아님 재정의시 스택 사용 x 

        m_eUI = nsENUM.eUI_CLOSE.eUI_CLOSE_2DEPTH_RESULT;

        SetPopUp();
    }

    protected override void OnDisable()//OnEsc 호출 후 파괴될때 호출
    {
        base.OnDisable();

        m_eUI = nsENUM.eUI_CLOSE.eUI_CLOSE_NONE;
    }

    public void SetPopUp()
    {
        SharedObject.g_AMode.UIEffectNotic.gameObject.SetActive(false);
        SharedObject.g_AMode.UIEffectItem.gameObject.SetActive(false);
        SharedObject.g_AMode.UIEffectCombo.gameObject.SetActive(false);

        if (SharedObject.g_SceneMgr.MonsterClear)
        {
            GOClear.SetActive(true);

            CreateItem();
        }
        else
        {
            GOFail.SetActive(true);

            InitFail();
        }
    }

    public void OnBtnOk()
    {
        if (SharedObject.g_SceneMgr.MonsterClear)
            Clear();
        else
            Fail();

        //SharedObject.g_DiceMgr.ReSet();

        if (SharedObject.g_PlayerPrefsData.Ending)//엔딩
        {
            if (0 != SharedObject.g_SceneMgr.MonsterSelect)
                transform.gameObject.SetActive(false);
            else
                GOEnding.SetActive(true);
        }
        else
            transform.gameObject.SetActive(false);
    }
}
