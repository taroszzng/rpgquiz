﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_2Depth_Payment : UI_2Depth
{
    public Text TEXTDec;
    public Text TEXTPay; //가격
    public Text TEXTCurrency; //통화

    protected override void OnEnable()
    {
        base.OnEnable();//base.OnEnable 호출시 무조건 Esc로 해제 해야함.. 아님 재정의시 스택 사용 x 

        m_eUI = nsENUM.eUI_CLOSE.eUI_CLOSE_2DEPTH_PAYMENT;

        SetPayment();
    }

    protected override void OnDisable()//OnEsc 호출 후 파괴될때 호출
    {
        base.OnDisable();

        m_eUI = nsENUM.eUI_CLOSE.eUI_CLOSE_NONE;
    }

    void SetPayment()
    {
        Table_Shop.Info info = SharedObject.g_TableMgr.m_Shop.Get(SharedObject.g_SceneMgr.BuyID);

        if (null == info)
            return;

        string strlen = SharedObject.g_TableMgr.m_Lan.Get(1380007).m_strDec;
        string str = SharedObject.g_TableMgr.m_Lan.Get(info.m_nName).m_strDec;

        strlen = string.Format(strlen, str);

        TEXTDec.text = strlen;

        strlen = SharedObject.g_TableMgr.m_Lan.Get(info.m_nPrice).m_strDec;

        TEXTPay.text = strlen;

        TEXTCurrency.text = SharedObject.g_Purchaser.GetCurrency();
    }

    public void OnBtnOk()
    {
        transform.gameObject.SetActive(false);

        SharedObject.g_Purchaser.BuyProductID(SharedObject.g_SceneMgr.BuyID.ToString()); 
    }

    public void OnBtnCancel()
    {
        transform.gameObject.SetActive(false);
    }
}
