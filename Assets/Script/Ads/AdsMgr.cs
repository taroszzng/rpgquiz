﻿using UnityEngine;
using UnityEngine.UI;

using System;

public class AdsMgr : MonoBehaviour
{
    public Text TEXTAdsCount;

    AdMob m_AdMob = new AdMob();
    UnityAds m_UnityAds = new UnityAds();

    void Awake()
    {
        m_AdMob.InItInterstitial();
        m_UnityAds.Init();
    }

    private void Start()
    {
        string strday = DateTime.Today.ToShortDateString();//오늘 날짜

        if (strday != SharedObject.g_PlayerPrefsData.Date)
        {
            SharedObject.g_PlayerPrefsData.Date = strday;
            SharedObject.g_PlayerPrefsData.AdsCount = (int)nsENUM.eADS.eADS_REWARDCOUNT;
        }

        TEXTAdsCount.text = SharedObject.g_PlayerPrefsData.AdsCount.ToString();

        m_AdMob.SetText(TEXTAdsCount);
        m_UnityAds.SetText(TEXTAdsCount);
    }

    void Update()
    {
        m_UnityAds.UpdateUnityAds();//유니티 광고 로드

#if !UNITY_EDITOR
        if (!m_AdMob.IsLoadAdMob())
            m_AdMob.LoadAd();
#endif
    }

    public void OnBtnClick()
    {
        if(0 >= SharedObject.g_PlayerPrefsData.AdsCount)
        {
            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390007);

            return;
        }

#if UNITY_EDITOR
        if (m_UnityAds.UpdateUnityAds())
        {
            Debug.Log(" =========== Unity Ads =========== ");
            m_UnityAds.ShowAd();
        }
#else
        if (m_AdMob.IsLoadAdMob())
        {
            Debug.Log(" =========== Admob =========== ");
            m_AdMob.AdMobShow();
        }
        else if (m_UnityAds.UpdateUnityAds())
        {
            Debug.Log(" =========== Unity Ads =========== ");
            m_UnityAds.ShowAd();
        }
        else
            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390005);
#endif
    }
}