﻿using UnityEngine;
using UnityEngine.UI;

public class AdsBase : MonoBehaviour
{
    Text m_Text;

    protected void RewardAds()//20회 고정하자
    {
        int nran = Random.Range(0, (int)nsENUM.eADS.eADS_RANCOUNT);//10030~10033 랜덤 지급

        nran += (int)nsENUM.eADS.eADS_REWARDITEM;

        SharedObject.g_PlayerPrefsData.ItemInsert(nran, 1);

        SharedObject.g_PlayerPrefsData.AdsCount -= 1;

        SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1390006);

        SharedObject.g_SceneMgr.ApplicationPause = false;
        SharedObject.g_SceneMgr.Pause = false;

        m_Text.text = SharedObject.g_PlayerPrefsData.AdsCount.ToString();
    }

    public void SetText(Text _text)
    {
        m_Text = _text;
    }
}