﻿#define RELEASE

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using UnityEngine.Advertisements;

public class UnityAds : AdsBase
{
#if UNITY_IOS
private string m_strGameID = "3069478";
#elif UNITY_ANDROID
    private string m_strGameID = "3069479";
#endif

    //public Button BTNUnityAds;

    public string m_strPlacementID = "rewardedVideo";

    bool m_bReady = false;

    public void Init()
    {
        if (Advertisement.isSupported)
        {
#if DEVELOPMENT
            Advertisement.Initialize(m_strGameID, true);
#else
            Advertisement.Initialize(m_strGameID, false);
#endif
            Debug.Log("UnityAds");
        }
    }

    public bool UpdateUnityAds()
    {
#if !DEVELOPMENT
        if (!m_bReady)//로드했으니 다시 로드하지말게
            m_bReady = Advertisement.IsReady(m_strPlacementID);

        return m_bReady;
#else
        return true;
#endif
    }

    public void ShowAd()
    {
        SharedObject.g_SceneMgr.ApplicationPause = true;
        SharedObject.g_SceneMgr.Pause = true;

#if !DEVELOPMENT
        ShowOptions options = new ShowOptions();

        options.resultCallback = HandleShowResult;

        Advertisement.Show(m_strPlacementID, options);

        m_bReady = false;
#endif
    }

#if !DEVELOPMENT
    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");

            RewardAds();
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.Log("Video was skipped - Do NOT reward the player");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.Log("Video failed to show");
        }

        SharedObject.g_SceneMgr.ApplicationPause = false;
        SharedObject.g_SceneMgr.Pause = false;
    }
#endif
}