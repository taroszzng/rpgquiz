﻿#define RELEASE

using System;
using UnityEngine;

using GoogleMobileAds.Api;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class AdMob : AdsBase
{
    RewardBasedVideoAd m_RewardAd = null; // 전면광고 변수
    //BannerView m_BannerView = null; // 배너 출력

    string m_AdID = "ca-app-pub-6596662249029582/9612036411"; // 전면배너 Key
    //string m_BannerViewID = "ca-app-pub-6596662249029582/8386711661"; // 배너 Key
    string m_AdMobID = "ca-app-pub-6596662249029582~1772364403";

    public void InItInterstitial()
    {
        //SharedObject.g_AdMob = this;
        MobileAds.SetiOSAppPauseOnBackground(true);

        //// Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(m_AdMobID);

        //InitBannerView();
        InItAd();
    }

    public void Start()
    {
        //Debug.Log("m_BannerView = " + m_BannerView);

        //m_BannerView.Show();
    }

    void InitBannerView()
    {
        //m_BannerView = new BannerView(m_BannerViewID, AdSize.Banner, AdPosition.Bottom);

        //// 애드몹 리퀘스트 초기화
        //AdRequest request = new AdRequest.Builder()
        //.AddTestDevice( AdRequest.TestDeviceSimulator )       // Simulator.
        //.AddTestDevice("18D160264F92D2E1")  // My test device.
        //.Build();

        //m_BannerView.LoadAd(request); //배너 광고 요청
    }

    public void InItAd()
    {
        // Create an interstitial.
        m_RewardAd = RewardBasedVideoAd.Instance;

        // Register for ad events.
        m_RewardAd.OnAdLoaded += OnAdLoaded;
        m_RewardAd.OnAdFailedToLoad += OnAdFailedToLoad;
        m_RewardAd.OnAdOpening += OnAdOpened;
        m_RewardAd.OnAdStarted += OnAdStarted;
        m_RewardAd.OnAdRewarded += OnAdRewarded;
        m_RewardAd.OnAdClosed += OnAdClosed;
        m_RewardAd.OnAdLeavingApplication += OnAdLeavingApplication;
       
        // Load an interstitial ad.
        LoadAd();
    }

    public void LoadAd()
    {
        AdRequest.Builder builder = new AdRequest.Builder();

#if !DEVELOPMENT
        AdRequest request = builder.Build();
#else
        AdRequest request = builder.AddTestDevice(AdRequest.TestDeviceSimulator)
            .AddTestDevice("CA732E51946B368A")
            .Build();
#endif
        m_RewardAd.LoadAd(request, m_AdID);// 전면 광고 요청
    }

    void OnAdLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialLoaded event received");
    }

    void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("OnAdFailedToLoad event received with message: " + args.Message);
    }

    void OnAdOpened(object sender, EventArgs args)
    {
        Debug.Log("OnAdOpened event received");
    }

    void OnAdStarted(object sender, EventArgs args)
    {
        Debug.Log("OnAdStarted event received");

        SharedObject.g_SceneMgr.Pause = true;
    }

    void OnAdClosed(object sender, EventArgs args)
    {
        Debug.Log("OnAdClosed event received");

        SharedObject.g_SceneMgr.ApplicationPause = false;
        SharedObject.g_SceneMgr.Pause = false;
    }

    void OnAdRewarded(object sender, EventArgs args)
    {
        Debug.Log("OnAdRewarded event received");

        RewardAds(); //보상
    }

    public void OnAdLeavingApplication(object sender, EventArgs args)
    {
        Debug.Log("OnAdLeavingApplication event received");
    }

    public void AdMobShow()//보여주기
    {
        Debug.Log("m_RewardAd = " + m_RewardAd);

        if (m_RewardAd.IsLoaded())
        {
            SharedObject.g_SceneMgr.ApplicationPause = true;
            SharedObject.g_SceneMgr.Pause = true;

            m_RewardAd.Show();
        }
    }

    public bool IsLoadAdMob()
    {
        bool bLoad = m_RewardAd.IsLoaded();

        return bLoad;
    }
}
