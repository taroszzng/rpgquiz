#define RELEASE

using UnityEngine;
using System.Collections;

public class DownloadObbExample : MonoBehaviour
{
    private string expPath;
    private string logtxt;
    private bool alreadyLogged = false;
    private string nextScene = "Main";
    private bool downloadStarted;

    void log(string t)
    {
        logtxt += t + "\n";
        print("MYLOG " + t);
    }

    private void Start()
    {
#if UNITY_EDITOR
        Application.LoadLevelAsync(nextScene);
        return;
#else
#if DEVELOPMENT
         Application.LoadLevelAsync(nextScene);
#else
        ObbPatch();
#endif
#endif
    }

    void ObbPatch()
    {
        if (!GooglePlayDownloader.RunningOnAndroid())
        {
            GUI.Label(new Rect(100, 50, Screen.width - 10, 30), "Use GooglePlayDownloader only on Android device!");
            return;
        }

        expPath = GooglePlayDownloader.GetExpansionFilePath();

        if (expPath == null)
        {
            GUI.Label(new Rect(100, 50, Screen.width - 10, 30), "External storage is not available!");
        }
        else
        {
            string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
            string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);

            if (alreadyLogged == false)
            {
                alreadyLogged = true;

                log("expPath = " + expPath);
                log("Main = " + mainPath);
                log("Main = " + mainPath.Substring(expPath.Length));

                if (mainPath != null)
                    StartCoroutine(loadLevel());

            }
            if (mainPath == null)
            {
                GooglePlayDownloader.FetchOBB();

                StartCoroutine(loadLevel());
            }
        }
    }

    protected IEnumerator loadLevel()
    {
        string mainPath;

        do
        {
            yield return new WaitForSeconds(0.5f);

            mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
            log("waiting mainPath " + mainPath);
        }

        while (mainPath == null);

        if (downloadStarted == false)
        {
            downloadStarted = true;

            string uri = "file://" + mainPath;

            log("downloading " + uri);

            WWW www = WWW.LoadFromCacheOrDownload(uri, 0);

            // Wait for download to complete
            yield return www;

            if (www.error != null)
            {
                log("wwww error " + www.error);
            }
            else
            {
                Application.LoadLevel(nextScene);
            }
        }
    }

	//void OnGUI()
	//{
	//	if (!GooglePlayDownloader.RunningOnAndroid())
	//	{
	//		GUI.Label(new Rect(10, 10, Screen.width-10, 20), "Use GooglePlayDownloader only on Android device!");
	//		return;
	//	}
		
	//	string expPath = GooglePlayDownloader.GetExpansionFilePath();

	//	if (expPath == null)
	//	{
	//			GUI.Label(new Rect(10, 10, Screen.width-10, 20), "External storage is not available!");
	//	}
	//	else
	//	{
	//		//string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
	//		//string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
			
	//		//GUI.Label(new Rect(10, 10, Screen.width-10, 20), "Main = ..."  + ( mainPath == null ? " NOT AVAILABLE" :  mainPath.Substring(expPath.Length)));
	//		//GUI.Label(new Rect(10, 25, Screen.width-10, 20), "Patch = ..." + (patchPath == null ? " NOT AVAILABLE" : patchPath.Substring(expPath.Length)));

 //  //         if (mainPath == null || patchPath == null)
	//		//	if (GUI.Button(new Rect(10, 100, 100, 100), "Fetch OBBs"))
	//		//		GooglePlayDownloader.FetchOBB();

 //           StartCoroutine(LoadLevel("Main"));
 //       }
	//}

 //   protected IEnumerator LoadLevel(string strSceneName)
 //   {
 //       string expPath = GooglePlayDownloader.GetExpansionFilePath();
 //       string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);

 //       string uri = string.Empty;

 //       uri = "jar:file://" + mainPath + "!/" + "scene.unity3d";

 //       Debug.Log("downloading " + uri);

 //       WWW www = WWW.LoadFromCacheOrDownload(uri, 0);

 //       // Wait for download to complete
 //       yield return www;

 //       if (www.error != null)
 //       {
 //           Debug.Log("wwww error " + www.error);
 //       }
 //       else
 //       {
 //           AssetBundle assetBundle = www.assetBundle;

 //           assetBundle.LoadAllAssets();

 //           Application.LoadLevel(strSceneName);
 //       }
 //   }
}
