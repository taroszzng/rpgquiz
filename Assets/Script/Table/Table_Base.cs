﻿using UnityEngine;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Table_Base : MonoBehaviour
{
    public static string GetRelativeAssetPath()
    {
        if (Application.isEditor)
            return System.Environment.CurrentDirectory.Replace("\\", "/") + "/Assets"; // Use the build output folder directly.
        else if (Application.isMobilePlatform || Application.isConsolePlatform)
            return Application.persistentDataPath + "/Assets";
        else // For standalone player.
            return Application.streamingAssetsPath;
    }

    protected void Load_Binary<T>(string _strName, ref T _obj)
    {
        var b = new BinaryFormatter();

        TextAsset asset = Resources.Load("Table_" + _strName) as TextAsset;
        assert_cs.set(asset != null, "Table_" + _strName + " File not exist");
        Stream stream = new MemoryStream(asset.bytes);
        _obj = (T)b.Deserialize(stream);
        stream.Close();
    }

    protected void Save_Binary(string _strName, object _obj)
    {
        var b = new BinaryFormatter();
        Stream stream = File.Open(GetRelativeAssetPath() + "/Table/Resources" + "/Table_" + _strName + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
        b.Serialize(stream, _obj);
        stream.Close();
    }

#if UNITY_EDITOR   
    protected void Load_BinaryEditor<T>(string _strName, ref T _obj)
    {
        var b = new BinaryFormatter();
        Stream stream = File.Open(GetRelativeAssetPath() + "/Table/Resources" + "/Table_" + _strName + ".txt", FileMode.OpenOrCreate, FileAccess.Read);
        _obj = (T)b.Deserialize(stream);
        stream.Close();
    }
#endif
}
