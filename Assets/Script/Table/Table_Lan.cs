﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Table_Lan : Table_Base
{
    [Serializable]
    public class Info
    {
        public int m_nID;
        public string m_strDec;
    }

    public Dictionary<int, Info> m_Dictionary = new Dictionary<int, Info>();

#if UNITY_EDITOR//에디터에서만 저장 하겠다. 파일로 뽑을때만 씀
    public class INFODICTIONARY
    {
        public nsENUM.eLANGUAGE m_LanType;//랭귀지 타입

        public Dictionary<int, Info> m_Info = new Dictionary<int, Info>();
    }

    public List<INFODICTIONARY> m_ListLan = new List<INFODICTIONARY>();

    public INFODICTIONARY GetLanInfo(int nType)//언어가 있는지
    {
        INFODICTIONARY infoditionary = null;

        int nCount = m_ListLan.Count;

        for (int i = 0; i < nCount; ++i)
        {
            if ((nsENUM.eLANGUAGE)nType == m_ListLan[i].m_LanType)
            {
                infoditionary = m_ListLan[i];
                break;
            }
        }
        return infoditionary;
    }
#endif

    public bool IsData(int _nID)
    {
        assert_cs.set(m_Dictionary.ContainsKey(_nID) == true, "ID = " + _nID);

        return m_Dictionary.ContainsKey(_nID);
    }

    public Info Get(int _nID)
    {
        if (!IsData(_nID))
            return null;

        return m_Dictionary[_nID];
    }

    static string LanguageName(nsENUM.eLANGUAGE nLan)
    {
        string strName = "";

        switch (nLan)
        {
            case nsENUM.eLANGUAGE.eLANGUAGE_KOREA:
                strName = "_KOREA";
                break;
            case nsENUM.eLANGUAGE.eLANGUAGE_ENGLISH:
                strName = "_ENGLISH";
                break;
            case nsENUM.eLANGUAGE.eLANGUAGE_JAPAN:
                strName = "_JAPAN";
                break;
            default:
                assert_cs.msg("Table_Lan LanType = " + nLan);
                break;
        }
        return strName;
    }

    static public nsENUM.eLANGUAGE GetLanType()
    {
        nsENUM.eLANGUAGE lan = nsENUM.eLANGUAGE.eLANGUAGE_ENGLISH;

        switch (Application.systemLanguage)
        {
            case SystemLanguage.Korean:
                lan = nsENUM.eLANGUAGE.eLANGUAGE_KOREA;
                break;
            case SystemLanguage.Japanese:
                lan = nsENUM.eLANGUAGE.eLANGUAGE_JAPAN;
                break;
        }

        return lan;
    }

    public void Init_Binary(string _strName)
    {
        string strLan = _strName;

        switch (Application.systemLanguage)
        {
            case SystemLanguage.Korean:
                strLan += "_KOREA";
                break;
            case SystemLanguage.Japanese:
                strLan += "_JAPAN";
                break;
            default:
                strLan += "_ENGLISH";
                break;
        }

        Load_Binary<Dictionary<int, Info>>(strLan, ref m_Dictionary);
    }

#if UNITY_EDITOR
    public void Save_Binary(string _name)
    {
        int nCount = (int)nsENUM.eLANGUAGE.eLANGUAGE_END;

        for (int i = 1; i < nCount; ++i)
        {
            string strName = LanguageName((nsENUM.eLANGUAGE)i);

            INFODICTIONARY info = GetLanInfo(i);

            assert_cs.set(info != null, "해당 언어가 테이블에 존재 하지 않습니다. 언어 index = " + i);

            Save_Binary(_name + strName, info.m_Info);
        }

        Debug.Log("[Table Save] " + _name + " : " + m_Dictionary.Count);
    }

#endif
}
