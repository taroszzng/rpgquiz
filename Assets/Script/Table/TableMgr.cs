﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TableMgr : MonoBehaviour
{
    public Table_Default m_Default = new Table_Default();
    public Table_Camera m_Camera = new Table_Camera();
    public Table_CameraZoom m_CameraZoom = new Table_CameraZoom();
    public Table_Lan m_Lan = new Table_Lan();
    public Table_Quiz m_Quiz = new Table_Quiz();
    public Table_Stage m_Stage = new Table_Stage();
    public Table_Player m_Player = new Table_Player();
    public Table_Level m_Level = new Table_Level();
    public Table_Combo m_Combo = new Table_Combo();
    public Table_Monster m_Monster = new Table_Monster();
    public Table_Reward m_Reward = new Table_Reward();
    public Table_Item m_Item = new Table_Item();
    public Table_Shop m_Shop = new Table_Shop();
    public Table_Sound m_Sound = new Table_Sound();

    public void Init()
    {
        Init_Binary();
    }

    protected void Init_Binary()
    {
        m_Default.Init_Binary("Default");
        m_Camera.Init_Binary("Camera");
        m_CameraZoom.Init_Binary("CameraZoom");
        m_Lan.Init_Binary("Lan");
        m_Quiz.Init_Binary("Quiz");
        m_Stage.Init_Binary("Stage");
        m_Player.Init_Binary("Player");
        m_Level.Init_Binary("Level");
        m_Combo.Init_Binary("Combo");
        m_Monster.Init_Binary("Monster");
        m_Reward.Init_Binary("Reward");
        m_Item.Init_Binary("Item");
        m_Shop.Init_Binary("Shop");
        m_Sound.Init_Binary("Sound");
    }
}