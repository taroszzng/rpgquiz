﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Table_Stage : Table_Base
{
    [Serializable]
    public class Info
    {
        public int m_nID;
        public int m_nType;//
        public int m_nName;
        public int m_nDec;
        public int m_nIcon;
        public int m_nCompType;
        public int m_nComp;//완료조건
        public string m_strBG;
        public string m_strEffect;
        public float m_fY;
    }

    public Dictionary<int, Info> m_Dictionary = new Dictionary<int, Info>();

    public bool IsData(int _nID)
    {
        assert_cs.set(m_Dictionary.ContainsKey(_nID) == true, "ID = " + _nID);

        return m_Dictionary.ContainsKey(_nID);
    }

    public Info Get(int _nID)
    {
        if (!IsData(_nID))
            return null;

        return m_Dictionary[_nID];
    }

    public void Init_Binary(string _strName)
    {
        Load_Binary<Dictionary<int, Info>>(_strName, ref m_Dictionary);
    }

#if UNITY_EDITOR
    public void Save_Binary(string _strName)
    {
        Save_Binary(_strName, m_Dictionary);

        Debug.Log("[Table Save] " + _strName + " : " + m_Dictionary.Count);
    }
#endif
}
