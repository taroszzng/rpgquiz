﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Table_Shop : Table_Base
{
    [Serializable]
    public class Info
    {
        public int m_nID;
        public int m_nType;//오픈여부 1=오픈,0=안팜
        public string m_strStore; //스토어 값
        public int m_nSort; //분류
        public int m_nName;
        public int m_nDec;
        public int m_nIcon;
        public int m_nPrice;
        public int m_nItemID;//능치 적용부위
        public int m_nItemValue;//수량
    }

    public Dictionary<int, Info> m_Dictionary = new Dictionary<int, Info>();

    public List<int> m_listEternal = new List<int>(); //영구
    public List<int> m_listConsume = new List<int>(); //소모

    public bool IsData(int _nID)
    {
        assert_cs.set(m_Dictionary.ContainsKey(_nID) == true, "ID = " + _nID);

        return m_Dictionary.ContainsKey(_nID);
    }

    public Info Get(int _nID)
    {
        if (!IsData(_nID))
            return null;

        return m_Dictionary[_nID];
    }

    public void Init_Binary(string _strName)
    {
        Load_Binary<Dictionary<int, Info>>(_strName, ref m_Dictionary);
    }

#if UNITY_EDITOR
    public void Save_Binary(string _strName)
    {
        Save_Binary(_strName, m_Dictionary);

        Debug.Log("[Table Save] " + _strName + " : " + m_Dictionary.Count);
    }
#endif
}
