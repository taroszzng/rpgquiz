﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections.Generic;

public class InAppPurchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController storeController;
    private static IExtensionProvider extensionProvider;

    #region 상품ID
    // 상품ID는 구글 개발자 콘솔에 등록한 상품ID와 동일하게 해주세요.
    //public const string productId1 = "gem1";
    #endregion

    public Dictionary<string, int> m_Dictionary = new Dictionary<string, int>();

    void Awake()
    {
        SharedObject.g_Purchaser = this;
    }

    void Start()
    {
        InitializePurchasing();
    }

    private bool IsInitialized()
    {
        return (storeController != null && extensionProvider != null);
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
            return;

        var module = StandardPurchasingModule.Instance();

        ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);

        var pair = SharedObject.g_TableMgr.m_Shop.m_Dictionary.GetEnumerator();

        while (pair.MoveNext())//
        {
            Table_Shop.Info info = pair.Current.Value;

            builder.AddProduct(info.m_strStore, ProductType.Consumable, new IDs
            {
                { info.m_strStore, AppleAppStore.Name },
                { info.m_strStore, GooglePlay.Name },
            });

            if (!m_Dictionary.ContainsKey(info.m_strStore))
                m_Dictionary.Add(info.m_strStore, info.m_nID);
        }

        //builder.AddProduct(productId1, ProductType.Consumable, new IDs
        //    {
        //        { productId1, AppleAppStore.Name },
        //        { productId1, GooglePlay.Name },
        //    });

        UnityPurchasing.Initialize(this, builder);
    }

    public void BuyProductID(string productId)
    {
        try
        {
            if (IsInitialized())
            {
                Product p = storeController.products.WithID(productId);

                if (p != null && p.availableToPurchase)
                {
                    Debug.Log("localizedPrice = " + p.metadata.localizedPrice);
                    Debug.Log("localizedDescription = " + p.metadata.localizedDescription);
                    Debug.Log("isoCurrencyCode = " + p.metadata.isoCurrencyCode);
                    Debug.Log("localizedTitle = " + p.metadata.localizedTitle);

                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", p.definition.id));

                    storeController.InitiatePurchase(p);
                }
                else
                {
                    SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 0,
                    "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 0,
                "BuyProductID FAIL. Not initialized.");
            }
        }
        catch (Exception e)
        {
            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 0,
                "BuyProductID: FAIL. Exception during purchase");
        }
    }

    public void RestorePurchase()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = extensionProvider.GetExtension<IAppleExtensions>();

            apple.RestoreTransactions
                (
                    (result) =>
                    { Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore."); }
                );
        }
        else
        {
            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 0,
                "RestorePurchases FAIL. Not supported on this platform. Current");
        }
    }

    public void OnInitialized(IStoreController sc, IExtensionProvider ep)
    {
        Debug.Log("OnInitialized : PASS");

        storeController = sc;
        extensionProvider = ep;
    }

    public void OnInitializeFailed(InitializationFailureReason reason)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + reason);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

        string strid = args.purchasedProduct.definition.id;

        if (strid != SharedObject.g_SceneMgr.BuyID.ToString())
            return PurchaseProcessingResult.Complete;

        if (!m_Dictionary.ContainsKey(strid))
        {
            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1300039);

            return PurchaseProcessingResult.Complete;
        }

        int nID = m_Dictionary[strid];

        Table_Shop.Info info = SharedObject.g_TableMgr.m_Shop.Get(nID);

        if (null != info)//아이템 지급
        {
            SharedObject.g_PlayerPrefsData.ItemInsert(info.m_nItemID, info.m_nItemValue);

            SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 1370016);

            string strpath = SharedObject.g_PlayerPrefsData.GetDataPath();

            SharedObject.g_Main.UI3DepthPopup.OnFileSave(true);

            SharedObject.g_SceneMgr.BuyID = 0;

            if (SharedObject.g_bGoogleLogin)
                GPGSIds.GoogleCashBuy();//캐쉬아이템 구입
        }

        //switch (args.purchasedProduct.definition.id)
        //{
        //    case productId1:
        //        // ex) gem 10개 지급
        //        break;
        //}

        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        SharedObject.g_Main.PopUp3Depth(nsENUM.eUI_3DEPTH.eUI3DEPTH_MSG, 0, failureReason.ToString());

        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public string GetCurrency()
    {
        string str = SharedObject.g_SceneMgr.BuyID.ToString();

        if (IsInitialized())
        {
            Product p = storeController.products.WithID(str);

            if(null != p)
                str = p.metadata.isoCurrencyCode;
        }

        return str;
    }
}