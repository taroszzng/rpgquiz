﻿using UnityEngine;

namespace nsENUM
{
    [System.Serializable]
    public enum ePHY_LAYER
    {
        DEFAULT = 0,
        RED = 8,
        BLUE = 9,
        RED_BULLET = 10,
        BLUE_BULLET = 11
    }

    [System.Serializable]
    public enum eTEAM
    {
        BLUE_TEAM = 0,
        RED_TEAM = 1,
        WHITE_TEAM = 2, //무승부
        BLACK_TEAM = 3, //무승부후 리셋값
    }

    public enum eBATTLEEVENT
    {
        eBATTLEEVENT_3 = 3,//3%
        eBATTLEEVENT_5 = 5,//5%
        eBATTLEEVENT_7 = 7,//7%
        eBATTLEEVENT_10 = 10,//10%
        eBATTLEEVENT_20 = 20,//20%
        eBATTLEEVENT_30 = 30,//30%
        eBATTLEEVENT_50 = 50,//50%
        eBATTLEEVENT_100 = 100,//100%
    }

    [System.Serializable]
    public enum eLANGUAGE//[kks][Language]
    {
        eLANGUAGE_NONE,
        eLANGUAGE_KOREA,//한국
        eLANGUAGE_ENGLISH,//영어
        eLANGUAGE_JAPAN,//일본
        eLANGUAGE_END
    }

    [System.Serializable]
    public enum eSCENE
    {
        eSCENE_NONE,
        eSCENE_START,
        eSCENE_MODE,
        eSCENE_BATTLE,
        eSCENE_END
    }

    public enum eANI_TYPE
    {
        eANI_TYPE_ANIMATION,
        eANI_TYPE_ANIMATOR,
        eANI_TYPE_END,
    }

    public enum eSOUNDTYPE
    {
        eSOUNDTYPE_BGM,
        eSOUNDTYPE_EFFECT,
        eSOUNDTYPE_END
    }

    public enum eSOUNDBGM
    {
        eSOUNDBGM_NONE,
        eSOUNDBGM_TITLE,
        eSOUNDBGM_BATTLE,
        eSOUNDBGM_END
    }

    public enum eUI //닫히지 않는 UI
    {
        eUI_NONE,
        eUI_TITLE,
        eUI_STAGE,
        eUI_MONSTERSELECT,
        eUI_AMODEEVENT,
        eUI_END
    }

    public enum eUI_CLOSE//닫히는 ui
    {
        eUI_CLOSE_NONE,
        eUI_CLOSE_2DEPTH_RESULT,
        eUI_CLOSE_2DEPTH_PAYMENT, //결제창
        eUI_CLOSE_3DEPTH_POPUP,
        eUI_CLOSE_END
    }

    public enum eUI_1DEPTH
    {
        eUI_1DEPTH_MAIN,
        eUI_1DEPTH_END
    }

    public enum eUI_3DEPTH
    {
        eUI3DEPTH_CLIENTUPDATE,
        eUI3DEPTH_ITEMTOOLTIP,
        eUI3DEPTH_OPTION,
        eUI3DEPTH_MSG,
        eUI3DEPTH_CREATIVE,
        eUI3DEPTH_END
    }

    [System.Serializable]
    public enum eSETPOPUP
    {
        eSETPOPUP_BATTLE,
        eSETPOPUP_INVEN,
        eSETPOPUP_ABILITY,
        eSETPOPUP_BOOK,
        eSETPOPUP_SHOP,
        eSETPOPUP_ADS,
        eSKINPOPUP_END
    }

    public enum eDICEPLAY
    {
        eDICEPLAY_DICE1,
        eDICEPLAY_PREFERENCE,
        eDICEPLAY_DICE2,
        eDICEPLAY_END
    }

    public enum eVALUE//디폴트 값
    {
        eVALUE_0,
        eVALUE_1,
        eVALUE_2,
        eVALUE_3,
        eVALUE_4,
        eVALUE_5,
        eVALUE_6,
        eVALUE_7,
        eVALUE_8,
        eVALUE_9,
        eVALUE_10 = 10,
        eVALUE_11,
        eVALUE_12,
        eVALUE_13 = 13,
        eVALUE_14,
        eVALUE_15,
        eVALUE_16,
        eVALUE_17,
        eVALUE_18,
        eVALUE_19,
        eVALUE_20 = 20,
        eVALUE_60 = 60,
        eVALUE_72 = 72,
        eVALUE_99 = 99,
        eVALUE_100,
        eVALUE_513 = 513,
        eVALUE_END
    }

    [System.Serializable]
    public enum eITEMTYPE 
    {
        eITEMTYPE_ETERNAL,//영구
        eITEMTYPE_CONSUME,//소모
        eITEMTYPE_END
    }

    [System.Serializable]
    public enum eITEMAPPLY//아이템 적용 부위
    {
        eITEMAPPLY_ALL,
        eITEMAPPLY_HP,
        eITEMAPPLY_ATK,
        eITEMAPPLY_LUCK,
        eITEMAPPLY_TIME, //시간
        eITEMAPPLY_DELETE, //삭제
        eITEMAPPLY_HINT, //힌트
        eITEMAPPLY_BUFF,//HP
        eITEMAPPLY_ADS = 100, //광고관련
        eITEMAPPLY_END
    }

    public enum eSTAGECOMP
    {
        eSTAGECOMP_NONE, //못깸
        eSTAGECOMP_LEVEL,//레벨
        eSTAGECOMP_END
    }

    public enum eEVENTBUFF
    {
        eEVENTBUFF_ATK_PLUS,
        eEVENTBUFF_HP_MINUS,
        eEVENTBUFF_ATK_COMBO,//콤보관련
        eEVENTBUFF_END
    }

    public enum eCOMBOTYPE
    {
        eCOMBOTYPE_WIN,
        eCOMBOTYPE_LOSE,
        eCOMBOTYPE_DRAW,
        eCOMBOTYPE_END
    }

    public enum eQUIZTYPE
    {
        eQUIZTYPE_NONE,
        eQUIZTYPE_HISTORY, //역사
        eQUIZTYPE_COMMON, //공통
        eQUIZTYPE_ENTER, //연예
        eQUIZTYPE_SPORTS, //스포츠
        eQUIZTYPE_ETC, //기타
        eQUIZTYPE_END
    }

    public enum eADS
    {
        eADS_RANCOUNT = 4,      //랜덤 카운트
        eADS_REWARDCOUNT = 10, //지급 카운트
        eADS_REWARDITEM = 10030,//보상아이템 시작 번호
        eADS_END
    }

    public enum eDEFINE
    {
        eDEFINE_QUIZTIME = 15,
    }
}
