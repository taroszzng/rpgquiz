﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EffectPanelMgr : MonoBehaviour
{
    public enum ePATH
    {
        EFFECT_PANEL,
        INGAME,
    }

    public enum eLAYER
    {
        LAYER_0,
        LAYER_1,
        LAYER_2,
        LAYER_3,
        _COUNT,
    }

    public Transform[] m_Tr = new Transform[(int)eLAYER._COUNT];
    public Texture[] m_CashingTexture;

    public enum eQUEUE_TYPE
    {
        ARCHIEVE = 0,
        INFO,
        COUNT
    }

    public enum eARCHIEVE
    {
        QUEST,
        DAILY_QUEST,
        TROPHY,
    }

    public class cQUEUE_VAL_BASE
    {
        public string m_EffectName;
    }

    public class cQUEUE_VAL_ARCHIEVE : cQUEUE_VAL_BASE
    {
        public eARCHIEVE m_Type;
        public string m_Title;        
    }

    public class cQUEUE_VAL_INFO : cQUEUE_VAL_BASE
    {
        public string m_Str;
        public int m_Icon;
    }

    [System.NonSerialized]
    public Queue<cQUEUE_VAL_BASE>[] m_Queue = new Queue<cQUEUE_VAL_BASE>[(int)eQUEUE_TYPE.COUNT];
    [System.NonSerialized]
    public bool[] m_QueueCoroutine = new bool[(int)eQUEUE_TYPE.COUNT];

    GameObject m_PlayObj = null;//[kks][Eff]:로비에서 쓰는 이펙트


    public GameObject LobbyPlayObj////[kks][Eff]:실행중인 이펙트 오브젝트
    {
        get { return m_PlayObj; }
    }


    void Awake()
    {
        SharedObject.g_EffectPanelMgr = this;

        SharedObject.g_UICamera = transform.Find("Camera").GetComponent<Camera>();
        DontDestroyOnLoad(gameObject);

        for (int i = 0; i < (int)eQUEUE_TYPE.COUNT; ++i)
        {
            m_Queue[i] = new Queue<cQUEUE_VAL_BASE>();
            m_QueueCoroutine[i] = false;
        }
    }

    void OnEnable()
    {
        transform.position = new Vector3(640, 360, 0);
    }

    //string _GetPath(ePATH type)
    //{
    //    switch (type)
    //    {
    //        case ePATH.EFFECT_PANEL:
    //            return "Prefabs/_Scene_Lobby/EffectPanel/";
    //        case ePATH.INGAME:
    //            return "Prefabs/_Scene_Ingame/UI/";
    //        default:
    //            assert_cs.msg("default : " + type);
    //            break;
    //    }
    //    return "";
    //}

    //public Transform PlayLobbyEffect(ePATH pathType, string name, eLAYER layer = eLAYER.LAYER_2)//[kks][Eff]:로비에서 사용 삭제하기위해
    //{
    //    m_PlayObj = AssetBundleManager.Load(string.Format("{0}{1}", _GetPath(pathType), name), "prefab") as GameObject;
    //    m_PlayObj = GameObject.Instantiate(m_PlayObj) as GameObject;
    //    m_PlayObj.transform.SetParent(m_Tr[(int)layer], false);
    //    m_PlayObj.name = name;

    //    return m_PlayObj.transform;
    //}

    //public Transform PlayEffect(ePATH pathType, string name, eLAYER layer = eLAYER.LAYER_2)
    //{
    //    GameObject obj = AssetBundleManager.Load(string.Format("{0}{1}", _GetPath(pathType), name), "prefab") as GameObject;
    //    obj = GameObject.Instantiate(obj) as GameObject;
    //    obj.transform.SetParent(m_Tr[(int)layer], false);
    //    obj.name = name;

    //    return obj.transform;
    //}

    public Transform FindEffect(string name, eLAYER layer = eLAYER.LAYER_2)
    {
        Transform tr = m_Tr[(int)layer];

        int cnt = tr.childCount;
        for (int i = 0; i < cnt; ++i)
        {
            if (tr.GetChild(i).name.Contains(name) == true)
                return tr.GetChild(i);
        }
        return null;
    }

    public void ChangeScene()
    {
        for (int l = 0; l < (int)eLAYER._COUNT; ++l)
        {
            Transform tr = m_Tr[l];
            int cnt = tr.childCount;
            for (int i = cnt - 1; i > -1; --i)
            {
                GameObject.DestroyImmediate(tr.GetChild(i).gameObject);
            }
        }
    }

    public void DestroyEffect(Transform obj)
    {
        if (obj == null)
            return;

        GameObject dest = obj.gameObject;

        for (int l = 0; l < (int)eLAYER._COUNT; ++l)
        {
            Transform tr = m_Tr[l];
            int cnt = tr.childCount;
            for (int i = cnt - 1; i > -1; --i)
            {
                if (tr.GetChild(i).gameObject == dest)
                {
                    GameObject.Destroy(tr.GetChild(i).gameObject);
                    return;
                }
            }
        }
    }

    //public Transform CombatPoint(int DeltaCP)
    //{
    //    if (DeltaCP == 0)
    //        return null;

    //    Transform tr = null;
    //    if (DeltaCP > 0)
    //    {
    //        tr = PlayEffect(ePATH.EFFECT_PANEL, "FightStat_Plus");
    //        tr.FindChild("Stat/Num").GetComponent<Text>().text = string.Format("+{0}", DeltaCP);
    //    }
    //    else
    //    {
    //        tr = PlayEffect(ePATH.EFFECT_PANEL, "FightStat_Minus");
    //        tr.FindChild("Stat/Num").GetComponent<Text>().text = string.Format("{0}", DeltaCP);
    //    }

    //    return tr;
    //}

    //IEnumerator _ArchieveCoroutine()
    //{
    //    m_QueueCoroutine[(int)eQUEUE_TYPE.ARCHIEVE] = true;

    //    while (m_Queue[(int)eQUEUE_TYPE.ARCHIEVE].Count > 0)
    //    {
    //        cQUEUE_VAL_ARCHIEVE val = (cQUEUE_VAL_ARCHIEVE)m_Queue[(int)eQUEUE_TYPE.ARCHIEVE].Dequeue();
    //        Transform tr = PlayEffect(ePATH.EFFECT_PANEL, val.m_EffectName, eLAYER.LAYER_3);

    //        switch( val.m_Type )
    //        {
    //            case eARCHIEVE.QUEST:
    //                tr.FindChild("Title").GetComponent<Text>().text = SharedObject.g_TableMng.m_Lan.Get(100519);
    //                break;
    //            case eARCHIEVE.DAILY_QUEST:
    //                tr.FindChild("Title").GetComponent<Text>().text = SharedObject.g_TableMng.m_Lan.Get(100521);
    //                break;
    //            case eARCHIEVE.TROPHY:
    //                tr.FindChild("Title").GetComponent<Text>().text = SharedObject.g_TableMng.m_Lan.Get(100520);
    //                break;
    //            default:
    //                assert_cs.msg("default : " + val.m_Type);
    //                break;
    //        }            
    //        tr.FindChild("Text").GetComponent<Text>().text = val.m_Title;
    //        GameObject obj = tr.gameObject;

    //        while (obj != null)
    //        {
    //            yield return new WaitForSeconds(0.1f);
    //        }
    //    }

    //    m_QueueCoroutine[(int)eQUEUE_TYPE.ARCHIEVE] = false;
    //}

    //IEnumerator _InfoCoroutine()
    //{
    //    m_QueueCoroutine[(int)eQUEUE_TYPE.INFO] = true;

    //    while (m_Queue[(int)eQUEUE_TYPE.INFO].Count > 0)
    //    {
    //        cQUEUE_VAL_INFO val = (cQUEUE_VAL_INFO)m_Queue[(int)eQUEUE_TYPE.INFO].Dequeue();
    //        Transform tr = PlayEffect(ePATH.EFFECT_PANEL, val.m_EffectName, eLAYER.LAYER_3);
    //        tr.FindChild("Text").GetComponent<Text>().text = val.m_Str;
    //        tr.FindChild("MenuIcon").gameObject.SetActive(val.m_Icon != 0);
    //        if (val.m_Icon > 0)
    //        {
    //            Transform trRawImg = tr.FindChild("MenuIcon/Image");
    //            trRawImg.GetComponent<RawImage>().texture = m_CashingTexture[0];
    //            trRawImg.GetComponent<RawImageEasy>().Set(val.m_Icon);
    //        }

    //        GameObject obj = tr.gameObject;
    //        while (obj != null)
    //        {
    //            yield return new WaitForSeconds(0.1f);
    //        }
    //    }

    //    m_QueueCoroutine[(int)eQUEUE_TYPE.INFO] = false;
    //}

    //public void Archieve( eARCHIEVE type, string title)
    //{
    //    cQUEUE_VAL_ARCHIEVE val = new cQUEUE_VAL_ARCHIEVE();
    //    val.m_EffectName = "Archieve";

    //    val.m_Type = type;
    //    val.m_Title = title;        

    //    m_Queue[(int)eQUEUE_TYPE.ARCHIEVE].Enqueue(val);

    //    if (m_QueueCoroutine[(int)eQUEUE_TYPE.ARCHIEVE] == false)
    //    {
    //        StartCoroutine(_ArchieveCoroutine());
    //    }
    //}

    //public void ArchieveQuest(int QuestID)
    //{
    //    Table_QuestText.INFO infoQT = SharedObject.g_TableMng.m_QuestText.Get(QuestID, SharedObject.g_SceneMng.m_CharData.m_MyModelID);

    //    eARCHIEVE eType = eARCHIEVE.QUEST;
    //    if( SharedObject.g_TableMng.m_Quest.Get( QuestID ).m_QuestType == nsENUM.eQUEST_TYPE.DAILY )
    //        eType = eARCHIEVE.DAILY_QUEST;
    //    Archieve(eType, SharedObject.g_TableMng.m_Lan.Get(infoQT.m_Title));

    //    if( QuestID == 1 )
    //    {
    //        SharedObject.g_Tutorial.SetResolveTutorial(eTUTORIAL.QUEST_COMP);
    //    }
    //}

    //public void Info(int LanID, int icon = 0)
    //{
    //    Info(SharedObject.g_TableMng.m_Lan.Get(LanID), icon);
    //}

    //public void Info_Item(int LanID, int item, int icon = 0)
    //{
    //    Info_ItemFormat(LanID, SharedObject.g_TableMng.m_Item.Get(item).m_Name, icon);
    //}

    //public void Info_Costume(int LanID, int costume, int icon = 0)
    //{
    //    Info_ItemFormat(LanID, SharedObject.g_TableMng.m_Costume.Get(costume, 1, 1).m_SetName, icon);
    //}

    //public void Info_ItemFormat(int LanID, int itemID, int icon = 0)
    //{
    //    Info(string.Format(SharedObject.g_TableMng.m_Lan.Get(LanID), SharedObject.g_TableMng.m_Lan.Get(itemID)), icon);
    //}

    //public void Info(string str, int icon = 0)
    //{
    //    cQUEUE_VAL_INFO val = new cQUEUE_VAL_INFO();
    //    val.m_EffectName = "InfoMessage";
    //    val.m_Str = str;
    //    val.m_Icon = icon;

    //    m_Queue[(int)eQUEUE_TYPE.INFO].Enqueue(val);

    //    if (m_QueueCoroutine[(int)eQUEUE_TYPE.INFO] == false)
    //    {
    //        StartCoroutine(_InfoCoroutine());
    //    }
    //}
}
