﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public partial class PlayerPrefsData : MonoBehaviour
{
    public List<int> m_listAbility = new List<int>();//능력치 아이템 = 영구성
    public bool m_bChangeAbility = true;

    bool CheckAbilityItem(int _nItemID)
    {
        bool bcheck = false;

        int nCount = m_listAbility.Count;

        for(int i=0; i<nCount; ++i)
        {
            if(_nItemID == m_listAbility[i])
            {
                bcheck = true;

                break;
            }
        }

        return bcheck;
    }

    void SetAbilityItemAdd(int _nItemID) //장착
    {
        if (CheckAbilityItem(_nItemID))
            return;

        m_listAbility.Add(_nItemID);
    }

    void SetAbilityItemDelete(int _nItemID)//해제
    {
        if (!CheckAbilityItem(_nItemID))
            return;

        m_listAbility.Remove(_nItemID);
    }

    public float GetAbility(nsENUM.eITEMAPPLY _e)
    {
        float fAbility = 0f;

        int nCount = m_listAbility.Count;

        for (int i = 0; i < nCount; ++i)
        {
            Item item = SharedObject.g_PlayerPrefsData.GetItem(SharedObject.g_PlayerPrefsData.m_listAbility[i]);

            if (null == item)
                continue;

            if (item.m_eItemApply == _e ||
                item.m_eItemApply == nsENUM.eITEMAPPLY.eITEMAPPLY_ALL)
                fAbility += item.m_fItemAbility;
        }

        return fAbility;
    }

    public int GetLuck(nsENUM.eTEAM _e)//플레이어 행운
    {
        int nLuck = 1;

        switch(_e)
        {
            case nsENUM.eTEAM.BLUE_TEAM:
                {
                    Table_Level.Info level = SharedObject.g_TableMgr.m_Level.Get(Level);

                    if (null != level)
                        nLuck = level.m_nLuck;
                }
                break;
            case nsENUM.eTEAM.RED_TEAM:
                {
                    Table_Monster.Info mon = SharedObject.g_TableMgr.m_Monster.Get(MonsterID);

                    if (null != mon)
                        nLuck = mon.m_nLuck;
                }
                break;
        }

        return nLuck;
    }
}