﻿
using System;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_CameraSpace : MonoBehaviour
{
    void Start()
    {
        assert_cs.set(SharedObject.g_UICamera);
        GetComponent<Canvas>().worldCamera = SharedObject.g_UICamera;
    }
}
