﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class ItemBase
{
    public nsENUM.eITEMTYPE m_eItemType;
    public nsENUM.eITEMAPPLY m_eItemApply;
    public bool m_bUse;//사용여부
    public float m_fItemAbility; //능력치
}

[Serializable]
public class Item : ItemBase
{
    public int m_nItemID;
    public int m_nItemValue;//수량
}

[Serializable]
public class Item_long : ItemBase
{
    public int m_nItemID;
    public long m_lItemValue;
}

[Serializable]
public class ItemRate_float : ItemBase //아이템 비율
{
    public float m_fItemPer;
    public float m_fItemProb;
}

[Serializable] 
public class AniValue
{
    public string m_strAni;
    public float m_fAniTime;
}

[Serializable] 
public class TableItem
{
    public int m_nItemID;
    public int m_nItemValue;
}