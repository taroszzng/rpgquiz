﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public partial class PlayerPrefsData : MonoBehaviour
{
    [Serializable]
    public class PlayerData
    {
        public int m_nLevel;
        public int m_nExp;

        public int m_nPlayerID;
        public int m_nMonsterID;
        public int m_nClearStage;

        public float m_fPlayerHp;
        public float m_fMonsterHp;

        //======= Option =======
        public int m_nOptionBgm;
        public int m_nOptionEffect;

        public eTUTORIAL m_eTutorial;

        public int m_nScore;//스코어

        public int m_nAdsCount;

        public string m_strDate;//날짜

        public bool m_bPrologue = true;

        public bool m_bEnding = false;
    }

    PlayerData m_PlayerData = new PlayerData();

    public Dictionary<int, Item> m_DictionaryItem = new Dictionary<int, Item>();//인벤아이템

    public int Level
    {
        set { m_PlayerData.m_nLevel = value; }
        get { return m_PlayerData.m_nLevel; }
    }

    public int Exp
    {
        set { m_PlayerData.m_nExp = value; }
        get { return m_PlayerData.m_nExp; }
    }

    public int PlayerID
    {
        set { m_PlayerData.m_nPlayerID = value; }
        get { return m_PlayerData.m_nPlayerID; }
    }

    public float PlayerHP
    {
        set { m_PlayerData.m_fPlayerHp = value; }
        get { return m_PlayerData.m_fPlayerHp; }
    }

    public int MonsterID
    {
        set { m_PlayerData.m_nMonsterID = value; }
        get { return m_PlayerData.m_nMonsterID; }
    }

    public float MonsterHP
    {
        set { m_PlayerData.m_fMonsterHp = value; }
        get { return m_PlayerData.m_fMonsterHp; }
    }

    public int ClearStage
    {
        set { m_PlayerData.m_nClearStage = value; }
        get { return m_PlayerData.m_nClearStage; }
    }

    public int OptionBgm
    {
        set { m_PlayerData.m_nOptionBgm = value; }
        get { return m_PlayerData.m_nOptionBgm; }
    }

    public int OptionEffect
    {
        set { m_PlayerData.m_nOptionEffect = value; }
        get { return m_PlayerData.m_nOptionEffect; }
    }

    public eTUTORIAL Tutorial
    {
        set { m_PlayerData.m_eTutorial = value; }
        get { return m_PlayerData.m_eTutorial; }
    }

    public int Score
    {
        set { m_PlayerData.m_nScore = value; }
        get { return m_PlayerData.m_nScore; }
    }

    public int AdsCount
    {
        set { m_PlayerData.m_nAdsCount = value; }
        get { return m_PlayerData.m_nAdsCount; }
    }

    public string Date
    {
        set { m_PlayerData.m_strDate = value; }
        get { return m_PlayerData.m_strDate; }
    }

    public bool Prologue
    {
        set { m_PlayerData.m_bPrologue = value; }
        get { return m_PlayerData.m_bPrologue; }
    }

    public bool Ending
    {
        set { m_PlayerData.m_bEnding = value; }
        get { return m_PlayerData.m_bEnding; }
    }

    public void ItemInsert(int _nItemID, int _nCount)//추가
    {
        Item item = null;

        if (m_DictionaryItem.ContainsKey(_nItemID))
        {
            item = m_DictionaryItem[_nItemID];

            item.m_nItemValue += _nCount;
        }
        else //아이템 추가
        {
            item = new Item();

            item.m_nItemID = _nItemID;
            item.m_nItemValue = _nCount;

            Table_Item.Info info = SharedObject.g_TableMgr.m_Item.Get(_nItemID);

            item.m_eItemType = (nsENUM.eITEMTYPE)info.m_nType;
            item.m_eItemApply = (nsENUM.eITEMAPPLY)info.m_nApply;
            item.m_fItemAbility = info.m_fAbility;

            m_DictionaryItem.Add(_nItemID, item);
        }
    }

    public void ItemDelete(int _nItemID, int _nCount)//제거만
    {
        Item item = null;

        if (m_DictionaryItem.ContainsKey(_nItemID))
        {
            item = m_DictionaryItem[_nItemID];

            item.m_nItemValue += _nCount;

            if (0 >= item.m_nItemValue)//제거
            {
                if(nsENUM.eITEMTYPE.eITEMTYPE_CONSUME == item.m_eItemType)
                    m_listBattle.Remove(item.m_nItemID);//베틀쪽 아이템 제거

                item.m_bUse = false;

                m_DictionaryItem.Remove(_nItemID);
            }
        }
    }

    public void ItemUse(int _nItemID, bool _bUse)//사용여부 설정
    {
        if (m_DictionaryItem.ContainsKey(_nItemID))
        {
            Item item = m_DictionaryItem[_nItemID];

            item.m_bUse = _bUse;

            switch(item.m_eItemType)
            {
                case nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL://버프
                    {
                        if (_bUse)//장착
                            SetAbilityItemAdd(item.m_nItemID);
                        else //헤제
                            SetAbilityItemDelete(item.m_nItemID);
                    }
                    break;
                case nsENUM.eITEMTYPE.eITEMTYPE_CONSUME://소모
                    {
                        if (_bUse)//장착
                            SetBattleItemAdd(item.m_nItemID);
                        else //헤제
                            SetBattleItemDelete(item.m_nItemID);
                    }
                    break;
            }
        }
    }

    public bool CheckItem(int _nItemID)//사용가능한지
    {
        bool bcheck = false;

        if (!m_DictionaryItem.ContainsKey(_nItemID))
            return bcheck;

        Item item = m_DictionaryItem[_nItemID];

        bcheck = item.m_bUse;

        return bcheck;
    }

    public int CheckEquipCount(nsENUM.eSETPOPUP _e)//장착중인 갯수
    {
        List<int> list = null;

        switch (_e)
        {
            case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                list = m_listBattle;
                break;
            case nsENUM.eSETPOPUP.eSETPOPUP_ABILITY:
                list = m_listAbility;
                break;
        }

        if (null == list)
            return 0;

        return list.Count;
    }

    public int CheckEquip(nsENUM.eSETPOPUP _e, int _nItemID)//장착하겠다.
    {
        int nIndex = -1;

        if (!m_DictionaryItem.ContainsKey(_nItemID))//장착할 아이템 있는지
            return nIndex;

        Item useitem = m_DictionaryItem[_nItemID];

        if (useitem.m_bUse)//장착중인 아이템이면 pass
            return nIndex;

        List<int> list = null;

        switch(_e)
        {
            case nsENUM.eSETPOPUP.eSETPOPUP_BATTLE:
                list = m_listBattle;
                break;
            case nsENUM.eSETPOPUP.eSETPOPUP_ABILITY:
                list = m_listAbility;
                break;
        }

        if (null == list)
            return -1;

        int nCount = list.Count;

        for (int i = 0; i < nCount; ++i)
        {
            if (!m_DictionaryItem.ContainsKey(list[i]))
                continue;

            Item item = m_DictionaryItem[list[i]];//현재 장착중인 아이템

            if (null == item)
                continue;

            if (item.m_eItemApply == useitem.m_eItemApply)//장작중인 아이템을 해제해야 하니
            {
                nIndex = item.m_nItemID;//장착중인 아이템 인데스
                break;
            }
        }

        if (-1 == nIndex)//중복아이템도 없고 장착 가능하면 장착
        {
            if ((int)nsENUM.eVALUE.eVALUE_4 > list.Count)//장착가능
                return _nItemID;
        }

        return nIndex;
    }

    public Item GetItem(int _nItemID)
    {
        if (m_DictionaryItem.ContainsKey(_nItemID))
            return m_DictionaryItem[_nItemID];

        return null;
    }

    public void AllLoadData()//전체 불러오기
    {
        bool breset = false;

        if (CheckReset())
            breset = true;

        if (0 == m_DictionaryItem.Count)
            breset = true;
        else
        {
            if (0 == m_PlayerData.m_nMonsterID || 0 == m_PlayerData.m_nPlayerID)
                breset = true;
        }

        if (breset)
            ResetData();

        ItemSort();
    }

    bool CheckReset()//리셋으로 새로 셋팅할지 여부
    {
        if (!LoadFile(GetDataPath()))
            return true;

        return false;
    }

    void ItemSort()
    {
        var pair = m_DictionaryItem.GetEnumerator();

        while (pair.MoveNext())//
        {
            Item item = pair.Current.Value;

            switch((nsENUM.eITEMTYPE)item.m_eItemType)
            {
                case nsENUM.eITEMTYPE.eITEMTYPE_ETERNAL://영구
                    {
                        if(item.m_bUse)
                            SetAbilityItemAdd(item.m_nItemID);
                    }
                    break;
                case nsENUM.eITEMTYPE.eITEMTYPE_CONSUME:
                    {
                        if (item.m_bUse)
                            SetBattleItemAdd(item.m_nItemID);
                    }
                    break;
            }
        }

        Debug.Log("ItemSort = " + m_DictionaryItem.Count);
    }

    public void ResetData()
    {
        Table_Default.Info info = SharedObject.g_TableMgr.m_Default.Get(1);

        if (null == info)
        {
            SharedObject.g_SceneMgr.Shutdown();
            return; 
        }

        m_PlayerData.m_nLevel = info.m_nLevel;
        m_PlayerData.m_nPlayerID = info.m_nPlayerID;
        m_PlayerData.m_nMonsterID = info.m_nMonsterID;
        m_PlayerData.m_nClearStage = info.m_nCreateStage;
        m_PlayerData.m_fPlayerHp = 0f;
        m_PlayerData.m_fMonsterHp = 0f;
        m_PlayerData.m_nExp = 0;

        m_PlayerData.m_nOptionBgm = 1;
        m_PlayerData.m_nOptionEffect = 1;

        m_DictionaryItem.Clear();
        m_listAbility.Clear();
        m_listBattle.Clear();

        for (int i = 0; i < (int)nsENUM.eVALUE.eVALUE_3; ++i)
        {
            ItemInsert(info.m_ArrayItem[i], 1);
            //ItemUse(info.m_ArrayItem[i], false);
        }

        ItemInsert(info.m_nTutorialItem, 1);

        m_PlayerData.m_eTutorial = eTUTORIAL.eTUTORIAL_INVEN;

        m_PlayerData.m_nScore = 0;

        m_PlayerData.m_nAdsCount = (int)nsENUM.eADS.eADS_REWARDCOUNT;

        m_PlayerData.m_strDate = DateTime.Today.ToShortDateString();

        m_PlayerData.m_bPrologue = true;

        m_PlayerData.m_bEnding = false;

        SaveFile(GetDataPath());
    }

    public string GetPath()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer:
                return Application.persistentDataPath;

            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.OSXEditor:
                return Application.dataPath;
            default:
                assert_cs.msg("default : " + Application.platform);
                return "";
        }
    }

    static public bool GetPlatformManifest()
    {
        bool bPlatform = true;

        switch (Application.platform)
        {
            case RuntimePlatform.OSXEditor://mac os
            case RuntimePlatform.OSXPlayer:
            case RuntimePlatform.IPhonePlayer:
                bPlatform = false;
                break;
        }
        return bPlatform;
    }

    bool CheckFile(string _strfilepath)
    {
        if (!System.IO.File.Exists(_strfilepath))
            return false;

        return true;
    }

    public bool SaveFile(string _StrPath, string _StrName = "/RPGQuiz.rpg")
    {
        if ("" == _StrPath)
            return false;

        string str = _StrPath + _StrName;

        var b = new BinaryFormatter();

        Stream stream = File.Open(str, FileMode.OpenOrCreate, FileAccess.Write);

        if (null == stream)
            return false;

        b.Serialize(stream, (object)m_PlayerData);
        b.Serialize(stream, (object)m_DictionaryItem);
        b.Serialize(stream, (object)m_listBattle);

        stream.Close();

        return true;
    }

    public bool LoadFile(string _StrPath, string _StrName = "/RPGQuiz.rpg")
    {
        string str = _StrPath + _StrName;

        if (!CheckFile(str))
            return false;

        var b = new BinaryFormatter();

        Stream stream = File.Open(str, FileMode.OpenOrCreate, FileAccess.Read);

        if (null == stream)
            return false;

        m_PlayerData = (PlayerData)b.Deserialize(stream);
        m_DictionaryItem = (Dictionary<int, Item>)b.Deserialize(stream);
        m_listBattle = (List<int>)b.Deserialize(stream);

        stream.Close();

        return true;
    }

    public string GetDataPath()//일반 경로
    {
        string strpath = "";

#if UNITY_EDITOR
        strpath = GetPath() + "/..RPGQuiz/";
#else
        strpath = GetPath() + "/";
#endif

        return strpath;
    }

    public string GetOptionPath()//옵션 경로
    {
        string strpath = GetPath();

#if UNITY_EDITOR
        DirectoryInfo info = System.IO.Directory.GetParent(strpath);

        strpath = info.Name;
#else
        DirectoryInfo info = System.IO.Directory.GetParent(strpath);

        info = info.Parent;

        strpath = info.FullName;  
#endif
        strpath += "/Backup";

        Debug.Log("GetOptionPath = " + strpath);

        return strpath;
    }
}