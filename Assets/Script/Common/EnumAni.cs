using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public enum eANIMODE
{
    eANIMODE_ETC,
    eANIMODE_SKILL,
    eANIMODE_MOVE,
}

[System.Serializable]
public enum eANI
{
    eANI_NONE = 0,
    eANI_IDLE,
    eANI_RUN,
    eANI_DAM01,
    eANI_ATK01,
    eANI_ATK02,
    eANI_ATK03,
    eANI_DIE,
}

[System.Serializable]
public enum eANIVALUE
{
    eANIVALUE_IDLE,
    eANIVALUE_RUN,
    eANIVALUE_DAM,
    eANIVALUE_ATK01,
    eANIVALUE_ATK02,
    eANIVALUE_ATK03,
    eANIVALUE_DIE,
    eANIVALUE_END
}

