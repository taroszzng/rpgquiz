﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Network;

public class UI_Event : MonoBehaviour
{
    public void Event()
    {
        Debug.Log("Event");
    }

    public void EventClose()
    {
        transform.gameObject.SetActive(false);
    }

    public void EventSound(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}