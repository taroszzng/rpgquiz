﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIToggleColor : MonoBehaviour {

    public UnityEngine.UI.Text text;

    public Color SelectColorText = Color.black;
    public Color UnSelectColorText = Color.black;

    public UnityEngine.UI.Image image;

    public Color SelectColorImage = Color.black;
    public Color UnSelectColorImage = Color.black;

    void Start()
    {
        var toggle = GetComponent<UnityEngine.UI.Toggle>();
        ChangedToggleColor(toggle.isOn);
        toggle.onValueChanged.AddListener((isSelect) =>
        {
            ChangedToggleColor(isSelect);
        });
    }

    void ChangedToggleColor(bool isSelect)
    {
        if (isSelect)
        {
            if(text != null)
                text.color = SelectColorText;
            if (image != null)
                image.color = SelectColorImage;
        }
        else
        {
            if (text != null)
                text.color = UnSelectColorText;
            if (image != null)
                image.color = UnSelectColorImage;
        }
    }
}