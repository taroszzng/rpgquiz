﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMatchHeightOrWidth : MonoBehaviour {

    public void Start()
    {
        var Scaler = GetComponent<UnityEngine.UI.CanvasScaler>();

        float rate = 0f;

        if (Screen.width > Screen.height)
            rate = Screen.width / Screen.height;
        else
            rate = Screen.height / Screen.width;

        if (rate > 1.4f)
            Scaler.matchWidthOrHeight = 1;
        else
            Scaler.matchWidthOrHeight = 0;
    }
}
