﻿/*
작성 : kks
검색 : [kks][UI]: UI OBJ
내용 : 상속 받는 BASE 
 */
using UnityEngine;
using UnityEngine.UI;

public class UI_1Depth : UI_Stack
{
    protected nsENUM.eUI_CLOSE m_eUI;

    public nsENUM.eUI_CLOSE eUI
    {
        set { m_eUI = value; }
        get { return m_eUI; }
    }

    protected virtual void AniPlay() { } //애니 시작
    protected virtual void AniClose() { }//애니 끝

    protected override void OnEnable()
    {
        base.OnEnable();//base.OnEnable 호출시 무조건 Esc로 해제 해야함.. 아님 재정의시 스택 사용 x 

        EnablePoint();
    }

    protected virtual void EnablePoint()
    {
        SharedObject.g_1Depth = this;
    }

    protected override void OnDisable()//OnEsc 호출 후 파괴될때 호출
    {
        base.OnEsc();

        DisablePoint();
    }

    protected virtual void DisablePoint()
    {
        SharedObject.g_1Depth = null;
    }
}