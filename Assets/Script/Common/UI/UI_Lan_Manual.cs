﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI_Lan_Manual : MonoBehaviour
{
    public List<string> m_LanList;

    void OnEnable()
    {
        // int nLanType = (int)Table_Lan.GetLanType();

        // assert_cs.set(m_Lan.Count > nLanType, "UI_Lan_Manual 에 값을 넣어주세요");
        //GetComponent<Text>().text = m_Lan[nLanType];
    }

    public void SetText(int nLanType)
    {
        assert_cs.set(m_LanList.Count > nLanType, "UI_Lan_Manual 에 값을 넣어주세요");
        GetComponent<Text>().text = m_LanList[nLanType];
    }

    public void SetText(string strText)//조합
    {
        //int nLanType = (int)Table_Lan.GetLanType();

        //assert_cs.set(m_Lan.Count > nLanType, "UI_Lan_Manual 에 값을 넣어주세요");
        //string str = string.Format(m_Lan[nLanType], strText);
        //GetComponent<Text>().text = str;
    }

    public string GetText(int nLan)//랭귀지 리턴
    {
        return m_LanList[nLan];
    }

    public void DownLoadFail()//다운로드를 접속 못할때
    {
        GetComponent<Text>().text = m_LanList[5];
    }
}