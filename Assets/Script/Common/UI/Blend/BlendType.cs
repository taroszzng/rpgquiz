namespace BlendModes
{
	public enum ObjectType
	{
		Unknown,
        UIDefault,
	}

    public enum RenderMode
    {
        UI,
    }

    public enum KeyType
    {
        Scale10,
        Scale11,
        Scale12,
        Scale13,
        Scale14,
        Scale15,
        Scale16,
        Scale17,
        Scale18,
        Scale19,
        Scale20
    }
}
