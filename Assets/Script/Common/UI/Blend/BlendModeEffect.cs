using UnityEngine;
using UnityEngine.UI;

namespace BlendModes
{
	/// <summary>
	/// Adds blend mode effect to the object with an UI graphic or compatible renderer.
	/// </summary>
	[AddComponentMenu("Effects/Blend Mode"), ExecuteInEditMode]
	public class BlendModeEffect : MonoBehaviour
	{
		/// <summary>
		/// Current blend mode of the object.
		/// </summary>
		public BlendMode BlendMode
		{
			get { return _blendMode; }
			set { SetBlendMode(value, RenderMode, KeyType); }
		}

		/// <summary>
		/// Current render mode used for blending.
		/// </summary>
		public RenderMode RenderMode
		{
			get { return _renderMode; }
			set { SetBlendMode(BlendMode, value, KeyType); }
		}

        public KeyType KeyType
        {
            get { return _KeyType; }
            set { SetBlendMode(BlendMode, RenderMode, value); }
        }
		/// <summary>
		/// Texture to use with the object.
		/// Have no effect for GUI and sprite objects.
		/// </summary>
		public Texture2D Texture
		{
			get { return _texture; }
			set { _texture = value; }
		}

		/// <summary>
		/// Tint color of the object.
		/// Have no effect for GUI and sprite objects.
		/// </summary>
		public Color TintColor
		{
			get { return _tintColor; }
			set { _tintColor = value; }
		}

		/// <summary>
		/// Type of the object in context of applying blend mode effect.
		/// </summary>
		public ObjectType ObjectType
		{
			get 
			{
				if (GetComponent<MaskableGraphic>()) 
                    return ObjectType.UIDefault;

				return ObjectType.Unknown;
			}
		}

		/// <summary>
		/// Material of the object.
		/// </summary>
		public Material Material
		{
			get
			{
				switch (ObjectType)
				{
					case ObjectType.UIDefault:
						return GetComponent<MaskableGraphic>().material;
					
					default:
						return null;
				}
			}
			set
			{
				switch (ObjectType)
				{
					case ObjectType.UIDefault:
						GetComponent<MaskableGraphic>().material = value;
						break;
				}
			}
		}

		[SerializeField]
		private BlendMode _blendMode;
		[SerializeField]
		private RenderMode _renderMode;
		[SerializeField]
		private Texture2D _texture;
		[SerializeField]
		private Color _tintColor = Color.white;

        [SerializeField]
        private KeyType _KeyType;

		/// <summary>
		/// Sets specific blend mode to the object.
		/// </summary>
		/// <param name="blendMode">Blend mode.</param>
		/// <param name="renderMode">Render mode to use for blending.</param>
		public void SetBlendMode (BlendMode blendMode, RenderMode renderMode = RenderMode.UI , KeyType keytype = KeyType.Scale10)
		{
			if (ObjectType == ObjectType.Unknown) 
                return;

            Material = BlendMaterials.GetMaterial(ObjectType, renderMode, blendMode, keytype);

			Texture = Texture;
			TintColor = TintColor;

			_blendMode = blendMode;
			_renderMode = renderMode;
		}

		public void OnEnable ()
		{
			if (Material && Material.mainTexture) 
				Texture = (Texture2D)Material.mainTexture;

			SetBlendMode(BlendMode, RenderMode, KeyType);
		}

		public void OnDisable ()
		{
			Texture2D curTex = Texture;

            Material = BlendMaterials.GetMaterial(ObjectType, RenderMode.UI, BlendMode.Century, KeyType.Scale10 );

			if (Material && curTex) Material.mainTexture = curTex;
		}
	}
}
