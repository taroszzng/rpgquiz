using UnityEngine;
using System.Collections.Generic;

namespace BlendModes
{
	/// <summary> 
	/// Manages all the materials used for blending and provides caching.
	/// </summary> 
	public static class BlendMaterials
	{
        private static Dictionary<ObjectType, Dictionary<RenderMode, Dictionary<BlendMode, Dictionary<KeyType, Material>>>> cachedMaterials =
            new Dictionary<ObjectType, Dictionary<RenderMode, Dictionary<BlendMode, Dictionary<KeyType, Material>>>>();

		public static Material GetMaterial (ObjectType objectType, RenderMode renderMode, BlendMode blendMode, KeyType keyType)
		{
            if (
                cachedMaterials.ContainsKey(objectType) &&
                cachedMaterials[objectType].ContainsKey(renderMode) &&
                cachedMaterials[objectType][renderMode].ContainsKey(blendMode) &&
                cachedMaterials[objectType][renderMode][blendMode].ContainsKey(keyType)
                )
            {
                return cachedMaterials[objectType][renderMode][blendMode][keyType];
            }
            else
			{
                string strShader = objectType.ToString() + "/" + renderMode.ToString();
                Shader _shader = Shader.Find(strShader);

                var mat = new Material(_shader);
				mat.hideFlags = HideFlags.HideAndDontSave;

                string strkey = "BM" + keyType.ToString();

                mat.EnableKeyword(strkey);
                //BMScale
                //BMScall

				if (!cachedMaterials.ContainsKey(objectType))
                    cachedMaterials.Add(objectType, new Dictionary<RenderMode, Dictionary<BlendMode, Dictionary<KeyType, Material>>>());

				if (!cachedMaterials[objectType].ContainsKey(renderMode))
                    cachedMaterials[objectType].Add(renderMode, new Dictionary<BlendMode, Dictionary<KeyType, Material>>());

				if (!cachedMaterials[objectType][renderMode].ContainsKey(blendMode))
                    cachedMaterials[objectType][renderMode].Add(blendMode, new Dictionary<KeyType, Material>());

                if (!cachedMaterials[objectType][renderMode][blendMode].ContainsKey(keyType))
                    cachedMaterials[objectType][renderMode][blendMode].Add(keyType, mat);

				return mat;
			}
		}
	}
}
