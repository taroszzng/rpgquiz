﻿/*
작성 : kks
검색 :[kks][MarkText] : text에 k(천), m(만)등 표시
내용 : Text를 상속받는 부모 객체 공통데이타 처리
 */
using UnityEngine;
using UnityEngine.UI;

public class UI_BaseText : Text
{
    protected long m_lValue = 0;

    virtual public long lValue
    {
        get
        {
            return m_lValue;
        }

        set
        {
            m_lValue = value;

            SetValue();
        }
    }

    virtual protected void SetValue() {}

    virtual public long lFinal
    {
        get
        {
            return m_lValue;
        }
    }
}
