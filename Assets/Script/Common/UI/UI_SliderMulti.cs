﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_SliderMulti : MonoBehaviour
{
    public Slider m_Front_Slider;
    public Slider m_Back_Slider;
    public float m_WaitTime = 0.5f;
    public float m_Speed = 1f;

    float m_FrontRate;
    float m_BackRate;
    float m_Time;
    public enum eCHANGE
    {
        NONE,
        LEFT,
        RIGHT,
    }
    eCHANGE m_Change;

    void Awake()
    {
        Reset();
    }

    public void Reset()
    {
        m_FrontRate = 1f;
        m_BackRate = 1f;
        m_Change = eCHANGE.NONE;

        m_Back_Slider.gameObject.SetActive(false);
        if( m_Front_Slider != null )
            m_Front_Slider.value = 1f;
    }

    public void SetRate(float rate)
    {
        switch (m_Change)
        {
            case eCHANGE.NONE:
                {
                    if (rate == m_FrontRate)
                        return;

                    m_Time = m_WaitTime;
                    m_Back_Slider.gameObject.SetActive(true);

                    if (rate < m_FrontRate)
                    {
                        m_Change = eCHANGE.LEFT;
                        m_FrontRate = rate;

                        m_Front_Slider.value = m_FrontRate;
                        m_Back_Slider.value = m_BackRate;
                    }
                    else
                    {
                        m_Change = eCHANGE.RIGHT;
                        m_BackRate = rate;

                        m_Back_Slider.value = m_BackRate;
                    }
                }
                break;
            case eCHANGE.LEFT:
                {
                    if (rate == m_FrontRate)
                        return;

                    if (rate > m_FrontRate)
                    {
                        m_Change = eCHANGE.RIGHT;
                        m_BackRate = rate;

                        m_Back_Slider.value = m_BackRate;
                    }
                    else
                    {
                        m_FrontRate = rate;
                        m_Front_Slider.value = m_FrontRate;
                    }
                }
                break;
            case eCHANGE.RIGHT:
                {
                    if (rate == m_BackRate)
                        return;

                    if (rate < m_FrontRate)
                    {
                        m_Change = eCHANGE.LEFT;
                        m_BackRate = m_FrontRate;
                        m_FrontRate = rate;

                        m_Front_Slider.value = m_FrontRate;
                        m_Back_Slider.value = m_BackRate;
                    }
                    else
                    {
                        m_BackRate = rate;
                        m_Back_Slider.value = m_BackRate;
                    }
                }
                break;
        }
    }

    void Update()
    {
        if (m_Change == eCHANGE.NONE)
            return;

        if ((m_Time -= Time.deltaTime) > 0f)
            return;

        switch (m_Change)
        {
            case eCHANGE.LEFT:
                {
                    m_BackRate -= Time.deltaTime * m_Speed;
                    if (m_BackRate < m_FrontRate)
                    {
                        m_Change = eCHANGE.NONE;
                        m_BackRate = m_FrontRate;
                        m_Back_Slider.gameObject.SetActive(false);
                    }
                    else
                    {
                        m_Back_Slider.value = m_BackRate;
                    }
                }
                break;
            case eCHANGE.RIGHT:
                {
                    m_FrontRate += Time.deltaTime * m_Speed;

                    if (m_FrontRate > m_BackRate)
                    {
                        m_Change = eCHANGE.NONE;
                        m_FrontRate = m_BackRate;
                        m_Back_Slider.gameObject.SetActive(false);
                    }
                    else
                    {
                        m_Front_Slider.value = m_FrontRate;
                    }
                }
                break;
        }
    }
}
