﻿/*
작성 : kks
검색 : [kks][UI]:UI OBJ 관리
내용 : OBJ 저장 및 삭제 
 */
using UnityEngine;

public class UI_Stack : MonoBehaviour
{
    protected bool m_bDestroy = false;

    protected virtual void OnEnable()
    {
        if (SharedObject.g_UIStack.Count > 0)
        {
            if (SharedObject.g_UIStack.Peek() == gameObject)
                return;
        }

        SharedObject.g_UIStack.Push(gameObject);
        Debug.Log("팝업윈도우 - 추가 : " + gameObject + " (" + SharedObject.g_UIStack.Count + ") ");
    }

    protected virtual void OnDisable() {}

    public virtual bool OnEsc()
    {
        if (SharedObject.g_UIStack.Count > 0)
        {
            GameObject aaa = SharedObject.g_UIStack.Peek();
            assert_cs.set(SharedObject.g_UIStack.Peek() == gameObject);
            SharedObject.g_UIStack.Pop();

            Debug.Log("팝업윈도우 - 제거 : " + gameObject + " (" + SharedObject.g_UIStack.Count + ") ");
        }

        if (!m_bDestroy)
            gameObject.SetActive(false);
        else
            GameObject.Destroy(gameObject);

        return true;
    }
}
