﻿/*
작성 : kks
검색 : [kks][UI] : 팝업창
내용 : 팝업 UI
 */
using UnityEngine;
using UnityEngine.UI;

public class UI_3Depth : UI_1Depth
{
    //public delegate void CommonCallBack();
    //protected CommonCallBack m_CommonCallBack = null;
    //protected CommonCallBack m_CommonCallBack2 = null;

    protected override void OnEnable()
    {
        base.OnEnable();//base.OnEnable 호출시 무조건 Esc로 해제 해야함.. 아님 재정의시 스택 사용 x 
    }

    protected override void EnablePoint()
    {
        SharedObject.g_3Depth = this;
    }

    protected override void OnDisable()//OnEsc 호출 후 파괴될때 호출
    {
        base.OnDisable();
    }

    protected override void DisablePoint()
    {
        SharedObject.g_3Depth = null;
    }

    //public Transform Active(string name, CommonCallBack cb, CommonCallBack cb2 = null)
    //{
    //    m_CommonCallBack = null;

    //    if (cb != null)
    //        m_CommonCallBack = new CommonCallBack(cb);

    //    m_CommonCallBack2 = null;

    //    if (cb2 != null)
    //        m_CommonCallBack2 = new CommonCallBack(cb2);

    //    Transform tr = transform.FindChild(string.Format("3_Depth/{0}", name));

    //    assert_cs.set(tr != null);

    //    tr.gameObject.SetActive(true);

    //    return tr;
    //}
}