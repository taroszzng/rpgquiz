﻿/*
작성 : kks
검색 : [kks][MarkText] : text에 k(천), m(만)등 표시
내용 : Text 1000단위 K, 10000단위 M을 표시
 */
using System;

public class UI_MarkText : UI_BaseText
{
    public enum EMARKTEXTTYPE
    {
        EMARKTEXTTYPE_NONE,
        EMARKTEXTTYPE_K,
        EMARKTEXTTYPE_M,
        EMARKTEXTTYPE_G,
        EMARKTEXTTYPE_T,
        EMARKTEXTTYPE_MARK,
        EMARKTEXTTYPE_END
    }

    int m_CutCount = 3;

    public EMARKTEXTTYPE eMarkTextType = EMARKTEXTTYPE.EMARKTEXTTYPE_K;

    override protected void SetValue()
    {
        text = lValue.ToString();

        switch(eMarkTextType)
        {
            case EMARKTEXTTYPE.EMARKTEXTTYPE_K:
                SetMarkK();
                break;
            case EMARKTEXTTYPE.EMARKTEXTTYPE_M:
                SetMarkM();
                break;
            case EMARKTEXTTYPE.EMARKTEXTTYPE_G:
                SetMarkG();
                break;
            case EMARKTEXTTYPE.EMARKTEXTTYPE_T:
                SetMarkT();
                break;
            case EMARKTEXTTYPE.EMARKTEXTTYPE_MARK:
                SetMark();
                break;
        }
    }

    void SetMarkK()
    {
        int nCut = (int)eMarkTextType + m_CutCount;

        string strValue = lValue.ToString();

        int nLength = strValue.Length;

        if (nCut > nLength - 1)
        {
            text = strValue;
            return;
        }

        string str = strValue.Substring(0, strValue.Length - nCut);

        nCut = nLength - nCut;

        string str2 = strValue.Substring(nCut, 1);

        str += "." + str2;

        text = str + "K";
    }

    void SetMarkM()
    {
        int nCut = (int)eMarkTextType + m_CutCount;

        string strValue = lValue.ToString();

        int nLength = strValue.Length;

        if (nCut > nLength - 1)
        {
            text = strValue;
            return;
        }

        string str = strValue.Substring(0, strValue.Length - nCut);

        nCut = nLength - nCut;

        string str2 = strValue.Substring(nCut, 1);

        str += "." + str2;

        text = str + "M";
    }

    void SetMarkG()
    {
        int nCut = (int)eMarkTextType + m_CutCount;

        string strValue = lValue.ToString();

        int nLength = strValue.Length;

        if (nCut > nLength - 1)
        {
            text = strValue;
            return;
        }

        string str = strValue.Substring(0, strValue.Length - nCut);

        nCut = nLength - nCut;

        string str2 = strValue.Substring(nCut, 1);

        str += "." + str2;

        text = str + "G";
    }

    void SetMarkT()
    {
        int nCut = (int)eMarkTextType + m_CutCount;

        string strValue = lValue.ToString();

        int nLength = strValue.Length;

        if (nCut > nLength - 1)
        {
            text = strValue;
            return;
        }

        string str = strValue.Substring(0, strValue.Length - nCut);

        nCut = nLength - nCut;

        string str2 = strValue.Substring(nCut, 1);

        str += "." + str2;

        text = str + "T";
    }

    void SetMark()
    {
        string strValue = lValue.ToString();

        int nLength = strValue.Length;

        if (m_CutCount > nLength - 1)
        {
            text = strValue;
            return;
        }

        int nCut = m_CutCount;

        string strmark = "K";

        if ((int)EMARKTEXTTYPE.EMARKTEXTTYPE_T + m_CutCount < nLength)
        {
            nCut += 3;

            strmark = "T";
        }
        else if((int)EMARKTEXTTYPE.EMARKTEXTTYPE_G + m_CutCount < nLength)
        {
            nCut += 2;

            strmark = "G";
        }
        else if((int)EMARKTEXTTYPE.EMARKTEXTTYPE_M + m_CutCount < nLength)
        {
            nCut += 1;

            strmark = "M";
        }

        string str = strValue.Substring(0, strValue.Length - nCut);

        nCut = nLength - nCut;

        string str2 = strValue.Substring(nCut, 1);

        str += "." + str2;

        text = str + strmark;
    }
}