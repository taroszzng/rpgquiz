﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class UI_RawImageEasy : MonoBehaviour
{
    public int m_nTexSize = 1024;
    public int m_nIconSize = 64;
    public int m_nIndex = -1;
    int m_nSetIndex = -1;

    void OnEnable()
    {
        Cal();
    }

#if UNITY_EDITOR
    void Update()
    {
        Cal();
    }
#endif

    public void Set( int _nIndex )
    {
        m_nIndex = _nIndex;
        m_nSetIndex = -1;

        Cal();
    }

    void Cal()
    {
        if (m_nSetIndex == m_nIndex)
            return;

        int cnt = m_nTexSize / m_nIconSize;
        int dummy = m_nTexSize % m_nIconSize;
        float uv = (float)m_nIconSize / (float)m_nTexSize;

        int idx = m_nIndex - 1;
        int x = idx % cnt;
        int y = idx / cnt;

        RawImage img = GetComponent<RawImage>();
        img.uvRect = new Rect(( x * m_nIconSize) / (float)m_nTexSize, (((cnt - 1) - y) * m_nIconSize + dummy) / (float)m_nTexSize, uv, uv);

        m_nSetIndex = m_nIndex;
    }
}