﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Lan : MonoBehaviour
{
    public int m_nLanID = 0;

    void OnEnable()
    {
#if UNITY_EDITOR        
        if (m_nLanID == 0)
        {
            Transform tr = transform;

            string log = tr.name;

            while( tr.parent != null )
            {
                tr = tr.parent;
                log += " + " + tr.name;
            }

            Debug.LogError( "Lan ID = 0 : " + log );
            return;
        }
#endif
        //if (null == SharedObject.g_TableMgr)
        //    return;

        //Debug.Log("LanID = " + m_nLanID);

        GetComponent<Text>().text = SharedObject.g_TableMgr.m_Lan.Get(m_nLanID).m_strDec;
    }
}