﻿using UnityEngine;
using System.Collections;

public class UI_Sound : MonoBehaviour
{
    public int ID;

    void PlayEffect()//버튼에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(ID);
    }

    public void PlayEffect(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}
