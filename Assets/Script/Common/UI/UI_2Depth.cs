﻿/*
작성 : kks
검색 : [kks][UI]:
내용 : 전체를 가리는 ui
 */
using UnityEngine;
using UnityEngine.UI;

public class UI_2Depth : UI_1Depth
{
    protected override void OnEnable()
    {
        base.OnEnable();//base.OnEnable 호출시 무조건 Esc로 해제 해야함.. 아님 재정의시 스택 사용 x 
    }

    protected override void EnablePoint()
    {
        SharedObject.g_2Depth = this;
    }

    protected override void OnDisable()//OnEsc 호출 후 파괴될때 호출
    {
        base.OnDisable();
    }

    protected override void DisablePoint()
    {
        SharedObject.g_2Depth = null;
    }
}