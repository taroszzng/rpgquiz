﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawLine
{
    public int gridSizeX;
    public int gridSizeY;
    public int gridSizeZ;
     
    public float smallStep;
    public float largeStep;
     
    public float startX;
    public float startY;
    public float startZ;
     
    private IDictionary<Color, Material> materialsByColor = new Dictionary<Color, Material>();

    private Material GetLineMaterial(Color _color)
    {
        Material lineMaterial;
        if (!materialsByColor.TryGetValue(_color, out lineMaterial)) 
        {
            lineMaterial = new Material( "Shader \"Lines/Colored Blended\" {" +
                                        " Properties { _Color (\"Main Color\", Color) = (" + _color.r + "," + _color.g + "," + _color.b + "," + _color.a + ") } " +
                                        " SubShader { Pass { " +
                                        " Blend SrcAlpha OneMinusSrcAlpha " +
                                        " ZWrite Off Cull Off Fog { Mode Off } " +
                                        " Color[_Color] " +
                                        " BindChannels {" +
                                        " Bind \"vertex\", vertex Bind \"color\", color }" +
                                        "} } }" );
 
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;

            materialsByColor.Add(_color, lineMaterial);
        }
        return lineMaterial;
    }

    public void Draw(BoxCollider _box, Color _color) 
    {
        //Material mat = GetLineMaterial(_color);
        //// set the current material
        //mat.SetPass( 0 );
         
        //GL.Begin( GL.LINES );
        //GL.Color(_color);

        //Vector3 c = _box.transform.rotation*_box.center + _box.transform.position;
        //Actor.To_SR_Coordinate( ref c );
        //Vector3 hsize = _box.extents;
        //hsize.x *= _box.gameObject.transform.localScale.x;
        //hsize.y *= _box.gameObject.transform.localScale.y;
        //hsize.z *= _box.gameObject.transform.localScale.z;

        //{
        //    GL.Vertex3(c.x - hsize.x, c.y + hsize.y, c.z);
        //    GL.Vertex3(c.x + hsize.x, c.y + hsize.y, c.z);

        //    GL.Vertex3(c.x - hsize.x, c.y + hsize.y, c.z);
        //    GL.Vertex3(c.x - hsize.x, c.y - hsize.y, c.z);

        //    GL.Vertex3(c.x + hsize.x, c.y + hsize.y, c.z);
        //    GL.Vertex3(c.x + hsize.x, c.y - hsize.y, c.z);

        //    GL.Vertex3(c.x - hsize.x, c.y - hsize.y, c.z);
        //    GL.Vertex3(c.x + hsize.x, c.y - hsize.y, c.z);
        //}
 
        //GL.End();
     }

}
