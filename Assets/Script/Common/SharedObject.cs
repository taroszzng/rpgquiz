﻿#define RELEASE

using UnityEngine;
using System.Collections.Generic;

public static class SharedObject
{
    static public PlayerPrefsData g_PlayerPrefsData = new PlayerPrefsData();

    static public TableMgr g_TableMgr;
    static public Zone g_CurZone;

    static public SceneMgr g_SceneMgr;

    static public MainCamera g_MainCamera;
    static public Camera g_UICamera;

    static public DamageMgr g_DamageMgr;

    static public CollectorMgr g_CollectorMgr;

    static public EffectPanelMgr g_EffectPanelMgr;

    static public SceneProperty g_SceneProperty;

    static public InAppPurchaser g_Purchaser;

    static public  bool g_bGoogleLogin = false;

    static public SoundScript g_SoundScript;

    static public Stack<GameObject> g_UIStack = new Stack<GameObject>();//ui관리

    static public UI_Main g_Main;
    static public UI_Mode g_Mode;
    static public UI_Stage g_Stage;
    static public UI_MonsterSelect g_MonsterSelect;
    static public UI_AMode g_AMode;
    static public EnhancedScrollerRpgDice.Set.UI_AMode_Set g_AModeSet;

    static public UI_1Depth g_1Depth;
    static public UI_2Depth g_2Depth;
    static public UI_3Depth g_3Depth;

    static public AdsMgr g_AdsMgr;

    static public Tutorial g_Tutorial;

    static public TableMgr GetTableMgr()
    {
        if (g_TableMgr == null)
        {
            g_TableMgr = new TableMgr();
            g_TableMgr.Init();
        }

        return g_TableMgr;
    }

    public static bool CheckStack()
    {
        if (0 < g_UIStack.Count)
        {
            GameObject aaa = g_UIStack.Peek();
            g_UIStack.Pop();

            Debug.Log("팝업윈도우 - 제거 : " + aaa + " (" + g_UIStack.Count + ") ");

            return true;
        }

        return false;
    }
}
