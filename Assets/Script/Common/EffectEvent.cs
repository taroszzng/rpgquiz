﻿using UnityEngine;
using System.Collections;

public class EffectEvent : MonoBehaviour
{
    public void Close()
    {
        transform.gameObject.SetActive(false);
    }

    public void EventSoundEffect(int _nID)//이벤트에서 사용
    {
        if (null != SharedObject.g_SoundScript)
            SharedObject.g_SoundScript.PlayEffect(_nID);
    }
}