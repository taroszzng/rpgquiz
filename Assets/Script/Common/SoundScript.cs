using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundScript : MonoBehaviour 
{
    float m_LastPlayedTime = 0f;
    string m_LastPlayedName;

    bool m_LockBGM;

    public bool LockBGM
    {
        set { m_LockBGM = value; }
        get { return m_LockBGM; }
    }

    public nsENUM.eSOUNDBGM m_CurBGM;

    AudioSource m_BGM;
    AudioSource m_Effect;

    string m_strPath = "Sound/";

    void Awake()
    {
        assert_cs.set(SharedObject.g_SoundScript == null, "SharedObject.g_SoundScript == null");

        DontDestroyOnLoad(gameObject);

        SharedObject.g_SoundScript = this;

        m_BGM = transform.Find("BGM").GetComponent<AudioSource>();
        m_BGM.loop = true;

        m_Effect = transform.Find("Effect").GetComponent<AudioSource>();

        m_LockBGM = false;
    }

    public void PlayEffect(int _nID)
    {
        if (0 == _nID)
            return;

        if (0 == SharedObject.g_PlayerPrefsData.OptionEffect)
            return;

        Table_Sound.Info info = SharedObject.g_TableMgr.m_Sound.Get(_nID);

        if (null == info)
            return;

        float dt = Time.time - m_LastPlayedTime;

        if (dt < info.m_fTime)
        {
            if (info.m_strSound == m_LastPlayedName)
                return;
        }

        m_LastPlayedTime = Time.time;
        m_LastPlayedName = info.m_strSound;

        AudioClip clip = Resources.Load<AudioClip>(m_strPath + info.m_strSound);

        assert_cs.set(clip, "���� ������ ���� ���� �ʽ��ϴ�. clip path = " + _nID);

        m_Effect.PlayOneShot(clip);
    }

    public void PlayBGM(nsENUM.eSOUNDBGM _eBGM)
    {
        if (m_LockBGM)
            return;

        if (m_CurBGM == _eBGM)
            return;

        if (m_CurBGM != nsENUM.eSOUNDBGM.eSOUNDBGM_NONE)
            m_BGM.Stop();

        if (0 == SharedObject.g_PlayerPrefsData.OptionBgm)
            return;

        m_CurBGM = _eBGM;

        Table_Sound.Info info = SharedObject.g_TableMgr.m_Sound.Get((int)_eBGM);

        if (null == info)
            return;

        m_BGM.mute = false;
        m_BGM.clip = Resources.Load<AudioClip>(m_strPath + info.m_strSound);
        m_BGM.loop = info.m_bLoop;
        m_BGM.Play();
    }

    public void MuteBgm(bool _bgmMute)
    {
        m_BGM.mute = _bgmMute;

        if (_bgmMute)
            CheckBgm();
        else
            m_BGM.Stop();
    }

    void CheckBgm()
    {
        m_CurBGM = nsENUM.eSOUNDBGM.eSOUNDBGM_NONE;

        nsENUM.eSOUNDBGM ebgm = nsENUM.eSOUNDBGM.eSOUNDBGM_TITLE;

        switch (SharedObject.g_SceneMgr.Scene)
        {
            case nsENUM.eSCENE.eSCENE_BATTLE:
                ebgm = nsENUM.eSOUNDBGM.eSOUNDBGM_BATTLE;
                break;
        }

        PlayBGM(ebgm);
    }
}


