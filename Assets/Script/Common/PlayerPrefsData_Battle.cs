﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public partial class PlayerPrefsData : MonoBehaviour
{
    public List<int> m_listBattle = new List<int>(); //전투셋팅아이템

    public bool CheckBattleItem(int _nItemID)
    {
        bool bcheck = false;

        int nCount = m_listBattle.Count;

        for (int i = 0; i < nCount; ++i)
        {
            if (_nItemID == m_listBattle[i])
            {
                bcheck = true;

                break;
            }
        }

        return bcheck;
    }

    void SetBattleItemAdd(int _nItemID) //장착
    {
        if (CheckBattleItem(_nItemID))//있으면 아웃
            return;

        m_listBattle.Add(_nItemID);
    }

    void SetBattleItemDelete(int _nItemID)//해제
    {
        if (!CheckBattleItem(_nItemID))//없으면
            return;

        m_listBattle.Remove(_nItemID);
    }
}