﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Util_Sort_Particle : MonoBehaviour
{
    Transform m_Tr;
    public int m_RenderOrder = 0;
    Renderer m_Renderer;
    int m_OldOrder = -1;
    void Awake()
    {
        m_Renderer = transform.GetComponent<ParticleSystem>().GetComponent<Renderer>();
    }

    void OnEnable()
    {
        m_Tr = transform;
    }

    void LateUpdate()
    {
        int order = 0;
        if (m_RenderOrder >= 0)
            order = 2000 + (int)(-m_Tr.position.z * 1000f) + m_RenderOrder + 100;
        else
            order = 2000 + (int)(-m_Tr.position.z * 1000f) + m_RenderOrder;

        if (order == m_OldOrder)
            return;

        m_OldOrder = order;
        m_Renderer.sortingOrder = order;
    }
}
