﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Util_Sort_Static : MonoBehaviour
{
    Transform m_Tr;
    SpriteRenderer m_SR;
    int m_OldOrder = -1;

    public int m_SortOrder = 0;

    void Awake()
    {
        m_SR = transform.GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        m_Tr = transform;
    }

    void LateUpdate()
    {
        int order = 2000 + (int)(-m_Tr.position.z * 1000f) + m_SortOrder;
        if (order == m_OldOrder)
            return;

        m_OldOrder = order;
        m_SR.sortingOrder = order; 
    }

}

