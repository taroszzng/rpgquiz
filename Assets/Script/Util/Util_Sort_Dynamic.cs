﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Util_Sort_Dynamic : MonoBehaviour
{
    [HideInInspector]
    public int m_AddOrder = 0;

    Transform m_Tr;
    SpriteRenderer m_SR;
    int m_OldOrder = -1;

    //public bool m_Renderer = false;//랜더러의 인덱스 사용 여부

    void Awake()
    {
        m_SR = transform.GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        m_Tr = transform;
    }

    void LateUpdate()
    {
        if (null == m_SR)
            return;

        //int order = m_SR.sortingOrder;//소팅 오더를 쓴다.

        //if(!m_Renderer)//소팅 오더의 셋팅을 안쓴다.
        //{ 
        //    order = 2000 + (int)(-m_Tr.position.z * 1000f) + m_AddOrder;

        //    if( order == m_OldOrder )
        //    return;
        //}

        int order = 2000 + (int)(-m_Tr.position.z * 1000f) + m_AddOrder;

        if (order == m_OldOrder)
            return;

        m_OldOrder = order;
        m_SR.sortingOrder = order; 
    }
}
