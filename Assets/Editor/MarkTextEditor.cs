﻿/*
작성 : kks
검색 : [kks][MarkText] : text에 k(천), m(만)등 표시
내용 : Text 1000단위 K, 10000단위 M을 표시
 */
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UI_MarkText))]
public class MarkTextEditor : UnityEditor.UI.TextEditor
{
    GUIStyle styleHelpboxInner;

    void Awake()
    {
        styleHelpboxInner = new GUIStyle("HelpBox");
        styleHelpboxInner.padding = new RectOffset(4, 4, 4, 4);
    }

    public override void OnInspectorGUI()
    {
        UI_MarkText component = (UI_MarkText)target;

        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Extention", EditorStyles.boldLabel);
        GUILayout.BeginVertical(styleHelpboxInner);

        component.lValue = EditorGUILayout.LongField(@"값", component.lValue);

        component.eMarkTextType = (UI_MarkText.EMARKTEXTTYPE)EditorGUILayout.EnumPopup("Mark Type:", component.eMarkTextType);

        component.lValue = component.lValue;

        if (GUI.changed)
            EditorUtility.SetDirty(component);

        GUILayout.EndVertical();
    }
}