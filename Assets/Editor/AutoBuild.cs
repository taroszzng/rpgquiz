﻿#define RELEASE
// Build script for Jenkins build. 

using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

class Unity3dBuilder
{
    static string[] SCENES = FindEnabledEditorScenes();
    static string TARGET_DIR = "Build";
    static string APP_NAME = "RPGQuiz";
    static int BUILD_DATE = 0;

    [MenuItem("Custom/Version/CurVersion", false, 1)]
    static void CurVersion()
    {
        Debug.Log(" ===== Code ===== " + PlayerSettings.Android.bundleVersionCode + " Version = " + Application.version);
    }

    [MenuItem("Custom/Version/CodeUp", false, 1)]
    static void CodeUp()
    {
        string str = Application.version;

        float fcode = Convert.ToSingle(str);

        fcode += 0.01f;

        str = fcode.ToString();

        PlayerSettings.bundleVersion = str;

        int code = PlayerSettings.Android.bundleVersionCode;
        code += 1;

        PlayerSettings.Android.bundleVersionCode = code;

        Debug.Log(" ===== Code ===== " + code + " Version = " + str);
    }

    [MenuItem("Custom/Version/VersionUp", false, 1)]
    static void VersionUp()
    {
        string str = Application.version;

        float fcode = Convert.ToSingle(str);

        fcode += 1f;

        str = fcode.ToString();

        PlayerSettings.bundleVersion = str;

        int code = PlayerSettings.Android.bundleVersionCode;
        code += 1;

        PlayerSettings.Android.bundleVersionCode = code;

        Debug.Log(" ===== Version ===== " + str + " code = " + code);
    }

    [MenuItem("Custom/CI/Test Edit Build")]
    static void TestEditBuildVersion()
    {
        //PlayerSettings.iOS.targetOSVersionString = iOSTargetOSVersion.iOS_8_0.ToString();

        string target_setting = Application.dataPath + "/Editor/BuildOptionEdited.cs";
        StreamReader streamReader = new StreamReader(target_setting);
        string contents = streamReader.ReadToEnd();
        streamReader.Close();
        streamReader.Dispose();

        string todayStringInt = SetBuildDate(ref contents);
        SetIosBuildNumber(ref contents);
        System.IO.File.WriteAllText(target_setting, contents);
    }

    static string SetBuildDate(ref string contents)
    {
        System.DateTime Today = System.DateTime.Now;
        string todayStringInt = string.Format("{0:00}{1:00}{2:00}{3:00}{4:00}", (Today.Year - 2000), Today.Month, Today.Day, Today.Hour, Today.Minute);

        string fnd = "public static int BuildDate";

        int nIndex = contents.IndexOf(fnd) + fnd.Length;
        int nIndex2 = contents.IndexOf(';', nIndex + 1);
        string previous_version = contents.Substring(nIndex, nIndex2 - nIndex);

        contents = contents.Replace(previous_version, " = " + todayStringInt);
        BUILD_DATE = int.Parse(todayStringInt);

        Debug.Log("SetBuildDate : " + todayStringInt);
        return todayStringInt;
    }

    static int SetIosBuildNumber(ref string contents)
    {
        string fnd = "public static int IosBuildNumber = ";

        int nIndex = contents.IndexOf(fnd) + fnd.Length;
        int nIndex2 = contents.IndexOf(';', nIndex + 1);
        string previous_version_str = contents.Substring(nIndex, nIndex2 - nIndex);

        int previous_version = int.Parse(previous_version_str);
        int nextVersion = EditorPrefs.GetInt("ios_Build_num", previous_version + 1);

        nextVersion = Math.Max(previous_version, nextVersion);
        //BuildOptionEdited.IosBuildNumber = nextVersion;
        PlayerSettings.iOS.buildNumber = nextVersion.ToString();

        contents = contents.Remove(nIndex, nIndex2 - nIndex);
        contents = contents.Insert(nIndex, nextVersion.ToString());

        EditorPrefs.SetInt("ios_Build_num", nextVersion + 1);
        Debug.Log("SetIosBuildNumber : " + nextVersion);
        return nextVersion;
    }

    static void UpdateBuildVersion()
    {
        string target_setting = Application.dataPath + "/Editor/BuildOptionEdited.cs";
        StreamReader streamReader = new StreamReader(target_setting);
        string contents = streamReader.ReadToEnd();
        streamReader.Close();
        streamReader.Dispose();
        string todayStringInt = SetBuildDate(ref contents);

#if UNITY_IOS
        SetIosBuildNumber(ref contents);
#endif
        using (StreamWriter sw = new StreamWriter(target_setting, false, System.Text.Encoding.UTF8))
            sw.Write(contents);

        AssetDatabase.Refresh();
    }

#if UNITY_ANDROID
    [MenuItem("Custom/CI/Build Android")]
#endif
    static void PerformAndroidBuild()
    {
        string BUILD_TARGET_PATH = TARGET_DIR + "/Android/";
        Directory.CreateDirectory(BUILD_TARGET_PATH);

        PlayerSettings.companyName = "common1overn";
        PlayerSettings.productName = "rpgquiz";

        PlayerSettings.Android.keystoreName = Application.dataPath + "/rpgquiz.keystore";
        PlayerSettings.Android.keystorePass = "common1overn";
        PlayerSettings.Android.keyaliasName = "rpgquiz";
        PlayerSettings.Android.keyaliasPass = "common1overn";

        UpdateBuildVersion();

        PlayerSettings.bundleVersion = Application.version;
        //PlayerSettings.bundleIdentifier = "com.part1.maid";

#if !RELEASE
        string target_filename = APP_NAME + BUILD_DATE.ToString() + ".apk";
#else
        string target_filename = APP_NAME + ".apk";
#endif

        GenericBuild(SCENES, BUILD_TARGET_PATH + target_filename, BuildTarget.Android, BuildOptions.None);

        Debug.Log("===================== Android Build Success ==================== ");
    }

#if UNITY_IOS
    [MenuItem ("Custom/CI/Build iOS Debug")]
#endif

    static void PerformiOSDebugBuild()
    {
        BuildOptions opt = BuildOptions.SymlinkLibraries |
            BuildOptions.ConnectWithProfiler |
            BuildOptions.AllowDebugging |
            BuildOptions.Development |
            BuildOptions.AcceptExternalModificationsToPlayer;

        PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
        PlayerSettings.iOS.targetOSVersion = iOSTargetOSVersion.iOS_8_0;
        //PlayerSettings.iOS.cameraUsageDescription = "Everyplay requires access to the photo library";
        PlayerSettings.statusBarHidden = true;

        UpdateBuildVersion();
        PlayerSettings.bundleVersion = Application.version;
        //PlayerSettings.Android.bundleVersionCode = BuildOption.BundleVersionCode;
        //PlayerSettings.applicationIdentifier = "com.zoomma.slot2casino";


        char sep = Path.DirectorySeparatorChar;
        string buildDirectory = Path.GetFullPath(".") + sep + TARGET_DIR;
        Directory.CreateDirectory(buildDirectory);

        string BUILD_TARGET_PATH = buildDirectory + "/iOS";
        Directory.CreateDirectory(BUILD_TARGET_PATH);

        GenericBuild(SCENES, BUILD_TARGET_PATH, BuildTarget.iOS, opt);

        Debug.Log("===================== iOS Debug Build Success ==================== ");
    }

#if UNITY_IOS
	[MenuItem ("Custom/CI/Build iOS Release")]
#endif

    static void PerformiOSReleaseBuild()
    {
        BuildOptions opt = BuildOptions.SymlinkLibraries |
            BuildOptions.AcceptExternalModificationsToPlayer;

        PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
        PlayerSettings.iOS.targetOSVersion = iOSTargetOSVersion.iOS_8_0;
        //PlayerSettings.iOS.cameraUsageDescription = "Everyplay requires access to the photo library";
        PlayerSettings.statusBarHidden = true;

        UpdateBuildVersion();
        PlayerSettings.bundleVersion = Application.version;
        //PlayerSettings.Android.bundleVersionCode = BuildOption.BundleVersionCode;
        //PlayerSettings.applicationIdentifier = "com.zoomma.slot2casino";

        char sep = Path.DirectorySeparatorChar;
        string buildDirectory = Path.GetFullPath(".") + sep + TARGET_DIR;
        Directory.CreateDirectory(buildDirectory);

        string BUILD_TARGET_PATH = buildDirectory + "/iOS";
        Directory.CreateDirectory(BUILD_TARGET_PATH);

        GenericBuild(SCENES, BUILD_TARGET_PATH, BuildTarget.iOS, opt);

        Debug.Log("===================== iOS Release Build Success ==================== ");
    }

    private static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();

        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled)
            {
                continue;
            }

            EditorScenes.Add(scene.path);
        }

        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string target_filename, BuildTarget build_target, BuildOptions build_options)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);

        UnityEditor.Build.Reporting.BuildReport build = BuildPipeline.BuildPlayer(scenes, target_filename, build_target, build_options);

        Debug.Log("result = " + build.summary.result);
    }
}