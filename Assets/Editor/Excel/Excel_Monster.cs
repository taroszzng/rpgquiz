﻿using UnityEngine;
using UnityEditor;

public class Excel_Monster : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Monster mon = new Table_Monster();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Monster.Info info = new Table_Monster.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nNextID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nLevel = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nName = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nDec = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nEvent = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nIcon = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_strPrefab = sheet.GetRow(row).GetCell(ncell++).ToString();
            info.m_fScale = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fPosX = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fPosY = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fPosZ = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fRot = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fHP = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fAtk = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nLuck = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            for(int i=0; i< (int)eANIVALUE.eANIVALUE_END; ++i)
            {
                AniValue ani = new AniValue();

                ani.m_strAni = sheet.GetRow(row).GetCell(ncell++).ToString();
                ani.m_fAniTime = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

                info.m_ArrayAniValue[i] = ani;
            }

            info.m_nPercentage = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nClearStage = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nReward = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            mon.m_Dictionary.Add(info.m_nID, info);
        }

        mon.Save_Binary("Monster");//바이너리 저장
    }
}
