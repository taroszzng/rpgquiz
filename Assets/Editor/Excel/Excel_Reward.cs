﻿using UnityEngine;
using UnityEditor;

public class Excel_Reward : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Reward reward = new Table_Reward();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Reward.Info info = new Table_Reward.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nCount = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            for(int i=0; i<info.m_nCount; ++i)//보상 리스트
            {
                TableItem item = new TableItem();

                item.m_nItemID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
                item.m_nItemValue = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

                info.m_listItem.Add(item);
            }

            reward.m_Dictionary.Add(info.m_nID, info);
        }

        reward.Save_Binary("Reward");//바이너리 저장
    }
}
