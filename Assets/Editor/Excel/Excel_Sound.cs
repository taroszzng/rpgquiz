﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Sound : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Sound sound = new Table_Sound();

        for (int row = 3; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Sound.Info info = new Table_Sound.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_bLoop = sheet.GetRow(row).GetCell(ncell++).ToString() != "0";
            info.m_strSound = sheet.GetRow(row).GetCell(ncell++).ToString(); ;
            info.m_fTime = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            sound.m_Dictionary.Add(info.m_nID, info);
        }

        sound.Save_Binary("Sound");//바이너리 저장
    }
}
