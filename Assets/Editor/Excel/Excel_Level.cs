﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Level : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Level level = new Table_Level();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Level.Info info = new Table_Level.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nNextLevel = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fHP = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fAtk = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nLuck = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nExp = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nMaxLevel = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            
            level.m_Dictionary.Add(info.m_nID, info);
        }

        level.Save_Binary("Level");//바이너리 저장
    }
}
