﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Lan : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Lan lan = new Table_Lan();

        for (int row = 3; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Lan.Info info = new Table_Lan.Info();

            Table_Lan.INFODICTIONARY infodictionary = null;
            string str = "";

            int nCount = (int)nsENUM.eLANGUAGE.eLANGUAGE_END;

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            for (int i = 1; i < nCount; ++i)
            {
                infodictionary = lan.GetLanInfo(i);

                if (null == infodictionary)//생성
                {
                    infodictionary = new Table_Lan.INFODICTIONARY();
                    infodictionary.m_LanType = (nsENUM.eLANGUAGE)i;
                    lan.m_ListLan.Add(infodictionary);
                }

                str = sheet.GetRow(row).GetCell(ncell++).ToString();

                if (i == (int)Table_Lan.GetLanType())//같을때 넣음
                    info.m_strDec = str;

                Table_Lan.Info listinfo = new Table_Lan.Info();

                listinfo.m_nID = info.m_nID;
                listinfo.m_strDec = str;

                assert_cs.set(infodictionary.m_Info.ContainsKey(info.m_nID) == false, "LanTable에 같은 ID 가 존재합니다 : " + info.m_nID);
                infodictionary.m_Info.Add(info.m_nID, listinfo);
            }

            lan.m_Dictionary.Add(info.m_nID, info);
        }

        lan.Save_Binary("Lan");//바이너리 저장
    }
}
