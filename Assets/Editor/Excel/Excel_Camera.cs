﻿using UnityEngine;
using UnityEditor;

public class Excel_Camera : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Camera camer = new Table_Camera();

        for (int row = 2; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Camera.Info info = new Table_Camera.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fStartDelay = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fTime = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fShake_X = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fShake_Y = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fSpeed = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fDamping = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nShakeCount = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_bLocal = sheet.GetRow(row).GetCell(ncell++).ToString() != "0";

            camer.m_Dictionary.Add(info.m_nID, info);
        }

        camer.Save_Binary("Camera");//바이너리 저장
    }
}
