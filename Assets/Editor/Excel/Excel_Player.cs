﻿using UnityEngine;
using UnityEditor;

public class Excel_Player : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Player p = new Table_Player();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Player.Info info = new Table_Player.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nNextID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nLevel = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nName = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nDec = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nEventID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_strPrefab = sheet.GetRow(row).GetCell(ncell++).ToString();
            info.m_fScale = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fRot = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fHP = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fAtk = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fLuck = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            for(int i=0; i< (int)eANIVALUE.eANIVALUE_END; ++i)
            {
                AniValue ani = new AniValue();

                ani.m_strAni = sheet.GetRow(row).GetCell(ncell++).ToString();
                ani.m_fAniTime = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

                info.m_ArrayAniValue[i] = ani;
            }

            p.m_Dictionary.Add(info.m_nID, info);
        }

        p.Save_Binary("Player");//바이너리 저장
    }
}
