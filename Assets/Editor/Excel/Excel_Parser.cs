﻿using UnityEngine;

using System;
using System.IO;
using System.Collections.Generic;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

public class Excel_Parser 
{
    protected IWorkbook workbook = null;
    protected ISheet sheet = null;

    public void ExcelLoad(string path, string sheetName = "")
    {
        try
        {
            using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                string extension = GetExtension(path);

                if (extension == "xls")
                {
                    workbook = new HSSFWorkbook(fileStream);
                }
                else if (extension == "xlsx")
                {
                    workbook = new XSSFWorkbook(fileStream);
                }
                else
                {
                    throw new Exception("Wrong file.");
                }

                //string[] sheetNames = GetSheetNames();
                //Debug.Log("sheetNames Length : " + sheetNames.Length + " = " + sheetNames[0]);

                if (!string.IsNullOrEmpty(sheetName))
                {
                    sheet = workbook.GetSheet(sheetName);
                    //Debug.Log("LastRowNum : " + sheet.LastRowNum);
                    ExcelSheetParser();
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    protected virtual void ExcelSheetParser()
    {
        for (int row = 0; row <= sheet.LastRowNum; row++)
        {
            //Debug.Log("LastCellNum : " + sheet.GetRow(row).LastCellNum);
            for (int col = 0; col <= sheet.GetRow(row).LastCellNum - 1; col++)
            {
                //Debug.Log(sheet.GetRow(row).GetCell(col).StringCellValue);
                Debug.Log(sheet.GetRow(row).GetCell(col).ToString());
            }
        }
    }

    string GetExtension(string path)
    {
        string ext = Path.GetExtension(path);
        string[] arg = ext.Split(new char[] { '.' });

        return arg[1];
    }

    string[] GetSheetNames()
    {
        List<string> sheetList = new List<string>();

        if (workbook != null)
        {
            int numSheets = workbook.NumberOfSheets;

            for (int i = 0; i < numSheets; i++)
            {
                sheetList.Add(workbook.GetSheetName(i));
            }
        }
        else
        {
            Debug.LogError("Workbook is null. Did you forget to import excel file first?");
        }

        return (sheetList.Count > 0) ? sheetList.ToArray() : null;
    }
}