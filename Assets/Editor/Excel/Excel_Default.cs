﻿using UnityEngine;
using UnityEditor;

public class Excel_Default : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Default de = new Table_Default();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Default.Info info = new Table_Default.Info();

            info.m_nLevel = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nPlayerID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nMonsterID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nCreateStage = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            
            for (int i = 0; i < (int)nsENUM.eVALUE.eVALUE_3; ++i)
                info.m_ArrayItem[i] = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            info.m_nTutorialItem = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            de.m_Dictionary.Add(info.m_nLevel, info);
        }

        de.Save_Binary("Default");//바이너리 저장
    }
}
