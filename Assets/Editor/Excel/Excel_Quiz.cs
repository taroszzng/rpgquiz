﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Quiz : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Quiz quiz = new Table_Quiz();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Quiz.Info info = new Table_Quiz.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nAnswer = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString()); //답
            
            for(int i=0; i<(int)nsENUM.eVALUE.eVALUE_5; ++i)//예제
            {
                string str = sheet.GetRow(row).GetCell(ncell++).ToString();

                info.m_ListExample.Add(str);
            }

            info.m_strHint = sheet.GetRow(row).GetCell(ncell++).ToString(); //힌트

            quiz.m_Dictionary.Add(info.m_nID, info);
        }

        quiz.Save_Binary("Quiz");//바이너리 저장
    }
}
