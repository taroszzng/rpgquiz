﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Combo : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Combo combo = new Table_Combo();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Combo.Info info = new Table_Combo.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nName = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nDec = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nCount = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fValue = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            combo.m_Dictionary.Add(info.m_nID, info);
        }

        combo.Save_Binary("Combo");//바이너리 저장
    }
}
