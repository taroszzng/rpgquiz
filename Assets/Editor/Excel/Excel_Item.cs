﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Item : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Item item = new Table_Item();

        for (int row = 2; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Item.Info info = new Table_Item.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nName = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nDec = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nIcon = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nApply = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fAbility = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            item.m_Dictionary.Add(info.m_nID, info);
        }

        item.Save_Binary("Item");//바이너리 저장
    }
}
