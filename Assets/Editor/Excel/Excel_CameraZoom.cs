﻿using UnityEngine;
using UnityEditor;

public class Excel_CameraZoom : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_CameraZoom m_CameraZoom = new Table_CameraZoom();

        for (int row = 2; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_CameraZoom.Info info = new Table_CameraZoom.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fStartDelay = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_fZoom = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString()); 
            info.m_fTime_Start = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString()); 
            info.m_fDuration = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString()); 
            info.m_fTime_End = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString()); 

            m_CameraZoom.m_Dictionary.Add(info.m_nID, info);
        }

        m_CameraZoom.Save_Binary("CameraZoom");
    }
}
