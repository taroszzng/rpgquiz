﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Stage : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Stage stage = new Table_Stage();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Stage.Info info = new Table_Stage.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nName = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nDec = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nIcon = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nCompType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nComp = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_strBG = sheet.GetRow(row).GetCell(ncell++).ToString();

            info.m_strEffect = sheet.GetRow(row).GetCell(ncell++).ToString();
            info.m_fY = float.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());

            stage.m_Dictionary.Add(info.m_nID, info);
        }

        stage.Save_Binary("Stage");//바이너리 저장
    }
}
