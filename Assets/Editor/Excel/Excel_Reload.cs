﻿#define RELEASE
//Excel Edtor에서만 가능하므로 파일을 만들자
using UnityEngine;
using UnityEditor;

public class Excel_Reload : MonoBehaviour
{
    [MenuItem("CS_Util/Excel/Reload And Save &F1", false, 1)]
    static public void Parser_Excel_DestroyAndCreate()
    {
        LoadExcel();
    }

    static public void LoadExcel()//파싱한다음 Table에 저장하자
    {
        string strExcel = "./Document/Excel.xlsx";

        Excel_Default de = new Excel_Default();
        de.ExcelLoad(strExcel, "Default");
        
        Excel_Camera camera = new Excel_Camera();
        camera.ExcelLoad(strExcel, "Camera");

        Excel_CameraZoom camerazoom = new Excel_CameraZoom();
        camerazoom.ExcelLoad(strExcel, "CameraZoom");

        Excel_Lan lan = new Excel_Lan();
        lan.ExcelLoad(strExcel, "Lan");

        Excel_Quiz quiz = new Excel_Quiz();
        quiz.ExcelLoad(strExcel, "Quiz");

        Excel_Stage stage = new Excel_Stage();
        stage.ExcelLoad(strExcel, "Stage");

        Excel_Player p = new Excel_Player();
        p.ExcelLoad(strExcel, "Player");

        Excel_Level level = new Excel_Level();
        level.ExcelLoad(strExcel, "Level");

        Excel_Combo combo = new Excel_Combo();
        combo.ExcelLoad(strExcel, "Combo");

        Excel_Monster mon = new Excel_Monster();
        mon.ExcelLoad(strExcel, "Monster");

        Excel_Reward reward = new Excel_Reward();
        reward.ExcelLoad(strExcel, "Reward");

        Excel_Item item = new Excel_Item();
        item.ExcelLoad(strExcel, "Item");

        Excel_Shop shop = new Excel_Shop();
        shop.ExcelLoad(strExcel, "Shop");

        Excel_Sound sound = new Excel_Sound();
        sound.ExcelLoad(strExcel, "Sound");

        AssetDatabase.Refresh();
    }
}