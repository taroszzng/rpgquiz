﻿using UnityEditor;
using System.Collections.Generic;

public class Excel_Shop : Excel_Parser
{
    protected override void ExcelSheetParser()//데이타 넣기
    {
        Table_Shop shop = new Table_Shop();

        for (int row = 1; row <= sheet.LastRowNum; row++)
        {
            int ncell = 0;

            Table_Shop.Info info = new Table_Shop.Info();

            info.m_nID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nType = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_strStore = sheet.GetRow(row).GetCell(ncell++).ToString();
            info.m_nSort = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nName = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nDec = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nIcon = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nPrice = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nItemID = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            info.m_nItemValue = int.Parse(sheet.GetRow(row).GetCell(ncell++).ToString());
            
            shop.m_Dictionary.Add(info.m_nID, info);
        }

        shop.Save_Binary("Shop");//바이너리 저장
    }
}
