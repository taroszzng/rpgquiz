﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

public class Custom : MonoBehaviour
{
    enum eBUILDTYPE
    {
        eBUILDTYPE_DEVELOPMENT,
        eBUILDTYPE_DEBUG,
        eBUILDTYPE_RELEASE,
        eBUILDTYPE_END
    }

    static List<string> m_list = new List<string>();

    [MenuItem("Custom/Mode/Development", false, 1)]
    static void BuildDevelopment()
    {
        BuildFile(eBUILDTYPE.eBUILDTYPE_DEVELOPMENT);
        BuildFolder(false);

        AssetDatabase.Refresh();

        Debug.Log("===================== Development Success ==================== ");
    }

    [MenuItem("Custom/Mode/Debug", false, 1)]
    static void BuildDebug()
    {
        BuildFile(eBUILDTYPE.eBUILDTYPE_DEBUG);
        BuildFolder(false);

        AssetDatabase.Refresh();

        Debug.Log("===================== Debug Success ==================== ");
    }

    [MenuItem("Custom/Mode/Release", false, 1)]
    static void BuildRelease()
    {
        BuildFile(eBUILDTYPE.eBUILDTYPE_RELEASE);
        BuildFolder(false);

        AssetDatabase.Refresh();

        Debug.Log("===================== Release Success ==================== ");
    }

    static void BuildFile(eBUILDTYPE _eBuildType)//빌드 파일 로드
    {
        m_list.Add("Lib/Reporter/Reporter.cs");
        m_list.Add("Script/Scene/SceneMgr.cs");
        m_list.Add("Script/Common/SharedObject.cs");
        m_list.Add("Script/Scene/SceneProperty.Cheat.cs");
        m_list.Add("Script/DownloadObbExample.cs");
        m_list.Add("Script/UI/Scroller/SetCellView.cs");
        m_list.Add("Editor/Excel/Excel_Reload.cs");
        m_list.Add("Script/Ads/AdMob.cs");
        m_list.Add("Script/Ads/UnityAds.cs");
        m_list.Add("Editor/AutoBuild.cs");
        //m_list.Add("GooglePlayGames/Editor/NearbyConnectionUI.cs");
        //m_list.Add("GooglePlayGames/Editor/GPGSAndroidSetupUI.cs");

        int nCount = m_list.Count;

        for (int i = 0; i < nCount; ++i)
            CheckFile(m_list[i], _eBuildType);

        m_list.Clear();
    }

    static void CheckFile(string csfile, eBUILDTYPE _eBuildType)
    {
        string totalpath = Application.dataPath + "/" + csfile;//최종파일 경로

        FileStream file = new FileStream(totalpath, FileMode.Open, FileAccess.Read);

        byte[] ReadArray = new byte[file.Length];
        file.Read(ReadArray, 0, (int)file.Length);

        System.Text.Encoding enc = System.Text.Encoding.UTF8;
        string str = enc.GetString(ReadArray);

        bool IsSave = false;

        switch (_eBuildType)
        {
            case eBUILDTYPE.eBUILDTYPE_DEVELOPMENT:
                {
                    if (str.Contains("#define RELEASE"))
                    {
                        str = str.Replace("#define RELEASE", "#define DEVELOPMENT");

                        IsSave = true;
                    }
                    else if (str.Contains("#define DEBUG"))
                    {
                        str = str.Replace("#define DEBUG", "#define DEVELOPMENT");
                        IsSave = true;
                    }
                }
                break;
            case eBUILDTYPE.eBUILDTYPE_DEBUG:
                {
                    if (str.Contains("#define RELEASE"))
                    {
                        str = str.Replace("#define RELEASE", "#define DEBUG");

                        IsSave = true;
                    }
                    else if (str.Contains("#define DEVELOPMENT"))
                    {
                        str = str.Replace("#define DEVELOPMENT", "#define DEBUG");
                        IsSave = true;
                    }
                }
                break;
            case eBUILDTYPE.eBUILDTYPE_RELEASE:
                {
                    if (str.Contains("#define DEVELOPMENT"))
                    {
                        str = str.Replace("#define DEVELOPMENT", "#define RELEASE");

                        IsSave = true;
                    }
                    else if (str.Contains("#define DEBUG"))
                    {
                        str = str.Replace("#define DEBUG", "#define RELEASE");
                        IsSave = true;
                    }
                }
                break;
        }

        if (IsSave == true)
        {
            byte[] WriteArray = enc.GetBytes(str);

            file.Close();
            file = new FileStream(totalpath, FileMode.Create, FileAccess.Write);
            file.Write(WriteArray, 0, WriteArray.Length);
        }

        file.Close();
    }

    static void BuildFolder(bool bDebug)
    {
        if (bDebug) //_없애기
        {
            if (Directory.Exists("Assets/Lib/Reporter/_Resources") == true)
                AssetDatabase.RenameAsset("Assets/Lib/Reporter/_Resources", "Resources");
        }
        else//_붙이기
        {
            if (Directory.Exists("Assets/Lib/Reporter/Resources") == true)
                AssetDatabase.RenameAsset("Assets/Lib/Reporter/Resources", "_Resources");
        }
    }

    static bool CheckDev(string _str)
    {
        return false;
    }

    static bool CheckDebug(string _str)
    {
        return false;
    }

    static bool CheckRelease(string _str)
    {
        return false;
    }

    //[MenuItem("Custom/Platform/Google", false, 1)]
    //static void BuildGoogle()
    //{
    //    string totalpath = Application.dataPath + "/" + "Script/Scene/Lobby/UI/CashShop/UI_CashShopElement.cs"; //
    //    string strPlatform = "Google";

    //    BuildPlatform(totalpath, 0, strPlatform);

    //    totalpath = Application.dataPath + "/" + "Script/OneStore.cs"; //

    //    BuildPlatform(totalpath, 0, strPlatform);

    //    totalpath = Application.dataPath + "/" + "Script/Scene/Title/Launcher.cs";

    //    BuildPlatform(totalpath, 0, strPlatform);

    //    totalpath = Application.dataPath + "/" + "Script/Scene/InGame/UI/GameResult/UI_GameResult.cs";

    //    BuildPlatform(totalpath, 0, strPlatform);

    //    totalpath = Application.dataPath + "/" + "Script/Network/GameManager.cs";

    //    BuildPlatform(totalpath, 0, strPlatform);

    //    Debug.Log("===================== Google Success ==================== ");
    //}

    //static void BuildPlatform(string strPath, int nLine, string strPlatform)//구글인지
    //{
    //    if ("" == strPath)
    //    {
    //        Debug.Log(" Build Path Error");
    //        return;
    //    }

    //    FileStream file = new FileStream(strPath, FileMode.Open, FileAccess.Read);

    //    byte[] ReadArray = new byte[file.Length];
    //    file.Read(ReadArray, 0, (int)file.Length);

    //    System.Text.Encoding enc = System.Text.Encoding.UTF8;
    //    string str = enc.GetString(ReadArray);

    //    bool bSave = true;

    //    if (str.Contains("#define ONESTORE"))
    //    {
    //        if (strPlatform.Contains("OneStore"))//원스토어이니 pass
    //        {
    //        }
    //        else if (strPlatform.Contains("Google"))
    //        {
    //            str = str.Replace("#define ONESTORE", "#define GOOGLE");
    //        }
    //        else
    //            bSave = false;
    //    }
    //    else if (str.Contains("#define GOOGLE"))
    //    {
    //        if (strPlatform.Contains("OneStore"))//원스토어이니 pass
    //        {
    //            str = str.Replace("#define GOOGLE", "#define ONESTORE");
    //        }
    //        else if (strPlatform.Contains("Google"))
    //        {
    //        }
    //        else
    //            bSave = false;
    //    }
    //    else
    //        bSave = false;

    //    if (bSave)
    //    {
    //        byte[] WriteArray = enc.GetBytes(str);

    //        file.Close();
    //        file = new FileStream(strPath, FileMode.Create, FileAccess.Write);
    //        file.Write(WriteArray, nLine, WriteArray.Length);
    //    }

    //    file.Close();
    //}
}
