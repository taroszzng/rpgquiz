﻿////////////////////////////////////////////////////////////////////////////////////
//  CAMERA FILTER PACK - by VETASOFT 2014 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[AddComponentMenu ("Camera Filter Pack/Blur/Focus")]
public class CameraFilterPack_Blur_Focus : MonoBehaviour {
	#region Variables
	public Shader SCShader;
	private float TimeX = 1.0f;
	private Vector4 ScreenResolution;
	private Material SCMaterial;
	[Range(-1, 1)]
	public float CenterX = 0f;
	[Range(-1, 1)]
	public float CenterY = 0f;
	[Range(0, 10)]
	public float _Size = 5f;
	[Range(0.12f, 64)]
	public float _Eyes = 2f;

	public static float ChangeCenterX ;
	public static float ChangeCenterY ;
	public static float ChangeSize ;
	public static float ChangeEyes ;

    Transform m_MyPlayer2DPos;

	#endregion
	
	#region Properties
	Material material
	{
		get
		{
			if(SCMaterial == null)
			{
				SCMaterial = new Material(SCShader);
				SCMaterial.hideFlags = HideFlags.HideAndDontSave;	
			}
			return SCMaterial;
		}
	}
	#endregion
	void Awake () 
	{
        //m_MyPlayer2DPos = SharedObject.g_CollectorMgr.GetPlayer().Get2DPos();

		ChangeCenterX = CenterX;
		ChangeCenterY = CenterY;
		ChangeSize = _Size;
		ChangeEyes = _Eyes;
		SCShader = Shader.Find("CameraFilterPack/Blur_Focus");

		if(!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}
	}
	
	void OnRenderImage (RenderTexture sourceTexture, RenderTexture destTexture)
	{
		if (SCShader != null)
		{
			TimeX+=Time.deltaTime;
			if (TimeX>100)  TimeX=0;
			material.SetFloat("_TimeX", TimeX);
			material.SetFloat("_CenterX", CenterX);
			material.SetFloat("_CenterY", CenterY);
#if UNITY_IOS
            float result = Mathf.Round(0.5f/0.2f)*0.2f;
#else
            float result = Mathf.Round(_Size / 0.2f) * 0.2f;
#endif
			material.SetFloat("_Size", result);
			material.SetFloat("_Circle", _Eyes);
			material.SetVector("_ScreenResolution",new Vector2(Screen.width,Screen.height));
			Graphics.Blit(sourceTexture, destTexture, material); //소스 텍스쳐에 저장
		}
		else
		{
			Graphics.Blit(sourceTexture, destTexture);
		}
	}

    void DieUpdate()//[kks][Camera]:죽었을때 처리
    {
        CenterX = 0f;
        CenterY = 0f;
        _Size = ChangeSize;
        _Eyes = ChangeEyes;
    }
	
	// Update is called once per frame
	void Update () 
	{
		if (Application.isPlaying)
		{
            if (SharedObject.g_CollectorMgr.Player.IsDie())//[kks][Camera]:죽었을때 처리는 따로하자
            {
                DieUpdate();
                return;
            }

            //Vector3 pos = m_MyPlayer2DPos.position;

            CenterX = 0f; //(pos.x - transform.position.x) / SharedObject.g_MainCamera.m_CameraFOV_X;
            CenterY = 0f; //(pos.y - transform.position.y + 2f) / SharedObject.g_MainCamera.m_CameraFOV_Y;
			_Size = ChangeSize;
			_Eyes = ChangeEyes;
		}
		#if UNITY_EDITOR
		if (Application.isPlaying!=true)
		{
			SCShader = Shader.Find("CameraFilterPack/Blur_Focus");

		}
		#endif
	}
	
	void OnDisable ()
	{
		if(SCMaterial)
		{
			DestroyImmediate(SCMaterial);	
		}
		
	}	
}